package org.jeecg.modules.toker.customer.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.UnsupportedEncodingException;

/**
 * @Description: 客户收款管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Data
@TableName("tk_custom_payable")
@ApiModel(value="tk_custom_payable对象", description="客户收款管理")
public class TkCustomPayable implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private String id;
	/**客户ID*/
    @ApiModelProperty(value = "客户ID")
    private String customerId;
	/**类型*/
	@Excel(name = "类型", width = 15)
    @Dict(dicCode = "tk_c_payable_type")
    @ApiModelProperty(value = "类型")
    private String type;
	/**收款项目*/
	@Excel(name = "收款项目", width = 15)
    @Dict(dicCode = "tk_c_payable_project")
    @ApiModelProperty(value = "收款项目")
    private String project;
	/**合同金额*/
	@Excel(name = "合同金额", width = 15)
    @ApiModelProperty(value = "合同金额")
    private java.math.BigDecimal contractValue;
	/**收总额*/
	@Excel(name = "收总额", width = 15)
    @ApiModelProperty(value = "收总额")
    private java.math.BigDecimal totalReceived;
	/**本次收款*/
	@Excel(name = "本次收款", width = 15)
    @ApiModelProperty(value = "本次收款")
    private java.math.BigDecimal collection;
	/**结算方式*/
	@Excel(name = "结算方式", width = 15)
    @ApiModelProperty(value = "结算方式")
    private String settlement;
	/**图片*/
	@Excel(name = "图片", width = 15)
    @ApiModelProperty(value = "图片")
    private String pic;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;


    /**跟进人ID*/
    @Excel(name = "跟进人ID", width = 15)
    @ApiModelProperty(value = "跟进人ID")
    private String directorId;

    /**跟进人姓名*/
    @Excel(name = "跟进人姓名", width = 15)
    @ApiModelProperty(value = "跟进人姓名")
    private String director;

    /**跟进人头像*/
    @Excel(name = "跟进人头像", width = 15)
    @ApiModelProperty(value = "跟进人头像")
    private String directorAvatar;

}
