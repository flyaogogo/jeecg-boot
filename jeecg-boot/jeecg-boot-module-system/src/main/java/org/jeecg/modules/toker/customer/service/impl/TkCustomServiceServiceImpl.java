package org.jeecg.modules.toker.customer.service.impl;

import org.jeecg.modules.toker.customer.entity.TkCustomService;
import org.jeecg.modules.toker.customer.mapper.TkCustomServiceMapper;
import org.jeecg.modules.toker.customer.service.ITkCustomServiceService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 客户服务管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Service
public class TkCustomServiceServiceImpl extends ServiceImpl<TkCustomServiceMapper, TkCustomService> implements ITkCustomServiceService {
	
	@Autowired
	private TkCustomServiceMapper tkCustomServiceMapper;
	
	@Override
	public List<TkCustomService> selectByMainId(String mainId) {
		return tkCustomServiceMapper.selectByMainId(mainId);
	}
}
