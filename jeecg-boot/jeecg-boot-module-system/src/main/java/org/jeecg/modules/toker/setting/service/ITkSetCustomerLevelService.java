package org.jeecg.modules.toker.setting.service;

import org.jeecg.modules.toker.setting.entity.TkSetCustomerLevel;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 客户级别设置
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
public interface ITkSetCustomerLevelService extends IService<TkSetCustomerLevel> {

	public List<TkSetCustomerLevel> selectByMainId(String mainId);
}
