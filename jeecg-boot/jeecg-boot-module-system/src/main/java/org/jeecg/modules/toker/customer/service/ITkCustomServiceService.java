package org.jeecg.modules.toker.customer.service;

import org.jeecg.modules.toker.customer.entity.TkCustomService;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 客户服务管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface ITkCustomServiceService extends IService<TkCustomService> {

	public List<TkCustomService> selectByMainId(String mainId);
}
