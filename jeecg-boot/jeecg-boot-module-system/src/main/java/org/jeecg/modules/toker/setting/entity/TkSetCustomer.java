package org.jeecg.modules.toker.setting.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 客户管理设置
 * @Author: jeecg-boot
 * @Date:   2021-08-12
 * @Version: V1.0
 */
@Data
@TableName("tk_set_customer")
@ApiModel(value="tk_set_customer对象", description="客户管理设置")
public class TkSetCustomer implements Serializable {
    private static final long serialVersionUID = 1L;

	/**客户管理id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "客户管理id")
    private String id;
	/**禁止跟进时间段设置开始*/
    @Excel(name = "禁止跟进时间段设置开始", width = 15)
    @ApiModelProperty(value = "禁止跟进时间段设置开始")
    private Integer noPeriodStart;
	/**禁止跟进时间段设置结束*/
    @Excel(name = "禁止跟进时间段设置结束", width = 15)
    @ApiModelProperty(value = "禁止跟进时间段设置结束")
    private Integer noPeriodEnd;
	/**跟进异常时是否提醒*/
    @Excel(name = "跟进异常时是否提醒", width = 15)
    @ApiModelProperty(value = "跟进异常时是否提醒")
    private String isWarn;
	/**客户手机号要求11位*/
    @Excel(name = "客户手机号要求11位", width = 15)
    @ApiModelProperty(value = "客户手机号要求11位")
    private String phoneIs11;
	/**设置描述*/
    @Excel(name = "设置描述", width = 15)
    @ApiModelProperty(value = "设置描述")
    private String remark;
	/**租户Id*/
    @Excel(name = "租户Id", width = 15)
    @ApiModelProperty(value = "租户Id")
    private String tenantId;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
