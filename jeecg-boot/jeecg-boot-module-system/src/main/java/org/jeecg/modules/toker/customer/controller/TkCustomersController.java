package org.jeecg.modules.toker.customer.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.customer.vo.CustomerStatistics;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.toker.customer.entity.TkCustomFlowup;
import org.jeecg.modules.toker.customer.entity.TkCustomPayable;
import org.jeecg.modules.toker.customer.entity.TkCustomService;
import org.jeecg.modules.toker.customer.entity.TkCustomOperation;
import org.jeecg.modules.toker.customer.entity.TkCustomers;
import org.jeecg.modules.toker.customer.service.ITkCustomersService;
import org.jeecg.modules.toker.customer.service.ITkCustomFlowupService;
import org.jeecg.modules.toker.customer.service.ITkCustomPayableService;
import org.jeecg.modules.toker.customer.service.ITkCustomServiceService;
import org.jeecg.modules.toker.customer.service.ITkCustomOperationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 客户信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Api(tags="客户信息")
@RestController
@RequestMapping("/customer/tkCustomers")
@Slf4j
public class TkCustomersController extends JeecgController<TkCustomers, ITkCustomersService> {

	@Autowired
	private ITkCustomersService tkCustomersService;

	@Autowired
	private ITkCustomFlowupService tkCustomFlowupService;

	@Autowired
	private ITkCustomPayableService tkCustomPayableService;

	@Autowired
	private ITkCustomServiceService tkCustomServiceService;

	@Autowired
	private ITkCustomOperationService tkCustomOperationService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param tkCustomers
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "客户信息-分页列表查询")
	@ApiOperation(value="客户信息-分页列表查询", notes="客户信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkCustomers tkCustomers,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkCustomers> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomers, req.getParameterMap());
		Page<TkCustomers> page = new Page<TkCustomers>(pageNo, pageSize);
		IPage<TkCustomers> pageList = tkCustomersService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	@AutoLog(value = "客户信息-两个并集参数 分页列表查询")
	@ApiOperation(value="客户信息-两个并集参数 分页列表查询", notes="客户信息-两个并集参数 分页列表查询")
	@GetMapping(value = "/twolist")
	public Result<?> queryTwoParamsList(TkCustomers tkCustomers,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		LambdaQueryWrapper<TkCustomers> lambdaQueryWrapper = new LambdaQueryWrapper<>();

		lambdaQueryWrapper.eq(TkCustomers::getStage, tkCustomers.getStage());
		lambdaQueryWrapper.eq(TkCustomers::getLevel, tkCustomers.getLevel());

		//QueryWrapper<TkCustomers> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomers, req.getParameterMap());
		Page<TkCustomers> page = new Page<TkCustomers>(pageNo, pageSize);
		IPage<TkCustomers> pageList = tkCustomersService.page(page, lambdaQueryWrapper);
		return Result.OK(pageList);
	}

	 @AutoLog(value = "客户信息-数据统计 并 分页列表查询")
	 @ApiOperation(value="客户信息-数据统计 并 分页列表查询", notes="客户信息-数据统计 并 分页列表查询")
	 @GetMapping(value = "/queryCustomersByState")
	 public Result<?> queryCustomersByState(TkCustomers tkCustomers,@Validated PageParamRequest pageParamRequest,
										 HttpServletRequest req) {
		 CustomerStatistics cs =  tkCustomersService.queryCustomersByState(tkCustomers,pageParamRequest) ;
		return Result.OK(cs) ;
	 }

	/**
     *   添加
     * @param tkCustomers
     * @return
     */
    @AutoLog(value = "客户信息-添加")
    @ApiOperation(value="客户信息-添加", notes="客户信息-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody TkCustomers tkCustomers) {
		// 后台跟进人员直接后台获取相关信息
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		tkCustomers.setFlowupPerson(sysUser.getRealname());
		tkCustomers.setTkuserId(sysUser.getId());
		tkCustomers.setTemplateId(sysUser.getAvatar()); // 临时借用此字段，传数据
		tkCustomers.setTenantId(sysUser.getRelTenantIds());

		tkCustomers.setCustomerLeader(sysUser.getRealname()); // 20220613 默认以添加人设置为 客户负责人，可单独变更
		/*
		tkCustomers.setDirector(sysUser.getRealname());
		tkCustomers.setDirectorId(sysUser.getId());
		tkCustomers.setDirectorAvatar(sysUser.getAvatar());

        tkCustomersService.save(tkCustomers);
		*/

		tkCustomersService.addTkCustomBatchMain(tkCustomers);

        return Result.OK("添加成功！");
    }

    /**
     *  编辑
     * @param tkCustomers
     * @return
     */
    @AutoLog(value = "客户信息-编辑")
    @ApiOperation(value="客户信息-编辑", notes="客户信息-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody TkCustomers tkCustomers) {
        tkCustomersService.updateById(tkCustomers);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "客户信息-通过id删除")
    @ApiOperation(value="客户信息-通过id删除", notes="客户信息-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        tkCustomersService.delMain(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "客户信息-批量删除")
    @ApiOperation(value="客户信息-批量删除", notes="客户信息-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.tkCustomersService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkCustomers tkCustomers) {
        return super.exportXls(request, tkCustomers, TkCustomers.class, "客户信息");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkCustomers.class);
    }

	 /**
	  * 通过客户ID查询 查询 对应客户信息
	  *  Luo.0022 add
	  * @return
	  */
	 @AutoLog(value = "通过客户ID查询 查询 对应客户信息")
	 @ApiOperation(value="通过客户ID查询 查询 对应客户信息", notes="通过客户ID查询 查询 对应客户信息")
	 @GetMapping(value = "/queryByCustomerId")
	 public Result<?>  queryByCustomerId(@RequestParam(name="cId") String cId) {
		 TkCustomers tkCustomers = tkCustomersService.getById(cId);
		 if (tkCustomers == null) {
			 return Result.error("未找到对应数据");
		 }
		 return Result.OK(tkCustomers);
	 }


	/*---------------------------------主表处理-end-------------------------------------*/
	

    /*--------------------------------子表处理-客户跟进信息-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "客户跟进信息-通过主表ID查询")
	@ApiOperation(value="客户跟进信息-通过主表ID查询", notes="客户跟进信息-通过主表ID查询")
	@GetMapping(value = "/listTkCustomFlowupByMainId")
    public Result<?> listTkCustomFlowupByMainId(TkCustomFlowup tkCustomFlowup,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<TkCustomFlowup> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomFlowup, req.getParameterMap());
        Page<TkCustomFlowup> page = new Page<TkCustomFlowup>(pageNo, pageSize);
        IPage<TkCustomFlowup> pageList = tkCustomFlowupService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

	 /**
	  * 通过主表中的客户ID查询 - customerId
	  *  Luo.0022 add
	  * @return
	  */
	 @AutoLog(value = "客户跟进信息-通过主表中的客户ID查询")
	 @ApiOperation(value="客户跟进信息-通过主表中的客户ID查询", notes="客户跟进信息-通过主表中的客户ID查询")
	 @GetMapping(value = "/listTkCustomFlowupByCstrId")
	 public Result<?> listTkCustomFlowupByCstrId(TkCustomFlowup tkCustomFlowup,
												 @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
												 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
												 HttpServletRequest req) {
//		 QueryWrapper<TkCustomFlowup> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomFlowup, req.getParameterMap());

		 LambdaQueryWrapper<TkCustomFlowup> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		 lambdaQueryWrapper.eq(TkCustomFlowup::getCustomerId, tkCustomFlowup.getCustomerId());
		 lambdaQueryWrapper.orderByDesc(TkCustomFlowup::getCreateTime) ;

		 Page<TkCustomFlowup> page = new Page<TkCustomFlowup>(pageNo, pageSize);
		 IPage<TkCustomFlowup> pageList = tkCustomFlowupService.page(page, lambdaQueryWrapper);
		 return Result.OK(pageList);
	 }

	 /**
	  * 通过跟进ID查询 查询 对应跟进信息
	  *  Luo.0022 add
	  * @return
	  */
	 @AutoLog(value = "客户跟进信息-通过客户跟进的ID查询")
	 @ApiOperation(value="客户跟进信息-通过客户跟进的ID查询", notes="客户跟进信息-通过客户跟进的ID查询")
	 @GetMapping(value = "/queryByFlowUpId")
	 public Result<?>  queryByFlowUpId(@RequestParam(name="fuId") String fuId) {
		 TkCustomFlowup customFlowup = tkCustomFlowupService.getById(fuId);
		 if (customFlowup == null) {
			 return Result.error("未找到对应数据");
		 }
		 return Result.OK(customFlowup);
	 }

	/**
	 * 添加
	 * @param tkCustomFlowup
	 * @return
	 */
	@AutoLog(value = "客户跟进信息-添加")
	@ApiOperation(value="客户跟进信息-添加", notes="客户跟进信息-添加")
	@PostMapping(value = "/addTkCustomFlowup")
	public Result<?> addTkCustomFlowup(@RequestBody TkCustomFlowup tkCustomFlowup) {

		// 后台跟进人员直接后台获取相关信息
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		tkCustomFlowup.setDirector(sysUser.getRealname());
		tkCustomFlowup.setDirectorId(sysUser.getId());
		tkCustomFlowup.setDirectorAvatar(sysUser.getAvatar());

		tkCustomFlowup.setIsLike("false");

		/*
		tkCustomFlowupService.save(tkCustomFlowup);

		// 添加完Flowup后，要把最新的内容更新到用户中
		TkCustomers tkCustomers = new TkCustomers() ;
		tkCustomers.setId(tkCustomFlowup.getCustomerId());
		tkCustomers.setFlowupModel(tkCustomFlowup.getFlowupModel());
		tkCustomers.setFlowupType(tkCustomFlowup.getFlowupType());
		tkCustomers.setLatelyDetails(tkCustomFlowup.getDetails());
		tkCustomers.setLatelyTime(new Date());
		tkCustomersService.updateById(tkCustomers);
		*/
		// 整合
		tkCustomersService.addTkCustomFlowupBatchMain(tkCustomFlowup);

		return Result.OK("添加成功！");
	}

    /**
	 * 编辑
	 * @param tkCustomFlowup
	 * @return
	 */
	@AutoLog(value = "客户跟进信息-编辑")
	@ApiOperation(value="客户跟进信息-编辑", notes="客户跟进信息-编辑")
	@PutMapping(value = "/editTkCustomFlowup")
	public Result<?> editTkCustomFlowup(@RequestBody TkCustomFlowup tkCustomFlowup) {
		// 后台跟进人员直接后台获取相关信息
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		tkCustomFlowup.setDirector(sysUser.getRealname());
		tkCustomFlowup.setDirectorId(sysUser.getId());
		tkCustomFlowup.setDirectorAvatar(sysUser.getAvatar());

		/*
		tkCustomFlowupService.updateById(tkCustomFlowup);

		// 添加完Flowup后，要把 最新  的内容更新到用户中
		TkCustomers tkCustomers = new TkCustomers() ;
		tkCustomers.setId(tkCustomFlowup.getCustomerId());
		tkCustomers.setFlowupModel(tkCustomFlowup.getFlowupModel());
		tkCustomers.setFlowupType(tkCustomFlowup.getFlowupType());
		tkCustomers.setLatelyDetails(tkCustomFlowup.getDetails());
		tkCustomers.setLatelyTime(new Date());
		tkCustomersService.updateById(tkCustomers);
		*/

		tkCustomersService.editTkCustomFlowupBatchMain(tkCustomFlowup);

		return Result.OK("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户跟进信息-通过id删除")
	@ApiOperation(value="客户跟进信息-通过id删除", notes="客户跟进信息-通过id删除")
	@DeleteMapping(value = "/deleteTkCustomFlowup")
	public Result<?> deleteTkCustomFlowup(@RequestParam(name="id",required=true) String id) {
		tkCustomFlowupService.removeById(id);
		return Result.OK("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "客户跟进信息-批量删除")
	@ApiOperation(value="客户跟进信息-批量删除", notes="客户跟进信息-批量删除")
	@DeleteMapping(value = "/deleteBatchTkCustomFlowup")
	public Result<?> deleteBatchTkCustomFlowup(@RequestParam(name="ids",required=true) String ids) {
	    this.tkCustomFlowupService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportTkCustomFlowup")
    public ModelAndView exportTkCustomFlowup(HttpServletRequest request, TkCustomFlowup tkCustomFlowup) {
		 // Step.1 组装查询条件
		 QueryWrapper<TkCustomFlowup> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomFlowup, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<TkCustomFlowup> pageList = tkCustomFlowupService.list(queryWrapper);
		 List<TkCustomFlowup> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "客户跟进信息"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, TkCustomFlowup.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("客户跟进信息报表", "导出人:" + sysUser.getRealname(), "客户跟进信息"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importTkCustomFlowup/{mainId}")
    public Result<?> importTkCustomFlowup(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<TkCustomFlowup> list = ExcelImportUtil.importExcel(file.getInputStream(), TkCustomFlowup.class, params);
				 for (TkCustomFlowup temp : list) {
                    temp.setCustomerId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 tkCustomFlowupService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.OK("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-客户跟进信息-end----------------------------------------------*/

    /*--------------------------------子表处理-客户收款管理-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "客户收款管理-通过主表ID查询")
	@ApiOperation(value="客户收款管理-通过主表ID查询", notes="客户收款管理-通过主表ID查询")
	@GetMapping(value = "/listTkCustomPayableByMainId")
    public Result<?> listTkCustomPayableByMainId(TkCustomPayable tkCustomPayable,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<TkCustomPayable> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomPayable, req.getParameterMap());
        Page<TkCustomPayable> page = new Page<TkCustomPayable>(pageNo, pageSize);
        IPage<TkCustomPayable> pageList = tkCustomPayableService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

	 /**
	  * 客户收款管理
	  * 通过主表中的客户ID查询 - customerId
	  *  Luo.0022 add
	  * @return
	  */
	 @AutoLog(value = "客户收款管理-通过主表中的客户ID查询")
	 @ApiOperation(value="客户收款管理-通过主表中的客户ID查询", notes="客户收款管理-通过主表中的客户ID查询")
	 @GetMapping(value = "/listTkCustomPayableByCstrId")
	 public Result<?> listTkCustomPayableByCstrId(TkCustomPayable tkCustomService,
												  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
												  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
												  HttpServletRequest req) {
		 LambdaQueryWrapper<TkCustomPayable> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		 lambdaQueryWrapper.eq(TkCustomPayable::getCustomerId, tkCustomService.getCustomerId());
		 lambdaQueryWrapper.orderByDesc(TkCustomPayable::getCreateTime) ;

		 Page<TkCustomPayable> page = new Page<TkCustomPayable>(pageNo, pageSize);
		 IPage<TkCustomPayable> pageList = tkCustomPayableService.page(page, lambdaQueryWrapper);
		 return Result.OK(pageList);
	 }

	/**
	 * 添加
	 * @param tkCustomPayable
	 * @return
	 */
	@AutoLog(value = "客户收款管理-添加")
	@ApiOperation(value="客户收款管理-添加", notes="客户收款管理-添加")
	@PostMapping(value = "/addTkCustomPayable")
	public Result<?> addTkCustomPayable(@RequestBody TkCustomPayable tkCustomPayable) {
		// 后台跟进人员直接后台获取相关信息
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		tkCustomPayable.setDirector(sysUser.getRealname());
		tkCustomPayable.setDirectorId(sysUser.getId());
		tkCustomPayable.setDirectorAvatar(sysUser.getAvatar());

		/*
		tkCustomPayableService.save(tkCustomPayable);

		// 同步合同金额信息到客户表中
		TkCustomers tkCustomers = new TkCustomers() ;
		tkCustomers.setId(tkCustomPayable.getCustomerId());
		tkCustomers.setTotalReceived(tkCustomPayable.getTotalReceived());
		tkCustomers.setContractValue(tkCustomPayable.getContractValue());
		tkCustomersService.updateById(tkCustomers) ;
		*/
		tkCustomersService.addTkCustomPayableBatchMain(tkCustomPayable);
		return Result.OK("添加成功！");
	}

    /**
	 * 编辑
	 * @param tkCustomPayable
	 * @return
	 */
	@AutoLog(value = "客户收款管理-编辑")
	@ApiOperation(value="客户收款管理-编辑", notes="客户收款管理-编辑")
	@PutMapping(value = "/editTkCustomPayable")
	public Result<?> editTkCustomPayable(@RequestBody TkCustomPayable tkCustomPayable) {
		tkCustomPayableService.updateById(tkCustomPayable);
		return Result.OK("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户收款管理-通过id删除")
	@ApiOperation(value="客户收款管理-通过id删除", notes="客户收款管理-通过id删除")
	@DeleteMapping(value = "/deleteTkCustomPayable")
	public Result<?> deleteTkCustomPayable(@RequestParam(name="id",required=true) String id) {
		tkCustomPayableService.removeById(id);
		return Result.OK("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "客户收款管理-批量删除")
	@ApiOperation(value="客户收款管理-批量删除", notes="客户收款管理-批量删除")
	@DeleteMapping(value = "/deleteBatchTkCustomPayable")
	public Result<?> deleteBatchTkCustomPayable(@RequestParam(name="ids",required=true) String ids) {
	    this.tkCustomPayableService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportTkCustomPayable")
    public ModelAndView exportTkCustomPayable(HttpServletRequest request, TkCustomPayable tkCustomPayable) {
		 // Step.1 组装查询条件
		 QueryWrapper<TkCustomPayable> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomPayable, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<TkCustomPayable> pageList = tkCustomPayableService.list(queryWrapper);
		 List<TkCustomPayable> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "客户收款管理"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, TkCustomPayable.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("客户收款管理报表", "导出人:" + sysUser.getRealname(), "客户收款管理"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importTkCustomPayable/{mainId}")
    public Result<?> importTkCustomPayable(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<TkCustomPayable> list = ExcelImportUtil.importExcel(file.getInputStream(), TkCustomPayable.class, params);
				 for (TkCustomPayable temp : list) {
                    temp.setCustomerId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 tkCustomPayableService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.OK("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-客户收款管理-end----------------------------------------------*/

    /*--------------------------------子表处理-客户服务管理-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "客户服务管理-通过主表ID查询")
	@ApiOperation(value="客户服务管理-通过主表ID查询", notes="客户服务管理-通过主表ID查询")
	@GetMapping(value = "/listTkCustomServiceByMainId")
    public Result<?> listTkCustomServiceByMainId(TkCustomService tkCustomService,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<TkCustomService> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomService, req.getParameterMap());
        Page<TkCustomService> page = new Page<TkCustomService>(pageNo, pageSize);
        IPage<TkCustomService> pageList = tkCustomServiceService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

	 /**
	  * 客户服务管理
	  * 通过主表中的客户ID查询 - customerId
	  *  Luo.0022 add
	  * @return
	  */
	 @AutoLog(value = "客户服务管理-通过主表中的客户ID查询")
	 @ApiOperation(value="客户服务管理-通过主表中的客户ID查询", notes="客户服务管理-通过主表中的客户ID查询")
	 @GetMapping(value = "/listTkCustomServiceByCstrId")
	 public Result<?> listTkCustomServiceByCstrId(TkCustomService tkCustomService,
												 @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
												 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
												 HttpServletRequest req) {
//		 QueryWrapper<TkCustomFlowup> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomFlowup, req.getParameterMap());

		 LambdaQueryWrapper<TkCustomService> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		 lambdaQueryWrapper.eq(TkCustomService::getCustomerId, tkCustomService.getCustomerId());
		 lambdaQueryWrapper.orderByDesc(TkCustomService::getCreateTime);

		 Page<TkCustomService> page = new Page<TkCustomService>(pageNo, pageSize);
		 IPage<TkCustomService> pageList = tkCustomServiceService.page(page, lambdaQueryWrapper);
		 return Result.OK(pageList);
	 }

	/**
	 * 添加
	 * @param tkCustomService
	 * @return
	 */
	@AutoLog(value = "客户服务管理-添加")
	@ApiOperation(value="客户服务管理-添加", notes="客户服务管理-添加")
	@PostMapping(value = "/addTkCustomService")
	public Result<?> addTkCustomService(@RequestBody TkCustomService tkCustomService) {
		// 后台跟进人员直接后台获取相关信息
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		tkCustomService.setDirector(sysUser.getRealname());
		tkCustomService.setDirectorId(sysUser.getId());
		tkCustomService.setDirectorAvatar(sysUser.getAvatar());

		tkCustomServiceService.save(tkCustomService);
		return Result.OK("添加成功！");
	}

    /**
	 * 编辑
	 * @param tkCustomService
	 * @return
	 */
	@AutoLog(value = "客户服务管理-编辑")
	@ApiOperation(value="客户服务管理-编辑", notes="客户服务管理-编辑")
	@PutMapping(value = "/editTkCustomService")
	public Result<?> editTkCustomService(@RequestBody TkCustomService tkCustomService) {
		tkCustomServiceService.updateById(tkCustomService);
		return Result.OK("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户服务管理-通过id删除")
	@ApiOperation(value="客户服务管理-通过id删除", notes="客户服务管理-通过id删除")
	@DeleteMapping(value = "/deleteTkCustomService")
	public Result<?> deleteTkCustomService(@RequestParam(name="id",required=true) String id) {
		tkCustomServiceService.removeById(id);
		return Result.OK("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "客户服务管理-批量删除")
	@ApiOperation(value="客户服务管理-批量删除", notes="客户服务管理-批量删除")
	@DeleteMapping(value = "/deleteBatchTkCustomService")
	public Result<?> deleteBatchTkCustomService(@RequestParam(name="ids",required=true) String ids) {
	    this.tkCustomServiceService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportTkCustomService")
    public ModelAndView exportTkCustomService(HttpServletRequest request, TkCustomService tkCustomService) {
		 // Step.1 组装查询条件
		 QueryWrapper<TkCustomService> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomService, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<TkCustomService> pageList = tkCustomServiceService.list(queryWrapper);
		 List<TkCustomService> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "客户服务管理"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, TkCustomService.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("客户服务管理报表", "导出人:" + sysUser.getRealname(), "客户服务管理"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importTkCustomService/{mainId}")
    public Result<?> importTkCustomService(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<TkCustomService> list = ExcelImportUtil.importExcel(file.getInputStream(), TkCustomService.class, params);
				 for (TkCustomService temp : list) {
                    temp.setCustomerId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 tkCustomServiceService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.OK("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-客户服务管理-end----------------------------------------------*/

    /*--------------------------------子表处理-客户操作信息-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "客户操作信息-通过主表ID查询")
	@ApiOperation(value="客户操作信息-通过主表ID查询", notes="客户操作信息-通过主表ID查询")
	@GetMapping(value = "/listTkCustomOperationByMainId")
    public Result<?> listTkCustomOperationByMainId(TkCustomOperation tkCustomOperation,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<TkCustomOperation> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomOperation, req.getParameterMap());
        Page<TkCustomOperation> page = new Page<TkCustomOperation>(pageNo, pageSize);
        IPage<TkCustomOperation> pageList = tkCustomOperationService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

	 /**
	  * 客户操作信息
	  * 通过主表中的客户ID查询 - customerId
	  *  Luo.0022 add
	  * @return
	  */
	 @AutoLog(value = "客户操作信息-通过主表中的客户ID查询")
	 @ApiOperation(value="客户操作信息-通过主表中的客户ID查询", notes="客户操作信息-通过主表中的客户ID查询")
	 @GetMapping(value = "/listTkCustomOperationByCstrId")
	 public Result<?> listTkCustomOperationByCstrId(TkCustomOperation tkCustomOpr,
												  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
												  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
												  HttpServletRequest req) {
		 LambdaQueryWrapper<TkCustomOperation> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		 lambdaQueryWrapper.eq(TkCustomOperation::getCustomerId, tkCustomOpr.getCustomerId());
		 lambdaQueryWrapper.orderByDesc(TkCustomOperation::getCreateTime) ;

		 Page<TkCustomOperation> page = new Page<TkCustomOperation>(pageNo, pageSize);
		 IPage<TkCustomOperation> pageList = tkCustomOperationService.page(page, lambdaQueryWrapper);
		 return Result.OK(pageList);
	 }

	 /**
	  * 客户操作信息
	  * 通过操作表中的租户ID查询 - tenantIdt
	  *  Luo.0022 add
	  * @return
	  */
	 @AutoLog(value = "客户操作信息-通过操作表中的租户ID查询")
	 @ApiOperation(value="客户操作信息-通过操作表中的租户ID查询", notes="客户操作信息-通过操作表中的租户ID查询")
	 @GetMapping(value = "/listTkCustomsByTenantId")
	 public Result<?> listTkCustomsByTenantId(TkCustomOperation tkCustomOpr,
													@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
													@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
													HttpServletRequest req) {
	 	// 把当天时间定义在从 00 ~~ 59 之间
		 String day = DateUtil.nowDateTime(TokerCommonConstant.DATE_FORMAT_START);
		 String end = DateUtil.nowDateTime(TokerCommonConstant.DATE_FORMAT_END);
		 Date startDay = DateUtil.strToDate(day,TokerCommonConstant.DATE_FORMAT) ;
		 Date endDay = DateUtil.strToDate(end,TokerCommonConstant.DATE_FORMAT) ;

		 LambdaQueryWrapper<TkCustomOperation> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		 lambdaQueryWrapper.eq(TkCustomOperation::getTenantId, tkCustomOpr.getTenantId());
		 lambdaQueryWrapper.eq(TkCustomOperation::getModel, "ADD_CUSTOM");
		 lambdaQueryWrapper.between(TkCustomOperation::getCreateTime,startDay,endDay) ; // 使用Between
		 lambdaQueryWrapper.orderByDesc(TkCustomOperation::getCreateTime) ;

		 Page<TkCustomOperation> page = new Page<TkCustomOperation>(pageNo, pageSize);
		 IPage<TkCustomOperation> pageList = tkCustomOperationService.page(page, lambdaQueryWrapper);
		 return Result.OK(pageList);
	 }

	/**
	 * 添加
	 * @param tkCustomOperation
	 * @return
	 */
	@AutoLog(value = "客户操作信息-添加")
	@ApiOperation(value="客户操作信息-添加", notes="客户操作信息-添加")
	@PostMapping(value = "/addTkCustomOperation")
	public Result<?> addTkCustomOperation(@RequestBody TkCustomOperation tkCustomOperation) {
		tkCustomOperationService.save(tkCustomOperation);
		return Result.OK("添加成功！");
	}

    /**
	 * 编辑
	 * @param tkCustomOperation
	 * @return
	 */
	@AutoLog(value = "客户操作信息-编辑")
	@ApiOperation(value="客户操作信息-编辑", notes="客户操作信息-编辑")
	@PutMapping(value = "/editTkCustomOperation")
	public Result<?> editTkCustomOperation(@RequestBody TkCustomOperation tkCustomOperation) {
		tkCustomOperationService.updateById(tkCustomOperation);
		return Result.OK("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户操作信息-通过id删除")
	@ApiOperation(value="客户操作信息-通过id删除", notes="客户操作信息-通过id删除")
	@DeleteMapping(value = "/deleteTkCustomOperation")
	public Result<?> deleteTkCustomOperation(@RequestParam(name="id",required=true) String id) {
		tkCustomOperationService.removeById(id);
		return Result.OK("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "客户操作信息-批量删除")
	@ApiOperation(value="客户操作信息-批量删除", notes="客户操作信息-批量删除")
	@DeleteMapping(value = "/deleteBatchTkCustomOperation")
	public Result<?> deleteBatchTkCustomOperation(@RequestParam(name="ids",required=true) String ids) {
	    this.tkCustomOperationService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportTkCustomOperation")
    public ModelAndView exportTkCustomOperation(HttpServletRequest request, TkCustomOperation tkCustomOperation) {
		 // Step.1 组装查询条件
		 QueryWrapper<TkCustomOperation> queryWrapper = QueryGenerator.initQueryWrapper(tkCustomOperation, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<TkCustomOperation> pageList = tkCustomOperationService.list(queryWrapper);
		 List<TkCustomOperation> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "客户操作信息"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, TkCustomOperation.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("客户操作信息报表", "导出人:" + sysUser.getRealname(), "客户操作信息"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importTkCustomOperation/{mainId}")
    public Result<?> importTkCustomOperation(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<TkCustomOperation> list = ExcelImportUtil.importExcel(file.getInputStream(), TkCustomOperation.class, params);
				 for (TkCustomOperation temp : list) {
                    temp.setCustomerId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 tkCustomOperationService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.OK("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-客户操作信息-end----------------------------------------------*/




}
