package org.jeecg.modules.toker.setting.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.toker.setting.entity.TkSetCustomer;
import org.jeecg.modules.toker.setting.entity.TkSetCustomerLevel;
import org.jeecg.modules.toker.setting.mapper.TkSetCustomerLevelMapper;
import org.jeecg.modules.toker.setting.mapper.TkSetCustomerMapper;
import org.jeecg.modules.toker.setting.service.ITkSetCustomerService;
import org.jeecg.modules.toker.setting.vo.TkSetCustomerPage;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 客户管理设置
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
@Service
public class TkSetCustomerServiceImpl extends ServiceImpl<TkSetCustomerMapper, TkSetCustomer> implements ITkSetCustomerService {

	@Autowired
	private TkSetCustomerMapper tkSetCustomerMapper;
	@Autowired
	private TkSetCustomerLevelMapper tkSetCustomerLevelMapper;

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Override
	@Transactional
	public void delMain(String id) {
		tkSetCustomerLevelMapper.deleteByMainId(id);
		tkSetCustomerMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			tkSetCustomerLevelMapper.deleteByMainId(id.toString());
			tkSetCustomerMapper.deleteById(id);
		}
	}

	/**
	 * 通过租户ID查询 - tenantId
	 * Luo.0022 2021-08-11
	 * @param tenantId
	 * @return
	 */
	public TkSetCustomer getByTenantId(String tenantId){
		LambdaQueryWrapper<TkSetCustomer> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(TkSetCustomer::getTenantId, tenantId);
		TkSetCustomer setCustomer = tkSetCustomerMapper.selectOne(lambdaQueryWrapper) ;
		return setCustomer ;
	}

	/**
	 * 通过租户ID查询 管理设置及级别所有信息
	 * Luo.0022 2021-08-11
	 * @param tenantId
	 * @return
	 */
	public TkSetCustomerPage getMainByTenantId(String tenantId) {

		LambdaQueryWrapper<TkSetCustomer> setQuery = new LambdaQueryWrapper<>();
		setQuery.eq(TkSetCustomer::getTenantId, tenantId);
		TkSetCustomer setCustomer = tkSetCustomerMapper.selectOne(setQuery) ;
		if(setCustomer == null ){
			return null ;
		}

		TkSetCustomerPage page = new TkSetCustomerPage() ;

		// source, target
		BeanUtils.copyProperties(setCustomer, page) ;

		List<TkSetCustomerLevel> tkSetCustomerLevelList = tkSetCustomerLevelMapper.selectByMainId(setCustomer.getId());
		page.setTkSetCustomerLevelList(tkSetCustomerLevelList);
		return page ;
	}

	/**
	 * 初始化租户下的客户设置
	 *  Luo.0022 2021-08-11
	 * @param tenantId
	 */
	public Boolean initCustomerSetAll(String tenantId) {
		Boolean execute = transactionTemplate.execute(e -> {
			// 初始化 客户设置
			TkSetCustomer tkSetCustomer = initCustomerSet(tenantId) ;

			// 初始化 客户级别设置
			List<TkSetCustomerLevel> levelList = initCustomerLevelSet() ;

			//saveMain(tkSetCustomer,levelList);

			// 批量添加 , insert后已经有对应的ID，可验证
			tkSetCustomerMapper.insert(tkSetCustomer);
			//tkSetCustomerMapper.insertCustomerSet(tkSetCustomer);

			if(levelList!=null && levelList.size()>0) {
				for(TkSetCustomerLevel entity:levelList) {
					//外键设置
					entity.setCustomerId(tkSetCustomer.getId());
					tkSetCustomerLevelMapper.insert(entity);
				}
			}
			return Boolean.TRUE;
		});
		return execute ;
	}

	/**
	 * 初始化 客户管理设置
	 *  Luo.0022 2021-08-11
	 * @param tenantId
	 * @return
	 */
	private TkSetCustomer initCustomerSet(String tenantId){
		TkSetCustomer tkSetCustomer = new TkSetCustomer() ;
		tkSetCustomer.setIsWarn("false");
		tkSetCustomer.setNoPeriodStart(23);
		tkSetCustomer.setNoPeriodEnd(7);
		tkSetCustomer.setPhoneIs11("true");
		//tkSetCustomer.setTenantId(tenantId);
		return tkSetCustomer ;
	}

	/**
	 * 初始化 客户管理级别设置
	 *  Luo.0022 2021-08-11
	 * @return
	 */
	private List<TkSetCustomerLevel> initCustomerLevelSet () {
		List<TkSetCustomerLevel> levelList = new ArrayList<>() ;

		List<String> levelTypes = Arrays.asList("A", "B", "C", "D");
		List<String> ruleTypes = Arrays.asList("2", "2", "1", "1"); // 规则类型
		List<Boolean> rules = Arrays.asList(true, true, false, false);//开启级别规则限制
		List<String> actions = Arrays.asList("1,2,3", "1,2", "", "");//动作选择
		List<Integer> conditionTimes = Arrays.asList(3, 2, 0, 0);//满足条件
		List<Integer> seanCycles = Arrays.asList(2, 3, 4, 6);//公海周期
		List<String> remarks = Arrays.asList("已进店、已测量、已确定方案及预算", "已进店、已测量、正在做方案",
				"有需求、已进店", "有意向、未进店");//名词解释
		//for(String level : levels){
		int m = levelTypes.size();
		for (int i = 0; i < m; i++) {
			TkSetCustomerLevel setLevel = new TkSetCustomerLevel() ;
			setLevel.setLevelName(levelTypes.get(i) + "级客户设置");
			setLevel.setLevelType(levelTypes.get(i));
			// 规则设置
			if(rules.get(i)) {
				if (ruleTypes.get(i) == "1"){
					setLevel.setCsaleProbability("10%");
				}else {
					setLevel.setCenableLevelRule(rules.get(i).toString());
					setLevel.setCruleType(ruleTypes.get(i));
					setLevel.setCsaleAction(actions.get(i));
					setLevel.setCconditionTimes(conditionTimes.get(i));
				}
				setLevel.setCautoExec("true");
			}
			// 个人设置
			setLevel.setOfollowUpNum(0);
			setLevel.setOcycle(seanCycles.get(i));
			setLevel.setOwithoutSigningSeas(0);
			setLevel.setOgoSeasTimes(2);

			// 部门设置
			setLevel.setDenableDepartSeas("false");
			setLevel.setDcycle(2);
			setLevel.setDcycleTimes(2);

			// 名词解释
			setLevel.setRemark(remarks.get(i)); //

			// 其他
			setLevel.setIsVisitRemind("false");
			setLevel.setIsAutoRemind("false");

			levelList.add(setLevel) ;
		}

		return levelList ;
	}
	
}
