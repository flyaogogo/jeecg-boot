package org.jeecg.modules.toker.customer.service.impl;

import org.jeecg.modules.toker.customer.entity.TkCustomPayable;
import org.jeecg.modules.toker.customer.mapper.TkCustomPayableMapper;
import org.jeecg.modules.toker.customer.service.ITkCustomPayableService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 客户收款管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Service
public class TkCustomPayableServiceImpl extends ServiceImpl<TkCustomPayableMapper, TkCustomPayable> implements ITkCustomPayableService {
	
	@Autowired
	private TkCustomPayableMapper tkCustomPayableMapper;
	
	@Override
	public List<TkCustomPayable> selectByMainId(String mainId) {
		return tkCustomPayableMapper.selectByMainId(mainId);
	}
}
