package org.jeecg.modules.toker.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.customer.entity.*;
import org.jeecg.modules.toker.customer.mapper.TkCustomFlowupMapper;
import org.jeecg.modules.toker.customer.mapper.TkCustomPayableMapper;
import org.jeecg.modules.toker.customer.mapper.TkCustomServiceMapper;
import org.jeecg.modules.toker.customer.mapper.TkCustomOperationMapper;
import org.jeecg.modules.toker.customer.mapper.TkCustomersMapper;
import org.jeecg.modules.toker.customer.service.ITkCustomersService;
import org.jeecg.modules.toker.customer.vo.CustomerStatistics;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @Description: 客户信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Service
public class TkCustomersServiceImpl extends ServiceImpl<TkCustomersMapper, TkCustomers> implements ITkCustomersService {

	@Autowired
	private TkCustomersMapper tkCustomersMapper;
	@Autowired
	private TkCustomFlowupMapper tkCustomFlowupMapper;
	@Autowired
	private TkCustomPayableMapper tkCustomPayableMapper;
	@Autowired
	private TkCustomServiceMapper tkCustomServiceMapper;
	@Autowired
	private TkCustomOperationMapper tkCustomOperationMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		tkCustomFlowupMapper.deleteByMainId(id);
		tkCustomPayableMapper.deleteByMainId(id);
		tkCustomServiceMapper.deleteByMainId(id);
		tkCustomOperationMapper.deleteByMainId(id);
		tkCustomersMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			tkCustomFlowupMapper.deleteByMainId(id.toString());
			tkCustomPayableMapper.deleteByMainId(id.toString());
			tkCustomServiceMapper.deleteByMainId(id.toString());
			tkCustomOperationMapper.deleteByMainId(id.toString());
			tkCustomersMapper.deleteById(id);
		}
	}

	/**
	 * 统计客户 相关信息，并按分页查询相关列表数据
	 * @param customers
	 * @return
	 */
	@Override
	public CustomerStatistics queryCustomersByState(TkCustomers customers, PageParamRequest pageParamRequest){
		Page<TkCustomers> page = new Page<>(pageParamRequest.getPage(), pageParamRequest.getLimit());

		// 初始化 返回前端数据
		CustomerStatistics cs = new CustomerStatistics() ;
		// 查询条件
		LambdaQueryWrapper<TkCustomers> lambdaQueryWrapper = new LambdaQueryWrapper<>();

		//followup还是以创建时间为准， else 主要针对 signing  delivery  highseas   loss，以切换各状态时间为准
		lambdaQueryWrapper.eq(TkCustomers::getStage, customers.getStage());
		if (customers.getStage().equals("followup")) {
			lambdaQueryWrapper.eq(TkCustomers::getLevel, customers.getLevel());
			lambdaQueryWrapper.orderByDesc(TkCustomers::getCreateTime) ;
		}else {
			lambdaQueryWrapper.isNotNull(TkCustomers::getStateTime) ;
			lambdaQueryWrapper.orderByDesc(TkCustomers::getStateTime) ;
		}
		// 以下是通过分页 展示 列表中的 数据
		IPage<TkCustomers> cstmrPage = tkCustomersMapper.selectPage(page, lambdaQueryWrapper) ;
		cs.setCustomersList(cstmrPage.getRecords());

		// 查 各 阶段中 总的 数据
		List<TkCustomers> cstmrs = tkCustomersMapper.selectList(lambdaQueryWrapper) ;
		// 针对查询出来的所有数据中 的 某些字段 做计算操作
		Long totalCustomer = cstmrs.stream().collect(Collectors.counting()) ;

		//BigDecimal totalReceived = new BigDecimal(0) ;
		//BigDecimal totalContract = new BigDecimal(0) ;
		BigDecimal totalReceived = cstmrs.stream().map(TkCustomers::getTotalReceived).filter(s->s!=null).reduce(BigDecimal.ZERO, BigDecimal::add) ;

		BigDecimal totalContract = cstmrs.stream().map(TkCustomers::getContractValue).filter(s->s!=null).reduce(BigDecimal.ZERO, BigDecimal::add) ;

		cs.setTotalCustomer(totalCustomer);
		cs.setTotalReceived(totalReceived);
		cs.setTotalContract(totalContract);

		return cs ;
	}

	@Override
	@Transactional
	public void addTkCustomBatchMain(TkCustomers customers){
		// 临时借用此字段，再清空
		String directorAvatar = customers.getTemplateId() ;
		customers.setTemplateId("");
		tkCustomersMapper.insert(customers);
		// 以下添加日志操作
		String TKC_MODE = "ADD_CUSTOM" ;
		String TKC_RECORD = customers.getFlowupPerson() + " 新增客户：" + customers.getName()  ;
		String TKC_REMARK = "添加了客户" ;

		TkCustomOperation tkCOper = doObjectJoin(customers.getId(),customers.getFlowupPerson(),
				directorAvatar,customers.getTkuserId(),TKC_MODE,TKC_RECORD,TKC_REMARK) ;
		tkCOper.setTenantId(customers.getTenantId());
		tkCustomOperationMapper.insert(tkCOper) ;
	}

	@Override
	@Transactional
	public void addTkCustomFlowupBatchMain(TkCustomFlowup tkCustomFlowup) {
		// 添加跟进记录
		tkCustomFlowupMapper.insert(tkCustomFlowup);

		// 添加完Flowup后，要把 最新  的内容更新到用户中
		TkCustomers tkCustomers = new TkCustomers() ;
		tkCustomers.setId(tkCustomFlowup.getCustomerId());
		tkCustomers.setFlowupModel(tkCustomFlowup.getFlowupModel());
		tkCustomers.setFlowupType(tkCustomFlowup.getFlowupType());
		tkCustomers.setLatelyDetails(tkCustomFlowup.getDetails());
		tkCustomers.setLatelyTime(new Date());
		tkCustomersMapper.updateById(tkCustomers);

		// 以下添加日志操作
		String TKC_MODE = "ADD_FLOWUP" ;
		String TKC_RECORD = tkCustomFlowup.getDirector() + "新增客户跟进信息" ;
		String TKC_REMARK = "新增" ;

		TkCustomOperation tkCOper = doObjectJoin(tkCustomFlowup.getCustomerId(),tkCustomFlowup.getDirector(),
				tkCustomFlowup.getDirectorAvatar(),tkCustomFlowup.getDirectorId(),TKC_MODE,TKC_RECORD,TKC_REMARK) ;

		tkCustomOperationMapper.insert(tkCOper) ;
	}

	private TkCustomOperation doObjectJoin(String customerId, String director,String directorAvatar,String directorId, String model,String record,String remark){

		TkCustomOperation tkCOper = new TkCustomOperation() ;
		tkCOper.setCustomerId(customerId);
		tkCOper.setModel(model);
		tkCOper.setRecord(record);
		tkCOper.setOperator(director);
		tkCOper.setOptTime(DateUtil.nowDateTime("yyyy-MM-dd HH:mm:ss"));
		tkCOper.setRemark(remark);

		tkCOper.setDirector(director);
		tkCOper.setDirectorAvatar(directorAvatar);
		tkCOper.setDirectorId(directorId);
		return tkCOper ;
	}


	@Override
	@Transactional
	public void editTkCustomFlowupBatchMain(TkCustomFlowup tkCustomFlowup) {
		tkCustomFlowupMapper.updateById(tkCustomFlowup);

		// 添加完Flowup后，要把 最新  的内容更新到用户中
		TkCustomers tkCustomers = new TkCustomers() ;
		tkCustomers.setId(tkCustomFlowup.getCustomerId());
		tkCustomers.setFlowupModel(tkCustomFlowup.getFlowupModel());
		tkCustomers.setFlowupType(tkCustomFlowup.getFlowupType());
		tkCustomers.setLatelyDetails(tkCustomFlowup.getDetails());
		tkCustomers.setLatelyTime(new Date());
		tkCustomersMapper.updateById(tkCustomers);

		// 以下添加日志操作
		String TKC_MODE = "EDIT_CUSTOM" ;
		String TKC_RECORD = tkCustomFlowup.getDirector() + "修改客户信息" ;
		String TKC_REMARK = "修改" ;

		TkCustomOperation tkCOper = doObjectJoin(tkCustomFlowup.getCustomerId(),tkCustomFlowup.getDirector(),
				tkCustomFlowup.getDirectorAvatar(),tkCustomFlowup.getDirectorId(),TKC_MODE,TKC_RECORD,TKC_REMARK) ;

		tkCustomOperationMapper.insert(tkCOper) ;
	}

	@Override
	@Transactional
	public void addTkCustomPayableBatchMain(TkCustomPayable tkCustomPayable) {


		tkCustomPayableMapper.insert(tkCustomPayable);

		// 同步合同金额信息到客户表中
		TkCustomers tkCustomers = new TkCustomers() ;
		tkCustomers.setId(tkCustomPayable.getCustomerId());
		tkCustomers.setTotalReceived(tkCustomPayable.getTotalReceived());
		tkCustomers.setContractValue(tkCustomPayable.getContractValue());
		tkCustomersMapper.updateById(tkCustomers) ;

		// 以下添加日志操作
		String TKC_MODE = "ADD_PAYABLE" ;
		String TKC_RECORD = tkCustomPayable.getDirector() + "跟进客户付款" ;
		String TKC_REMARK = "新增" ;

		TkCustomOperation tkCOper = doObjectJoin(tkCustomPayable.getCustomerId(),tkCustomPayable.getDirector(),
				tkCustomPayable.getDirectorAvatar(),tkCustomPayable.getDirectorId(),TKC_MODE,TKC_RECORD,TKC_REMARK) ;
		tkCustomOperationMapper.insert(tkCOper) ;
	}

	@Override
	@Transactional
	public void editTkCustomPayableBatchMain(TkCustomPayable tkCustomPayable) {

	}

	@Override
	@Transactional
	public void addTkCustomServiceBatchMain(TkCustomService tkCustomService) {

	}

	@Override
	@Transactional
	public void editTkCustomServiceBatchMain(TkCustomService tkCustomService) {

	}

}
