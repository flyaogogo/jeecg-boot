package org.jeecg.modules.toker.customer.vo;

import lombok.Data;
import org.jeecg.modules.toker.customer.entity.TkCustomers;

import java.util.List;

@Data
public class CustomerStatistics {

    /** 客户总人数 **/
    private Long totalCustomer;
    /**合同金额*/
    private java.math.BigDecimal totalContract;
    /**收总额*/
    private java.math.BigDecimal totalReceived;
    /** 客户列表 */
    private List<TkCustomers> customersList ;
}
