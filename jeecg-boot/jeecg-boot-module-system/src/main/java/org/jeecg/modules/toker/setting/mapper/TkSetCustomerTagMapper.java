package org.jeecg.modules.toker.setting.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.setting.entity.TkSetCustomerTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 客户标签设置
 * @Author: Luo.0022
 * @Date:   2021-06-22
 * @Version: V1.0
 */
public interface TkSetCustomerTagMapper extends BaseMapper<TkSetCustomerTag> {

}
