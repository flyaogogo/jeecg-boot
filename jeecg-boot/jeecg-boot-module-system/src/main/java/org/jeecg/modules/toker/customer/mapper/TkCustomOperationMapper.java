package org.jeecg.modules.toker.customer.mapper;

import java.util.List;
import org.jeecg.modules.toker.customer.entity.TkCustomOperation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 客户操作信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface TkCustomOperationMapper extends BaseMapper<TkCustomOperation> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<TkCustomOperation> selectByMainId(@Param("mainId") String mainId);

}
