package org.jeecg.modules.toker.customer.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.UnsupportedEncodingException;

/**
 * @Description: 客户服务管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Data
@TableName("tk_custom_service")
@ApiModel(value="tk_custom_service对象", description="客户服务管理")
public class TkCustomService implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private String id;
	/**客户ID*/
    @ApiModelProperty(value = "客户ID")
    private String customerId;
	/**服务单类型*/
	@Excel(name = "服务单类型", width = 15)
    @Dict(dicCode = "tk_c_service_type")
    @ApiModelProperty(value = "服务单类型")
    private String type;

    /**服务单状态*/
    @Excel(name = "服务单状态", width = 15)
    @ApiModelProperty(value = "服务单状态")
    private String status;


	/**服务人*/
	@Excel(name = "服务人", width = 15)
    @ApiModelProperty(value = "服务人")
    private String servicer;
	/**服务时间*/
	@Excel(name = "服务时间", width = 15)
    @ApiModelProperty(value = "服务时间")
    private String serviceTime;
	/**测量单*/
	@Excel(name = "测量单", width = 15)
    @ApiModelProperty(value = "测量单")
    private String surveySheet;
	/**图片*/
	@Excel(name = "图片", width = 15)
    @ApiModelProperty(value = "图片")
    private String pic;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;


    /**跟进人ID*/
    @Excel(name = "跟进人ID", width = 15)
    @ApiModelProperty(value = "跟进人ID")
    private String directorId;

    /**跟进人姓名*/
    @Excel(name = "跟进人姓名", width = 15)
    @ApiModelProperty(value = "跟进人姓名")
    private String director;

    /**跟进人头像*/
    @Excel(name = "跟进人头像", width = 15)
    @ApiModelProperty(value = "跟进人头像")
    private String directorAvatar;
}
