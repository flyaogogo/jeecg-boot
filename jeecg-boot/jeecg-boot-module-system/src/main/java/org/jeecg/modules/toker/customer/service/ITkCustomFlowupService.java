package org.jeecg.modules.toker.customer.service;

import org.jeecg.modules.toker.customer.entity.TkCustomFlowup;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 客户跟进信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface ITkCustomFlowupService extends IService<TkCustomFlowup> {

	public List<TkCustomFlowup> selectByMainId(String mainId);
}
