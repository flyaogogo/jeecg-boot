package org.jeecg.modules.toker.customer.service;

import org.jeecg.modules.toker.customer.entity.TkCustomPayable;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 客户收款管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface ITkCustomPayableService extends IService<TkCustomPayable> {

	public List<TkCustomPayable> selectByMainId(String mainId);
}
