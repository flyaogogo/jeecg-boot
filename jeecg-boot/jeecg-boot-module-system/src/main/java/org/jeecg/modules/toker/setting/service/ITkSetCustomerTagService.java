package org.jeecg.modules.toker.setting.service;

import org.jeecg.modules.toker.setting.entity.TkSetCustomerTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 客户标签设置
 * @Author: Luo.0022
 * @Date:   2021-06-22
 * @Version: V1.0
 */
public interface ITkSetCustomerTagService extends IService<TkSetCustomerTag> {

}
