package org.jeecg.modules.toker.setting.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.toker.setting.entity.TkSetCustomerTag;
import org.jeecg.modules.toker.setting.service.ITkSetCustomerTagService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 客户标签设置
 * @Author: Luo.0022
 * @Date:   2021-06-22
 * @Version: V1.0
 */
@Api(tags="客户标签设置")
@RestController
@RequestMapping("/setting/tkSetCustomerTag")
@Slf4j
public class TkSetCustomerTagController extends JeecgController<TkSetCustomerTag, ITkSetCustomerTagService> {
	@Autowired
	private ITkSetCustomerTagService tkSetCustomerTagService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tkSetCustomerTag
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "客户标签设置-分页列表查询")
	@ApiOperation(value="客户标签设置-分页列表查询", notes="客户标签设置-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkSetCustomerTag tkSetCustomerTag,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkSetCustomerTag> queryWrapper = QueryGenerator.initQueryWrapper(tkSetCustomerTag, req.getParameterMap());
		Page<TkSetCustomerTag> page = new Page<TkSetCustomerTag>(pageNo, pageSize);
		IPage<TkSetCustomerTag> pageList = tkSetCustomerTagService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	 /**
	  * 列表 所有 的 可用标签
	  * Luo.0022 add 2021-09-07
	  * @return
	  */
	 @AutoLog(value = "客户标签设置-所有可用标签")
	 @ApiOperation(value="客户标签设置-所有可用标签", notes="客户标签设置-所有可用标签")
	 @GetMapping(value = "/all")
	 public Result<?> queryAllList() {
		 LambdaQueryWrapper<TkSetCustomerTag> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		 lambdaQueryWrapper.eq(TkSetCustomerTag::getStatus, "Y");
		 List<TkSetCustomerTag> pageList = tkSetCustomerTagService.list(lambdaQueryWrapper) ;
		 return Result.OK(pageList);
	 }

	/**
	 *   添加
	 *
	 * @param tkSetCustomerTag
	 * @return
	 */
	@AutoLog(value = "客户标签设置-添加")
	@ApiOperation(value="客户标签设置-添加", notes="客户标签设置-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkSetCustomerTag tkSetCustomerTag) {
		tkSetCustomerTagService.save(tkSetCustomerTag);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkSetCustomerTag
	 * @return
	 */
	@AutoLog(value = "客户标签设置-编辑")
	@ApiOperation(value="客户标签设置-编辑", notes="客户标签设置-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkSetCustomerTag tkSetCustomerTag) {
		tkSetCustomerTagService.updateById(tkSetCustomerTag);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户标签设置-通过id删除")
	@ApiOperation(value="客户标签设置-通过id删除", notes="客户标签设置-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkSetCustomerTagService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "客户标签设置-批量删除")
	@ApiOperation(value="客户标签设置-批量删除", notes="客户标签设置-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkSetCustomerTagService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户标签设置-通过id查询")
	@ApiOperation(value="客户标签设置-通过id查询", notes="客户标签设置-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkSetCustomerTag tkSetCustomerTag = tkSetCustomerTagService.getById(id);
		if(tkSetCustomerTag==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkSetCustomerTag);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tkSetCustomerTag
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkSetCustomerTag tkSetCustomerTag) {
        return super.exportXls(request, tkSetCustomerTag, TkSetCustomerTag.class, "客户标签设置");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkSetCustomerTag.class);
    }

}
