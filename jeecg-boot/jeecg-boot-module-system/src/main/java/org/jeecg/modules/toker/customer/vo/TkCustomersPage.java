package org.jeecg.modules.toker.customer.vo;

import java.util.List;
import org.jeecg.modules.toker.customer.entity.TkCustomers;
import org.jeecg.modules.toker.customer.entity.TkCustomFlowup;
import org.jeecg.modules.toker.customer.entity.TkCustomPayable;
import org.jeecg.modules.toker.customer.entity.TkCustomService;
import org.jeecg.modules.toker.customer.entity.TkCustomOperation;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 客户信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="tk_customersPage对象", description="客户信息")
public class TkCustomersPage {

	/**用户id*/
	@ApiModelProperty(value = "用户id")
    private String id;
	/**业主姓名*/
	@Excel(name = "业主姓名", width = 15)
	@ApiModelProperty(value = "业主姓名")
    private String name;
	/**业主电话*/
	@Excel(name = "业主电话", width = 15)
	@ApiModelProperty(value = "业主电话")
    private String phone;
	/**备选电话*/
	@Excel(name = "备选电话", width = 15)
	@ApiModelProperty(value = "备选电话")
    private String telephone;
	/**标签*/
	@Excel(name = "标签", width = 15, dictTable = "tk_set_customer_tag", dicText = "tag_name", dicCode = "id")
    @Dict(dictTable = "tk_set_customer_tag", dicText = "tag_name", dicCode = "id")
	@ApiModelProperty(value = "标签")
    private String tags;
	/**客户户型*/
	@Excel(name = "客户户型", width = 15, dicCode = "tk_c_house_type")
    @Dict(dicCode = "tk_c_house_type")
	@ApiModelProperty(value = "客户户型")
    private String houseType;
	/**客户级别*/
	@Excel(name = "客户级别", width = 15, dicCode = "tk_level_type")
    @Dict(dicCode = "tk_level_type")
	@ApiModelProperty(value = "客户级别")
    private String level;
	/**客户来源*/
	@Excel(name = "客户来源", width = 15, dicCode = "tk_c_source")
    @Dict(dicCode = "tk_c_source")
	@ApiModelProperty(value = "客户来源")
    private String source;
	/**指数*/
	@Excel(name = "指数", width = 15)
	@ApiModelProperty(value = "指数")
    private Integer cindex;
	/**5 星评价*/
	@Excel(name = "5 星评价", width = 15)
	@ApiModelProperty(value = "5 星评价")
    private String fiveStar;
	/**客户阶段*/
	@Excel(name = "客户阶段", width = 15, dicCode = "tk_c_stage")
    @Dict(dicCode = "tk_c_stage")
	@ApiModelProperty(value = "客户阶段")
    private String stage;
	/**销售机会*/
	@Excel(name = "销售机会", width = 15)
	@ApiModelProperty(value = "销售机会")
    private String opportunities;
	/**客户负责人*/
	@Excel(name = "客户负责人", width = 15)
	@ApiModelProperty(value = "客户负责人")
    private String customerLeader;
	/**跟进人*/
	@Excel(name = "跟进人", width = 15)
	@ApiModelProperty(value = "跟进人")
    private String flowupPerson;
	/**共享给*/
	@Excel(name = "共享给", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
	@ApiModelProperty(value = "共享给")
    private String shareTo;
	/**进入公海次数*/
	@Excel(name = "进入公海次数", width = 15)
	@ApiModelProperty(value = "进入公海次数")
    private Integer seasTimes;
	/**进入公海天数*/
	@Excel(name = "进入公海天数", width = 15)
	@ApiModelProperty(value = "进入公海天数")
    private Integer seasDays;
	/**掉进公海时间*/
	@Excel(name = "掉进公海时间", width = 15)
	@ApiModelProperty(value = "掉进公海时间")
    private String intoSeaTime;
	/**客户地址*/
	@Excel(name = "客户地址", width = 15)
	@ApiModelProperty(value = "客户地址")
    private String address;
	/**头像地址*/
	@Excel(name = "头像地址", width = 15)
	@ApiModelProperty(value = "头像地址")
    private String headPic;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
    private String remark;
	/**自动跟进模板ID*/
	@Excel(name = "自动跟进模板ID", width = 15)
	@ApiModelProperty(value = "自动跟进模板ID")
    private String templateId;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	/**业主地址Id*/
	@Excel(name = "业主地址Id", width = 15)
	@ApiModelProperty(value = "业主地址Id")
    private String tkaddrId;
	/**客户跟进人Id*/
	@Excel(name = "客户跟进人Id", width = 15)
	@ApiModelProperty(value = "客户跟进人Id")
    private String tkuserId;
	/**租户id*/
	@Excel(name = "租户id", width = 15)
	@ApiModelProperty(value = "租户id")
    private String tenantId;

	@ExcelCollection(name="客户跟进信息")
	@ApiModelProperty(value = "客户跟进信息")
	private List<TkCustomFlowup> tkCustomFlowupList;
	@ExcelCollection(name="客户收款管理")
	@ApiModelProperty(value = "客户收款管理")
	private List<TkCustomPayable> tkCustomPayableList;
	@ExcelCollection(name="客户服务管理")
	@ApiModelProperty(value = "客户服务管理")
	private List<TkCustomService> tkCustomServiceList;
	@ExcelCollection(name="客户操作信息")
	@ApiModelProperty(value = "客户操作信息")
	private List<TkCustomOperation> tkCustomOperationList;

}
