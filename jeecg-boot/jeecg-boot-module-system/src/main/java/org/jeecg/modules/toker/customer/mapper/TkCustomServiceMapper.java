package org.jeecg.modules.toker.customer.mapper;

import java.util.List;
import org.jeecg.modules.toker.customer.entity.TkCustomService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 客户服务管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface TkCustomServiceMapper extends BaseMapper<TkCustomService> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<TkCustomService> selectByMainId(@Param("mainId") String mainId);

}
