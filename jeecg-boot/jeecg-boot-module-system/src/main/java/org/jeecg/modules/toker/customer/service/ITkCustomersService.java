package org.jeecg.modules.toker.customer.service;

import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.customer.entity.TkCustomFlowup;
import org.jeecg.modules.toker.customer.entity.TkCustomPayable;
import org.jeecg.modules.toker.customer.entity.TkCustomService;
import org.jeecg.modules.toker.customer.entity.TkCustomOperation;
import org.jeecg.modules.toker.customer.entity.TkCustomers;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.toker.customer.vo.CustomerStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 客户信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface ITkCustomersService extends IService<TkCustomers> {

	/**
	 * 删除一对多
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);

	/**
	 * 统计客户相关信息
	 * @param customers
	 * @param pageParamRequest
	 *
	 * @return
	 */
	public CustomerStatistics queryCustomersByState(TkCustomers customers, PageParamRequest pageParamRequest) ;

	public void addTkCustomBatchMain(TkCustomers customers) ;

	public void addTkCustomFlowupBatchMain(TkCustomFlowup tkCustomFlowup) ;

	public void editTkCustomFlowupBatchMain(TkCustomFlowup tkCustomFlowup) ;

	public void addTkCustomPayableBatchMain(TkCustomPayable tkCustomPayable) ;

	public void editTkCustomPayableBatchMain(TkCustomPayable tkCustomPayable) ;

	public void addTkCustomServiceBatchMain(TkCustomService tkCustomService) ;

	public void editTkCustomServiceBatchMain(TkCustomService tkCustomService) ;


}
