package org.jeecg.modules.toker.setting.service.impl;

import org.jeecg.modules.toker.setting.entity.TkSetCustomerTag;
import org.jeecg.modules.toker.setting.mapper.TkSetCustomerTagMapper;
import org.jeecg.modules.toker.setting.service.ITkSetCustomerTagService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 客户标签设置
 * @Author: Luo.0022
 * @Date:   2021-06-22
 * @Version: V1.0
 */
@Service
public class TkSetCustomerTagServiceImpl extends ServiceImpl<TkSetCustomerTagMapper, TkSetCustomerTag> implements ITkSetCustomerTagService {

}
