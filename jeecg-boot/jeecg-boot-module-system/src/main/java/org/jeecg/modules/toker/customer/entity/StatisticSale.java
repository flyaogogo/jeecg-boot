package org.jeecg.modules.toker.customer.entity;

import lombok.Data;
import org.apache.poi.hpsf.Decimal;

import java.io.Serializable;

/**
 * @Description: 销售看板信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Data
public class StatisticSale implements Serializable {

    private static final long serialVersionUID = 1L;

    // 新增意向
    private Integer newIntention ;

    // 总跟进数
    private Integer totalFlows ;

    // 店面接待
    private Integer reception ;

    // 邀约成功
    private Integer invite ;

    // 测量数
    private Integer measured ;

    // 方案数
    private Integer programme ;

    // 签单总数
    private Integer totalSignatures ;

    // 回款金额
    private Decimal collectionAmount ;

    // 平均单值
    private Decimal averageValue ;

}
