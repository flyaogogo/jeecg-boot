package org.jeecg.modules.toker.setting.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.setting.entity.TkSetCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 客户管理设置
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
public interface TkSetCustomerMapper extends BaseMapper<TkSetCustomer> {

    /**
     *  插入 客户设置数据
     * @param setCustomer
     * @return
     */
    @Insert("INSERT INTO tk_set_customer (no_period_start, no_period_end, is_warn, phone_is11, tenant_id) " +
            "VALUES ( #{noPeriodStart}, #{noPeriodEnd}, #{isWarn}, #{phoneIs11}, #{tenantId})")
    Boolean insertCustomerSet(TkSetCustomer setCustomer) ;
}
