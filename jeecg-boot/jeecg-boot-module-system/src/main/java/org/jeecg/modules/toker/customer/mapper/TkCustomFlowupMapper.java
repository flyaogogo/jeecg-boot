package org.jeecg.modules.toker.customer.mapper;

import java.util.List;
import org.jeecg.modules.toker.customer.entity.TkCustomFlowup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 客户跟进信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface TkCustomFlowupMapper extends BaseMapper<TkCustomFlowup> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<TkCustomFlowup> selectByMainId(@Param("mainId") String mainId);

}
