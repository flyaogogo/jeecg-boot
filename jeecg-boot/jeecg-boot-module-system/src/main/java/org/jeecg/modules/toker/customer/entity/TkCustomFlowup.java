package org.jeecg.modules.toker.customer.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.UnsupportedEncodingException;

/**
 * @Description: 客户跟进信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Data
@TableName("tk_custom_flowup")
@ApiModel(value="tk_custom_flowup对象", description="客户跟进信息")
public class TkCustomFlowup implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private String id;
	/**客户ID*/
    @ApiModelProperty(value = "客户ID")
    private String customerId;
	/**跟进方式*/
	@Excel(name = "跟进方式", width = 15)
    @Dict(dicCode = "tk_c_flowup_model")
    @ApiModelProperty(value = "跟进方式")
    private String flowupModel;
	/**跟进状态*/
	@Excel(name = "跟进状态", width = 15)
    @Dict(dicCode = "tk_c_flowup_type")
    @ApiModelProperty(value = "跟进状态")
    private String flowupType;
	/**语音*/
	@Excel(name = "语音", width = 15)
    @ApiModelProperty(value = "语音")
    private String voice;
	/**评论*/
	@Excel(name = "评论", width = 15)
    @ApiModelProperty(value = "评论")
    private String comments;

    /**回复数*/
    @Excel(name = "回复数", width = 15)
    @ApiModelProperty(value = "回复数")
    private Integer allReply;

	/**点赞数 ：记录点赞人员信息 */
	@Excel(name = "点赞数", width = 15)
    @ApiModelProperty(value = "点赞数")
    private Integer likes;

    /**是否点赞 , 默认为：false */
    @Excel(name = "是否点赞", width = 15)
    @ApiModelProperty(value = "是否点赞")
    private String isLike;

	/**下次跟进时间*/
	@Excel(name = "下次跟进时间", width = 15)
    @ApiModelProperty(value = "下次跟进时间")
    private String nextime;

	/**提醒方式*/
	@Excel(name = "提醒方式", width = 15)
    @ApiModelProperty(value = "提醒方式")
    private String reminderMode;

	/**邀约进店*/
	@Excel(name = "邀约进店", width = 15)
    @ApiModelProperty(value = "邀约进店")
    private String invite;
	/**图片*/
	@Excel(name = "图片", width = 15)
    @ApiModelProperty(value = "图片")
    private String pic;
	/**进度细节*/
	@Excel(name = "进度细节", width = 15)
    @ApiModelProperty(value = "进度细节")
    private String details;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**跟进人ID*/
    @Excel(name = "跟进人ID", width = 15)
    @ApiModelProperty(value = "跟进人ID")
    private String directorId;

    /**跟进人姓名*/
    @Excel(name = "跟进人姓名", width = 15)
    @ApiModelProperty(value = "跟进人姓名")
    private String director;

    /**跟进人头像*/
    @Excel(name = "跟进人头像", width = 15)
    @ApiModelProperty(value = "跟进人头像")
    private String directorAvatar;

}
