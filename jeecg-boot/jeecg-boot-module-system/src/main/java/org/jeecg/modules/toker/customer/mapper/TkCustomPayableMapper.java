package org.jeecg.modules.toker.customer.mapper;

import java.util.List;
import org.jeecg.modules.toker.customer.entity.TkCustomPayable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 客户收款管理
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface TkCustomPayableMapper extends BaseMapper<TkCustomPayable> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<TkCustomPayable> selectByMainId(@Param("mainId") String mainId);

}
