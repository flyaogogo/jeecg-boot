package org.jeecg.modules.toker.setting.service;

import org.jeecg.modules.toker.setting.entity.TkSetCustomerLevel;
import org.jeecg.modules.toker.setting.entity.TkSetCustomer;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.toker.setting.vo.TkSetCustomerPage;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 客户管理设置
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
public interface ITkSetCustomerService extends IService<TkSetCustomer> {

	/**
	 * 删除一对多
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);

	/**
	 * 通过租户ID查询 管理设置信息
	 *
	 * @param tenantId
	 * @return
	 */
	TkSetCustomer getByTenantId(String tenantId) ;

	/**
	 * 通过租户ID查询 管理设置及级别所有信息
	 *
	 * @param tenantId
	 * @return
	 */
	TkSetCustomerPage getMainByTenantId(String tenantId) ;
	/**
	 * 初始化租户下的客户设置
	 * @param tenantId
	 */
	Boolean initCustomerSetAll(String tenantId) ;
}
