package org.jeecg.modules.toker.customer.service.impl;

import org.jeecg.modules.toker.customer.entity.TkCustomOperation;
import org.jeecg.modules.toker.customer.mapper.TkCustomOperationMapper;
import org.jeecg.modules.toker.customer.service.ITkCustomOperationService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 客户操作信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Service
public class TkCustomOperationServiceImpl extends ServiceImpl<TkCustomOperationMapper, TkCustomOperation> implements ITkCustomOperationService {
	
	@Autowired
	private TkCustomOperationMapper tkCustomOperationMapper;
	
	@Override
	public List<TkCustomOperation> selectByMainId(String mainId) {
		return tkCustomOperationMapper.selectByMainId(mainId);
	}
}
