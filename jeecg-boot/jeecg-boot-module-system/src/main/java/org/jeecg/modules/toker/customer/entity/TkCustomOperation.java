package org.jeecg.modules.toker.customer.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.UnsupportedEncodingException;

/**
 * @Description: 客户操作信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Data
@TableName("tk_custom_operation")
@ApiModel(value="tk_custom_operation对象", description="客户操作信息")
public class TkCustomOperation implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private String id;
	/**客户ID*/
    @ApiModelProperty(value = "客户ID")
    private String customerId;
	/**操作方式*/
	@Excel(name = "操作方式", width = 15)
    @ApiModelProperty(value = "操作方式")
    private String model;
	/**操作记录*/
	@Excel(name = "操作记录", width = 15)
    @ApiModelProperty(value = "操作记录")
    private String record;
	/**操作时间*/
	@Excel(name = "操作时间", width = 15)
    @ApiModelProperty(value = "操作时间")
    private String optTime;
	/**操作人*/
	@Excel(name = "操作人", width = 15)
    @ApiModelProperty(value = "操作人")
    private String operator;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**跟进人ID*/
    @Excel(name = "跟进人ID", width = 15)
    @ApiModelProperty(value = "跟进人ID")
    private String directorId;

    /**跟进人姓名*/
    @Excel(name = "跟进人姓名", width = 15)
    @ApiModelProperty(value = "跟进人姓名")
    private String director;

    /**跟进人头像*/
    @Excel(name = "跟进人头像", width = 15)
    @ApiModelProperty(value = "跟进人头像")
    private String directorAvatar;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private String tenantId;
}
