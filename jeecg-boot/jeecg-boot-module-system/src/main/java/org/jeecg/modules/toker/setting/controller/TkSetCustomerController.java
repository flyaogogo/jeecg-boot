package org.jeecg.modules.toker.setting.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.toker.setting.vo.TkSetCustomerPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.toker.setting.entity.TkSetCustomerLevel;
import org.jeecg.modules.toker.setting.entity.TkSetCustomer;
import org.jeecg.modules.toker.setting.service.ITkSetCustomerService;
import org.jeecg.modules.toker.setting.service.ITkSetCustomerLevelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 客户管理设置
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
@Api(tags="客户管理设置")
@RestController
@RequestMapping("/setting/tkSetCustomer")
@Slf4j
public class TkSetCustomerController extends JeecgController<TkSetCustomer, ITkSetCustomerService> {

	@Autowired
	private ITkSetCustomerService tkSetCustomerService;

	@Autowired
	private ITkSetCustomerLevelService tkSetCustomerLevelService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param tkSetCustomer
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "客户管理设置-分页列表查询")
	@ApiOperation(value="客户管理设置-分页列表查询", notes="客户管理设置-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkSetCustomer tkSetCustomer,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkSetCustomer> queryWrapper = QueryGenerator.initQueryWrapper(tkSetCustomer, req.getParameterMap());
		Page<TkSetCustomer> page = new Page<TkSetCustomer>(pageNo, pageSize);
		IPage<TkSetCustomer> pageList = tkSetCustomerService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	/**
     *   添加
     * @param tkSetCustomer
     * @return
     */
    @AutoLog(value = "客户管理设置-添加")
    @ApiOperation(value="客户管理设置-添加", notes="客户管理设置-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody TkSetCustomer tkSetCustomer) {
        tkSetCustomerService.save(tkSetCustomer);
        return Result.OK("添加成功！");
    }

    /**
     *  编辑
     * @param tkSetCustomer
     * @return
     */
    @AutoLog(value = "客户管理设置-编辑")
    @ApiOperation(value="客户管理设置-编辑", notes="客户管理设置-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody TkSetCustomer tkSetCustomer) {
        tkSetCustomerService.updateById(tkSetCustomer);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "客户管理设置-通过id删除")
    @ApiOperation(value="客户管理设置-通过id删除", notes="客户管理设置-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        tkSetCustomerService.delMain(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "客户管理设置-批量删除")
    @ApiOperation(value="客户管理设置-批量删除", notes="客户管理设置-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.tkSetCustomerService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkSetCustomer tkSetCustomer) {
        return super.exportXls(request, tkSetCustomer, TkSetCustomer.class, "客户管理设置");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkSetCustomer.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/
	

    /*--------------------------------子表处理-客户级别设置-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "客户级别设置-通过主表ID查询")
	@ApiOperation(value="客户级别设置-通过主表ID查询", notes="客户级别设置-通过主表ID查询")
	@GetMapping(value = "/listTkSetCustomerLevelByMainId")
    public Result<?> listTkSetCustomerLevelByMainId(TkSetCustomerLevel tkSetCustomerLevel,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<TkSetCustomerLevel> queryWrapper = QueryGenerator.initQueryWrapper(tkSetCustomerLevel, req.getParameterMap());
        Page<TkSetCustomerLevel> page = new Page<TkSetCustomerLevel>(pageNo, pageSize);
        IPage<TkSetCustomerLevel> pageList = tkSetCustomerLevelService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

	/**
	 * 添加
	 * @param tkSetCustomerLevel
	 * @return
	 */
	@AutoLog(value = "客户级别设置-添加")
	@ApiOperation(value="客户级别设置-添加", notes="客户级别设置-添加")
	@PostMapping(value = "/addTkSetCustomerLevel")
	public Result<?> addTkSetCustomerLevel(@RequestBody TkSetCustomerLevel tkSetCustomerLevel) {
		tkSetCustomerLevelService.save(tkSetCustomerLevel);
		return Result.OK("添加成功！");
	}

    /**
	 * 编辑
	 * @param tkSetCustomerLevel
	 * @return
	 */
	@AutoLog(value = "客户级别设置-编辑")
	@ApiOperation(value="客户级别设置-编辑", notes="客户级别设置-编辑")
	@PutMapping(value = "/editTkSetCustomerLevel")
	public Result<?> editTkSetCustomerLevel(@RequestBody TkSetCustomerLevel tkSetCustomerLevel) {
		tkSetCustomerLevelService.updateById(tkSetCustomerLevel);
		return Result.OK("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "客户级别设置-通过id删除")
	@ApiOperation(value="客户级别设置-通过id删除", notes="客户级别设置-通过id删除")
	@DeleteMapping(value = "/deleteTkSetCustomerLevel")
	public Result<?> deleteTkSetCustomerLevel(@RequestParam(name="id",required=true) String id) {
		tkSetCustomerLevelService.removeById(id);
		return Result.OK("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "客户级别设置-批量删除")
	@ApiOperation(value="客户级别设置-批量删除", notes="客户级别设置-批量删除")
	@DeleteMapping(value = "/deleteBatchTkSetCustomerLevel")
	public Result<?> deleteBatchTkSetCustomerLevel(@RequestParam(name="ids",required=true) String ids) {
	    this.tkSetCustomerLevelService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportTkSetCustomerLevel")
    public ModelAndView exportTkSetCustomerLevel(HttpServletRequest request, TkSetCustomerLevel tkSetCustomerLevel) {
		 // Step.1 组装查询条件
		 QueryWrapper<TkSetCustomerLevel> queryWrapper = QueryGenerator.initQueryWrapper(tkSetCustomerLevel, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<TkSetCustomerLevel> pageList = tkSetCustomerLevelService.list(queryWrapper);
		 List<TkSetCustomerLevel> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "客户级别设置"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, TkSetCustomerLevel.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("客户级别设置报表", "导出人:" + sysUser.getRealname(), "客户级别设置"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importTkSetCustomerLevel/{mainId}")
    public Result<?> importTkSetCustomerLevel(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<TkSetCustomerLevel> list = ExcelImportUtil.importExcel(file.getInputStream(), TkSetCustomerLevel.class, params);
				 for (TkSetCustomerLevel temp : list) {
                    temp.setCustomerId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 tkSetCustomerLevelService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.OK("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-客户级别设置-end----------------------------------------------*/


	 /**
	  * 通过租户ID查询
	  * Luo.0022 2021-08-11
	  * @param tId
	  * @return
	  */
	 @AutoLog(value = "客户管理设置-通过租户ID查询")
	 @ApiOperation(value="客户管理设置-通过租户ID查询", notes="客户管理设置-通过租户ID查询")
	 @GetMapping(value = "/queryByTenantId")
	 public Result<?> queryByTenantId(@RequestParam(name="tId") String tId) {
		 TkSetCustomer tkSetCustomer = tkSetCustomerService.getByTenantId(tId);
		 if(tkSetCustomer==null) {
			 return Result.error("未找到对应数据");
		 }
		 return Result.OK(tkSetCustomer);

	 }

	 /**
	  * 通过租户ID查询 管理设置及级别所有信息
	  *  Luo.0022 2021-08-11
	  * @param tId
	  * @return
	  */
	 @AutoLog(value = "通过租户ID查询 管理设置及级别所有信息")
	 @ApiOperation(value="通过租户ID查询 管理设置及级别所有信息", notes="客户级别设置-通主表ID查询")
	 @GetMapping(value = "/getMainByTenantId")
	 public Result<?> getMainByTenantId(@RequestParam(name="tId", required = false) String tId ) {
		 TkSetCustomerPage tkSetCustomerPage = tkSetCustomerService.getMainByTenantId(tId);
		 if(tkSetCustomerPage==null) {
			 return Result.error("未找到对应数据,请初始化租户下的客户设置！");
		 }
		 return Result.OK(tkSetCustomerPage);
	 }

	 /**
	  * 初始化租户下的客户设置
	  *
	  * 通过租户ID查询
	  * Luo.0022 2021-08-11
	  * @param tId
	  * @return
	  */
	 @AutoLog(value = "客户管理设置-初始化租户下的客户设置")
	 @ApiOperation(value="客户管理设置-初始化租户下的客户设置", notes="客户管理设置-初始化租户下的客户设置")
	 @GetMapping(value = "/initCSet")
	 public Result<?> initCustomerSetAll(@RequestParam(name="tId") String tId) {
		 Boolean flag = tkSetCustomerService.initCustomerSetAll(tId) ;
		 return Result.OK(flag);
	 }

	 /**
	  * 通过级别设置ID查询
	  * Luo.0022 2021-08-13
	  * @param id
	  * @return
	  */
	 @AutoLog(value = "客户管理设置-通过级别设置ID查询")
	 @ApiOperation(value="客户管理设置-通过级别设置ID查询", notes="客户管理设置-通过级别设置ID查询")
	 @GetMapping(value = "/queryLevelSetById")
	 public Result<?> queryLevelSetById(@RequestParam(name="id", required = false) String id) {
		 TkSetCustomerLevel tkSetLevel = tkSetCustomerLevelService.getById(id);
		 if(tkSetLevel==null) {
			 return Result.error("未找到对应级别设置数据");
		 }
		 return Result.OK(tkSetLevel);

	 }
}
