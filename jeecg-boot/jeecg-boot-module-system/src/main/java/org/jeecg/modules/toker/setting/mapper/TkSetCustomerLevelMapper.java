package org.jeecg.modules.toker.setting.mapper;

import java.util.List;
import org.jeecg.modules.toker.setting.entity.TkSetCustomerLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 客户级别设置
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
public interface TkSetCustomerLevelMapper extends BaseMapper<TkSetCustomerLevel> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<TkSetCustomerLevel> selectByMainId(@Param("mainId") String mainId);

}
