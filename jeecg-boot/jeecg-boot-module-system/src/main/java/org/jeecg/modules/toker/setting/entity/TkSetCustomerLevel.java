package org.jeecg.modules.toker.setting.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.UnsupportedEncodingException;

/**
 * @Description: 客户级别设置
 * @Author: jeecg-boot
 * @Date:   2021-08-12
 * @Version: V1.0
 */
@Data
@TableName("tk_set_customer_level")
@ApiModel(value="tk_set_customer_level对象", description="客户级别设置")
public class TkSetCustomerLevel implements Serializable {
    private static final long serialVersionUID = 1L;

	/**客户级别id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "客户级别id")
    private String id;
	/**客户管理设置ID*/
    @ApiModelProperty(value = "客户管理设置ID")
    private String customerId;
	/**级别名称*/
	@Excel(name = "级别名称", width = 15)
    @ApiModelProperty(value = "级别名称")
    private String levelName;
	/**类型级别*/
	@Excel(name = "类型级别", width = 15)
    @Dict(dicCode = "tk_level_type")
    @ApiModelProperty(value = "类型级别")
    private String levelType;
	/**开启级别规则限制*/
	@Excel(name = "开启级别规则限制", width = 15)
    @ApiModelProperty(value = "开启级别规则限制")
    private String cenableLevelRule;
	/**规则类型*/
	@Excel(name = "规则类型", width = 15)
    @Dict(dicCode = "tk_c_rule_type")
    @ApiModelProperty(value = "规则类型")
    private String cruleType;
	/**客户销售动作*/
	@Excel(name = "客户销售动作", width = 15)
    @Dict(dicCode = "tk_c_sale_action")
    @ApiModelProperty(value = "客户销售动作")
    private String csaleAction;
	/**客户销售机会(%)*/
	@Excel(name = "客户销售机会(%)", width = 15)
    @Dict(dicCode = "tk_c_sale_probability")
    @ApiModelProperty(value = "客户销售机会(%)")
    private String csaleProbability;
	/**满足条件数*/
	@Excel(name = "满足条件数", width = 15)
    @ApiModelProperty(value = "满足条件数")
    private Integer cconditionTimes;
	/**是否自动执行*/
	@Excel(name = "是否自动执行", width = 15)
    @ApiModelProperty(value = "是否自动执行")
    private String cautoExec;
	/**最大跟进人数*/
	@Excel(name = "最大跟进人数", width = 15)
    @ApiModelProperty(value = "最大跟进人数")
    private Integer ofollowUpNum;
	/**公海周期*/
	@Excel(name = "公海周期", width = 15)
    @ApiModelProperty(value = "公海周期")
    private Integer ocycle;
	/**不签单进公海*/
	@Excel(name = "不签单进公海", width = 15)
    @ApiModelProperty(value = "不签单进公海")
    private Integer owithoutSigningSeas;
	/**进入公海次数*/
	@Excel(name = "进入公海次数", width = 15)
    @ApiModelProperty(value = "进入公海次数")
    private Integer ogoSeasTimes;
	/**是否只进入部门公海*/
	@Excel(name = "是否只进入部门公海", width = 15)
    @ApiModelProperty(value = "是否只进入部门公海")
    private String denableDepartSeas;
	/**部门公海周期*/
	@Excel(name = "部门公海周期", width = 15)
    @ApiModelProperty(value = "部门公海周期")
    private Integer dcycle;
	/**部门进入公海次数*/
	@Excel(name = "部门进入公海次数", width = 15)
    @ApiModelProperty(value = "部门进入公海次数")
    private Integer dcycleTimes;
	/**名词解释*/
	@Excel(name = "名词解释", width = 15)
    @ApiModelProperty(value = "名词解释")
    private String remark;
	/**开启回访提醒*/
	@Excel(name = "开启回访提醒", width = 15)
    @ApiModelProperty(value = "开启回访提醒")
    private String isVisitRemind;
	/**开启自动提醒*/
	@Excel(name = "开启自动提醒", width = 15)
    @ApiModelProperty(value = "开启自动提醒")
    private String isAutoRemind;
	/**交付客户访问间隔*/
	@Excel(name = "交付客户访问间隔", width = 15)
    @ApiModelProperty(value = "交付客户访问间隔")
    private Integer deliveryVisitInterval;
	/**跟进自动化（模板）*/
	@Excel(name = "跟进自动化（模板）", width = 15)
    @ApiModelProperty(value = "跟进自动化（模板）")
    private String autoTemplate;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
