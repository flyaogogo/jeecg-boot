package org.jeecg.modules.toker.setting.service.impl;

import org.jeecg.modules.toker.setting.entity.TkSetCustomerLevel;
import org.jeecg.modules.toker.setting.mapper.TkSetCustomerLevelMapper;
import org.jeecg.modules.toker.setting.service.ITkSetCustomerLevelService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 客户级别设置
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
@Service
public class TkSetCustomerLevelServiceImpl extends ServiceImpl<TkSetCustomerLevelMapper, TkSetCustomerLevel> implements ITkSetCustomerLevelService {
	
	@Autowired
	private TkSetCustomerLevelMapper tkSetCustomerLevelMapper;
	
	@Override
	public List<TkSetCustomerLevel> selectByMainId(String mainId) {
		return tkSetCustomerLevelMapper.selectByMainId(mainId);
	}
}
