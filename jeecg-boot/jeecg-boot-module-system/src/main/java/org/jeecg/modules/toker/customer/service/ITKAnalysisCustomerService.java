package org.jeecg.modules.toker.customer.service;

import org.jeecg.modules.toker.customer.entity.StatisticSale;
import org.jeecg.modules.toker.customer.entity.TkCustomers;

public interface ITKAnalysisCustomerService {

    /**
     * 根据当前登陆用户信息，对公司内部数据进行分析
     * @param customers
     * @return
     */
    public StatisticSale getStatisticSaleValue(TkCustomers customers) ;
}
