package org.jeecg.modules.toker.customer.service.impl;

import org.jeecg.modules.toker.customer.entity.TkCustomFlowup;
import org.jeecg.modules.toker.customer.mapper.TkCustomFlowupMapper;
import org.jeecg.modules.toker.customer.service.ITkCustomFlowupService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 客户跟进信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Service
public class TkCustomFlowupServiceImpl extends ServiceImpl<TkCustomFlowupMapper, TkCustomFlowup> implements ITkCustomFlowupService {
	
	@Autowired
	private TkCustomFlowupMapper tkCustomFlowupMapper;
	
	@Override
	public List<TkCustomFlowup> selectByMainId(String mainId) {
		return tkCustomFlowupMapper.selectByMainId(mainId);
	}
}
