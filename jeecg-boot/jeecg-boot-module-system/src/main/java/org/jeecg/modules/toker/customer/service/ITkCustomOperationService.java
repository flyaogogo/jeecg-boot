package org.jeecg.modules.toker.customer.service;

import org.jeecg.modules.toker.customer.entity.TkCustomOperation;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 客户操作信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
public interface ITkCustomOperationService extends IService<TkCustomOperation> {

	public List<TkCustomOperation> selectByMainId(String mainId);
}
