package org.jeecg.modules.toker.setting.vo;

import java.util.List;
import org.jeecg.modules.toker.setting.entity.TkSetCustomer;
import org.jeecg.modules.toker.setting.entity.TkSetCustomerLevel;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 客户管理设置
 * @Author: jeecg-boot
 * @Date:   2021-06-22
 * @Version: V1.0
 */
@Data
@ApiModel(value="tk_set_customerPage对象", description="客户管理设置")
public class TkSetCustomerPage {

	/**客户管理id*/
	@ApiModelProperty(value = "客户管理id")
    private java.lang.String id;

	/**类型级别*/
	/*
	@Excel(name = "类型级别", width = 15)
	@ApiModelProperty(value = "类型级别")
	private java.lang.String levelType;
	*/

	/**禁止跟进时间段设置开始*/
	@Excel(name = "禁止跟进时间段设置开始", width = 15)
	@ApiModelProperty(value = "禁止跟进时间段设置开始")
    private java.lang.Integer noPeriodStart;
	/**禁止跟进时间段设置结束*/
	@Excel(name = "禁止跟进时间段设置结束", width = 15)
	@ApiModelProperty(value = "禁止跟进时间段设置结束")
    private java.lang.Integer noPeriodEnd;
	/**跟进异常时是否提醒*/
	@Excel(name = "跟进异常时是否提醒", width = 15)
	@ApiModelProperty(value = "跟进异常时是否提醒")
    private java.lang.String isWarn;
	/**客户手机号要求11位*/
	@Excel(name = "客户手机号要求11位", width = 15)
	@ApiModelProperty(value = "客户手机号要求11位")
    private java.lang.String phoneIs11;
	/**设置描述*/
	@Excel(name = "设置描述", width = 15)
	@ApiModelProperty(value = "设置描述")
    private java.lang.String remark;
	/**租户Id*/
	@Excel(name = "租户Id", width = 15)
	@ApiModelProperty(value = "租户Id")
    private java.lang.String tenantId;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;

	@ExcelCollection(name="客户级别设置")
	@ApiModelProperty(value = "客户级别设置")
	private List<TkSetCustomerLevel> tkSetCustomerLevelList;

}
