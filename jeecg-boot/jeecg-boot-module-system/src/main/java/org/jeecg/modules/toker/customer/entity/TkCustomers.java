package org.jeecg.modules.toker.customer.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 客户信息
 * @Author: jeecg-boot
 * @Date:   2021-07-13
 * @Version: V1.0
 */
@Data
@TableName("tk_customers")
@ApiModel(value="tk_customers对象", description="客户信息")
public class TkCustomers implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private String id;
	/**业主姓名*/
    @Excel(name = "业主姓名", width = 15)
    @ApiModelProperty(value = "业主姓名")
    private String name;
	/**业主电话*/
    @Excel(name = "业主电话", width = 15)
    @ApiModelProperty(value = "业主电话")
    private String phone;
	/**备选电话*/
    @Excel(name = "备选电话", width = 15)
    @ApiModelProperty(value = "备选电话")
    private String telephone;
	/**标签*/
    @Excel(name = "标签", width = 15, dictTable = "tk_set_customer_tag", dicText = "tag_name", dicCode = "id")
    @Dict(dictTable = "tk_set_customer_tag", dicText = "tag_name", dicCode = "id")
    @ApiModelProperty(value = "标签")
    private String tags;
	/**客户户型*/
    @Excel(name = "客户户型", width = 15, dicCode = "tk_c_house_type")
    @Dict(dicCode = "tk_c_house_type")
    @ApiModelProperty(value = "客户户型")
    private String houseType;
	/**客户级别*/
    @Excel(name = "客户级别", width = 15, dicCode = "tk_level_type")
    @Dict(dicCode = "tk_level_type")
    @ApiModelProperty(value = "客户级别")
    private String level;
	/**客户来源*/
    @Excel(name = "客户来源", width = 15, dicCode = "tk_c_source")
    @Dict(dicCode = "tk_c_source")
    @ApiModelProperty(value = "客户来源")
    private String source;
	/**指数*/
    @Excel(name = "指数", width = 15)
    @ApiModelProperty(value = "指数")
    private Integer cindex;
	/**5 星评价*/
    @Excel(name = "5 星评价", width = 15)
    @ApiModelProperty(value = "5 星评价")
    private String fiveStar;
	/**客户阶段*/
    @Excel(name = "客户阶段", width = 15, dicCode = "tk_c_stage")
    @Dict(dicCode = "tk_c_stage")
    @ApiModelProperty(value = "客户阶段")
    private String stage;
	/**销售机会*/
    @Excel(name = "销售机会", width = 15)
    @ApiModelProperty(value = "销售机会")
    private String opportunities;
	/**客户负责人*/
    @Excel(name = "客户负责人", width = 15)
    @ApiModelProperty(value = "客户负责人")
    private String customerLeader;
	/**跟进人*/
    @Excel(name = "跟进人", width = 15)
    @ApiModelProperty(value = "跟进人")
    private String flowupPerson;
	/**共享给*/
    @Excel(name = "共享给", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @ApiModelProperty(value = "共享给")
    private String shareTo;
	/**进入公海次数*/
    @Excel(name = "进入公海次数", width = 15)
    @ApiModelProperty(value = "进入公海次数")
    private Integer seasTimes;
	/**进入公海天数*/
    @Excel(name = "进入公海天数", width = 15)
    @ApiModelProperty(value = "进入公海天数")
    private Integer seasDays;
	/**掉进公海时间*/
    @Excel(name = "掉进公海时间", width = 15)
    @ApiModelProperty(value = "掉进公海时间")
    private String intoSeaTime;
	/**客户地址*/
    @Excel(name = "客户地址", width = 15)
    @ApiModelProperty(value = "客户地址")
    private String address;
	/**头像地址*/
    @Excel(name = "头像地址", width = 15)
    @ApiModelProperty(value = "头像地址")
    private String headPic;
	/**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**自动跟进模板ID*/
    @Excel(name = "自动跟进模板ID", width = 15)
    @ApiModelProperty(value = "自动跟进模板ID")
    private String templateId;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
	/**业主地址Id*/
    @Excel(name = "业主地址Id", width = 15)
    @ApiModelProperty(value = "业主地址Id")
    private String tkaddrId;
	/**客户跟进人Id*/
    @Excel(name = "客户跟进人Id", width = 15)
    @ApiModelProperty(value = "客户跟进人Id")
    private String tkuserId;
	/**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private String tenantId;

    /**跟进方式*/
    @Excel(name = "跟进方式", width = 15)
    @ApiModelProperty(value = "跟进方式")
    private String flowupModel;
    /**跟进状态*/
    @Excel(name = "跟进状态", width = 15)
    @ApiModelProperty(value = "跟进状态")
    private String flowupType;

    /**进度细节*/
    @Excel(name = "进度细节", width = 15)
    @ApiModelProperty(value = "进度细节")
    private String latelyDetails;
    /**进度时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "进度时间")
    private Date latelyTime;

    /**合同金额*/
    @Excel(name = "合同金额", width = 15)
    @ApiModelProperty(value = "合同金额")
    private java.math.BigDecimal contractValue;
    /**收总额*/
    @Excel(name = "收总额", width = 15)
    @ApiModelProperty(value = "收总额")
    private java.math.BigDecimal totalReceived;

    /** 状态切换时间，如signing,delivery,highseas,losing */
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "状态切换时间")
    private Date stateTime;

    /**切换的上一个阶段*/
    @Excel(name = "跟进状态", width = 15)
    @ApiModelProperty(value = "上一个阶段")
    private String latestState;

    /**签单时间  为方便展示 签单的时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "签单时间")
    private Date signTime;

    /**交付时间  为方便展示 交付的时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "交付时间")
    private Date deliveryTime;
}
