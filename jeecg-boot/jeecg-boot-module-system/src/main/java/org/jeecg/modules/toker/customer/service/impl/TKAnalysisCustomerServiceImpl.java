package org.jeecg.modules.toker.customer.service.impl;

import org.jeecg.modules.toker.customer.entity.StatisticSale;
import org.jeecg.modules.toker.customer.entity.TkCustomers;
import org.jeecg.modules.toker.customer.service.ITKAnalysisCustomerService;
import org.springframework.stereotype.Service;

@Service
public class TKAnalysisCustomerServiceImpl implements ITKAnalysisCustomerService {
    @Override
    public StatisticSale getStatisticSaleValue(TkCustomers customers) {
        StatisticSale ss = new StatisticSale() ;

        ss.setInvite(3);
        ss.setMeasured(50);
        return ss;
    }
}
