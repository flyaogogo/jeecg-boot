package org.jeecg.modules.toker.customer.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.modules.toker.customer.entity.StatisticSale;
import org.jeecg.modules.toker.customer.entity.TkCustomers;
import org.jeecg.modules.toker.customer.service.ITKAnalysisCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 客户数据分析
 * @Author: Luo.0022
 * @Date:   2022-03-26
 * @Version: V1.0
 */
@Api(tags="客户数据分析")
@RestController
@RequestMapping("/customer/analysis")
@Slf4j
public class TKAnalysisCustomerController {

    @Autowired
    private ITKAnalysisCustomerService analysisCustomerService ;


    @AutoLog(value = "分析销售看板-数据统计")
    @ApiOperation(value="分析销售看板-数据统计", notes="分析销售看板-数据统计")
    @GetMapping(value = "/statisticSale")
    public Result<?> statisticCustomersSale(TkCustomers tkCustomers, HttpServletRequest req) {
        StatisticSale ss =  analysisCustomerService.getStatisticSaleValue(tkCustomers) ;
        return Result.OK(ss) ;
    }

}
