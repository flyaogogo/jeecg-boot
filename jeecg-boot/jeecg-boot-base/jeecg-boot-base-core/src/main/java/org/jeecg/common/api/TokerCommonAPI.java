package org.jeecg.common.api;

import org.jeecg.common.system.vo.WxUser;

public interface TokerCommonAPI {

    /**
     * 5根据用户账号查询用户信息
     * @param username
     * @return
     */
    WxUser getUserByName(String username);

    /**
     * 删除过期用户Token
     * @param userId
     * @return
     */
    boolean deleteUserToken(String userId) ;

    /**
     * 更新用户Token
     * @param token
     * @param userId
     * @return
     */
    boolean updateUserTokenByUid(String token , String userId) ;

}
