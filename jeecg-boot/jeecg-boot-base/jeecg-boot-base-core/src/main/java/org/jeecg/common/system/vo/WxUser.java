package org.jeecg.common.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 第三方登陆在线用户类
 *
 * @author Luo.0022
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxUser {
    /**用户id*/
    private java.lang.String id;
    /**UUID*/
    private java.lang.String uuid;
    /**用户账号*/
    private java.lang.String account;
    /**用户密码*/
    private java.lang.String pwd;
    /**真实姓名*/
    private java.lang.String realName;

    /**生日*/
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private java.util.Date birthday;
    /**用户类型*/
    private java.lang.String userType;
    /**手机号码*/
    private java.lang.String phone;
    /**性别*/
    private java.lang.Integer sex;
    /**状态*/
    private java.lang.Integer status;
    /**店内联系人*/
    private java.lang.String contact;
    /**身份证号码*/
    private java.lang.String cardId;
    /**详细地址*/
    private java.lang.String addres;
    /**用户备注*/
    private java.lang.String mark;
    /**合伙人id*/
    private java.lang.Integer partnerId;
    /**用户分组id*/
    private java.lang.String groupId;
    /**租户id*/
    private java.lang.String tenantId;
    /**标签id*/
    private java.lang.String tagId;
    /**用户昵称*/
    private java.lang.String nickname;
    /**用户头像*/
    private java.lang.String avatar;
    /**添加ip*/
    private java.lang.String addIp;
    /**最后一次登录ip*/
    private java.lang.String lastIp;
    /**用户余额*/
    private java.math.BigDecimal nowMoney;
    /**佣金金额*/
    private java.math.BigDecimal brokeragePrice;
    /**用户剩余积分*/
    private java.math.BigDecimal integral;
    /**用户剩余经验*/
    private java.lang.Integer experience;
    /**连续签到天数*/
    private java.lang.Integer signNum;
    /**等级*/
    private java.lang.Integer level;
    /**推广元id*/
    private java.lang.Integer spreadUid;
    /**推广员关联时间*/
    private java.util.Date spreadTime;
    /**是否为推广员*/
    private java.lang.Integer isPromoter;
    /**用户购买次数*/
    private java.lang.Integer payCount;
    /**下级人数*/
    private java.lang.Integer spreadCount;
    /**管理员编号 */
    private java.lang.Integer adminid;
    /**用户登陆类型，h5,wechat,routine*/
    private java.lang.String loginType;
    /**创建时间*/
    private java.util.Date createTime;
    /**更新时间*/
    private java.util.Date updateTime;
    /**最后一次登录时间*/
    private java.util.Date lastLoginTime;
    /**清除时间*/
    private java.util.Date cleanTime;
    /**推广等级记录*/
    private java.lang.String path;
    /**是否关注公众号*/
    private java.lang.Integer subscribe;
    /**关注公众号时间*/
    private java.util.Date subscribeTime;
    /**国家，中国CN，其他OTHER*/
    private java.lang.String country;
}
