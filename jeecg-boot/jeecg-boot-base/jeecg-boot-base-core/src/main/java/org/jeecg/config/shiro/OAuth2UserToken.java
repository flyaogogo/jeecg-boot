package org.jeecg.config.shiro;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;
import org.jeecg.common.system.vo.WxUser;

/**
 * 第三方授权登录凭证
 * 注意这里要实现AuthenticationToken，不能继承UsernamePasswordToken
 * 同时重写getPrincipal()和getCredentials()两个方法
 * @author Luo.0022
 */
@Data
public class OAuth2UserToken implements AuthenticationToken {

    /**
     *  授权类型
     *  这里可以使用枚举
     */
    private String type;

    // 第三方登录后获取的用户信息
    private WxUser user ;

    private String token;

    public OAuth2UserToken(String token) {
        this.token = token;
    }
    public OAuth2UserToken(final String type, final WxUser user) {
        this.type = type;
        this.user = user;
    }

    @Override
    public Object getPrincipal() {
        return token ;
    }

    @Override
    public Object getCredentials() {
        return token ;
    }


}
