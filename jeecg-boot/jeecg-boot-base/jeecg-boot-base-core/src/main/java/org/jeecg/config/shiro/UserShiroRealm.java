package org.jeecg.config.shiro;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.jeecg.common.api.TokerCommonAPI;
import org.jeecg.common.constant.ThreadLocalUtil;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.WxUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *  Shiro认证和授权
 *
 *  第三方登陆
 *
 *  @author Luo.0022
 *
 */
@Component
@Slf4j
public class UserShiroRealm extends AuthorizingRealm {

    public static final String REALM_NAME = "oauth2_user_realm";

    @Lazy
    @Resource
    private TokerCommonAPI commonAPI;

    @Lazy
    @Resource
    private RedisUtil redisUtil;

    /**
     * 检查是否支持该Realm
     * 一定要重写support()方法，在后面的身份验证器中会用到
     * @param token
     * @return
     */
    @Override
    public boolean supports(AuthenticationToken token) {

        return token instanceof OAuth2UserToken;
    }

    /**
     * 认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        //OAuth2UserToken token = (OAuth2UserToken)auth;
        //WxUser oAuth2User = token.getUser();
        //String token = (String) auth.getCredentials();
        log.debug("===============Shiro身份认证开始============doGetAuthenticationInfo==========");
        String token = (String) auth.getCredentials();
        if (token == null) {
            log.info("————————身份认证失败——————————IP地址:  "+ oConvertUtils.getIpAddrByRequest(SpringContextUtils.getHttpServletRequest()));
            throw new AuthenticationException("token为空!");
        }


        WxUser user = this.checkUserTokenIsEffect(token) ;
        String username = JwtUtil.getUsername(token);
        if (username == null) {
            throw new AuthenticationException("token非法无效!");
        }
        return new SimpleAuthenticationInfo(user, token, getName());
    }


    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        return null;
    }

    /**
     * 校验token的有效性
     *
     * @param token
     */
    public WxUser checkUserTokenIsEffect(String token) throws AuthenticationException {
        // 解密获得username，用于和数据库进行对比
        String username = JwtUtil.getUsername(token);
        if (username == null) {
            throw new AuthenticationException("token非法无效!");
        }

        // 查询用户信息
        //log.debug("———校验token是否有效————checkUserTokenIsEffect——————— ");
        log.debug("———校验token是否有效————checkUserTokenIsEffect——————— "+ token);
        WxUser loginUser = commonAPI.getUserByName(username) ;
        if (loginUser == null) {
            throw new AuthenticationException("用户不存在!");
        }
        // 判断用户状态
        if (loginUser.getStatus() != 1) {
            throw new AuthenticationException("账号已被锁定,请联系管理员!");
        }
        // 校验token是否超时失效 & 或者账号密码是否错误
        if (!tkrTokenRefresh(token, username, loginUser.getId())) {
            // 删除过期的用户Token
            //commonAPI.deleteUserToken(loginUser.getId()) ;
            throw new AuthenticationException("Token失效，请重新登录!");
        }

        return loginUser;
    }

    /**
     * JWTToken刷新生命周期 （实现： 用户在线操作不掉线功能）
     * 1、登录成功后将用户的JWT生成的Token作为k、v存储到cache缓存里面(这时候k、v值一样)，缓存有效期设置为Jwt有效时间的2倍
     * 2、当该用户再次请求时，通过JWTFilter层层校验之后会进入到doGetAuthenticationInfo进行身份验证
     * 3、当该用户这次请求jwt生成的token值已经超时，但该token对应cache中的k还是存在，则表示该用户一直在操作只是JWT的token失效了，程序会给token对应的k映射的v值重新生成JWTToken并覆盖v值，该缓存生命周期重新计算
     * 4、当该用户这次请求jwt在生成的token值已经超时，并在cache中不存在对应的k，则表示该用户账户空闲超时，返回用户信息已失效，请重新登录。
     * 注意： 前端请求Header中设置Authorization保持不变，校验有效性以缓存中的token为准。
     *       用户过期时间 = Jwt有效时间 * 2。
     *
     * @param userName
     * @param passWord
     * @return
     */
    public boolean tkrTokenRefresh(String token, String userName, String passWord) {
        String cacheToken = String.valueOf(redisUtil.get(TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX + token));
        if (oConvertUtils.isNotEmpty(cacheToken)) {
            // 校验token有效性
            if (!JwtUtil.verify(cacheToken, userName, passWord)) {
                //String newAuthorization = JwtUtil.sign(userName, passWord);
                // 设置超时时间
                String tokenKey = TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX + token ;
//                redisUtil.set(tokenKey, passWord);
//                redisUtil.expire(tokenKey, JwtUtil.EXPIRE_TIME *2 / 1000);

                redisUtil.set(tokenKey, passWord ,
                        TokerCommonConstant.TOKEN_EXPRESS_MINUTES, TimeUnit.MINUTES);

                //commonAPI.updateUserTokenByUid(newAuthorization, passWord ) ;// 更新数据Token

                //log.info("——————————用户在线操作，更新token保证不掉线————UserTokenRefresh——————— "+ token);

                Map<String, Object> hashedMap = new HashMap<>();
                hashedMap.put("id", passWord);
                ThreadLocalUtil.set(hashedMap);
            }
            return true;
        }
        return false;
    }
    private Boolean check(String token ){

        try {
            boolean exists = redisUtil.hasKey(TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX + token);
            if(exists){
                Object value = redisUtil.get(TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX + token);

                /*Map<String, Object> hashedMap = new HashMap<>();
                hashedMap.put("id", value);
                ThreadLocalUtil.set(hashedMap);*/

                redisUtil.set(TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX +token, value, TokerCommonConstant.TOKEN_EXPRESS_MINUTES);
            }else{
                //判断路由，部分路由不管用户是否登录/token过期都可以访问
                //exists = checkRouter(RequestUtil.getUri(request));
            }


            return exists;
        }catch (Exception e){
            return false;
        }
    }

    //路由在此处，则返回true，无论用户是否登录都可以访问
    private boolean checkRouter(String uri) {
        String[] routerList = {
                "api/front/product/detail",
                "api/front/coupons",
                "api/front/index",
                "api/front/bargain/list",
                "api/front/combination/list"
        };

        return ArrayUtils.contains(routerList, uri);
    }

    private String getTokenFormRequest(HttpServletRequest request){
        return request.getHeader(TokerCommonConstant.HEADER_AUTHORIZATION_KEY);
    }
}
