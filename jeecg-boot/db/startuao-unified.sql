
-- -------------------
-- 小程序端   客户管理设置
--   `level_type` varchar(10) NULL DEFAULT '' COMMENT '类型级别',
-- -------------------
DROP TABLE IF EXISTS `tk_set_customer`;
CREATE TABLE `tk_set_customer`  (
  `id` varchar(36) NOT NULL COMMENT '客户管理id',
  `no_period_start` int(20) NOT NULL DEFAULT '23' COMMENT '禁止跟进时间段设置开始',
  `no_period_end` int(20) NOT NULL DEFAULT '7' COMMENT '禁止跟进时间段设置结束',
  `is_warn` varchar(6) NULL DEFAULT 0 COMMENT '跟进异常时是否提醒(Y/N)',
  `phone_is11` varchar(16) NULL DEFAULT 0 COMMENT '客户手机号要求11位(Y/N)',
  `remark` varchar(100) NULL DEFAULT 0 COMMENT '设置描述',
  `tenant_id` varchar(36) NULL DEFAULT '0' COMMENT '租户Id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户管理设置' ROW_FORMAT = Dynamic ;

-- -------------------
-- 小程序端   客户级别设置
-- -------------------
DROP TABLE IF EXISTS `tk_set_customer_level`;
CREATE TABLE `tk_set_customer_level`  (
  `id` varchar(36) NOT NULL COMMENT '客户级别id',
  `customer_id` varchar(36) NOT NULL DEFAULT '' COMMENT '客户管理设置ID',
  `level_type` varchar(10) NULL DEFAULT '' COMMENT '类型级别',
  `cenable_level_rule` varchar(16) NULL DEFAULT 'Y' COMMENT '开启级别规则限制',
  `crule_type` varchar(20) NULL DEFAULT '0' COMMENT '规则类型',
  `csale_action` varchar(20) NULL DEFAULT '0' COMMENT '客户销售动作选择',
  `csale_probability` varchar(10) NULL DEFAULT '0' COMMENT '客户销售机会大于等于',
  `ccondition_times` int(11) NULL DEFAULT '0' COMMENT '满足条件数',
  `cauto_exec` varchar(6) NULL DEFAULT '0' COMMENT '是否自动执行',
  `ofollow_up_num` int(6) NULL DEFAULT '0' COMMENT '最大跟进人数',
  `ocycle` int(6) NULL DEFAULT '0' COMMENT '公海周期',
  `owithout_signing_seas` int(6) NULL DEFAULT '0' COMMENT '不签单进公海',
  `ogo_seas_times` int(6) NULL DEFAULT '0' COMMENT '进入公海次数',
  `denable_depart_seas` varchar(6) NULL DEFAULT '' COMMENT '是否只进入部门公海',
  `dcycle` int(11) NULL DEFAULT '0' COMMENT '部门公海周期',
  `dcycle_times` int(11) NULL DEFAULT '0' COMMENT '部门进入公海次数',

  `remark` varchar(120) NULL DEFAULT '0' COMMENT '名词解释',
  `is_visit_remind` varchar(6) NULL DEFAULT '0' COMMENT '开启回访提醒',
  `delivery_visit_interval` int(11) NULL DEFAULT '0' COMMENT '交付客户访问间隔',
  `is_auto_remind` varchar(6) NULL DEFAULT '0' COMMENT '开启自动提醒',

  `auto_template` varchar(200) NULL DEFAULT '' COMMENT '跟进自动化（模板）',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户级别设置' ROW_FORMAT = Dynamic ;

-- -------------------
-- 小程序端   客户标签设置
-- -------------------
DROP TABLE IF EXISTS `tk_set_customer_tag`;
CREATE TABLE `tk_set_customer_tag`  (
  `id` varchar(36) NOT NULL COMMENT '客户级别id',
  `tag_name` varchar(36) NOT NULL DEFAULT '' COMMENT '标签名称',
  `status` varchar(10) NOT NULL DEFAULT '1' COMMENT '状态,0:禁止,1:正常，默认正常',
  `remark` varchar(100) NULL DEFAULT '0' COMMENT '备注',
  `tenant_id` varchar(36) NULL DEFAULT '0' COMMENT '租户Id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户标签设置' ROW_FORMAT = Dynamic ;


-- -------------------
-- 小程序端   客户信息
-- -------------------
DROP TABLE IF EXISTS `tk_customers`;
CREATE TABLE `tk_customers`  (
  `id` varchar(36) NOT NULL COMMENT '用户id',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '业主姓名',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '业主电话',
  `telephone` varchar(64) NULL DEFAULT '' COMMENT '备选电话（可以Json添加多个）',
  `tags` varchar(1000) NULL DEFAULT '' COMMENT '标签',
  `house_type` varchar(64) NOT NULL DEFAULT '' COMMENT '客户户型（120平以内、120-180平、180平以上、别墅/自建、工程）',

  `level` varchar(36) NOT NULL DEFAULT '' COMMENT '客户级别（A/B/C/D类客户）',
  `source` varchar(36) NOT NULL DEFAULT '' COMMENT '客户来源（自然进店，小区开发，卖场拦截。。。）',

  `cindex` int(11) NULL COMMENT '指数',

  `head_pic` varchar(256) NULL COMMENT '头像地址',
  `five_star` varchar(10) NULL COMMENT '5 星评价',


  `stage` varchar(20) NULL COMMENT '客户阶段',
  `opportunities` varchar(16) NULL COMMENT '销售机会（与级别计算对应）',
  `customer_leader` varchar(36) NULL COMMENT '客户负责人',
  `flowup_person` varchar(36) NULL COMMENT '跟进人（公海前跟进人）',
  `share_to` varchar(200) NULL COMMENT '共享给',
  `seas_times` int(3) NULL COMMENT '进入公海次数',
  `seas_days` int(3) NULL COMMENT '进入公海天数',
  `into_sea_time` varchar(26) NULL COMMENT '掉进公海时间',
  `template_id` varchar(36) NULL COMMENT '自动跟进模板ID',
  `remark` varchar(256) NULL COMMENT '备注',
  `lately_details` varchar(300) NULL DEFAULT '' COMMENT '最新进度细节',
  `lately_time` timestamp(0) NULL COMMENT '最新跟进时间',

  `flowup_model` varchar(32) NULL DEFAULT '' COMMENT '跟进方式',
  `flowup_type` varchar(32) NULL DEFAULT '' COMMENT '跟进状态',

  `contract_value` decimal (16,2) NULL DEFAULT 0 COMMENT '合同金额',
  `total_received` decimal (16,2) NULL DEFAULT 0 COMMENT '收总额',

  `sign_time` timestamp(0) NULL COMMENT '签单时间',
  `delivery_time` timestamp(0) NULL COMMENT '交付时间',

  `state_time` timestamp(0) NULL COMMENT '切换阶段时间',
  `latest_state` varchar(36) NULL COMMENT '切换上一个阶段',

  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `address` varchar(256) NOT NULL DEFAULT '' COMMENT '客户地址',
  `tkaddr_id` varchar(36) NULL DEFAULT '' COMMENT '业主地址Id，存小区Id',
  `tkuser_id` varchar(36) NULL DEFAULT '' COMMENT '客户负责人Id',
  `tenant_id` varchar(64) NULL DEFAULT '' COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户信息' ROW_FORMAT = Dynamic;

-- -------------------
-- 小程序端   客户跟进信息
-- -------------------
DROP TABLE IF EXISTS `tk_custom_flowup`;
CREATE TABLE `tk_custom_flowup`  (
  `id` varchar(36) NOT NULL COMMENT '用户id',
  `customer_id` varchar(36) NOT NULL DEFAULT '' COMMENT '客户ID',
  `flowup_model` varchar(20) NULL DEFAULT '' COMMENT '跟进方式',
  `flowup_type` varchar(20) NULL DEFAULT '' COMMENT '跟进状态',
  `director_id` varchar(36) NULL DEFAULT '' COMMENT '跟进人ID',
  `director` varchar(25) NULL DEFAULT '' COMMENT '跟进人姓名',
  `director_avatar` varchar(255) NULL DEFAULT '' COMMENT '跟进人头像',
  `voice` varchar(64) NULL DEFAULT '' COMMENT '语音',
  `comments` varchar(1000) NULL DEFAULT '' COMMENT '评论',
  `all_reply` int(5) NULL DEFAULT '0' COMMENT '回复评论数',

  `likes` int(10) NULL DEFAULT 0 COMMENT '点赞数',
  `is_like` varchar(10) NULL DEFAULT 'false' COMMENT '是否点赞',
  `details` varchar(300) NULL DEFAULT '' COMMENT '进度细节',
  `pic` varchar(256) NULL DEFAULT '' COMMENT '图片',
  `nextime` varchar(30) NULL DEFAULT '' COMMENT '下次跟进时间',
  `reminder_mode` varchar(30) NULL DEFAULT '' COMMENT '提醒方式',
  `invite` varchar(64) NULL DEFAULT '' COMMENT '邀约进店',

  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户跟进信息' ROW_FORMAT = Dynamic;


-- -------------------
-- 小程序端   客户收款管理
-- -------------------
DROP TABLE IF EXISTS `tk_custom_payable`;
CREATE TABLE `tk_custom_payable`  (
  `id` varchar(36) NOT NULL COMMENT '用户id',
  `customer_id` varchar(36) NOT NULL DEFAULT '' COMMENT '客户ID',
  `type` varchar(64) NULL DEFAULT '' COMMENT '类型（收款、退款）',
  `project` varchar(64) NULL DEFAULT '' COMMENT '收款项目',
  `contract_value` decimal (16,2) NULL DEFAULT 0 COMMENT '合同金额',
  `total_received` decimal (16,2) NULL DEFAULT 0 COMMENT '收总额',
  `collection` decimal (16,2) NULL DEFAULT 0 COMMENT '本次收款',
  `settlement` varchar(20) NULL DEFAULT '' COMMENT '结算方式',
  `pic` varchar(256) NULL DEFAULT '' COMMENT '图片',
  `remark` varchar(256) NULL DEFAULT '' COMMENT '备注',

  `director_id` varchar(36) NULL DEFAULT '' COMMENT '跟进人ID',
  `director` varchar(25) NULL DEFAULT '' COMMENT '跟进人姓名',
  `director_avatar` varchar(255) NULL DEFAULT '' COMMENT '跟进人头像',

  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户收款信息' ROW_FORMAT = Dynamic;


-- -------------------
-- 小程序端   客户服务管理
-- -------------------
DROP TABLE IF EXISTS `tk_custom_service`;
CREATE TABLE `tk_custom_service`  (
  `id` varchar(36) NOT NULL COMMENT '用户id',
  `customer_id` varchar(36) NOT NULL DEFAULT '' COMMENT '客户ID',
  `type` varchar(64) NOT NULL DEFAULT '' COMMENT '服务单类型',
  `status` varchar(64) NULL DEFAULT '' COMMENT '服务单状态',
  `servicer` varchar(64) NOT NULL DEFAULT '' COMMENT '服务人',
  `service_time` varchar(64) NULL DEFAULT '' COMMENT '服务时间',
  `survey_sheet` varchar(64) NULL DEFAULT '' COMMENT '测量单',
  `pic` varchar(256) NULL DEFAULT '' COMMENT '图片',
  `remark` varchar(256) NULL DEFAULT '' COMMENT '备注',

  `director_id` varchar(36) NULL DEFAULT '' COMMENT '跟进人ID',
  `director` varchar(25) NULL DEFAULT '' COMMENT '跟进人姓名',
  `director_avatar` varchar(255) NULL DEFAULT '' COMMENT '跟进人头像',

  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户服务管理' ROW_FORMAT = Dynamic;


-- -------------------
-- 小程序端   客户操作信息
-- -------------------
DROP TABLE IF EXISTS `tk_custom_operation`;
CREATE TABLE `tk_custom_operation`  (
  `id` varchar(36) NOT NULL COMMENT '用户id',
  `customer_id` varchar(36) NOT NULL DEFAULT '' COMMENT '客户ID',
  `model` varchar(36) NOT NULL DEFAULT '' COMMENT '操作方式',
  `record` varchar(256) NOT NULL DEFAULT '' COMMENT '操作记录',
  `opt_time` varchar(25) NOT NULL DEFAULT '' COMMENT '操作时间',
  `operator` varchar(20) NULL DEFAULT '' COMMENT '操作人',
  `remark` varchar(256) NULL DEFAULT '' COMMENT '备注',

  `director_id` varchar(36) NULL DEFAULT '' COMMENT '跟进人ID',
  `director` varchar(25) NULL DEFAULT '' COMMENT '跟进人姓名',
  `director_avatar` varchar(255) NULL DEFAULT '' COMMENT '跟进人头像',

  `tenant_id` varchar(36) NULL DEFAULT '0' COMMENT '租户Id',

  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='客户操作信息' ROW_FORMAT = Dynamic;






