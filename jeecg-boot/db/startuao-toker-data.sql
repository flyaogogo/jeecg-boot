-- --------------------------------------------------------
-- 存放新创建表结构数据，以及修改的表结构等。
-- --------------------------------------------------------

-- ----------------------------
-- tk_system_config 微信配置
-- ----------------------------
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350437403664515073', 'sign_integral', '首次签到积分', NULL, '10', 'N', '2021-01-16 21:39:02', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350437616101818369', 'sign_experience', '首次签到经验值', NULL, '10', 'N', '2021-01-16 21:39:52', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350437616101818370', 'site_url', '域名地址', NULL, '', 'N', '2021-01-16 21:39:52', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350437782422749186', 'wechat_token', 'wechat Token', NULL, NULL, 'N', '2021-01-16 21:40:32', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350438095598845953', 'wechat_share_img', 'wechat_share_img', NULL, NULL, 'N', '2021-01-16 21:41:47', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350438149806030850', 'wechat_encode', 'wechat_encode', NULL, NULL, 'N', '2021-01-16 21:41:59', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350438365342924801', 'wechat_share_title', 'wechat_share_title', NULL, NULL, 'N', '2021-01-16 21:42:51', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350438431562596354', 'wechat_share_synopsis', 'wechat_share_synopsis', NULL, NULL, 'N', '2021-01-16 21:43:07', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350438528379715586', 'wechat_avatar', 'wechat_avatar头像', NULL, NULL, 'N', '2021-01-16 21:43:30', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350439567875047425', 'routine_appid', '微信小程序AppId', NULL, 'wxc73170bae7023b7d', 'N', '2021-01-16 21:47:38', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1350439744425885697', 'routine_appsecret', '微信小程序AppSecret', NULL, '244c33420ca86e519b33bf7cbfc3dadc', 'N', '2021-01-16 21:48:20', NULL);
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1365837374055739394', 'extract_time', '冻结时间', 0, '1', 'N', '2021-02-28 09:33:01', '2021-02-28 09:33:00');
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1365838690085728257', 'store_brokerage_ratio', '报备返佣金比例', 0, '1', 'N', '2021-02-28 09:38:14', '2021-02-28 09:38:14');
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1366039435615617025', 'report_success_money', '设置报备返佣金数', 0, '5', 'N', '2021-02-28 22:55:56', '2021-02-28 22:55:55');

INSERT INTO `tk_member_user_level` (`id`, `name`, `experience`, `is_show`, `grade`, `discount`, `image`, `icon`, `memo`, `is_del`, `create_time`, `update_time`)
VALUES
	(1,'普通会员',500,1,1,9.90,'image/content/2020/12/07/9c2a3160cbe04f36aa3340cc8f97dc67a9ai2hbt9f.png','image/content/2020/12/07/9c2a3160cbe04f36aa3340cc8f97dc67a9ai2hbt9f.png',NULL,0,'2020-12-15 15:03:12','2020-12-15 17:01:23'),
	(2,'黄铜会员',1000,1,2,9.80,'image/content/2020/12/15/1cf475d3fc0647d59f17ea724e2c8fe2wf6ohlvtzz.png','image/content/2020/12/15/1cf475d3fc0647d59f17ea724e2c8fe2wf6ohlvtzz.png',NULL,0,'2020-12-15 15:03:47','2020-12-15 17:01:25'),
	(3,'白银会员',1500,1,3,9.70,'image/content/2020/12/15/7e7b4599f6074a72a79dc27950b504e84z1qjcadfx.jpg','image/content/2020/12/15/7e7b4599f6074a72a79dc27950b504e84z1qjcadfx.jpg',NULL,0,'2020-12-15 15:04:19','2020-12-15 17:01:26'),
	(4,'黄金会员',2000,1,4,9.60,'image/user/2020/12/15/72b701aad808457695a49b472f7548ec54rrx6rl74.png','image/user/2020/12/15/72b701aad808457695a49b472f7548ec54rrx6rl74.png',NULL,0,'2020-12-15 15:05:09','2020-12-18 15:13:58'),
	(5,'钻石会员',2500,1,5,9.50,'image/user/2020/12/15/22f159f3c4f94173b1461dedcb993696i8wlul1mqj.jpg','image/user/2020/12/15/22f159f3c4f94173b1461dedcb993696i8wlul1mqj.jpg',NULL,0,'2020-12-15 15:05:41','2020-12-21 11:44:35');

-- ----------------------------
-- @date 2021-03-28 修改birthday类型为字符串
-- ----------------------------
ALTER  TABLE tk_user MODIFY birthday varchar(16);

-- ----------------------------
-- @date 2021-04-18 增加签单积分比率
-- ----------------------------
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1383453513430568961', 'integral_ratio', '积分抵用比例(1积分抵多少金额)', 0, '0.01', 'N', '2021-04-18 00:13:16', '2021-04-18 00:13:15');
INSERT INTO `tk_system_config`(`id`, `name`, `title`, `form_id`, `value`, `status`, `create_time`, `update_time`) VALUES ('1383453913974018049', 'order_give_integral', '下单支付金额按比例赠送积分（实际支付1元赠送多少积分)，默认为：1元送1积分', 0, '1', 'N', '2021-04-18 00:14:51', '2021-04-18 00:14:51');

ALTER  TABLE tk_user MODIFY contact varchar(36);




