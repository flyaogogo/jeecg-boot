DROP TABLE IF EXISTS `tk_user`;
CREATE TABLE `tk_user`  (
  `id` varchar(32) NOT NULL COMMENT '用户id',
  `uuid` varchar(64) NOT NULL DEFAULT '' COMMENT 'UUID',
  `account` varchar(32) NOT NULL DEFAULT '' COMMENT '用户账号',
  `pwd` varchar(32) NULL DEFAULT '' COMMENT '用户密码',
  `real_name` varchar(25) NULL DEFAULT '' COMMENT '真实姓名',
  `birthday` varchar(16) DEFAULT NULL COMMENT '生日',
  `user_type` varchar(32) NOT NULL DEFAULT '' COMMENT '用户类型',
  `phone` char(15) NULL DEFAULT NULL COMMENT '手机号码',
  `sex` tinyint(1) NULL DEFAULT 1 COMMENT '性别，0未知，1男，2女，3保密',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态,1为正常，0为禁止',
  `contact` varchar(20) NULL DEFAULT '' COMMENT '店内联系人',
  `card_id` varchar(20) NULL DEFAULT '' COMMENT '身份证号码',
  `addres` varchar(255) NULL DEFAULT '' COMMENT '详细地址',
  `mark` varchar(255) NULL DEFAULT '' COMMENT '用户备注',
  `partner_id` int(11) NULL DEFAULT NULL COMMENT '合伙人id',
  `group_id` varchar(255) NULL DEFAULT '' COMMENT '用户分组id',
  `tenant_id` varchar(64) NULL DEFAULT '' COMMENT '租户id',
  `tag_id` varchar(255) NULL DEFAULT '' COMMENT '标签id',
  `nickname` varchar(16) NULL DEFAULT '' COMMENT '用户昵称',
  `avatar` varchar(256) NULL DEFAULT '' COMMENT '用户头像',
  `add_ip` varchar(16) NULL DEFAULT '' COMMENT '添加ip',
  `last_ip` varchar(16) NULL DEFAULT '' COMMENT '最后一次登录ip',
  `now_money` decimal(16, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '用户余额',
  `brokerage_price` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '佣金金额',
  `integral` decimal(8, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '用户剩余积分',
  `experience` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '用户剩余经验',
  `sign_num` int(11) NULL DEFAULT 0 COMMENT '连续签到天数',
  `level` tinyint(2) UNSIGNED NULL DEFAULT 0 COMMENT '等级',
  `spread_uid` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '推广元id',
  `spread_time` timestamp(0) NULL DEFAULT NULL COMMENT '推广员关联时间',
  `is_promoter` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否为推广员',
  `pay_count` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '用户购买次数',
  `spread_count` int(11) NULL DEFAULT 0 COMMENT '下级人数',
  `adminid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '管理员编号 ',
  `login_type` varchar(36) NOT NULL DEFAULT '' COMMENT '用户登陆类型，h5,wechat,routine',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `last_login_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `clean_time` timestamp(0) NULL DEFAULT NULL COMMENT '清除时间',
  `path` varchar(255) NOT NULL DEFAULT '/0/' COMMENT '推广等级记录',
  `subscribe` tinyint(3) NULL DEFAULT 0 COMMENT '是否关注公众号',
  `subscribe_time` timestamp(0) NULL DEFAULT NULL COMMENT '关注公众号时间',
  `country` varchar(20) NULL DEFAULT 'CN' COMMENT '国家，中国CN，其他OTHER',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE,
  INDEX `spreaduid`(`spread_uid`) USING BTREE,
  INDEX `level`(`level`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `is_promoter`(`is_promoter`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `tk_info_report`;
CREATE TABLE `tk_info_report`  (
  `id` varchar(36) NOT NULL COMMENT '用户id',
  `owner_name` varchar(64) NOT NULL DEFAULT '' COMMENT '业主姓名',
  `owner_phone` varchar(64) NOT NULL DEFAULT '' COMMENT '业主电话',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '装修地址，未找到地址前，添加的是临时地址，审批通过后是装修住户ID',
  `progress` varchar(64) NOT NULL DEFAULT '' COMMENT '装修进度',
  `remark` varchar(255) NULL DEFAULT '' COMMENT '其他信息',
  `approval_status` tinyint(1) NULL DEFAULT 1 COMMENT '审批状态,1为未审批，2为拒绝，3为通过',
  `approval_note` varchar(255) NULL DEFAULT '' COMMENT '审批说明',
  `follow_up` varchar(16) NULL DEFAULT '' COMMENT '跟进情况',
  `ext_href` varchar(255) NULL DEFAULT '' COMMENT '酷家乐全景链接',
  `product_id` varchar(255) NULL DEFAULT '' COMMENT '所选产品Id',
  `address_status` varchar(15) NULL DEFAULT '' COMMENT '地址状态：1为新增的小区-addCell，2为选择小区-chooseCell，3为新增小区内屋室-addHouse，4为选择小区内屋室-chooseHouse',
  `address_temp` varchar(50) NULL DEFAULT '' COMMENT '临时地址，方便小程序展示',
  `create_by` varchar(32) NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(32) NULL DEFAULT NULL COMMENT '修改人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `tkaddr_id` varchar(36) NOT NULL DEFAULT '' COMMENT '业主地址Id，存小区Id',
  `tkuser_id` varchar(36) NOT NULL DEFAULT '' COMMENT '用户Id',
  `tenant_id` varchar(64) NULL DEFAULT '' COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='信息报备' ROW_FORMAT = Dynamic;

-- -------------------
-- 此表 生成一个注册添加时的临时表，当审批通过时，把相关数据存到相关地址表中。
-- -------------------
DROP TABLE IF EXISTS `tk_village_address`;
CREATE TABLE `tk_village_address`  (
  `id` varchar(36) NOT NULL COMMENT '地址id',
  `uuid` varchar(36) NULL COMMENT 'UUID',
  `real_estate` varchar(64) NOT NULL DEFAULT '' COMMENT '楼盘名称',
  `building_number` int(2) NULL DEFAULT 0 COMMENT '楼栋号',
  `unit` int(2) NULL DEFAULT 0 COMMENT '单元',
  `floor` int(4) NULL DEFAULT 0 COMMENT '楼层',
  `house_number` int(11) NULL DEFAULT 0 COMMENT '门牌号',
  `house_address` varchar(255)  NOT NULL DEFAULT '' COMMENT '房屋地址',
  `cell_address` varchar(255)  NOT NULL DEFAULT '' COMMENT '小区地址',
  `house_type` varchar(50) NULL DEFAULT '' COMMENT '户型面积类型',
  `region` varchar(100)  NULL DEFAULT '' COMMENT '小区地址',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='业主地址' ROW_FORMAT = Dynamic ;


-- -------------------
--  出售 产品 相关信息
-- -------------------
DROP TABLE IF EXISTS `tk_product`;

CREATE TABLE `tk_product` (
  `id` varchar(36) NOT NULL COMMENT '产品id',
  `image` varchar(256) NOT NULL COMMENT '产品图片',
  `slider_image` varchar(2000) NOT NULL COMMENT '轮播图',
  `product_name` varchar(128) NOT NULL COMMENT '产品名称',
  `product_model` varchar(128) NOT NULL COMMENT '产品型号',
  `product_info` varchar(256) NOT NULL COMMENT '产品简介',
  `keywords` varchar(256) NULL COMMENT '关键字',
  `cate_id` varchar(64) NOT NULL COMMENT '分类id',
  `description` text NOT NULL COMMENT '产品详情',
  `sales` mediumint(11) unsigned NULL DEFAULT '0' COMMENT '销量',
  `stock` mediumint(11) unsigned NULL DEFAULT '0' COMMENT '库存',
  `is_show` varchar(6) NOT NULL DEFAULT 'Y' COMMENT '状态（N：未上架，Y：上架）',
  `is_hot` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否热卖',
  `is_best` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否精品',
  `is_new` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否新品',
  `unit_name` varchar(32) NULL COMMENT '单位名',
  `browse` int(11) DEFAULT '0' COMMENT '浏览量',
  `code_path` varchar(64) NULL COMMENT '产品二维码地址(用户小程序海报)',
  `spec_type` tinyint(1) NULL DEFAULT '0' COMMENT '规格 0单 1多',
  `pro_rule_id` varchar(1000) NULL COMMENT '产品规格',
  `sort` smallint(11) NULL DEFAULT '0' COMMENT '排序',
  `tenant_id` varchar(36) NULL DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)',
  `update_by` varchar(32) NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(32) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `cate_id` (`cate_id`) USING BTREE,
  KEY `is_show` (`is_show`) USING BTREE,
  KEY `is_hot` (`is_hot`) USING BTREE,
  KEY `is_best` (`is_best`) USING BTREE,
  KEY `is_new` (`is_new`) USING BTREE,
  KEY `sort` (`sort`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=Dynamic COMMENT='产品表';


DROP TABLE IF EXISTS `tk_product_description`;

CREATE TABLE `tk_product_description` (
  `id` varchar(36) NOT NULL COMMENT '产品描述id',
  `product_id` varchar(36) NOT NULL COMMENT '产品ID',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '商品类型',
  `description` text NOT NULL COMMENT '产品详情',
  KEY `product_id` (`product_id`,`type`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=Dynamic COMMENT='产品描述表';


DROP TABLE IF EXISTS `tk_product_rule`;

CREATE TABLE `tk_product_rule` (
  `id` varchar(36) NOT NULL COMMENT '规格id',
  `product_id` varchar(36) NOT NULL COMMENT '产品ID',
  `rule_name` varchar(32) NOT NULL COMMENT '规格名称',
  `rule_attr` varchar(32) NULL COMMENT '规格属性',
  `rule_value` text NULL COMMENT '规格值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=Dynamic COMMENT='产品规格表';



DROP TABLE IF EXISTS `tk_system_config`;

CREATE TABLE `tk_system_config` (
  `id` varchar(36) NOT NULL COMMENT '配置id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '字段名称',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '字段提示文字',
  `form_id` int(10) DEFAULT '0' COMMENT '表单id',
  `value` varchar(5000) DEFAULT '' COMMENT '值',
  `status` varchar(4) DEFAULT 'N' COMMENT '是否隐藏',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status+name` (`name`),
  KEY `name` (`name`)
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=Dynamic COMMENT='配置表';


DROP TABLE IF EXISTS `tk_wechat_reply`;

CREATE TABLE `tk_wechat_reply` (
  `id` varchar(36) NOT NULL COMMENT '微信关键字回复id',
  `keywords` varchar(64) NOT NULL DEFAULT '' COMMENT '关键字',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT '回复类型',
  `data` text NOT NULL COMMENT '回复数据',
  `status` varchar(4) NULL DEFAULT 'Y' COMMENT '回复状态 N=不可用  Y =可用',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `key` (`keywords`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=Dynamic COMMENT='微信关键字回复表';

DROP TABLE IF EXISTS `tk_user_token`;

CREATE TABLE `tk_user_token` (
  `id` varchar(36) NOT NULL COMMENT '用户Token id',
  `uid` varchar(36) NOT NULL COMMENT '用户 id',
  `token` varchar(255) NOT NULL COMMENT 'token',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型，1 公众号， 2 小程序, 3 unionid',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `expires_time` datetime DEFAULT NULL COMMENT '到期时间',
  `login_ip` varchar(32) DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `type+token` (`type`,`token`)
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=Dynamic COMMENT='用户Token表';


-- ----------------------------
-- tk_user_sign 签到记录表
-- ----------------------------
DROP TABLE IF EXISTS `tk_user_sign`;

CREATE TABLE `tk_user_sign` (
  `id` varchar(36) NOT NULL COMMENT '签到记录ID',
  `uid` varchar(36) NOT NULL DEFAULT '0' COMMENT '用户uid',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '签到说明',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '获得',
  `balance` int(11) NOT NULL DEFAULT '0' COMMENT '剩余',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型，1积分，2经验',
  `create_day` date NOT NULL COMMENT '签到日期',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`,`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='签到记录表';

-- ----------------------------
-- tk_user_bill 用户账单表
-- ----------------------------
DROP TABLE IF EXISTS `tk_user_bill`;

CREATE TABLE `tk_user_bill` (
  `id` varchar(36) NOT NULL COMMENT '用户账单id',
  `uid` varchar(36) NOT NULL DEFAULT '0' COMMENT '用户uid',
  `link_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '关联id',
  `pm` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用途，0 = 支出 1 = 获得',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT '账单标题',
  `category` varchar(64) NOT NULL DEFAULT '' COMMENT '明细种类',
  `type` varchar(64) NOT NULL DEFAULT '' COMMENT '明细类型',
  `number` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '明细数字',
  `balance` decimal(16,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '剩余',
  `mark` varchar(512) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = 待确定 1 = 有效 -1 = 无效',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `openid` (`uid`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `add_time` (`create_time`) USING BTREE,
  KEY `pm` (`pm`) USING BTREE,
  KEY `type` (`category`,`type`,`link_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户账单表';

-- ----------------------------
-- tk_member_user_level 普通会员等级
-- ----------------------------
DROP TABLE IF EXISTS `tk_member_user_level`;

CREATE TABLE `tk_member_user_level` (
  `id` varchar(36) NOT NULL COMMENT '会员等级ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '会员名称',
  `experience` int(11) NOT NULL DEFAULT '0' COMMENT '达到多少升级经验',
  `is_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示 1=显示,0=隐藏',
  `grade` int(11) NOT NULL DEFAULT '0' COMMENT '会员等级',
  `discount` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '享受折扣',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '会员卡背景',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '会员图标',
  `memo` text COMMENT '说明',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除.1=删除,0=未删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='普通会员等级';


-- ----------------------------
-- tk_user_level 用户等级记录表
-- ----------------------------

DROP TABLE IF EXISTS `tk_user_level`;

CREATE TABLE `tk_user_level` (
  `id` varchar(36) NOT NULL COMMENT '用户级别ID',
  `uid` varchar(36) NOT NULL DEFAULT '0000'  COMMENT '用户uid',
  `level_id` varchar(36) NOT NULL DEFAULT '0' COMMENT '等级vip',
  `grade` int(11) NOT NULL DEFAULT '0' COMMENT '会员等级',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态,0:禁止,1:正常',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `remind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已通知',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除,0=未删除,1=删除',
  `discount` int(11) NOT NULL DEFAULT '0' COMMENT '享受折扣',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `expired_time` timestamp NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户等级记录表';

-- ----------------------------
-- tk_housing_estate 商品房楼盘记录表
-- create 2021/02/08
-- ----------------------------
DROP TABLE IF EXISTS `tk_housing_estate`;
CREATE TABLE `tk_housing_estate`  (
  `id` varchar(36) NOT NULL COMMENT '小区id',
  `real_estate` varchar(64) NOT NULL DEFAULT '' COMMENT '楼盘名称',
  `cell_address` varchar(255) NOT NULL DEFAULT '' COMMENT '楼盘地址',
  `cell_herf` varchar(255) NULL DEFAULT '' COMMENT '鸟瞰图',
  `status` tinyint(1) NULL DEFAULT '0' COMMENT '状态,1:入住；2:在建；3:预售',
  `is_show` varchar(6) NOT NULL DEFAULT 'Y' COMMENT '是否上架',
  `is_hot` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否热卖',
  `is_best` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否精品',
  `is_new` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否新品',
  `mark` varchar(255) DEFAULT '' COMMENT '备注',
  `region` varchar(100) DEFAULT '' COMMENT '区域',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='商品房楼盘' ROW_FORMAT = Dynamic ;

-- ----------------------------
-- tk_decorated_address 热装小区客户地址表
-- 此表增加外链接 、 是否购买产品状态 、及关联产品
-- 与前表有冲突， tk_village_address ，后面需要废弃
-- create 2021/02/15
-- ----------------------------
DROP TABLE IF EXISTS `tk_decorated_address`;
CREATE TABLE `tk_decorated_address`  (
  `id` varchar(36) NOT NULL COMMENT '地址id',
  `uuid` varchar(64) DEFAULT '' COMMENT 'UUID',
  `estate_id` varchar(36) NOT NULL DEFAULT '' COMMENT '小区ID',
  `building_number` varchar(5) NULL DEFAULT 0 COMMENT '楼栋号',
  `unit` varchar(4) NULL DEFAULT 0 COMMENT '单元',
  `floor` varchar(4) NULL DEFAULT 0 COMMENT '楼层',
  `house_number` varchar(10) NULL DEFAULT 0 COMMENT '门牌号',
  `house_address` varchar(255) NOT NULL DEFAULT '' COMMENT '房屋地址 - 多字段拼接',
  `cell_address` varchar(255) NOT NULL DEFAULT '' COMMENT '小区地址',
  `house_type` varchar(50) NULL DEFAULT '' COMMENT '户型面积类型',
  `ext_href` varchar(500) NULL DEFAULT '' COMMENT '酷家乐全景链接',
  `product_id` varchar(500) NULL DEFAULT '' COMMENT '所选产品Id',
  `status` tinyint(1) NULL DEFAULT '3' COMMENT '使用状态,1:已购买；2:报备中；3:拓展中',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='热装小区客户地址表' ROW_FORMAT = Dynamic ;

-- ----------------------------
-- tk_approval_address_temp 客户找不到小区时，主动添加信息
-- 与前表有冲突， tk_village_address ，后面需要废弃
-- create 2021/02/19
-- ----------------------------
/*DROP TABLE IF EXISTS `tk_approval_address_temp`;
CREATE TABLE `tk_approval_address_temp`  (
  `id` varchar(36) NOT NULL COMMENT '地址id',
  `real_estate` varchar(64) NOT NULL DEFAULT '' COMMENT '楼盘名称',
  `building_number` int(2) NULL DEFAULT 0 COMMENT '楼栋号',
  `unit` int(2) NULL DEFAULT 0 COMMENT '单元',
  `floor` int(4) NULL DEFAULT 0 COMMENT '楼层',
  `house_number` int(11) NULL DEFAULT 0 COMMENT '门牌号',
  `house_address` varchar(255) NOT NULL DEFAULT '' COMMENT '房屋地址',
  `cell_address` varchar(255) NOT NULL DEFAULT '' COMMENT '小区地址',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='业主地址' ROW_FORMAT = Dynamic ;
*/

-- ----------------------------
-- tk_user_brokerage_record 用户佣金记录表
-- create 2021/02/27
-- ----------------------------
DROP TABLE IF EXISTS `tk_user_brokerage_record`;

CREATE TABLE `tk_user_brokerage_record` (
  `id` varchar(36) NOT NULL COMMENT '记录id',
  `uid` varchar(36) NOT NULL DEFAULT '0' COMMENT '用户uid',
  `link_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '关联id（orderNo,提现id）',
  `link_type` varchar(32) NOT NULL DEFAULT '0' COMMENT '关联类型（order,extract，yue）',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类型：1-增加，2-扣减（提现）',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT '标题',
  `price` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `balance` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '剩余',
  `mark` varchar(512) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1-订单创建，2-冻结期，3-完成，4-失效（订单退款），5-提现申请',
  `frozen_time` int(3) NOT NULL DEFAULT '0' COMMENT '冻结期时间（天）',
  `thaw_time` decimal(20) NOT NULL DEFAULT '0' COMMENT '解冻时间',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `openid` (`uid`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `add_time` (`create_time`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `type_link` (`type`,`link_id`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户佣金记录表';

-- ----------------------------
-- tk_user_brokerage_record 用户提现表
-- create 2021/02/27
-- ----------------------------
DROP TABLE IF EXISTS `tk_user_extract`;

CREATE TABLE `tk_user_extract` (
  `id` varchar(36) NOT NULL COMMENT '提现id',
  `uid` varchar(36) NOT NULL DEFAULT '0' COMMENT '用户uid',
  `real_name` varchar(64) DEFAULT NULL COMMENT '名称',
  `extract_type` varchar(32) DEFAULT 'bank' COMMENT '提现类型：bank = 银行卡 alipay = 支付宝 weixin=微信',
  `bank_code` varchar(32) DEFAULT '0' COMMENT '银行卡',
  `bank_address` varchar(256) DEFAULT '' COMMENT '开户地址',
  `alipay_code` varchar(64) DEFAULT '' COMMENT '支付宝账号',
  `extract_price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '提现金额',
  `mark` varchar(512) DEFAULT NULL COMMENT '备注',
  `balance` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '剩余',
  `fail_msg` varchar(128) DEFAULT NULL COMMENT '无效原因',
  `status` tinyint(2) DEFAULT '0' COMMENT '提现状态：-1 未通过 0 审核中 1 已提现',
  `wechat` varchar(15) DEFAULT NULL COMMENT '微信号',
  `bank_name` varchar(512) DEFAULT NULL COMMENT '银行名称',
  `qrcode_url` varchar(512) DEFAULT NULL COMMENT '微信收款二维码',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `fail_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '失败时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `extract_type` (`extract_type`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `openid` (`uid`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户提现表';

-- ----------------------------
-- tk_user_address 用户地址表
-- create 2021/02/27
-- ----------------------------
DROP TABLE IF EXISTS `tk_user_address`;

CREATE TABLE `tk_user_address` (
  `id` varchar(36) NOT NULL COMMENT '用户地址id',
  `uid` varchar(36) NOT NULL DEFAULT '0' COMMENT '用户id',
  `real_name` varchar(32) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `phone` varchar(16) NOT NULL DEFAULT '' COMMENT '收货人电话',
  `province` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在省',
  `city` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在市',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `district` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在区',
  `detail` varchar(256) NOT NULL DEFAULT '' COMMENT '收货人详细地址',
  `post_code` int(10) NULL DEFAULT '0' COMMENT '邮编',
  `longitude` varchar(16) NULL DEFAULT '0' COMMENT '经度',
  `latitude` varchar(16) NULL DEFAULT '0' COMMENT '纬度',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `is_default` (`is_default`) USING BTREE,
  KEY `is_del` (`is_del`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户地址表';

DROP TABLE IF EXISTS `tk_system_city`;

CREATE TABLE `tk_system_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '省市级别',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `area_code` varchar(30) NOT NULL DEFAULT '' COMMENT '区号',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `merger_name` varchar(255) NOT NULL DEFAULT '' COMMENT '合并名称',
  `lng` varchar(50) NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(50) NOT NULL DEFAULT '' COMMENT '纬度',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否展示',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=DYNAMIC COMMENT='城市表';

-- ----------------------------
-- tk_store_product 积分或价格 商品表
-- create 2021/03/24
-- ----------------------------
DROP TABLE IF EXISTS `tk_store_product`;

CREATE TABLE `tk_store_product` (
  `id` varchar(36) NOT NULL COMMENT '商品id',
  `mer_id` varchar(36) NOT NULL DEFAULT '0' COMMENT '商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)',
  `store_name` varchar(128) NOT NULL COMMENT '商品名称',
  `store_info` varchar(256) NOT NULL COMMENT '商品简介',
  `keyword` varchar(256) NOT NULL COMMENT '关键字',
  `bar_code` varchar(15) DEFAULT '' COMMENT '商品条码（一维码）',
  `cate_id` varchar(64) NOT NULL COMMENT '分类id',
  `pricing_type` varchar(64) DEFAULT '1' COMMENT '计价类型，1-价格；2-积分',
  `price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '商品价格',
  `vip_price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '会员价格',
  `ot_price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '市场价',
  `cost` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '成本价',
  `postage` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '邮费',
  `sales` mediumint(11) unsigned NOT NULL DEFAULT '0' COMMENT '销量',
  `stock` mediumint(11) unsigned NOT NULL DEFAULT '0' COMMENT '库存',
  `ficti` mediumint(11) DEFAULT '100' COMMENT '虚拟销量',
  `cost_integral` mediumint(11) unsigned DEFAULT '0' COMMENT '消费积分',
  `give_integral` mediumint(11) unsigned DEFAULT '0' COMMENT '获得积分',
  `browse` int(11) DEFAULT '0' COMMENT '浏览量',
  `unit_name` varchar(32) NOT NULL COMMENT '单位名',
  `sort` smallint(11) DEFAULT '0' COMMENT '排序',
  `image` varchar(256) NOT NULL COMMENT '商品图片',
  `slider_image` varchar(2000) NOT NULL COMMENT '轮播图',
  `is_show` varchar(6) NOT NULL DEFAULT 'Y' COMMENT '状态（N：未上架，Y：上架）',
  `is_hot` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否热卖',
  `is_benefit` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否优惠',
  `is_best` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否精品',
  `is_new` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否新品',
  `is_good` varchar(6) NOT NULL DEFAULT 'N' COMMENT '是否优品推荐',
  `is_postage` varchar(6) DEFAULT 'Y' COMMENT '是否包邮',
  `is_del` varchar(6) DEFAULT 'N' COMMENT '是否删除',
  `is_seckill` tinyint(1) unsigned DEFAULT '0' COMMENT '秒杀状态 0 未开启 1已开启',
  `is_bargain` tinyint(1) unsigned DEFAULT NULL COMMENT '砍价状态 0未开启 1开启',
  `is_sub` varchar(6) DEFAULT 'N' COMMENT '是否单独分佣',
  `code_path` varchar(64) DEFAULT '' COMMENT '商品二维码地址(用户小程序海报)',
  `soure_link` varchar(255) DEFAULT '' COMMENT '淘宝京东1688类型',
  `video_link` varchar(200) DEFAULT '' COMMENT '主图视频链接',
  `temp_id` varchar(64)  DEFAULT '1' COMMENT '运费模板ID',
  `spec_type` tinyint(1) DEFAULT '0' COMMENT '规格 0单 1多',
  `pro_rule_id` varchar(36) DEFAULT NULL COMMENT '产品规格ID',
  `pro_spec` text DEFAULT NULL COMMENT '产品规格',
  `activity` varchar(255) DEFAULT '' COMMENT '活动显示排序0=默认, 1=秒杀，2=砍价，3=拼团',
  `description` text DEFAULT NULL COMMENT '产品详情',
  `tenant_id` varchar(36) DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)',
  `update_by` varchar(32) DEFAULT NULL COMMENT '更新人',
  `update_time` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `cate_id` (`cate_id`) USING BTREE,
  KEY `is_hot` (`is_hot`) USING BTREE,
  KEY `is_benefit` (`is_benefit`) USING BTREE,
  KEY `is_best` (`is_best`) USING BTREE,
  KEY `is_new` (`is_new`) USING BTREE,
  KEY `toggle_on_sale, is_del` (`is_del`) USING BTREE,
  KEY `price` (`price`) USING BTREE,
  KEY `is_show` (`is_show`) USING BTREE,
  KEY `sort` (`sort`) USING BTREE,
  KEY `sales` (`sales`) USING BTREE,
  KEY `is_postage` (`is_postage`) USING BTREE
) ENGINE=InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT=COMPACT COMMENT='商品表';


-- -------------------
-- 导购跟进表并记录表。通过导购ID及客户装修地址ID，列出跟进进度
-- 此表为在审批通过后，数据在此跟进
-- create 2021/04/12

-- -------------------
DROP TABLE IF EXISTS `tk_flowup_custom`;
CREATE TABLE `tk_followup_custom`  (
  `id` varchar(36) NOT NULL COMMENT '地址id',
  `report_id` varchar(36) NULL COMMENT '报备ID',
  `user_id` varchar(36) NULL COMMENT '报备用户ID',
  `guide_id` varchar(36) NULL COMMENT '导购ID',
  `address_id` varchar(64) NULL COMMENT '装修地址ID',
  `product_id` varchar(255) NULL COMMENT '所选产品Id',
  `follow_up` varchar(16) NULL COMMENT '跟进情况',
  `total_price` decimal(16,2) NULL DEFAULT '0.00' COMMENT '签单总价',
  `mark` varchar(512) NULL DEFAULT '' COMMENT '跟进说明',
  `tenant_id` varchar(36) DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT='导购跟进' ROW_FORMAT = Dynamic ;

-- ----------------------------
-- @date 2021-04-12 增加两个字段
-- guide_id ：导购ID
-- total_price ： 表示签单成功后的总价
-- ----------------------------
alter table tk_info_report add column total_price decimal(16,2) NULL DEFAULT '0.00' comment '签单总价' AFTER product_id;
alter table tk_info_report add column guide_id varchar(36) NULL DEFAULT '' comment '导购ID' AFTER tkuser_id;

-- ----------------------------
-- @date 2021-05-12 对前期未加租户字段表增加 tenant_id
-- tenant_id ：租户ID
-- ----------------------------
alter table tk_village_address add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER region;
alter table tk_system_config add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER status;
alter table tk_user_token add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID';
alter table tk_user_sign add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' ;
alter table tk_user_bill add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER status;
alter table tk_user_brokerage_record add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER status;
alter table tk_member_user_level add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER is_del;
alter table tk_user_level add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER discount;
alter table tk_housing_estate add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER region;
alter table tk_decorated_address add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER mark;
alter table tk_user_extract add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER qrcode_url;
alter table tk_user_address add column tenant_id varchar(36) NULL DEFAULT '0' comment '租户ID' AFTER is_del;



















