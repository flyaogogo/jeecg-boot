package org.jeecg.modules.toker.tkuser.service;

import org.jeecg.modules.toker.tkuser.entity.TkUserLevel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 用户等级记录表
 * @Author: Luo.0022
 * @Date:   2021-01-18
 * @Version: V1.0
 */
public interface ITkUserLevelService extends IService<TkUserLevel> {

    /**
     * 修改用户等级
     * @param userId String id
     * @param levelId String 等级
     * @return Boolean
     */
    boolean level(String userId, String levelId) ;
}
