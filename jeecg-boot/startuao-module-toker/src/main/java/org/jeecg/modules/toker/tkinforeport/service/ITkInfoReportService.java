package org.jeecg.modules.toker.tkinforeport.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 拓客用户报备信息
 * @Author: Luo.0022
 * @Date:   2021-01-02
 * @Version: V1.0
 */
public interface ITkInfoReportService extends IService<TkInfoReport> {

    /**
     * 通过用户ID，列出所有的报备信息
     * @param userId
     * @return
     */
    List<TkInfoReport> getMyReportListByUserId(String userId) ;

    /**
     * 审批用户报备信息
     * @param tkInfoReport
     */
    Boolean approvalReportInfoByUid(TkInfoReport tkInfoReport) ;

    /**
     * 导购跟进信息列表
     * @param tkInfoReport
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    IPage<TkInfoReport> guideFollowupCustomList(TkInfoReport tkInfoReport,Integer pageNo,
                                                Integer pageSize, HttpServletRequest req) ;

    /**
     * 添加一对多
     *
     */
    void saveMain(TkInfoReport tkInfoReport,List<TkFollowupCustom> tkFollowupCustomList) ;

    /**
     * 修改一对多
     *
     */
    void updateMain(TkInfoReport tkInfoReport,List<TkFollowupCustom> tkFollowupCustomList);

    /**
     * 删除一对多
     */
    void delMain (String id);

    /**
     * 批量删除一对多
     */
    void delBatchMain (Collection<? extends Serializable> idList);
}
