package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 用户账单表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
@Data
@TableName("tk_user_bill")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_user_bill对象", description="用户账单表")
public class TkUserBill implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户账单id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户账单id")
    private String id;
	/**用户uid*/
	@Excel(name = "用户uid", width = 15)
    @ApiModelProperty(value = "用户uid")
    private String uid;
	/**关联id*/
	@Excel(name = "关联id", width = 15)
    @ApiModelProperty(value = "关联id")
    private String linkId;
	/**用途*/
	@Excel(name = "用途", width = 15)
    @ApiModelProperty(value = "用途")
    private Integer pm;
	/**账单标题*/
	@Excel(name = "账单标题", width = 15)
    @ApiModelProperty(value = "账单标题")
    private String title;
	/**明细种类*/
	@Excel(name = "明细种类", width = 15)
    @ApiModelProperty(value = "明细种类")
    private String category;
	/**明细类型*/
	@Excel(name = "明细类型", width = 15)
    @ApiModelProperty(value = "明细类型")
    private String type;
	/**明细数字*/
	@Excel(name = "明细数字", width = 15)
    @ApiModelProperty(value = "明细数字")
    private BigDecimal number;
	/**剩余*/
	@Excel(name = "剩余", width = 15)
    @ApiModelProperty(value = "剩余")
    private BigDecimal balance;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String mark;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "tk_bill_status")
	@Dict(dicCode = "tk_bill_status")
    @ApiModelProperty(value = "状态")
    private Integer status;
	/**添加时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "添加时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;
}
