package org.jeecg.modules.toker.front.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.HotHouseInfoRequest;
import org.jeecg.modules.toker.front.response.HotBuildResponse;
import org.jeecg.modules.toker.front.service.HotBuildingService;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import org.jeecg.modules.toker.tkestate.service.ITkHousingEstateService;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController("FrontHotBuildingController")
@RequestMapping("api/front/estate")
@Api(tags = "楼盘 -- 热销楼盘")
public class HotBuildingController {

    @Autowired
    private HotBuildingService hotBuildingService ;

    @Autowired
    private ITkHousingEstateService tkHousingEstateService;

    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<?> queryPageList(TkHousingEstate tkHousingEstate, @Validated PageParamRequest pageParamRequest ,
                                   HttpServletRequest req) {
        //QueryWrapper<TkHousingEstate> queryWrapper = QueryGenerator.initQueryWrapper(tkHousingEstate, req.getParameterMap());
        LambdaQueryWrapper<TkHousingEstate> queryWrapper = Wrappers.lambdaQuery();
        if(StringUtils.isNotBlank(tkHousingEstate.getRealEstate())){
            queryWrapper.like(TkHousingEstate::getRealEstate,tkHousingEstate.getRealEstate());
        }
        Page<TkHousingEstate> page = new Page<TkHousingEstate>(pageParamRequest.getPage(), pageParamRequest.getLimit());
        IPage<TkHousingEstate> pageList = tkHousingEstateService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     *  通过小区Id获取热装小区住户
     *  通过幢数进行分组
     * @param hotHouseInfoRequest  热装小区参数
     * @return
     */
    @ApiOperation(value = "热装小区住户列表")
    @RequestMapping(value = "/house", method = RequestMethod.GET)
    public Result<?> getUserHouseByCellId( @Validated HotHouseInfoRequest hotHouseInfoRequest) {

        Map<String, List<TkDecoratedAddress>> map = hotBuildingService.getUserHouseByCellId(hotHouseInfoRequest.getEstateId()) ;

        String mapKey ;
        List<TkDecoratedAddress> mapValue ;
        Map<String,Object> mapJson ;
        List<Map<String,Object>> hotHouseJsonList = new ArrayList<>();
        for (Map.Entry<String, List<TkDecoratedAddress>> entry : map.entrySet()) {
            mapKey = entry.getKey();
            mapValue = entry.getValue();
            mapJson = new HashMap<>() ;
            mapJson.put("name",mapKey) ;
            mapJson.put("houses",mapValue) ;
            mapJson.put("count",mapValue.size()) ;
            hotHouseJsonList.add(mapJson) ;
        }

        return Result.OK(hotHouseJsonList) ;
    }

    /**
     *  通过产品Id获取热装小区住户产品信息
     *  住户选购产品列表
     * @param proIds  热装小区参数
     * @return
     */
    @ApiOperation(value = "住户选购产品列表")
    @RequestMapping(value = "/house/products", method = RequestMethod.GET)
    public Result<?> getProductsByProId( @Validated String proIds) {

        if(StringUtils.isNotBlank(proIds)){

        }
        List<TkProduct> proList = hotBuildingService.getProductsByIds(proIds) ;

        return Result.OK(proList) ;
    }

}
