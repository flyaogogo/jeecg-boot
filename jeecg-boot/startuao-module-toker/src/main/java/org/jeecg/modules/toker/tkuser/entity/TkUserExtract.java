package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 用户提现表
 * @Author: Luo.0022
 * @Date:   2021-02-27
 * @Version: V1.0
 */
@Data
@TableName("tk_user_extract")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_user_extract对象", description="用户提现表")
public class TkUserExtract implements Serializable {
    private static final long serialVersionUID = 1L;

	/**提现id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "提现id")
    private String id;
	/**用户uid*/
	@Excel(name = "用户uid", width = 15)
    @ApiModelProperty(value = "用户uid")
    private String uid;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private String realName;
	/**提现类型*/
	@Excel(name = "提现类型", width = 15)
    @ApiModelProperty(value = "提现类型")
    private String extractType;
	/**银行卡*/
	@Excel(name = "银行卡", width = 15)
    @ApiModelProperty(value = "银行卡")
    private String bankCode;
	/**开户地址*/
	@Excel(name = "开户地址", width = 15)
    @ApiModelProperty(value = "开户地址")
    private String bankAddress;
	/**支付宝账号*/
	@Excel(name = "支付宝账号", width = 15)
    @ApiModelProperty(value = "支付宝账号")
    private String alipayCode;
	/**提现金额*/
	@Excel(name = "提现金额", width = 15)
    @ApiModelProperty(value = "提现金额")
    private BigDecimal extractPrice;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String mark;
	/**剩余*/
	@Excel(name = "剩余", width = 15)
    @ApiModelProperty(value = "剩余")
    private BigDecimal balance;
	/**无效原因*/
	@Excel(name = "无效原因", width = 15)
    @ApiModelProperty(value = "无效原因")
    private String failMsg;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "tk_extract_status")
	@Dict(dicCode = "tk_extract_status")
    @ApiModelProperty(value = "状态")
    private Integer status;
	/**微信号*/
	@Excel(name = "微信号", width = 15)
    @ApiModelProperty(value = "微信号")
    private String wechat;
	/**银行名称*/
	@Excel(name = "银行名称", width = 15)
    @ApiModelProperty(value = "银行名称")
    private String bankName;
	/**微信收款二维码*/
	@Excel(name = "微信收款二维码", width = 15)
    @ApiModelProperty(value = "微信收款二维码")
    private String qrcodeUrl;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
	/**失败时间*/
	@Excel(name = "失败时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "失败时间")
    private Date failTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;

}
