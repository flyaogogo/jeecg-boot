package org.jeecg.modules.toker.tkuser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkuser.entity.TkUserSign;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 签到记录表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
public interface TkUserSignMapper extends BaseMapper<TkUserSign> {

}
