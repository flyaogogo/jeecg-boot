package org.jeecg.modules.toker.tkuser.service.impl;

import org.jeecg.modules.toker.tkuser.entity.TkUserExtract;
import org.jeecg.modules.toker.tkuser.mapper.TkUserExtractMapper;
import org.jeecg.modules.toker.tkuser.service.ITkUserExtractService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 用户提现表
 * @Author: Luo.0022
 * @Date:   2021-02-27
 * @Version: V1.0
 */
@Service
public class TkUserExtractServiceImpl extends ServiceImpl<TkUserExtractMapper, TkUserExtract> implements ITkUserExtractService {

}
