package org.jeecg.modules.toker.sysconfig.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.toker.sysconfig.entity.TkSystemConfig;
import org.jeecg.modules.toker.sysconfig.mapper.TkSystemConfigMapper;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 系统参数配置
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
@Service
public class TkSystemConfigServiceImpl extends ServiceImpl<TkSystemConfigMapper, TkSystemConfig> implements ITkSystemConfigService {

    @Autowired
    private TkSystemConfigMapper systemConfigMapper ;

    @Autowired
    private RedisUtil redisUtil;

    private static final String redisKey = TokerCommonConstant.CONFIG_LIST;

    @Value("${server.asyncConfig}")
    private Boolean asyncConfig;


    /**
     * 根据menu name 获取 value
     * @param name menu name
     * @author Luo.0022
     * @since 2021-01-15
     * @return String
     */
    @Override
    public String getValueByKey(String name) {
        return get(name);
    }


    /**
     * 同时获取多个配置
     * @param keys 多个配置key
     * @return List<String>
     *
     * @author Luo.0022
     * @since 2021-01-15
     */
    @Override
    public List<String> getValuesByKes(List<String> keys) {
        List<String> result = new ArrayList<>();
        for (String key : keys) {
            result.add(getValueByKey(key));
        }
        return result;
    }

    /**
     * 根据 name 获取 value 找不到抛异常
     * @param name menu name
     * @author Luo.0022
     * @since 2021-01-15
     * @return String
     */
    @Override
    public String getValueByKeyException(String name) {
        String value = get(name);
        if(null == value){
            throw new JeecgBootException("没有找到"+ name +"数据");
        }

        return value;
    }
    /**
     * 把数据同步到redis
     * @param name String
     * @author Luo.0022
     * @since 2021-01-15
     * @return String
     */
    private String get(String name){
        if (!asyncConfig) {
            //如果配置没有开启
            LambdaQueryWrapper<TkSystemConfig> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(TkSystemConfig::getStatus, false).eq(TkSystemConfig::getName, name);
            TkSystemConfig systemConfig = systemConfigMapper.selectOne(lambdaQueryWrapper);
            if(null == systemConfig || StringUtils.isBlank(systemConfig.getValue())){
                return null;
            }

            return systemConfig.getValue();

        }
        setRedisByVoList();
        Object data = redisUtil.hget(redisKey, name);
        if(null == data || StringUtils.isBlank(data.toString())){
            //没有找到数据
            return null;
        }

        //去数据库查找，然后写入redis
        return data.toString();
    }
    /**
     * 把数据同步到redis, 此方法适用于redis为空的时候进行一次批量输入
     * @author Luo.0022
     * @since 2021-01-15
     */
    private void setRedisByVoList(){
        //检测redis是否为空
        Long size = redisUtil.sGetSetSize(redisKey);
        if(size > 0 || !asyncConfig){
            return;
        }

        LambdaQueryWrapper<TkSystemConfig> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkSystemConfig::getStatus, false);
        List<TkSystemConfig> systemConfigList = systemConfigMapper.selectList(lambdaQueryWrapper);
        async(systemConfigList);
    }
    /**
     * 把数据同步到redis
     * @param systemConfigList List<SystemConfig> 需要同步的数据
     * @author Luo.0022
     * @since 2021-01-15
     */
    private void async(List<TkSystemConfig> systemConfigList){
        if (!asyncConfig) {
            //如果配置没有开启
            return;
        }

        for (TkSystemConfig systemConfig : systemConfigList) {
//            if(systemConfig.getStatus()){
//                //隐藏之后，删除redis的数据
//                deleteRedis(systemConfig.getName());
//                continue;
//            }
            redisUtil.hset(redisKey, systemConfig.getName(), systemConfig.getValue());
        }
    }
}
