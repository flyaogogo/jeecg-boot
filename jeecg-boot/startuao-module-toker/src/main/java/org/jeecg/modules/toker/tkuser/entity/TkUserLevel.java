package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 用户等级记录表
 * @Author: Luo.0022
 * @Date:   2021-01-18
 * @Version: V1.0
 */
@Data
@TableName("tk_user_level")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_user_level对象", description="用户等级记录表")
public class TkUserLevel implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户级别ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户级别ID")
    private String id;
	/**用户uid*/
	@Excel(name = "用户uid", width = 15)
    @ApiModelProperty(value = "用户uid")
    private String uid;
	/**等级vip*/
	@Excel(name = "等级vip", width = 15)
    @ApiModelProperty(value = "等级vip")
    private String levelId;
	/**会员等级*/
	@Excel(name = "会员等级", width = 15)
    @ApiModelProperty(value = "会员等级")
    private Integer grade;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "status")
	@Dict(dicCode = "status")
    @ApiModelProperty(value = "状态")
    private Integer status;
	/**是否已通知*/
	@Excel(name = "是否已通知", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否已通知")
    private Integer remind;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String mark;
	/**是否删除,0=未删除,1=删除*/
	@Excel(name = "是否删除,0=未删除,1=删除", width = 15)
    @ApiModelProperty(value = "是否删除,0=未删除,1=删除")
    private Integer isDel;
	/**享受折扣*/
	@Excel(name = "享受折扣", width = 15)
    @ApiModelProperty(value = "享受折扣")
    private BigDecimal discount;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
	/**过期时间*/
	@Excel(name = "过期时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "过期时间")
    private Date expiredTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;

}
