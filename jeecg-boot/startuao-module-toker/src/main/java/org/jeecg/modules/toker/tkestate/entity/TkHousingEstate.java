package org.jeecg.modules.toker.tkestate.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 商品房楼盘
 * @Author: Luo.0022
 * @Date:   2021-02-10
 * @Version: V1.0
 */
@Data
@TableName("tk_housing_estate")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_housing_estate对象", description="商品房楼盘")
public class TkHousingEstate implements Serializable {
    private static final long serialVersionUID = 1L;

	/**小区id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "小区id")
    private java.lang.String id;
	/**楼盘名称*/
	@Excel(name = "楼盘名称", width = 15)
    @ApiModelProperty(value = "楼盘名称")
    private java.lang.String realEstate;
	/**楼盘地址*/
	@Excel(name = "楼盘地址", width = 15)
    @ApiModelProperty(value = "楼盘地址")
    private java.lang.String cellAddress;
	/**鸟瞰图*/
	@Excel(name = "鸟瞰图", width = 15)
    @ApiModelProperty(value = "鸟瞰图")
    private java.lang.String cellHerf;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "tk_cell_status")
	@Dict(dicCode = "tk_cell_status")
    @ApiModelProperty(value = "状态")
    private java.lang.Integer status;
	/**是否上架*/
	@Excel(name = "是否上架", width = 15)
    @ApiModelProperty(value = "是否上架")
    private java.lang.String isShow;
	/**是否热卖*/
	@Excel(name = "是否热卖", width = 15)
    @ApiModelProperty(value = "是否热卖")
    private java.lang.String isHot;
	/**是否精品*/
	@Excel(name = "是否精品", width = 15)
    @ApiModelProperty(value = "是否精品")
    private java.lang.String isBest;
	/**是否新品*/
	@Excel(name = "是否新品", width = 15)
    @ApiModelProperty(value = "是否新品")
    private java.lang.String isNew;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String mark;

    /**区域*/
    @Excel(name = "区域", width = 15)
    @ApiModelProperty(value = "区域")
    private java.lang.String region;

	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;


}
