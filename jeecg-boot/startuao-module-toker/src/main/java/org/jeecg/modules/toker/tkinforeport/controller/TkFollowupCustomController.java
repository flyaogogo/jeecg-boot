package org.jeecg.modules.toker.tkinforeport.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import org.jeecg.modules.toker.tkinforeport.service.ITkFollowupCustomService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 导购跟跟踪信息记录
 * @Author: Luo.0022
 * @Date:   2021-04-12
 * @Version: V1.0
 */
@Api(tags="导购跟跟踪信息记录")
@RestController
@RequestMapping("/tkinforeport/tkFollowupCustom")
@Slf4j
public class TkFollowupCustomController extends JeecgController<TkFollowupCustom, ITkFollowupCustomService> {
	@Autowired
	private ITkFollowupCustomService tkFollowupCustomService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tkFollowupCustom
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "导购跟跟踪信息记录-分页列表查询")
	@ApiOperation(value="导购跟跟踪信息记录-分页列表查询", notes="导购跟跟踪信息记录-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkFollowupCustom tkFollowupCustom,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkFollowupCustom> queryWrapper = QueryGenerator.initQueryWrapper(tkFollowupCustom, req.getParameterMap());
		Page<TkFollowupCustom> page = new Page<TkFollowupCustom>(pageNo, pageSize);
		IPage<TkFollowupCustom> pageList = tkFollowupCustomService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tkFollowupCustom
	 * @return
	 */
	@AutoLog(value = "导购跟跟踪信息记录-添加")
	@ApiOperation(value="导购跟跟踪信息记录-添加", notes="导购跟跟踪信息记录-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkFollowupCustom tkFollowupCustom) {
		//tkFollowupCustomService.save(tkFollowupCustom);
		tkFollowupCustomService.addFollowupInfo(tkFollowupCustom) ;
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkFollowupCustom
	 * @return
	 */
	@AutoLog(value = "导购跟跟踪信息记录-编辑")
	@ApiOperation(value="导购跟跟踪信息记录-编辑", notes="导购跟跟踪信息记录-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkFollowupCustom tkFollowupCustom) {
		//tkFollowupCustomService.updateById(tkFollowupCustom) ;
		tkFollowupCustomService.editFollowupInfo(tkFollowupCustom) ;
		return Result.OK("编辑成功!");
	}

	 /**
	  * 需要前端单独一个功能处理
	  * @param tkFollowupCustom
	  * @return
	  */
	 @AutoLog(value = "导购跟跟踪信息记录-取消签单")
	 @ApiOperation(value="导购跟跟踪信息记录-取消签单", notes="导购跟跟踪信息记录-取消签单")
	 @PutMapping(value = "/cancel/order")
	 public Result<?> cancelOrder(@RequestBody TkFollowupCustom tkFollowupCustom) {
		 //tkFollowupCustomService.updateById(tkFollowupCustom);
		 tkFollowupCustomService.cancelOrderFollowupInfo(tkFollowupCustom) ;
		 return Result.OK("取消签单成功!");
	 }
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "导购跟跟踪信息记录-通过id删除")
	@ApiOperation(value="导购跟跟踪信息记录-通过id删除", notes="导购跟跟踪信息记录-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkFollowupCustomService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "导购跟跟踪信息记录-批量删除")
	@ApiOperation(value="导购跟跟踪信息记录-批量删除", notes="导购跟跟踪信息记录-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkFollowupCustomService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "导购跟跟踪信息记录-通过id查询")
	@ApiOperation(value="导购跟跟踪信息记录-通过id查询", notes="导购跟跟踪信息记录-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkFollowupCustom tkFollowupCustom = tkFollowupCustomService.getById(id);
		if(tkFollowupCustom==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkFollowupCustom);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tkFollowupCustom
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkFollowupCustom tkFollowupCustom) {
        return super.exportXls(request, tkFollowupCustom, TkFollowupCustom.class, "导购跟跟踪信息记录");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkFollowupCustom.class);
    }

}
