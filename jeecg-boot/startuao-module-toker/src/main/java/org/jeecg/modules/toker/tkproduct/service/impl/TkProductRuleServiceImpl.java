package org.jeecg.modules.toker.tkproduct.service.impl;

import org.jeecg.modules.toker.tkproduct.entity.TkProductRule;
import org.jeecg.modules.toker.tkproduct.mapper.TkProductRuleMapper;
import org.jeecg.modules.toker.tkproduct.service.ITkProductRuleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 产品规则值(规格)表
 * @Author: Luo.0022
 * @Date:   2021-01-07
 * @Version: V1.0
 */
@Service
public class TkProductRuleServiceImpl extends ServiceImpl<TkProductRuleMapper, TkProductRule> implements ITkProductRuleService {

}
