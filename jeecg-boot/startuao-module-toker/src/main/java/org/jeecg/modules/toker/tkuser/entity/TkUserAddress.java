package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 用户地址表
 * @Author: Luo.0022
 * @Date:   2021-03-02
 * @Version: V1.0
 */
@Data
@TableName("tk_user_address")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_user_address对象", description="用户地址表")
public class TkUserAddress implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户地址id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户地址id")
    private String id;
	/**用户id*/
	@Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
    private String uid;
	/**收货人姓名*/
	@Excel(name = "收货人姓名", width = 15)
    @ApiModelProperty(value = "收货人姓名")
    private String realName;
	/**收货人电话*/
	@Excel(name = "收货人电话", width = 15)
    @ApiModelProperty(value = "收货人电话")
    private String phone;
	/**收货人所在省*/
	@Excel(name = "收货人所在省", width = 15)
    @ApiModelProperty(value = "收货人所在省")
    private String province;
	/**收货人所在市*/
	@Excel(name = "收货人所在市", width = 15)
    @ApiModelProperty(value = "收货人所在市")
    private String city;
	/**城市id*/
	@Excel(name = "城市id", width = 15)
    @ApiModelProperty(value = "城市id")
    private Integer cityId;
	/**收货人所在区*/
	@Excel(name = "收货人所在区", width = 15)
    @ApiModelProperty(value = "收货人所在区")
    private String district;
	/**收货人详细地址*/
	@Excel(name = "收货人详细地址", width = 15)
    @ApiModelProperty(value = "收货人详细地址")
    private String detail;
	/**邮编*/
	@Excel(name = "邮编", width = 15)
    @ApiModelProperty(value = "邮编")
    private Integer postCode;
	/**经度*/
	@Excel(name = "经度", width = 15)
    @ApiModelProperty(value = "经度")
    private String longitude;
	/**纬度*/
	@Excel(name = "纬度", width = 15)
    @ApiModelProperty(value = "纬度")
    private String latitude;
	/**是否默认*/
	@Excel(name = "是否默认", width = 15)
    @ApiModelProperty(value = "是否默认")
    private Boolean isDefault;
	/**是否删除*/
	@Excel(name = "是否删除", width = 15)
    @ApiModelProperty(value = "是否删除")
    private Boolean isDel;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;

}
