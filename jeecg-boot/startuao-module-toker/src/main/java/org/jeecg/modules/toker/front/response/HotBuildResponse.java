package org.jeecg.modules.toker.front.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 热销楼盘信息
 * @Author: Luo.0022
 * @Date:   2021-02-14
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_housing_estate对象", description="商品房楼盘")
public class HotBuildResponse implements Serializable {
    private static final long serialVersionUID = 1L;

	/**小区id*/
    @ApiModelProperty(value = "小区id")
    private String id;

	/**楼盘名称*/
    @ApiModelProperty(value = "楼盘名称")
    private String realEstate;

	/**楼盘地址*/
    @ApiModelProperty(value = "楼盘地址")
    private String cellAddress;

	/**鸟瞰图*/
    @ApiModelProperty(value = "鸟瞰图")
    private String cellHerf;

	/**状态 1:入住；2:在建；3:预售 */
	@Dict(dicCode = "tk_cell_status")
    @ApiModelProperty(value = "状态")
    private Integer status;

	/**是否上架*/
    @ApiModelProperty(value = "是否上架")
    private String isShow;

	/**是否热卖*/
    @ApiModelProperty(value = "是否热卖")
    private String isHot;

	/**是否精品*/
    @ApiModelProperty(value = "是否精品")
    private String isBest;

	/**是否新品*/
    @ApiModelProperty(value = "是否新品")
    private String isNew;

	/**备注*/
    @ApiModelProperty(value = "备注")
    private String mark;

	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
