package org.jeecg.modules.toker.tkvaddress.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 业主装修地址
 * @Author: Luo.0022
 * @Date:   2020-12-31
 * @Version: V1.0
 */
@Data
@TableName("tk_village_address")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_village_address对象", description="业主装修地址")
public class TkVillageAddress implements Serializable {

    private static final long serialVersionUID = 1L;

	/**地址id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "地址id")
    private java.lang.String id;

    /**UUID*/
    @ApiModelProperty(value = "UUID")
    private java.lang.String uuid;

	/**楼盘名称*/
	@Excel(name = "楼盘名称", width = 15)
    @ApiModelProperty(value = "楼盘名称")
    private java.lang.String realEstate;
	/**楼栋号*/
	@Excel(name = "楼栋号", width = 15)
    @ApiModelProperty(value = "楼栋号")
    private java.lang.Integer buildingNumber;
	/**单元*/
	@Excel(name = "单元", width = 15)
    @ApiModelProperty(value = "单元")
    private java.lang.Integer unit;
	/**楼层*/
	@Excel(name = "楼层", width = 15)
    @ApiModelProperty(value = "楼层")
    private java.lang.Integer floor;
	/**门牌号*/
	@Excel(name = "门牌号", width = 15)
    @ApiModelProperty(value = "门牌号")
    private java.lang.Integer houseNumber;
	/**房屋地址*/
	@Excel(name = "房屋地址", width = 15)
    @ApiModelProperty(value = "房屋地址")
    private java.lang.String houseAddress;

    /**户型面积*/
    @Excel(name = "户型面积", width = 15)
    @ApiModelProperty(value = "户型面积")
    private String houseType;

	/**小区地址*/
	@Excel(name = "小区地址", width = 15)
    @ApiModelProperty(value = "小区地址")
    private java.lang.String cellAddress;

    /**区域*/
    @Excel(name = "区域", width = 15)
    @ApiModelProperty(value = "区域")
    private java.lang.String region;

    /**租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)*/
    @Excel(name = "租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)", width = 15)
    @ApiModelProperty(value = "租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)")
    private java.lang.String tenantId;

	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
}
