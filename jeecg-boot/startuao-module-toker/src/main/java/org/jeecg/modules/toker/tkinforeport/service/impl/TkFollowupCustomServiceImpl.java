package org.jeecg.modules.toker.tkinforeport.service.impl;

import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.front.request.UserOperateFundsRequest;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import org.jeecg.modules.toker.tkinforeport.mapper.TkFollowupCustomMapper;
import org.jeecg.modules.toker.tkinforeport.mapper.TkInfoReportMapper;
import org.jeecg.modules.toker.tkinforeport.service.ITkFollowupCustomService;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 导购跟跟踪信息记录
 * @Author: Luo.0022
 * @Date:   2021-04-12
 * @Version: V1.0
 */
@Service
public class TkFollowupCustomServiceImpl extends ServiceImpl<TkFollowupCustomMapper, TkFollowupCustom> implements ITkFollowupCustomService {

    @Autowired
    private TkFollowupCustomMapper tkFollowupCustomMapper;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ITkSystemConfigService systemConfigService ;

    @Autowired
    private ITkUserService userService ;

    @Autowired
    private TkInfoReportMapper infoReportMapper ;

    @Override
    public List<TkFollowupCustom> selectByMainId(String mainId) {
        return tkFollowupCustomMapper.selectByMainId(mainId);
    }

    /**
     * 添加导购跟进客户信息，
     * 当：客户签单成功后，需要按比例更新用户积分
     * @param followupCustom
     * @return
     */
    @Override
    public Boolean addFollowupInfo(TkFollowupCustom followupCustom) {
        Boolean execute ;
        // 客户签单
        if ("1".equals(followupCustom.getFollowUp())){
            execute = assemblingUserBillData(followupCustom,1,"add") ;
        }else {
            int res = tkFollowupCustomMapper.insert(followupCustom) ;
            execute = res != 0 ? true : false ;
        }

        return execute;
    }

    /**
     * 修改导购跟进客户信息，
     * 当：客户签单成功后，需要按比例更新用户积分
     * TODO 未处理 当从签单状态，更为其他状态，可以从前端做限制，签单后，只有从另一种入口取消
     * @param followupCustom
     * @return
     */
    @Override
    public Boolean editFollowupInfo(TkFollowupCustom followupCustom) {

        Boolean execute ;
        // 客户签单
        if ("1".equals(followupCustom.getFollowUp())){
            execute = assemblingUserBillData(followupCustom,1,"edit") ;
        }else {
            //int res = tkFollowupCustomMapper.updateById(followupCustom) ;
            //execute = res != 0 ? true : false ;

            TkInfoReport reportInfo = new TkInfoReport() ;
            reportInfo.setId(followupCustom.getReportId()) ;
            reportInfo.setFollowUp(followupCustom.getFollowUp()) ;

            execute = transactionTemplate.execute(e -> {
                // 当修改导购跟进状态时，则需要同时更新报备信息表，保持数据一致性
                infoReportMapper.updateById(reportInfo) ;
                tkFollowupCustomMapper.updateById(followupCustom) ;
                return Boolean.TRUE;
            });
        }
        return execute;
    }

    /**
     * 修改导购跟进客户信息，
     * 当从签单状态，更为其他状态，可以从前端做限制，签单后，只有从另一种入口取消
     * @param followupCustom
     * @return
     */
    @Override
    public Boolean cancelOrderFollowupInfo(TkFollowupCustom followupCustom) {

        Boolean execute = assemblingUserBillData(followupCustom,0,"edit") ;

        return execute;
    }

    /**
     * 客户签单成功后， 组装用户数据，包括跟进信息，用户积分及经验数据，同时更新报备信息跟进及订单价
     * @param followupCustom
     * @param pm 1:获得，0：支出
     * @param operationType 操作类型为：添加：add，更新：edit
     */
    private Boolean assemblingUserBillData (TkFollowupCustom followupCustom, Integer pm , String operationType ){
        // 计算签单价格，并按比率折算
        BigDecimal signTotalPrice = followupCustom.getTotalPrice() ;
        if (signTotalPrice == null && signTotalPrice.equals(BigDecimal.ZERO)){
            new JeecgBootException("当客户签单后，对应签单额度必须填写！") ;
        }

        TkUser user = userService.getById(followupCustom.getUserId()) ;

        // 获取配置库中的 折算比率
        String giveIntegralRate = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_INTEGRAL_RATE_ORDER_GIVE);

        BigDecimal integral = signTotalPrice.multiply(new BigDecimal(giveIntegralRate)) ;
        BigDecimal experience = signTotalPrice.multiply(new BigDecimal(giveIntegralRate)) ;
        //签单后，总金额按比例折合后的积分数，向下取整,
        // 更新用户积分信息
        UserOperateFundsRequest integralFunds = new UserOperateFundsRequest();
        integralFunds.setUid(user.getId());
        integralFunds.setFoundsCategory(TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL);
        integralFunds.setFoundsType(TokerCommonConstant.USER_BILL_TYPE_ORDER);
        //integralFunds.setTitle(TokerCommonConstant.USER_REPORT_INTEGRAL_TITLE);
        integralFunds.setType(pm);
        //integralFunds.setValue(integral);

        //签单后，总金额按比例折合后的经验数，向下取整
        // 更新用户经验信息
        UserOperateFundsRequest experienceFunds = new UserOperateFundsRequest();
        experienceFunds.setUid(user.getId());
        experienceFunds.setFoundsCategory(TokerCommonConstant.USER_BILL_CATEGORY_EXPERIENCE);
        experienceFunds.setFoundsType(TokerCommonConstant.USER_BILL_TYPE_ORDER);
        //experienceFunds.setTitle(TokerCommonConstant.USER_REPORT_EXPERIENCE_TITLE);
        experienceFunds.setType(pm);
        //experienceFunds.setValue(experience);

        // 获得积分，相加，即签单成功
        if( pm == 1){
            integralFunds.setValue(integral);
            experienceFunds.setValue(experience);
            //更新用户积分
            user.setIntegral(user.getIntegral().add(integral));
            // 更新用户经验
            user.setExperience(user.getExperience() + experience.intValue());

            integralFunds.setTitle(TokerCommonConstant.USER_REPORT_INTEGRAL_TITLE);

            experienceFunds.setTitle(TokerCommonConstant.USER_REPORT_EXPERIENCE_TITLE);

            // 支出积分，即：签单后，又取消
        }else if(pm == 0){
            //更新用户积分
            user.setIntegral(user.getIntegral().subtract(integral));
            // 更新用户经验
            user.setExperience(user.getExperience() - experience.setScale(0, BigDecimal.ROUND_DOWN ).intValue());

            //
            integralFunds.setTitle(TokerCommonConstant.USER_REPORT_CANCEL_INTEGRAL_TITLE);
            integralFunds.setValue(integral.negate());//转为负数
            experienceFunds.setTitle(TokerCommonConstant.USER_REPORT_CANCEL_EXPERIENCE_TITLE);
            experienceFunds.setValue(experience.negate());//转为负数
        }

        TkInfoReport reportInfo = new TkInfoReport() ;
        reportInfo.setId(followupCustom.getReportId()) ;
        reportInfo.setFollowUp(followupCustom.getFollowUp()) ;
        reportInfo.setTotalPrice(signTotalPrice) ;

        Boolean execute = transactionTemplate.execute(e -> {
            //
            if("add".equals(operationType)){
                tkFollowupCustomMapper.insert(followupCustom) ;
            } else if("edit".equals(operationType)){
                tkFollowupCustomMapper.updateById(followupCustom) ;
            }

            // 当签单成功，则需要同时更新报备信息表，保持数据一致性
            infoReportMapper.updateById(reportInfo) ;
            //更新用户积分信息
            userService.updateFounds(integralFunds, true);
            //更新用户经验信息
            userService.updateFounds(experienceFunds, true);
            //更新用户 签到天数、积分、经验
            //userService.updateById(user);
            return Boolean.TRUE;
        });

        return execute ;

    }


}
