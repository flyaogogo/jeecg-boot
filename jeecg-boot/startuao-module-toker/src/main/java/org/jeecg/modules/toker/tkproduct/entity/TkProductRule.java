package org.jeecg.modules.toker.tkproduct.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 产品规则值(规格)表
 * @Author: Luo.0022
 * @Date:   2021-01-07
 * @Version: V1.0
 */
@Data
@TableName("tk_product_rule")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_product_rule对象", description="产品规则值(规格)表")
public class TkProductRule implements Serializable {
    private static final long serialVersionUID = 1L;

	/**规格id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "规格id")
    private String id;
	/**规格名称*/
	@Excel(name = "规格名称", width = 15)
    @ApiModelProperty(value = "规格名称")
    private String ruleName;
	/**规格属性*/
	@Excel(name = "规格属性", width = 15)
    @ApiModelProperty(value = "规格属性")
    private String ruleAttr;
	/**规格值*/
	@Excel(name = "规格值", width = 15)
    @ApiModelProperty(value = "规格值")
    private String ruleValue;
}
