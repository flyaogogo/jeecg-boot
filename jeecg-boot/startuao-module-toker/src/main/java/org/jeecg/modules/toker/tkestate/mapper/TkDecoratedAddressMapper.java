package org.jeecg.modules.toker.tkestate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 热装小区客户地址表
 * @Author: Luo.0022
 * @Date:   2021-02-15
 * @Version: V1.0
 */
public interface TkDecoratedAddressMapper extends BaseMapper<TkDecoratedAddress> {

}
