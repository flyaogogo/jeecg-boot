package org.jeecg.modules.toker.tkuser.service;

import org.jeecg.modules.toker.tkuser.entity.TkUserBrokerageRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * @Description: 用户佣金记录表
 * @Author: Luo.0022
 * @Date:   2021-02-27
 * @Version: V1.0
 */
public interface ITkUserBrokerageRecordService extends IService<TkUserBrokerageRecord> {

    /**
     * 计算佣金并存储
     * @param uid
     * @param reportInfoId  把报备单Id当订单Id使用
     * @param orderPrice
     * @param type
     */
    TkUserBrokerageRecord calculateCommissionAndSave(String uid, String reportInfoId, BigDecimal orderPrice ,String type) ;
}
