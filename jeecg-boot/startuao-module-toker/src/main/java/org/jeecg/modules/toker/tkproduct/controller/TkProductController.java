package org.jeecg.modules.toker.tkproduct.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import org.jeecg.modules.toker.tkproduct.service.ITkProductService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 产品详细信息
 * @Author: Luo.0022
 * @Date:   2021-01-05
 * @Version: V1.0
 */
@Api(tags="产品详细信息")
@RestController
@RequestMapping("/tkproduct/tkProduct")
@Slf4j
public class TkProductController extends JeecgController<TkProduct, ITkProductService> {
	@Autowired
	private ITkProductService tkProductService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tkProduct
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "产品详细信息-分页列表查询")
	@ApiOperation(value="产品详细信息-分页列表查询", notes="产品详细信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkProduct tkProduct,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkProduct> queryWrapper = QueryGenerator.initQueryWrapper(tkProduct, req.getParameterMap());
		Page<TkProduct> page = new Page<TkProduct>(pageNo, pageSize);
		IPage<TkProduct> pageList = tkProductService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tkProduct
	 * @return
	 */
	@AutoLog(value = "产品详细信息-添加")
	@ApiOperation(value="产品详细信息-添加", notes="产品详细信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkProduct tkProduct) {
		tkProductService.save(tkProduct);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkProduct
	 * @return
	 */
	@AutoLog(value = "产品详细信息-编辑")
	@ApiOperation(value="产品详细信息-编辑", notes="产品详细信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkProduct tkProduct) {
		tkProductService.updateById(tkProduct);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "产品详细信息-通过id删除")
	@ApiOperation(value="产品详细信息-通过id删除", notes="产品详细信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkProductService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "产品详细信息-批量删除")
	@ApiOperation(value="产品详细信息-批量删除", notes="产品详细信息-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkProductService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "产品详细信息-通过id查询")
	@ApiOperation(value="产品详细信息-通过id查询", notes="产品详细信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkProduct tkProduct = tkProductService.getById(id);
		if(tkProduct==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkProduct);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tkProduct
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkProduct tkProduct) {
        return super.exportXls(request, tkProduct, TkProduct.class, "产品详细信息");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkProduct.class);
    }

	 /**
	  *  修改 产品 是否上架
	  *
	  * @param json 是否上架（Y  ， N）
	  *
	  * @return
	  */
	 @AutoLog(value = "产品是否上架-编辑")
	 @ApiOperation(value="产品是否上架-编辑", notes="产品是否上架-编辑")
	 @RequestMapping(value = "/ishow", method = RequestMethod.PUT)
	 public Result<String> editIsShow(@RequestBody JSONObject json) {
		 Result<String> result = new Result<>();
	 	try {
			String productId = json.getString("id");
			String isShow = json.getString("isShow");
			tkProductService.editStatusById(productId ,isShow) ;
			result.success("修改成功!");
			//return Result.OK("修改成功!");
		} catch (Exception e) {
			result.error500("操作失败！");
			log.error(e.getMessage(), e);

		}
		 return result ;
	 }
}
