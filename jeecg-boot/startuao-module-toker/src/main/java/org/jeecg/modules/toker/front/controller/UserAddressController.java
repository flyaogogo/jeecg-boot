package org.jeecg.modules.toker.front.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.UserAddressDelRequest;
import org.jeecg.modules.toker.front.request.UserAddressRequest;
import org.jeecg.modules.toker.tkuser.entity.TkUserAddress;
import org.jeecg.modules.toker.tkuser.service.ITkUserAddressService;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 用户地址 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/address")
@Api(tags = "用户 -- 地址")
public class UserAddressController {

    @Autowired
    private ITkUserAddressService userAddressService;

    @Autowired
    private ITkUserService userService;


    /**
     * 分页显示用户地址
     */
    @ApiOperation(value = "列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<List<TkUserAddress>>  getList(PageParamRequest pageParamRequest){

        TkUserAddress userAddress = new TkUserAddress();
        userAddress.setUid(userService.getUserIdStr());
        userAddress.setIsDel(false);
        List<TkUserAddress> userAddressCommonPage = userAddressService.getListByUserAddress(userAddress,pageParamRequest);

         return Result.OK(userAddressCommonPage);
    }

    /**
     * 新增用户地址
     * @param request 新增参数
     * @author Mr.Zhang
     * @since 2020-04-28
     */
    @ApiOperation(value = "保存")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result<TkUserAddress> save(@RequestBody @Validated UserAddressRequest request){
        return Result.OK(userAddressService.create(request));
    }

    /**
     * 删除用户地址
     * @param request UserAddressDelRequest 参数
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result<String> delete(@RequestBody UserAddressDelRequest request){
        if(userAddressService.delete(request.getId())){
            return Result.OK();
        }else{
            return Result.OK("删除失败！") ; // 注意：前端要判断删除失败情况 。
        }
    }

    /**
     * 详情
     * @param id Integer
     * @author Mr.Zhang
     * @since 2020-04-28
     */
    @ApiOperation(value = "获取单个地址")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public Result<TkUserAddress> info(@PathVariable("id") Integer id){
        TkUserAddress userAddress = userAddressService.getById(id);
        return Result.OK(userAddress);
    }

    /**
     * 获取默认地址
     * @author Mr.Zhang
     * @since 2020-04-28
     */
    @ApiOperation(value = "获取默认地址")
    @RequestMapping(value = "/default", method = RequestMethod.GET)
    public Result<TkUserAddress> getDefault(){
        return Result.OK(userAddressService.getDefault());

    }

    /**
     * 设置默认地址
     * @param request UserAddressDelRequest 参数
     * @author Mr.Zhang
     * @since 2020-04-28
     */
    @ApiOperation(value = "设置默认地址")
    @RequestMapping(value = "/default/set", method = RequestMethod.POST)
    public Result<TkUserAddress> def(@RequestBody UserAddressDelRequest request){
        if(userAddressService.def(request.getId())){
            return Result.OK();
        }else{
            return Result.OK(); //
        }
    }
}



