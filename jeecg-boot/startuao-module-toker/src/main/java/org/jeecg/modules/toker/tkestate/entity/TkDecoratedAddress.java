package org.jeecg.modules.toker.tkestate.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 热装小区客户地址表
 * @Author: Luo.0022
 * @Date:   2021-02-15
 * @Version: V1.0
 */
@Data
@TableName("tk_decorated_address")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_decorated_address对象", description="热装小区客户地址表")
public class TkDecoratedAddress implements Serializable {
    private static final long serialVersionUID = 1L;

	/**地址id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "地址id")
    private String id;

    /**UUID*/
    @ApiModelProperty(value = "UUID")
    private java.lang.String uuid;

	/**小区名称*/
	@Excel(name = "小区名称", width = 15, dictTable = "tk_housing_estate", dicText = "real_estate", dicCode = "id")
	@Dict(dictTable = "tk_housing_estate", dicText = "real_estate", dicCode = "id")
    @ApiModelProperty(value = "小区名称")
    private String estateId;
	/**楼栋号*/
	@Excel(name = "楼栋号", width = 15)
    @ApiModelProperty(value = "楼栋号")
    private String buildingNumber;
	/**单元*/
	@Excel(name = "单元", width = 15)
    @ApiModelProperty(value = "单元")
    private String unit;
	/**楼层*/
	@Excel(name = "楼层", width = 15)
    @ApiModelProperty(value = "楼层")
    private String floor;
	/**门牌号*/
	@Excel(name = "门牌号", width = 15)
    @ApiModelProperty(value = "门牌号")
    private String houseNumber;
	/**房屋地址*/
	@Excel(name = "房屋地址", width = 15)
    @ApiModelProperty(value = "房屋地址")
    private String houseAddress;
	/**小区地址*/
	@Excel(name = "小区地址", width = 15)
    @ApiModelProperty(value = "小区地址")
    private String cellAddress;

    /**户型面积*/
    @Excel(name = "户型面积", width = 15)
    @ApiModelProperty(value = "户型面积")
    private String houseType;

	/**酷家乐全景链接*/
	@Excel(name = "酷家乐全景链接", width = 15)
    @ApiModelProperty(value = "酷家乐全景链接")
    private String extHref;
	/**所选产品Id*/
	@Excel(name = "所选产品Id", width = 15, dictTable = "tk_product", dicText = "product_name", dicCode = "id")
	@Dict(dictTable = "tk_product", dicText = "product_name", dicCode = "id")
    @ApiModelProperty(value = "所选产品Id")
    private String productId;
	/**使用状态*/
	@Excel(name = "客户跟进状态", width = 15, dicCode = "tk_follow_up_status")
	@Dict(dicCode = "tk_follow_up_status")
    @ApiModelProperty(value = "客户跟进状态")
    private Integer status;
	/**备注信息*/
	@Excel(name = "备注信息", width = 15)
    @ApiModelProperty(value = "备注信息")
    private String mark;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;

}
