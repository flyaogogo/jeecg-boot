package org.jeecg.modules.toker.tkestate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 商品房楼盘
 * @Author: Luo.0022
 * @Date:   2021-02-10
 * @Version: V1.0
 */
public interface TkHousingEstateMapper extends BaseMapper<TkHousingEstate> {

}
