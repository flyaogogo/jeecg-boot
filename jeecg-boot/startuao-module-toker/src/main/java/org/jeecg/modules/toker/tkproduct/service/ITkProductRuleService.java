package org.jeecg.modules.toker.tkproduct.service;

import org.jeecg.modules.toker.tkproduct.entity.TkProductRule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 产品规则值(规格)表
 * @Author: Luo.0022
 * @Date:   2021-01-07
 * @Version: V1.0
 */
public interface ITkProductRuleService extends IService<TkProductRule> {

}
