package org.jeecg.modules.toker.tkuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;
import org.jeecg.modules.toker.tkuser.entity.UserToken;

public interface UserTokenMapper extends BaseMapper<UserToken> {

    @Delete("delete from tk_user_token where uid=#{uid}")
    boolean deleteUserTokenByUid(String uid) ;

    @Update("update tk_user_token set token=#{token} where uid=#{userId}")
    boolean updateUserTokenByUid(String token , String userId) ;
}
