package org.jeecg.modules.toker.tkuser.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.toker.tkuser.entity.TkUserSign;
import org.jeecg.modules.toker.tkuser.service.ITkUserSignService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 签到记录表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
@Api(tags="签到记录表")
@RestController
@RequestMapping("/tkuser/tkUserSign")
@Slf4j
public class TkUserSignController extends JeecgController<TkUserSign, ITkUserSignService> {
	@Autowired
	private ITkUserSignService tkUserSignService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tkUserSign
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "签到记录表-分页列表查询")
	@ApiOperation(value="签到记录表-分页列表查询", notes="签到记录表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkUserSign tkUserSign,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkUserSign> queryWrapper = QueryGenerator.initQueryWrapper(tkUserSign, req.getParameterMap());
		Page<TkUserSign> page = new Page<TkUserSign>(pageNo, pageSize);
		IPage<TkUserSign> pageList = tkUserSignService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tkUserSign
	 * @return
	 */
	@AutoLog(value = "签到记录表-添加")
	@ApiOperation(value="签到记录表-添加", notes="签到记录表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkUserSign tkUserSign) {
		tkUserSignService.save(tkUserSign);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkUserSign
	 * @return
	 */
	@AutoLog(value = "签到记录表-编辑")
	@ApiOperation(value="签到记录表-编辑", notes="签到记录表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkUserSign tkUserSign) {
		tkUserSignService.updateById(tkUserSign);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "签到记录表-通过id删除")
	@ApiOperation(value="签到记录表-通过id删除", notes="签到记录表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkUserSignService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "签到记录表-批量删除")
	@ApiOperation(value="签到记录表-批量删除", notes="签到记录表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkUserSignService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "签到记录表-通过id查询")
	@ApiOperation(value="签到记录表-通过id查询", notes="签到记录表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkUserSign tkUserSign = tkUserSignService.getById(id);
		if(tkUserSign==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkUserSign);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tkUserSign
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkUserSign tkUserSign) {
        return super.exportXls(request, tkUserSign, TkUserSign.class, "签到记录表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkUserSign.class);
    }

}
