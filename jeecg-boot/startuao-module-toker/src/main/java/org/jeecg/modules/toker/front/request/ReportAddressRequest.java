package org.jeecg.modules.toker.front.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ReportAddressRequest对象", description="客户信息报备")
public class ReportAddressRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /**用户id*/
    private java.lang.String id;
    /**业主姓名*/
    private java.lang.String ownerName;
    /**业主电话*/
    private java.lang.String ownerPhone;
    /**装修地址*/
    private java.lang.String address;
    /**装修客户地址ID*/
    private java.lang.String decoratedAddressId;
    /**装修进度*/
    private java.lang.String progress;
    /**其他信息*/
    private java.lang.String remark;

    /**楼盘名称*/
    private java.lang.String realEstate;
    /**楼盘Id*/
    private java.lang.String realEstateId;
    /**楼栋号*/
    private java.lang.Integer buildingNumber;
    /**单元*/
    private java.lang.Integer unit;
    /**楼层*/
    private java.lang.Integer floor;
    /**门牌号*/
    private java.lang.Integer houseNumber;
    /**房屋地址*/
    private java.lang.String houseAddress;
    /**小区地址*/
    private java.lang.String cellAddress;

    /**户型面积类型*/
    private java.lang.String houseType;

    /**区域*/
    private java.lang.String region;
    /**地址状态：
     * 1为新增的小区-addCell，
     * 2为选择小区-chooseCell，
     * 3为新增小区内屋室-addEstate，
     * 4为选择小区内屋室-chooseHouse
     * */
    @Excel(name = "地址状态", width = 15)
    @ApiModelProperty(value = "地址状态")
    private java.lang.String addressStatus;
}
