package org.jeecg.modules.toker.front.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.modules.toker.front.request.HotHouseInfoRequest;
import org.jeecg.modules.toker.front.request.ReportAddressRequest;
import org.jeecg.modules.toker.front.response.LoginResponse;
import org.jeecg.modules.toker.front.service.ReportAddressService;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController("FrontReportController")
@RequestMapping("api/front/report")
@Api(tags = "报备 -- 报备中心")
public class ReportAddressController {



    @Autowired
    private ReportAddressService reportAddressService ;
    /**
     * 新增报备信息
     * @param reportAddressRequest
     * @param request
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result<LoginResponse> addReportInfo (@RequestBody ReportAddressRequest reportAddressRequest , HttpServletRequest request) {
        Result<LoginResponse> result = new Result<LoginResponse>();
        //String username = JwtUtil.getUserNameByToken(request);

        reportAddressService.addReportAndAddrs(reportAddressRequest) ;

        return  result.success("添加成功") ;
    }

    @ApiOperation(value = "热装小区列表")
    @RequestMapping(value = "/cells", method = RequestMethod.GET)
    public Result<?> getAllHotCells( @Validated String keywords){
        List<TkHousingEstate> list = reportAddressService.getAllHotCells(keywords) ;
        return Result.OK(list) ;
    }

    @ApiOperation(value = "热装小区内的装潢地址列表")
    @RequestMapping(value = "/decorate", method = RequestMethod.GET)
    public Result<?> getUserAddressByCellId( @Validated String cellId){
        List<TkDecoratedAddress> list = reportAddressService.getUserAddressByCellId(cellId) ;
        return Result.OK(list) ;
    }

    @ApiOperation(value = "通过用户ID，列出所有的报备信息")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<?> getMyReportsByUserId( @Validated String uid){
        List<TkInfoReport> list = reportAddressService.getMyReportsByUserId(uid) ;
        return Result.OK(list) ;
    }
}
