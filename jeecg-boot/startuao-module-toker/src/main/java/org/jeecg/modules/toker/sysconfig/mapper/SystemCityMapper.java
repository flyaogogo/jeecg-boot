package org.jeecg.modules.toker.sysconfig.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.toker.sysconfig.entity.SystemCity;

/**
 * 城市表 Mapper 接口
 */
public interface SystemCityMapper extends BaseMapper<SystemCity> {

}
