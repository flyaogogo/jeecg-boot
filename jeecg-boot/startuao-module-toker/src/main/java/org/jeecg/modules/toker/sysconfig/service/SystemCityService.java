package org.jeecg.modules.toker.sysconfig.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.toker.front.request.SystemCityRequest;
import org.jeecg.modules.toker.front.request.SystemCitySearchRequest;
import org.jeecg.modules.toker.sysconfig.entity.SystemCity;

import java.util.List;

/**
 * SystemCityService 接口
 */
public interface SystemCityService extends IService<SystemCity> {

    Object getList(SystemCitySearchRequest request);

    boolean updateStatus(Integer id, Boolean status);

    boolean update(Integer id, SystemCityRequest request);

    Object getListTree();

    String getStringNameInId(String cityIdList);

    List<Integer> getCityIdList();

    SystemCity getCityByCityId(Integer cityId);
}