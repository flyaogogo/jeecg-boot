package org.jeecg.modules.toker.front.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 用户编辑Request
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserEditRequest对象", description="修改个人资料")
public class UserEditRequest implements Serializable {

    private static final long serialVersionUID=1L;


    @ApiModelProperty(value = "用户昵称")
    @NotBlank(message = "请填写用户昵称")
    private String nickname;

    @ApiModelProperty(value = "用户头像")
    @NotBlank(message = "请上传用户头像")
    private String avatar;

    /**真实姓名*/
    @ApiModelProperty(value = "真实姓名")
    @NotBlank(message = "请上传用户头像")
    private java.lang.String realName;
    /**生日*/
    @ApiModelProperty(value = "生日")
    private java.lang.String birthday;
    /**用户类型*/
    @ApiModelProperty(value = "用户类型")
    private java.lang.String userType;
    /**手机号码*/
    @ApiModelProperty(value = "手机号码")
    private java.lang.String phone;
    /**性别*/
    @ApiModelProperty(value = "性别")
    private java.lang.Integer sex;

    /**身份证号码*/
    @ApiModelProperty(value = "身份证号码")
    private java.lang.String cardId;

    /**店内联系人*/
    @ApiModelProperty(value = "店内联系人")
    private java.lang.String contact;
}
