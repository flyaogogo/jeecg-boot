package org.jeecg.modules.toker.front.service;


import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.IndexTkProductSearchRequest;
import org.jeecg.modules.toker.front.response.IndexInfoResponse;
import org.jeecg.modules.toker.front.response.IndexProductBannerResponse;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;

import java.util.HashMap;
import java.util.List;

/**
* IndexService 接口
*/
public interface IndexService {
    IndexProductBannerResponse getProductBanner(int type, PageParamRequest pageParamRequest);

    IndexInfoResponse getIndexInfo();

    List<HashMap<String, Object>> hotKeywords();

    HashMap<String, String> getShareConfig();

    /**
     * 获取公共配置
     * @return 公共配置
     */
    HashMap<String,String> getCommConfig();

    /**
     * 首页 加载 选中产品信息列表
     * @param request
     * @param pageParamRequest
     * @return
     */
    List<TkProduct> getIndexProduct(IndexTkProductSearchRequest request, PageParamRequest pageParamRequest) ;

    /**
     * 搜索查询产品信息列表
     * @param request
     * @param pageParamRequest
     * @return
     */
    List<TkProduct> getSearchProduct(IndexTkProductSearchRequest request, PageParamRequest pageParamRequest) ;

}
