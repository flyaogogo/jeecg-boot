package org.jeecg.modules.toker.tkuser.service;

import org.jeecg.modules.toker.front.request.*;
import org.jeecg.modules.toker.front.response.LoginResponse;
import org.jeecg.modules.toker.front.response.UserCenterResponse;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.toker.tkuser.vo.SysUserComboModel;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 拓客用户信息
 * @Author: Luo.0022
 * @Date:   2020-12-30
 * @Version: V1.0
 */
public interface ITkUserService extends IService<TkUser> {


    LoginResponse login(LoginRequest loginRequest) throws Exception;

    /**
     * 退出
     * @param request HttpServletRequest
     */
    void loginOut(HttpServletRequest request) ;
    /**
     * 通过账号获取用户信息
     * @author Luo.0022
     * @since 2021-01-14
     */
    TkUser getUserByAccount(String account) ;

    /**
     * 绑定手机号
     * @author Luo.0022
     * @since 2021-01-15
     * @return boolean
     */
    boolean bind(UserBindingRequest request) ;

    /**
     * 检测手机验证码
     * @param phone String 手机号
     * @return String
     */
    String getValidateCodeRedisKey(String phone) ;

    /**
     * 获取个人资料
     * @return User
     */
    TkUser getUserPromoter() ;
    /**
     * 获取个人资料
     * @return User
     */
    TkUser getInfoException() ;

    TkUser getInfoEmpty() ;

    /**
     * 获取当前用户id
     * @return String
     */
    String getUserIdException() ;

    /**
     * 获取当前用户id
     * @return Integer
     */
    Integer getUserId() ;

    /**
     * 获取当前用户id
     * @return String
     */
    String getUserIdStr() ;

    /**
     * 获取个人资料
     * @return User
     */
    TkUser getInfo() ;
    /**
     * 修改密码
     * @param request PasswordRequest 密码
     * @return boolean
     */
    boolean password(PasswordRequest request) ;

    /**
     * 用户中心
     * @return UserCenterResponse
     */
    UserCenterResponse getUserCenter() ;

    /**
     * 登录用户生成token
     */
    String token(TkUser user) throws Exception ;

    /**
     * 通过微信信息注册用户
     * @param thirdUserRequest RegisterThirdUser 三方用户登录注册信息
     * @return User
     */
    TkUser registerByThird(RegisterThirdUserRequest thirdUserRequest, String type) ;

    /**
     * 重置连续签到天数
     * @param userId Integer 用户id
     */
    void repeatSignNum(String userId) ;

    /**
     * 操作资金
     */
    boolean updateFounds(UserOperateFundsRequest request, boolean isSaveBill) ;

    /**
     * 等级升级
     * @param userId String 用户id
     */
    void upLevel(String userId) ;

    /**
     * 客户通过报备之后增加经验和积分
     * @param uid String 用户id
     * @param price BigDecimal 实际支付金额
     * @return void
     */
    void consumeAfterUpdateUserFounds(String uid, BigDecimal price, String type) ;

    /**
     * 添加/扣减佣金
     * @param uid 用户id
     * @param price 金额
     * @param brokeragePrice 历史金额
     * @param type 类型：add—添加，sub—扣减
     * @return Boolean
     */
    Boolean operationBrokerage(String uid, BigDecimal price, BigDecimal brokeragePrice, String type) ;

    /**
     * 添加/扣减余额
     * @param uid 用户id
     * @param price 金额
     * @param nowMoney 历史金额
     * @param type 类型：add—添加，sub—扣减
     */
    Boolean operationNowMoney(String uid, BigDecimal price, BigDecimal nowMoney, String type) ;

    /**
     * 添加/扣减积分
     * @param uid 用户id
     * @param integral 积分
     * @param nowIntegral 历史积分
     * @param type 类型：add—添加，sub—扣减
     * @return Boolean
     */
    Boolean operationIntegral(String uid, Integer integral, Integer nowIntegral, String type) ;

    /**
     * add Luo.0022 at 2021-04-20
     * 查询后台系统所有用户 SysUserComboModel
     * @return
     */
    List<SysUserComboModel> queryAllUserBackSysCombo() ;
}
