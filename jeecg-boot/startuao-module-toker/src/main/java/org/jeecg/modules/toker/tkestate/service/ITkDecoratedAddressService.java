package org.jeecg.modules.toker.tkestate.service;

import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 热装小区客户地址表
 * @Author: Luo.0022
 * @Date:   2021-02-15
 * @Version: V1.0
 */
public interface ITkDecoratedAddressService extends IService<TkDecoratedAddress> {

    /**
     * 通过uuid获取实体信息
     * @param uuid
     * @return
     */
    TkDecoratedAddress getTkDecoratedAddressByUUID(String uuid) ;

    /**
     * 通过热装小区Id获取所有住户信息，并组装数据
     * @param cellId
     * @return
     */
    List<TkDecoratedAddress> getUserHouseByCellId(String cellId) ;

    /**
     * 通过小区ID及楼号获取热装住户
     * @param cellId
     * @param buildNum
     * @return
     */
    List<TkDecoratedAddress> getUserHouseByCellIdAndbuildNum(String cellId, String buildNum) ;
}
