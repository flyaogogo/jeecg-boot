package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 拓客用户信息
 * @Author: Luo.0022
 * @Date:   2020-12-30
 * @Version: V1.0
 */
@Data
@TableName("tk_user")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_user对象", description="拓客用户信息")
public class TkUser implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private java.lang.String id;
	/**UUID*/
	@Excel(name = "UUID", width = 15)
    @ApiModelProperty(value = "UUID")
    private java.lang.String uuid;
	/**用户账号*/
	@Excel(name = "用户账号", width = 15)
    @ApiModelProperty(value = "用户账号")
    private java.lang.String account;
	/**用户密码*/
	@Excel(name = "用户密码", width = 15)
    @ApiModelProperty(value = "用户密码")
    private java.lang.String pwd;
	/**真实姓名*/
	@Excel(name = "真实姓名", width = 15)
    @ApiModelProperty(value = "真实姓名")
    private java.lang.String realName;
	/**生日*/
	@Excel(name = "生日", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "生日")
    private java.lang.String birthday;
	/**用户类型*/
	@Excel(name = "用户类型", width = 15, dicCode = "tk_identity")
	@Dict(dicCode = "tk_identity")
    @ApiModelProperty(value = "用户类型")
    private java.lang.String userType;
	/**手机号码*/
	@Excel(name = "手机号码", width = 15)
    @ApiModelProperty(value = "手机号码")
    private java.lang.String phone;
	/**性别*/
	@Excel(name = "性别", width = 15, dicCode = "sex")
	@Dict(dicCode = "sex")
    @ApiModelProperty(value = "性别")
    private java.lang.Integer sex;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "status")
	@Dict(dicCode = "status")
    @ApiModelProperty(value = "状态")
    private java.lang.Integer status;
	/**店内联系人*/
	@Excel(name = "店内联系人", width = 15)
    @ApiModelProperty(value = "店内联系人")
    private java.lang.String contact;
	/**身份证号码*/
	@Excel(name = "身份证号码", width = 15)
    @ApiModelProperty(value = "身份证号码")
    private java.lang.String cardId;
	/**详细地址*/
	@Excel(name = "详细地址", width = 15)
    @ApiModelProperty(value = "详细地址")
    private java.lang.String addres;
	/**用户备注*/
	@Excel(name = "用户备注", width = 15)
    @ApiModelProperty(value = "用户备注")
    private java.lang.String mark;
	/**合伙人id*/
	@Excel(name = "合伙人id", width = 15)
    @ApiModelProperty(value = "合伙人id")
    private java.lang.Integer partnerId;
	/**用户分组id*/
	@Excel(name = "用户分组id", width = 15)
    @ApiModelProperty(value = "用户分组id")
    private java.lang.String groupId;
	/**租户id*/
	@Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;
	/**标签id*/
	@Excel(name = "标签id", width = 15)
    @ApiModelProperty(value = "标签id")
    private java.lang.String tagId;
	/**用户昵称*/
	@Excel(name = "用户昵称", width = 15)
    @ApiModelProperty(value = "用户昵称")
    private java.lang.String nickname;
	/**用户头像*/
	@Excel(name = "用户头像", width = 15)
    @ApiModelProperty(value = "用户头像")
    private java.lang.String avatar;
	/**添加ip*/
	@Excel(name = "添加ip", width = 15)
    @ApiModelProperty(value = "添加ip")
    private java.lang.String addIp;
	/**最后一次登录ip*/
	@Excel(name = "最后一次登录ip", width = 15)
    @ApiModelProperty(value = "最后一次登录ip")
    private java.lang.String lastIp;
	/**用户余额*/
	@Excel(name = "用户余额", width = 15)
    @ApiModelProperty(value = "用户余额")
    private java.math.BigDecimal nowMoney;
	/**佣金金额*/
	@Excel(name = "佣金金额", width = 15)
    @ApiModelProperty(value = "佣金金额")
    private java.math.BigDecimal brokeragePrice;
	/**用户剩余积分*/
	@Excel(name = "用户剩余积分", width = 15)
    @ApiModelProperty(value = "用户剩余积分")
    private java.math.BigDecimal integral;
	/**用户剩余经验*/
	@Excel(name = "用户剩余经验", width = 15)
    @ApiModelProperty(value = "用户剩余经验")
    private java.lang.Integer experience;
	/**连续签到天数*/
	@Excel(name = "连续签到天数", width = 15)
    @ApiModelProperty(value = "连续签到天数")
    private java.lang.Integer signNum;
	/**等级*/
	@Excel(name = "等级", width = 15)
    @ApiModelProperty(value = "等级")
    private java.lang.Integer level;
	/**推广元id*/
	@Excel(name = "推广元id", width = 15)
    @ApiModelProperty(value = "推广元id")
    private java.lang.Integer spreadUid;
	/**推广员关联时间*/
	@Excel(name = "推广员关联时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "推广员关联时间")
    private java.util.Date spreadTime;
	/**是否为推广员*/
	@Excel(name = "是否为推广员", width = 15)
    @ApiModelProperty(value = "是否为推广员")
    private java.lang.Integer isPromoter;
	/**用户购买次数*/
	@Excel(name = "用户购买次数", width = 15)
    @ApiModelProperty(value = "用户购买次数")
    private java.lang.Integer payCount;
	/**下级人数*/
	@Excel(name = "下级人数", width = 15)
    @ApiModelProperty(value = "下级人数")
    private java.lang.Integer spreadCount;
	/**管理员编号 */
	@Excel(name = "管理员编号 ", width = 15)
    @ApiModelProperty(value = "管理员编号 ")
    private java.lang.Integer adminid;
	/**用户登陆类型，h5,wechat,routine*/
	@Excel(name = "用户登陆类型，h5,wechat,routine", width = 15)
    @ApiModelProperty(value = "用户登陆类型，h5,wechat,routine")
    private java.lang.String loginType;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**最后一次登录时间*/
	@Excel(name = "最后一次登录时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "最后一次登录时间")
    private java.util.Date lastLoginTime;
	/**清除时间*/
	@Excel(name = "清除时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "清除时间")
    private java.util.Date cleanTime;
	/**推广等级记录*/
	@Excel(name = "推广等级记录", width = 15)
    @ApiModelProperty(value = "推广等级记录")
    private java.lang.String path;
	/**是否关注公众号*/
	@Excel(name = "是否关注公众号", width = 15)
    @ApiModelProperty(value = "是否关注公众号")
    private java.lang.Integer subscribe;
	/**关注公众号时间*/
	@Excel(name = "关注公众号时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "关注公众号时间")
    private java.util.Date subscribeTime;
	/**国家，中国CN，其他OTHER*/
	@Excel(name = "国家，中国CN，其他OTHER", width = 15)
    @ApiModelProperty(value = "国家，中国CN，其他OTHER")
    private java.lang.String country;
}
