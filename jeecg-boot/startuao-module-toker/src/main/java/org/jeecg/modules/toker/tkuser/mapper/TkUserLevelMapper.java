package org.jeecg.modules.toker.tkuser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkuser.entity.TkUserLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 用户等级记录表
 * @Author: Luo.0022
 * @Date:   2021-01-18
 * @Version: V1.0
 */
public interface TkUserLevelMapper extends BaseMapper<TkUserLevel> {

}
