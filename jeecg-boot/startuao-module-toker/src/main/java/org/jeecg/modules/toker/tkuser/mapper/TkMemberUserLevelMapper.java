package org.jeecg.modules.toker.tkuser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkuser.entity.TkMemberUserLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 普通会员等级
 * @Author: Luo.0022
 * @Date:   2021-01-29
 * @Version: V1.0
 */
public interface TkMemberUserLevelMapper extends BaseMapper<TkMemberUserLevel> {

}
