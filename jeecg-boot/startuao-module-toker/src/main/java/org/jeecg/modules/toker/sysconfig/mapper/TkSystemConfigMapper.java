package org.jeecg.modules.toker.sysconfig.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.sysconfig.entity.TkSystemConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 系统参数配置
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
public interface TkSystemConfigMapper extends BaseMapper<TkSystemConfig> {

}
