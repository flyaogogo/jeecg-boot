package org.jeecg.modules.toker.tkuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.tkuser.entity.TkMemberUserLevel;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.entity.TkUserLevel;
import org.jeecg.modules.toker.tkuser.mapper.TkMemberUserLevelMapper;
import org.jeecg.modules.toker.tkuser.mapper.TkUserLevelMapper;
import org.jeecg.modules.toker.tkuser.service.ITkMemberUserLevelService;
import org.jeecg.modules.toker.tkuser.service.ITkUserLevelService;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;

/**
 * @Description: 用户等级记录表
 * @Author: Luo.0022
 * @Date:   2021-01-18
 * @Version: V1.0
 */
@Service
public class TkUserLevelServiceImpl extends ServiceImpl<TkUserLevelMapper, TkUserLevel> implements ITkUserLevelService {

    @Autowired
    private TkUserLevelMapper userLevelMapper ;

    @Autowired
    private ITkMemberUserLevelService memberUserLevelService ;

    @Autowired
    private ITkUserService userService ;
    /**
     * 修改用户等级
     * @param userId integer id
     * @param levelId integer 等级
     * @return Boolean
     */
    @Override
    public boolean level(String userId, String levelId) {
        TkMemberUserLevel systemUserLevel = memberUserLevelService.getById(levelId);
        TkUser user = userService.getById(userId);
        TkUserLevel userLevelVo = checkUserLevel(userId, levelId, systemUserLevel);

        TkUserLevel userLevel = new TkUserLevel();
        userLevel.setStatus(1);
        userLevel.setRemind(0) ; //add Luo.0022 默认为0，未通知
        userLevel.setIsDel(0);
        userLevel.setGrade(systemUserLevel.getGrade());
        userLevel.setUid(userId);
        userLevel.setLevelId(levelId);
        userLevel.setDiscount(systemUserLevel.getDiscount());

        Date date = DateUtil.nowDateTimeReturnDate(TokerCommonConstant.DATE_FORMAT);
        String mark = TokerCommonConstant.USER_LEVEL_OPERATE_LOG_MARK.replace("【{$userName}】", user.getNickname()).
                replace("{$date}", DateUtil.dateToStr(date, TokerCommonConstant.DATE_FORMAT)).
                replace("{$levelName}", systemUserLevel.getName());
        userLevel.setMark(mark);

        if(userLevelVo == null){
            //创建新的会员等级信息
            save(userLevel);
        }else{
            //有数据，更新即可
            userLevel.setId(userLevelVo.getId());
            updateById(userLevel);
        }

        //更新会员等级
        user.setLevel(systemUserLevel.getGrade());
        userService.updateById(user);
        return true;
    }

    /**
     * 获取会员的等级信息，并且验证
     * @param userId String id
     * @param levelId String 等级
     * @return Boolean
     */
    private TkUserLevel checkUserLevel(String userId, String levelId, TkMemberUserLevel systemUserLevel) {
        //查询等级是否存在
        if(systemUserLevel == null){
            throw new JeecgBootException("当前会员等级不存在");
        }

        if(systemUserLevel.getIsDel()==1){
            throw new JeecgBootException("当前会员等级已删除");
        }

        //当前用户会员是否过期
        return getUserLevel(userId, levelId);
    }

    /**
     * 获取会员的等级信息
     * @param userId String id
     * @param levelId String id
     * @return Boolean
     */
    private TkUserLevel getUserLevel(String userId, String levelId) {
        LambdaQueryWrapper<TkUserLevel> levelLambdaQueryWrapper = new LambdaQueryWrapper<>();
        levelLambdaQueryWrapper.eq(TkUserLevel::getUid, userId);
        levelLambdaQueryWrapper.eq(TkUserLevel::getLevelId, levelId);
        levelLambdaQueryWrapper.eq(TkUserLevel::getIsDel, 0);
        return userLevelMapper.selectOne(levelLambdaQueryWrapper);
    }
}
