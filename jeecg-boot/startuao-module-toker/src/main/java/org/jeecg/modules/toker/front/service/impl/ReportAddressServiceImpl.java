package org.jeecg.modules.toker.front.service.impl;


import org.jeecg.modules.toker.authorization.manager.TokenManager;
import org.jeecg.modules.toker.front.request.ReportAddressRequest;
import org.jeecg.modules.toker.front.service.ReportAddressService;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import org.jeecg.modules.toker.tkestate.service.ITkDecoratedAddressService;
import org.jeecg.modules.toker.tkestate.service.ITkHousingEstateService;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import org.jeecg.modules.toker.tkinforeport.service.ITkInfoReportService;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.tkvaddress.entity.TkVillageAddress;
import org.jeecg.modules.toker.tkvaddress.service.ITkVillageAddressService;
import org.jeecg.modules.toker.utils.CrmebUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

@Service
public class ReportAddressServiceImpl implements ReportAddressService {

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ITkInfoReportService tkInfoReportService;

    @Autowired
    private ITkHousingEstateService housingEstateService ;

    @Autowired
    private ITkDecoratedAddressService decoratedAddressService ;

    @Autowired
    private ITkVillageAddressService villageAddressService ;

    @Autowired
    private TokenManager tokenManager;

    @Autowired
    private ITkUserService userService ;

    /**
     * 报备信息添加
     * 先对用户地址添加到临时表中 : tk_village_address
     * 审批通过后,再添加到正式的表中。
     * @param reportReq
     * @return
     */
    public boolean addReportAndAddrs(ReportAddressRequest reportReq) {
        TkInfoReport tkInfoReport = new TkInfoReport() ;
        BeanUtils.copyProperties(reportReq, tkInfoReport) ;

        TkUser user = userService.getInfo(); // 增加租户ID

        tkInfoReport.setTenantId(user.getTenantId()) ;

        Boolean execute = transactionTemplate.execute(e -> {
            /**
             * 小程序端为区分是否新增的地址Id，特以‘1234567890’为标识
             * 新增地址Id，则存储到TkInfoReport中的address字段，为存储临时表中的Id。
             * 选择的存在的地址，则小区地址ID 存储到kInfoReport中的tkaddrId字段。
             */
            if (reportReq.getAddressStatus().contains("add")) {
                TkVillageAddress villAddress = new TkVillageAddress();

                BeanUtils.copyProperties(reportReq, villAddress);

                // 此uuid为获取新增数据，并能方便查询
//                String uuid = CrmebUtil.getUuid();
//                villAddress.setUuid(uuid);

                villAddress.setHouseAddress(joinAddressInfo(villAddress));

                villAddress.setTenantId(user.getTenantId()) ;

                villageAddressService.save(villAddress);

                // 获取新添加地址ID
//                TkVillageAddress address = villageAddressService.getTkVillageAddressByUUID(uuid);

                // 保存报备信息
                tkInfoReport.setAddress(villAddress.getId()); // 从保存中的villaddress中的ID获取
                if ("addHouse".equals(reportReq.getAddressStatus())) {
                    tkInfoReport.setTkaddrId(reportReq.getRealEstateId());// 存放的是小区地址ID
                }
            } else {
                tkInfoReport.setAddress(reportReq.getDecoratedAddressId());// 存中不存在，则存放的是临时表中的地址ID，审批通过后存放的是正式库中的ID
                tkInfoReport.setTkaddrId(reportReq.getRealEstateId());// 存放的是小区地址ID
            }

            tkInfoReport.setAddressTemp(reportReq.getRealEstate() + "-" + reportReq.getHouseAddress());
            tkInfoReport.setTkuserId(tokenManager.getLocalInfoException("id"));
            tkInfoReportService.save(tkInfoReport);

            return Boolean.TRUE ;
        });
        return execute ;
    }

    /**
     * 拼装 业住 地址信息
     * @param va
     */
    private String joinAddressInfo(TkVillageAddress va) {
        StringBuffer str = new StringBuffer() ;
        str.append(va.getBuildingNumber()).append("幢") ;
        str.append("-") ;
        str.append(va.getUnit()).append("单元") ;
        str.append("-") ;
        str.append(va.getFloor()).append("层") ;
        str.append("-") ;
        str.append(va.getHouseNumber()).append("室") ;

        return str.toString() ;
    }

    /**
     * 查找所有热门小区
     * @param keywords
     * @return
     */
    @Override
    public List<TkHousingEstate> getAllHotCells(String keywords) {
//        LambdaQueryWrapper<TkHousingEstate> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//        lambdaQueryWrapper.eq(TkHousingEstate::getIsShow,'Y') ;
//        QueryWrapper<TkHousingEstate> wrapper = Wrappers.query();
//        wrapper.eq("isShow",'Y') ;

//        Wrapper<TkHousingEstate> wrapper = Wrappers.query();
//        wrapper.eq("isShow","Y") ;
        List<TkHousingEstate> list = housingEstateService.list() ;
        return list ;
    }

    /**
     * 通过楼盘ID获取对应小区热装住户地址
     * @param estateId
     * @return
     */
    @Override
    public List<TkDecoratedAddress> getUserAddressByCellId(String estateId) {
        List<TkDecoratedAddress> addressList = decoratedAddressService.getUserHouseByCellId(estateId) ;
        return addressList ;
    }

    /**
     * 通过用户ID，列出所有的报备信息
     * @param userId
     * @return
     */
    @Override
    public List<TkInfoReport> getMyReportsByUserId(String userId) {
        List<TkInfoReport> list = tkInfoReportService.getMyReportListByUserId(userId) ;
        return list ;
    }


}
