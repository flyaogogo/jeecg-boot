package org.jeecg.modules.toker.tkuser.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.FundsMonitorSearchRequest;
import org.jeecg.modules.toker.tkuser.entity.TkUserBill;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 用户账单表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
public interface ITkUserBillService extends IService<TkUserBill> {

    /**
     * 新增/消耗 总数
     * @param pm Integer 0 = 支出 1 = 获得
     * @param userId String 用户uid
     * @param category String 类型
     * @param date String 时间范围
     * @param type String 小类型
     * @return UserBill
     */
    Integer getSumInteger(Integer pm, String userId, String category, String date, String type) ;

    /**
     * 新增/消耗  总金额
     * @param pm Integer 0 = 支出 1 = 获得
     * @param userId String 用户uid
     * @param category String 类型
     * @param date String 时间范围
     * @param type String 小类型
     * @return UserBill
     */
    BigDecimal getSumBigDecimal(Integer pm, String userId, String category, String date, String type) ;

    /**
     * 列表
     * @param request 请求参数
     * @param pageParamRequest 分页类参数
     * @return List<TkUserBill>
     */
    IPage<TkUserBill> getList(FundsMonitorSearchRequest request, PageParamRequest pageParamRequest) ;

    /**
     * 用户账单记录（现金）
     * @param uid 用户uid
     * @param type 记录类型：all-全部，expenditure-支出，income-收入
     * @return
     */
    List<TkUserBill> nowMoneyBillRecord(String uid, String type, PageParamRequest pageRequest) ;
}
