package org.jeecg.modules.toker.tkproduct.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkproduct.entity.TkProductRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 产品规则值(规格)表
 * @Author: Luo.0022
 * @Date:   2021-01-07
 * @Version: V1.0
 */
public interface TkProductRuleMapper extends BaseMapper<TkProductRule> {

}
