package org.jeecg.modules.toker.tkinforeport.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import org.jeecg.modules.toker.tkinforeport.service.ITkFollowupCustomService;
import org.jeecg.modules.toker.tkinforeport.service.ITkInfoReportService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.toker.tkinforeport.vo.TkInfoReportPage;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 拓客用户报备信息
 * @Author: Luo.0022
 * @Date:   2021-01-02
 * @Version: V1.0
 */
@Api(tags="拓客用户报备信息")
@RestController
@RequestMapping("/tkinforeport/tkInfoReport")
@Slf4j
public class TkInfoReportController extends JeecgController<TkInfoReport, ITkInfoReportService> {
	@Autowired
	private ITkInfoReportService tkInfoReportService;

	 @Autowired
	 private ITkFollowupCustomService followupCustomService ;
	/**
	 * 分页列表查询
	 *
	 * @param tkInfoReport
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "拓客用户报备信息-分页列表查询")
	@ApiOperation(value="拓客用户报备信息-分页列表查询", notes="拓客用户报备信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkInfoReport tkInfoReport,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkInfoReport> queryWrapper = QueryGenerator.initQueryWrapper(tkInfoReport, req.getParameterMap());
		Page<TkInfoReport> page = new Page<TkInfoReport>(pageNo, pageSize);
		IPage<TkInfoReport> pageList = tkInfoReportService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	 /**
	  * 分页列表查询
	  *
	  * @param tkInfoReport
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "拓客用户报备信息-分页列表查询")
	 @ApiOperation(value="拓客用户报备信息-分页列表查询", notes="拓客用户报备信息-分页列表查询")
	 @GetMapping(value = "/followuplist")
	 public Result<?> queryGuideFollowUpPageList(TkInfoReport tkInfoReport,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 /*
		 QueryWrapper<TkInfoReport> queryWrapper = QueryGenerator.initQueryWrapper(tkInfoReport, req.getParameterMap());
		 Page<TkInfoReport> page = new Page<TkInfoReport>(pageNo, pageSize);
		 */
		 IPage<TkInfoReport> pageList = tkInfoReportService.guideFollowupCustomList(tkInfoReport,pageNo,pageSize,req) ;
		 return Result.OK(pageList);
	 }
	/**
	 *   添加
	 *
	 * @param tkInfoReport
	 * @return
	 */
	@AutoLog(value = "拓客用户报备信息-添加")
	@ApiOperation(value="拓客用户报备信息-添加", notes="拓客用户报备信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkInfoReport tkInfoReport) {
		tkInfoReportService.save(tkInfoReport);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkInfoReport
	 * @return
	 */
	@AutoLog(value = "拓客用户报备信息-编辑")
	@ApiOperation(value="拓客用户报备信息-编辑", notes="拓客用户报备信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkInfoReport tkInfoReport) {
		tkInfoReportService.updateById(tkInfoReport);
		return Result.OK("编辑成功!");
	}

	 /**
	  *  编辑
	  *
	  * @param tkInfoReportPage
	  * @return
	  */
	 @AutoLog(value = "信息报备表-编辑")
	 @ApiOperation(value="信息报备表-编辑", notes="信息报备表-编辑")
	 @PutMapping(value = "/follow/edit")
	 public Result<?> edit(@RequestBody TkInfoReportPage tkInfoReportPage) {
		 TkInfoReport tkInfoReport = new TkInfoReport();
		 BeanUtils.copyProperties(tkInfoReportPage, tkInfoReport);
		 TkInfoReport tkInfoReportEntity = tkInfoReportService.getById(tkInfoReport.getId());
		 if(tkInfoReportEntity==null) {
			 return Result.error("未找到对应数据");
		 }
		 tkInfoReportService.updateMain(tkInfoReport, tkInfoReportPage.getTkFollowupCustomList());
		 return Result.OK("编辑成功!");
	 }
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "拓客用户报备信息-通过id删除")
	@ApiOperation(value="拓客用户报备信息-通过id删除", notes="拓客用户报备信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkInfoReportService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "拓客用户报备信息-批量删除")
	@ApiOperation(value="拓客用户报备信息-批量删除", notes="拓客用户报备信息-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkInfoReportService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "拓客用户报备信息-通过id查询")
	@ApiOperation(value="拓客用户报备信息-通过id查询", notes="拓客用户报备信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkInfoReport tkInfoReport = tkInfoReportService.getById(id);
		if(tkInfoReport==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkInfoReport);
	}
	 /**
	  * 通过id查询
	  *
	  * @param id
	  * @return
	  */
	 @AutoLog(value = "导购跟跟踪信息记录通过主表ID查询")
	 @ApiOperation(value="导购跟跟踪信息记录主表ID查询", notes="导购跟跟踪信息记录-通主表ID查询")
	 @GetMapping(value = "/queryTkFollowupCustomByMainId")
	 public Result<?> queryTkFollowupCustomListByMainId(@RequestParam(name="id",required=true) String id) {
		 List<TkFollowupCustom> tkFollowupCustomList = followupCustomService.selectByMainId(id);
		 return Result.OK(tkFollowupCustomList);
	 }
    /**
    * 导出excel
    *
    * @param request
    * @param tkInfoReport
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkInfoReport tkInfoReport) {
        return super.exportXls(request, tkInfoReport, TkInfoReport.class, "拓客用户报备信息");
    }

	 /**
	  * 导出导购跟进用户信息 excel
	  *
	  * @param request
	  * @param tkInfoReport
	  */
	 @RequestMapping(value = "/exportGuideXls")
	 public ModelAndView exportGuideInfoXls(HttpServletRequest request, TkInfoReport tkInfoReport) {
		 return super.exportXls(request, tkInfoReport, TkInfoReport.class, "导购跟进用户信息");
	 }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkInfoReport.class);
    }

	 /**
	  *  审批信息是否正确
	  *
	  * @param tkInfoReport
	  * @return
	  */
	 @AutoLog(value = "拓客用户报备信息-审批")
	 @ApiOperation(value="拓客用户报备信息-审批", notes="拓客用户报备信息-审批")
	 @PutMapping(value = "/approval")
	 public Result<?> approval(@RequestBody TkInfoReport tkInfoReport) {
		 tkInfoReportService.approvalReportInfoByUid(tkInfoReport);
		 return Result.OK("审批成功!");
	 }
}
