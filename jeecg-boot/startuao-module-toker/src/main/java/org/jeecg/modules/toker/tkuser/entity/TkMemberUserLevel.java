package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 普通会员等级
 * @Author: Luo.0022
 * @Date:   2021-01-29
 * @Version: V1.0
 */
@Data
@TableName("tk_member_user_level")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_member_user_level对象", description="普通会员等级")
public class TkMemberUserLevel implements Serializable {
    private static final long serialVersionUID = 1L;

	/**会员等级ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "会员等级ID")
    private String id;
	/**会员名称*/
	@Excel(name = "会员名称", width = 15)
    @ApiModelProperty(value = "会员名称")
    private String name;
	/**达到多少升级经验*/
	@Excel(name = "达到多少升级经验", width = 15)
    @ApiModelProperty(value = "达到多少升级经验")
    private Integer experience;
	/**是否显示*/
	@Excel(name = "是否显示", width = 15)
    @ApiModelProperty(value = "是否显示")
    private Integer isShow;
	/**会员等级*/
	@Excel(name = "会员等级", width = 15)
    @ApiModelProperty(value = "会员等级")
    private Integer grade;
	/**享受折扣*/
	@Excel(name = "享受折扣", width = 15)
    @ApiModelProperty(value = "享受折扣")
    private BigDecimal discount;
	/**会员卡背景*/
	@Excel(name = "会员卡背景", width = 15)
    @ApiModelProperty(value = "会员卡背景")
    private String image;
	/**会员图标*/
	@Excel(name = "会员图标", width = 15)
    @ApiModelProperty(value = "会员图标")
    private String icon;
	/**说明*/
	@Excel(name = "说明", width = 15)
    @ApiModelProperty(value = "说明")
    private String memo;
	/**是否删除*/
	@Excel(name = "是否删除", width = 15)
    @ApiModelProperty(value = "是否删除")
    private Integer isDel;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;

}
