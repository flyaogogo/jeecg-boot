package org.jeecg.modules.toker.tkuser.service;

import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.UserAddressRequest;
import org.jeecg.modules.toker.tkuser.entity.TkUserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 用户地址表
 * @Author: Luo.0022
 * @Date:   2021-03-02
 * @Version: V1.0
 */
public interface ITkUserAddressService extends IService<TkUserAddress> {

    List<TkUserAddress> getList(PageParamRequest pageParamRequest);

    /**
     * 根据基本条件查询
     * @param address 查询条件
     * @return 查询到的地址
     */
    TkUserAddress getUserAddress(TkUserAddress address);

    TkUserAddress create(UserAddressRequest request);

    boolean def(String id);

    boolean delete(String id);

    TkUserAddress getDefault();

    TkUserAddress getById(Integer addressId);

    /**
     * 根据地址参数获取用户收货地址
     * @param userAddress
     * @param pageParamRequest
     * @return
     */
    List<TkUserAddress> getListByUserAddress(TkUserAddress userAddress, PageParamRequest pageParamRequest);
}
