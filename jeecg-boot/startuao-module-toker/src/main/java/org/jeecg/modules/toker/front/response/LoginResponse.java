package org.jeecg.modules.toker.front.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.modules.toker.tkuser.entity.TkUser;

import java.io.Serializable;
import java.util.Date;

/**
 * Login Response
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="LoginResponse", description="用户登录返回数据")
public class LoginResponse implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户登录密钥")
    private String token;

    @ApiModelProperty(value = "用户登录密钥到期时间")
    private Date expiresTime;

    @ApiModelProperty(value = "user对象")
    private TkUser user;

    @ApiModelProperty(value = "session_key")
    private String sessionKey;

}
