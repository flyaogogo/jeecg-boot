package org.jeecg.modules.toker.front.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.toker.front.service.HotBuildingService;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.service.ITkDecoratedAddressService;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import org.jeecg.modules.toker.tkproduct.service.ITkProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HotBuildingServiceImpl implements HotBuildingService {

    @Autowired
    private ITkDecoratedAddressService decoratedAddressService ;

    @Autowired
    private ITkProductService productService ;

    /**
     * 通过热装小区Id获取所有住户信息，并组装数据
     * @param cellId
     * @return
     */
    @Override
    public Map<String, List<TkDecoratedAddress>> getUserHouseByCellId(String cellId) {
        Map<String, List<TkDecoratedAddress>> map = new HashMap<>() ;
        List<TkDecoratedAddress> addrTempList ;

        List<TkDecoratedAddress> hotHouseList = decoratedAddressService.getUserHouseByCellId(cellId) ;
        for(TkDecoratedAddress address : hotHouseList){
            List<TkDecoratedAddress> houseInfoList = map.get(address.getBuildingNumber()) ;
            if(houseInfoList == null){
                addrTempList = new ArrayList<>();
                addrTempList.add(address) ;
                map.put(address.getBuildingNumber(),addrTempList) ;
            } else {
                houseInfoList.add(address) ;
            }
        }




        return map;
    }

    /**
     * 通过id表，查找所有的产品信息
     * @param ids
     * @return
     */
    @Override
    public List<TkProduct> getProductsByIds(String ids) {

        return productService.getProductsByIds(ids);
    }
}
