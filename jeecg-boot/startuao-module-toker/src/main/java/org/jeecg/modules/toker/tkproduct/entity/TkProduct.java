package org.jeecg.modules.toker.tkproduct.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 产品详细信息
 * @Author: Luo.0022
 * @Date:   2021-01-05
 * @Version: V1.0
 */
@Data
@TableName("tk_product")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_product对象", description="产品详细信息")
public class TkProduct implements Serializable {
    private static final long serialVersionUID = 1L;

	/**产品id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "产品id")
    private java.lang.String id;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private java.lang.String productName;
	/**产品简介*/
	@Excel(name = "产品简介", width = 15)
    @ApiModelProperty(value = "产品简介")
    private java.lang.String productInfo;
	/**产品型号*/
	@Excel(name = "产品型号", width = 15)
    @ApiModelProperty(value = "产品型号")
    private java.lang.String productModel;
	/**产品图片*/
	@Excel(name = "产品图片", width = 15)
    @ApiModelProperty(value = "产品图片")
    private java.lang.String image;
	/**轮播图*/
	@Excel(name = "轮播图", width = 15)
    @ApiModelProperty(value = "轮播图")
    private java.lang.String sliderImage;
	/**关键字*/
	@Excel(name = "关键字", width = 15)
    @ApiModelProperty(value = "关键字")
    private java.lang.String keywords;
	/**产品分类*/
	@Excel(name = "产品分类", width = 15)
    @ApiModelProperty(value = "产品分类")
    private java.lang.String cateId;
	/**销量*/
	@Excel(name = "销量", width = 15)
    @ApiModelProperty(value = "销量")
    private java.lang.Integer sales;
	/**库存*/
	@Excel(name = "库存", width = 15)
    @ApiModelProperty(value = "库存")
    private java.lang.Integer stock;
	/**是否上架*/
	@Excel(name = "是否上架", width = 15)
    @ApiModelProperty(value = "是否上架")
    private java.lang.String isShow;
	/**是否热卖*/
	@Excel(name = "是否热卖", width = 15)
    @ApiModelProperty(value = "是否热卖")
    private java.lang.String isHot;
	/**是否精品*/
	@Excel(name = "是否精品", width = 15)
    @ApiModelProperty(value = "是否精品")
    private java.lang.String isBest;
	/**是否新品*/
	@Excel(name = "是否新品", width = 15)
    @ApiModelProperty(value = "是否新品")
    private java.lang.String isNew;
    /**是否优惠*/
//    @Excel(name = "是否优惠", width = 15)
//    @ApiModelProperty(value = "是否优惠")
//    private java.lang.String isBenefit;


	/**单位名*/
	@Excel(name = "单位名", width = 15)
    @ApiModelProperty(value = "单位名")
    private java.lang.String unitName;
	/**产品详情*/
	@Excel(name = "产品详情", width = 15)
    @ApiModelProperty(value = "产品详情")
    private java.lang.String description;
	/**浏览量*/
	@Excel(name = "浏览量", width = 15)
    @ApiModelProperty(value = "浏览量")
    private java.lang.Integer browse;
	/**产品二维码地址(用户小程序海报)*/
	@Excel(name = "产品二维码地址(用户小程序海报)", width = 15)
    @ApiModelProperty(value = "产品二维码地址(用户小程序海报)")
    private java.lang.String codePath;
	/**规格 0单 1多*/
	@Excel(name = "规格 0单 1多", width = 15)
    @ApiModelProperty(value = "规格 0单 1多")
    private java.lang.Integer specType;
	/**产品规格*/
	@Excel(name = "产品规格", width = 15)
    @ApiModelProperty(value = "产品规格")
    private java.lang.String proRuleId;
	/**排序*/
	@Excel(name = "排序", width = 15)
    @ApiModelProperty(value = "排序")
    private java.lang.Integer sort;
	/**租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)*/
	@Excel(name = "租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)", width = 15)
    @ApiModelProperty(value = "租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)")
    private java.lang.String tenantId;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
}
