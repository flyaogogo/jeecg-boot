package org.jeecg.modules.toker.tkuser.service;

import org.jeecg.modules.toker.front.request.UserLevelSearchRequest;
import org.jeecg.modules.toker.tkuser.entity.TkMemberUserLevel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 普通会员等级
 * @Author: Luo.0022
 * @Date:   2021-01-29
 * @Version: V1.0
 */
public interface ITkMemberUserLevelService extends IService<TkMemberUserLevel> {

    /**
     * 分页显示设置用户等级表
     * @param request 搜索条件
     * @return List<TkMemberUserLevel>
     */
    List<TkMemberUserLevel> getList(UserLevelSearchRequest request) ;
}
