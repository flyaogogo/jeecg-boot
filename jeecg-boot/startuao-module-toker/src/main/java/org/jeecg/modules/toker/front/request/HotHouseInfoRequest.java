package org.jeecg.modules.toker.front.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * 热装小区住户查询
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="HotHouseInfoRequest对象", description="热装小区住户查询")
public class HotHouseInfoRequest implements Serializable {

    private static final long serialVersionUID=1L;

    /**小区名称*/
    private String estateName;

    /**
     * 小区ID
     */
    private String estateId;

    /**楼栋号*/
    private String buildingNumber;

    /**
     * 关键字查询
     */
    private String keyword;

}
