package org.jeecg.modules.toker.tkuser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkuser.entity.TkUserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 用户地址表
 * @Author: Luo.0022
 * @Date:   2021-03-02
 * @Version: V1.0
 */
public interface TkUserAddressMapper extends BaseMapper<TkUserAddress> {

}
