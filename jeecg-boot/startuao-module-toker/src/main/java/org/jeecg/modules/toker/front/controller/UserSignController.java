package org.jeecg.modules.toker.front.controller;



import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.UserSignInfoRequest;
import org.jeecg.modules.toker.front.response.UserSignInfoResponse;
import org.jeecg.modules.toker.tkuser.service.ITkUserSignService;
import org.jeecg.modules.toker.tkuser.vo.UserSignConfigVo;
import org.jeecg.modules.toker.tkuser.vo.UserSignMonthVo;
import org.jeecg.modules.toker.tkuser.vo.UserSignVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;


/**
 * 签到记录表 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("api/front/user/sign")
@Api(tags = "用户 -- 签到")
public class UserSignController {

    @Autowired
    private ITkUserSignService userSignService;

    /**
     * 签到列表
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2020-04-30
     */
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result<Page<UserSignVo>>  getList(@Validated PageParamRequest pageParamRequest){
//      Page<UserSignVo> userSignCommonPage = Page.restPage(userSignService.getList(pageParamRequest));
//      return Result.OK(userSignCommonPage);

        Page<UserSignVo> pageList = userSignService.getUserSignList(pageParamRequest) ;
        return Result.OK(pageList);
    }

    /**
     * 签到列表，年月纬度
     * @param pageParamRequest 分页参数
     * @author Mr.Zhang
     * @since 2020-04-30
     */
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/month", method = RequestMethod.GET)
    public Result<Page<UserSignMonthVo>>  getListGroupMonth(@Validated PageParamRequest pageParamRequest){
        Page<UserSignMonthVo> userSignCommonPage = userSignService.getListGroupMonth(pageParamRequest);
        return Result.OK(userSignCommonPage);
    }

    /**
     * 配置
     */
    @ApiOperation(value = "配置")
    @RequestMapping(value = "/config", method = RequestMethod.GET)
    public Result<Page<UserSignConfigVo>> config(){
        Page<UserSignConfigVo> signConfigVoCommonPage = userSignService.config();
        return Result.OK(signConfigVoCommonPage);
    }

    /**
     * 查询签到记录表信息
     */
    @ApiOperation(value = "签到")
    @RequestMapping(value = "/integral", method = RequestMethod.GET)
    public Result<UserSignConfigVo> info(){
        return Result.OK(userSignService.sign());
    }

    /**
     * 查询签到记录表信息
     * @author Mr.Zhang
     * @since 2020-04-30
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Result<HashMap<String, Object>> get(){
        return Result.OK(userSignService.get());
    }

    /**
     * 查询签到记录表信息
     * @author Mr.Zhang
     * @since 2020-04-30
     */
    @ApiOperation(value = "签到用户信息")
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Result<UserSignInfoResponse> getUserInfo(@RequestBody @Validated UserSignInfoRequest request){
        return Result.OK(userSignService.getUserInfo(request));
    }
}



