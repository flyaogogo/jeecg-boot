package org.jeecg.modules.toker.tkvaddress.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkvaddress.entity.TkVillageAddress;
import org.jeecg.modules.toker.tkvaddress.mapper.TkVillageAddressMapper;
import org.jeecg.modules.toker.tkvaddress.service.ITkVillageAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 业主装修地址
 * @Author: Luo.0022
 * @Date:   2020-12-31
 * @Version: V1.0
 */
@Service
public class TkVillageAddressServiceImpl extends ServiceImpl<TkVillageAddressMapper, TkVillageAddress> implements ITkVillageAddressService {

    @Autowired
    private TkVillageAddressMapper villageAddressMapper ;
    /**
     * 通过uuid获取实体信息
     * @param uuid
     * @return
     */
    @Override
    public TkVillageAddress getTkVillageAddressByUUID(String uuid) {
        LambdaQueryWrapper<TkVillageAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkVillageAddress::getUuid, uuid);
        TkVillageAddress address = villageAddressMapper.selectOne(lambdaQueryWrapper);
        return address ;
    }
}
