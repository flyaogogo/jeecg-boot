package org.jeecg.modules.toker.tkuser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkuser.entity.TkUserBrokerageRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 用户佣金记录表
 * @Author: Luo.0022
 * @Date:   2021-02-27
 * @Version: V1.0
 */
public interface TkUserBrokerageRecordMapper extends BaseMapper<TkUserBrokerageRecord> {

}
