package org.jeecg.modules.toker.tkuser.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.FundsMonitorSearchRequest;
import org.jeecg.modules.toker.tkuser.entity.TkUserBill;
import org.jeecg.modules.toker.tkuser.entity.TkUserSign;
import org.jeecg.modules.toker.tkuser.mapper.TkUserBillMapper;
import org.jeecg.modules.toker.tkuser.service.ITkUserBillService;
import org.jeecg.modules.toker.utils.DateLimitUtilVo;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.List;

import static cn.hutool.core.util.BooleanUtil.or;

/**
 * @Description: 用户账单表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
@Service
public class TkUserBillServiceImpl extends ServiceImpl<TkUserBillMapper, TkUserBill> implements ITkUserBillService {

    @Autowired
    private TkUserBillMapper userBillMapper ;

    /**
     * 新增/消耗 总数
     * @param pm Integer 0 = 支出 1 = 获得
     * @param userId String 用户uid
     * @param category String 类型
     * @param date String 时间范围
     * @param type String 小类型
     * @return TkUserBill
     */
    @Override
    public Integer getSumInteger(Integer pm, String userId, String category, String date, String type) {
        QueryWrapper<TkUserBill> queryWrapper = new QueryWrapper<>();


        queryWrapper.select("sum(number) as number").
                eq("category", category).
                eq("uid", userId).
                eq("status", 1);
        if(null != pm){
            queryWrapper.eq("pm", pm);
        }
        if(null != type){
            queryWrapper.eq("type", type);
        }
        if(null != date){
            DateLimitUtilVo dateLimit = DateUtil.getDateLimit(date);
            queryWrapper.between("create_time", dateLimit.getStartTime(), dateLimit.getEndTime());
        }
        TkUserBill userBill = userBillMapper.selectOne(queryWrapper);
        if(null == userBill || null == userBill.getNumber()){
            return 0;
        }
        return userBill.getNumber().intValue();
    }

    /**
     * 新增/消耗  总金额
     * @param pm Integer 0 = 支出 1 = 获得
     * @param userId String 用户uid
     * @param category String 类型
     * @param date String 时间范围
     * @param type String 小类型
     * @return TkUserBill
     */
    @Override
    public BigDecimal getSumBigDecimal(Integer pm, String userId, String category, String date, String type) {
        QueryWrapper<TkUserBill> queryWrapper = new QueryWrapper<>();


        queryWrapper.select("sum(number) as number").
                eq("category", category).
                eq("status", 1);
        if (ObjectUtil.isNotNull(userId)) {
            queryWrapper.eq("uid", userId);
        }
        if(null != pm){
            queryWrapper.eq("pm", pm);
        }
        if(null != type){
            queryWrapper.eq("type", type);
        }
        if(null != date){
            DateLimitUtilVo dateLimit = DateUtil.getDateLimit(date);
            queryWrapper.between("create_time", dateLimit.getStartTime(), dateLimit.getEndTime());
        }
        TkUserBill userBill = userBillMapper.selectOne(queryWrapper);
        if(null == userBill || null == userBill.getNumber()){
            return BigDecimal.ZERO;
        }
        return userBill.getNumber();
    }

    /**
     * 列表
     * @param request 请求参数
     * @param pageParamRequest 分页类参数
     * @return List<TkUserBill>
     */
    @Override
    public IPage<TkUserBill> getList(FundsMonitorSearchRequest request, PageParamRequest pageParamRequest) {
        Page<TkUserBill> page = new Page<>(pageParamRequest.getPage(), pageParamRequest.getLimit());
        QueryWrapper<TkUserBill> queryWrapper = new QueryWrapper<>();
        getMonthSql(request, queryWrapper);

        //排序
        if(request.getSort() == null){
            queryWrapper.orderByDesc("create_time");
        }else{
            if(request.getSort().equals("asc")){
                queryWrapper.orderByAsc("number");
            }else{
                queryWrapper.orderByDesc("number");
            }
        }

        // 查询类型
        if(StringUtils.isNotBlank(request.getCategory())){
            queryWrapper.eq("category", request.getCategory());
        }
        if (ObjectUtil.isNotNull(request.getPm())) {
            queryWrapper.eq("pm", request.getPm());
        }

        IPage<TkUserBill> userBillIPage = userBillMapper.selectPage(page, queryWrapper) ;
        return userBillIPage ;
    }

    private void getMonthSql(FundsMonitorSearchRequest request, QueryWrapper<TkUserBill> queryWrapper){
        queryWrapper.gt("status", 0); // -1无效
        if(!StringUtils.isBlank(request.getKeywords())){
            queryWrapper.and(i -> i.
                    or().eq("id", request.getKeywords()).   //用户账单id
                    or().eq("uid", request.getKeywords()). //用户uid
                    or().eq("link_id", request.getKeywords()). //关联id
                    or().like("title", request.getKeywords()) //账单标题
            );
        }

        //时间范围
        if(StringUtils.isNotBlank(request.getDateLimit())){
            DateLimitUtilVo dateLimit = DateUtil.getDateLimit(request.getDateLimit());
            //判断时间
            int compareDateResult = DateUtil.compareDate(dateLimit.getEndTime(), dateLimit.getStartTime(), TokerCommonConstant.DATE_FORMAT);
            if(compareDateResult == -1){
                throw new JeecgBootException("开始时间不能大于结束时间！");
            }

            queryWrapper.between("create_time", dateLimit.getStartTime(), dateLimit.getEndTime());

            //资金范围
            if(request.getMax() != null && request.getMin() != null){
                //判断时间
                if(request.getMax().compareTo(request.getMin()) < 0){
                    throw new JeecgBootException("最大金额不能小于最小金额！");
                }
                queryWrapper.between("number", request.getMin(), request.getMax());
            }
        }


        //关联id
        if(StringUtils.isNotBlank(request.getLinkId())){
            if(request.getLinkId().equals("gt")){
                queryWrapper.ne("link_id", 0);
            }else{
                queryWrapper.eq("link_id", request.getLinkId());
            }
        }

        //用户id集合
        if(null != request.getUserIdList() && request.getUserIdList().size() > 0){
            queryWrapper.in("uid", request.getUserIdList());
        } else if (ObjectUtil.isNotNull(request.getUid())) {
            queryWrapper.eq("uid", request.getUid());
        }



        if(StringUtils.isNotBlank(request.getCategory())){
            queryWrapper.eq("category", request.getCategory());
        }

        if(StringUtils.isNotBlank(request.getType())){
            queryWrapper.eq("type", request.getType());
        }
    }

    /**
     * 用户账单记录（现金）
     * @param uid 用户uid
     * @param type 记录类型：all-全部，expenditure-支出，income-收入
     * @return
     */
    @Override
    public List<TkUserBill> nowMoneyBillRecord(String uid, String type, PageParamRequest pageRequest) {
        //Page<TkUserBill> billPage = PageHelper.startPage(pageRequest.getPage(), pageRequest.getLimit());
        Page<TkUserBill> billPage = new Page<>(pageRequest.getPage(), pageRequest.getLimit());

        LambdaQueryWrapper<TkUserBill> lqw = Wrappers.lambdaQuery();
        lqw.eq(TkUserBill::getUid, uid);
        //lqw.eq(TkUserBill::getCategory, TokerCommonConstant.USER_BILL_CATEGORY_MONEY);
//        lqw.eq(TkUserBill::getCategory, TokerCommonConstant.USER_BILL_CATEGORY_BROKERAGE_PRICE);
        lqw.and(wrapper->wrapper.eq(TkUserBill::getCategory, TokerCommonConstant.USER_BILL_CATEGORY_MONEY)
                .or().eq(TkUserBill::getCategory, TokerCommonConstant.USER_BILL_CATEGORY_BROKERAGE_PRICE)) ;
        switch (type) {
            case "all":
                break;
            case "expenditure":
                lqw.eq(TkUserBill::getPm, 0);
                break;
            case "income":
                lqw.eq(TkUserBill::getPm, 1);
                break;
        }
        lqw.eq(TkUserBill::getStatus, 1);
        lqw.orderByDesc(TkUserBill::getId);
        List<TkUserBill> billList = userBillMapper.selectList(lqw);

        //BeanUtils.copyProperties(billPage, billList);
        //BeanUtils.copyProperties(originPageInfo, billList, billList);

        return billList ;
    }


}
