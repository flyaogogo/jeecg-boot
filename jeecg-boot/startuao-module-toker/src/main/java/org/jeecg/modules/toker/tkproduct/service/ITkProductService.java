package org.jeecg.modules.toker.tkproduct.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.IndexTkProductSearchRequest;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 产品详细信息
 * @Author: Luo.0022
 * @Date:   2021-01-05
 * @Version: V1.0
 */
public interface ITkProductService extends IService<TkProduct> {

    public boolean editStatusById(String id, String isShow) ;

    /**
     * 产品列表
     * @param request 商品查询参数
     * @param pageParamRequest  分页参数
     * @return  商品查询结果
     */
    IPage<TkProduct> getList(IndexTkProductSearchRequest request, PageParamRequest pageParamRequest) ;

    /**
     * 通过id表，查找所有的产品信息
     * @param ids
     * @return
     */
    List<TkProduct> getProductsByIds(String ids) ;

}
