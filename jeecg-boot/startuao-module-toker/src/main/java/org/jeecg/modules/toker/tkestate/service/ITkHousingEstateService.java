package org.jeecg.modules.toker.tkestate.service;

import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 商品房楼盘
 * @Author: Luo.0022
 * @Date:   2021-02-10
 * @Version: V1.0
 */
public interface ITkHousingEstateService extends IService<TkHousingEstate> {

}
