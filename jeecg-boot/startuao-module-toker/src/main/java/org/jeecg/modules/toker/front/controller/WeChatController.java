package org.jeecg.modules.toker.front.controller;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.toker.front.request.RegisterThirdUserRequest;
import org.jeecg.modules.toker.front.request.UserDecryptPhoneNumberRequest;
import org.jeecg.modules.toker.front.response.LoginResponse;
import org.jeecg.modules.toker.front.service.UserCenterService;
import org.jeecg.modules.toker.utils.WechatDecryptDataUtil;
import org.jeecg.modules.toker.wechat.service.WeChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController("WeChatFrontController")
@RequestMapping("api/front/wechat")
@Api(tags = "微信 -- 开放平台")
public class WeChatController {

    @Autowired
    private WeChatService weChatService;

    @Autowired
    private UserCenterService userCenterService;

    /**
     * 获取授权页面跳转地址
     * @author Luo.0022
     * @since 2020-01-15
     */
    @ApiOperation(value = "获取授权页面跳转地址")
    @RequestMapping(value = "/authorize/get", method = RequestMethod.GET)
    public Result<Object> get(){
        return Result.OK(weChatService.getAuthorizeUrl());
    }
    /**
     * 通过微信code登录
     * @author Mr.Zhang
     * @since 2020-05-25
     */
    @ApiOperation(value = "微信登录公共号授权登录")
    @RequestMapping(value = "/authorize/login", method = RequestMethod.GET)
    public Result<LoginResponse> login(@RequestParam(value = "spread_spid", defaultValue = "0", required = false) Integer spreadUid,
                                             @RequestParam(value = "code") String code){
        return Result.OK(userCenterService.weChatAuthorizeLogin(code, spreadUid));
    }

    /**
     * 通过小程序code登录
     * @author Mr.Zhang
     * @since 2020-05-25
     */

    @ApiOperation(value = "微信登录小程序授权登录")
    @RequestMapping(value = "/authorize/program/login", method = RequestMethod.POST)
    public Result<LoginResponse> programLogin(@RequestParam String code, @RequestBody @Validated RegisterThirdUserRequest request){
        return Result.OK(userCenterService.weChatAuthorizeProgramLogin(code, request));
    }

    /**
     * 小程序获取授权logo
     */
    @ApiOperation(value = "小程序获取授权logo")
    @RequestMapping(value = "/getLogo", method = RequestMethod.GET)
    public Result<Map<String, String>> getLogo(){
        Map<String, String> map = new HashMap<>();
        map.put("logoUrl", userCenterService.getLogo());
        return Result.OK(map);
    }

    /**
     * 解码获取微信号中的手机号码
     * @param request
     * @return
     */
    @ApiOperation(value = "获取微信号中的手机号码")
    @RequestMapping(value = "/authorize/get/phone", method = RequestMethod.POST)
    public Result<Map> getPhoneNumber(@RequestBody @Validated UserDecryptPhoneNumberRequest request){
        String encryptDataB64 = request.getEncryptedData() ;
        String sessionKeyB64 = request.getSessionKey() ;
        String ivB64 = request.getIv() ;
        String v_result = WechatDecryptDataUtil.decryptData(encryptDataB64, sessionKeyB64, ivB64) ;

        JSONObject resultObject = JSONObject.parseObject(v_result);
        Map<String,Object> reMap = new HashMap<>();
        reMap.put("phoneNumber", resultObject.getString("phoneNumber"));
        reMap.put("purePhoneNumber", resultObject.getString("purePhoneNumber"));
        reMap.put("countryCode", resultObject.getString("countryCode"));//国家代码

        //更新手机号
        userCenterService.updateUserPhoneInfo(request.getId(), resultObject.getString("phoneNumber")) ;

        return Result.OK(reMap);
    }
}
