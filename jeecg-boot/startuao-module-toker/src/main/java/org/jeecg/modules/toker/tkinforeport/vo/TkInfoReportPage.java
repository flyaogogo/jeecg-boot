package org.jeecg.modules.toker.tkinforeport.vo;

import java.util.List;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 信息报备表
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="tk_info_reportPage对象", description="信息报备表")
public class TkInfoReportPage {

	/**用户id*/
	@ApiModelProperty(value = "用户id")
	private String id;
	/**业主姓名*/
	@Excel(name = "业主姓名", width = 15)
	@ApiModelProperty(value = "业主姓名")
	private String ownerName;
	/**业主电话*/
	@Excel(name = "业主电话", width = 15)
	@ApiModelProperty(value = "业主电话")
	private String ownerPhone;
	/**装修地址*/
	@Excel(name = "装修地址", width = 15)
	@ApiModelProperty(value = "装修地址")
	private String addressTemp;
	/**装修地址，未找到地址前，添加的是临时地址，审批通过后是装修住户ID*/
	@Excel(name = "装修地址，未找到地址前，添加的是临时地址，审批通过后是装修住户ID", width = 15)
	@ApiModelProperty(value = "装修地址，未找到地址前，添加的是临时地址，审批通过后是装修住户ID")
	private String address;
	/**装修进度*/
	@Excel(name = "装修进度", width = 15)
	@ApiModelProperty(value = "装修进度")
	private String progress;
	/**其他信息*/
	@Excel(name = "其他信息", width = 15)
	@ApiModelProperty(value = "其他信息")
	private String remark;
	/**审批状态*/
	@Excel(name = "审批状态", width = 15)
	@ApiModelProperty(value = "审批状态")
	private Integer approvalStatus;
	/**审批说明*/
	@Excel(name = "审批说明", width = 15)
	@ApiModelProperty(value = "审批说明")
	private String approvalNote;
	/**跟进情况*/
	@Excel(name = "跟进情况", width = 15, dicCode = "tk_follow_up_status")
    @Dict(dicCode = "tk_follow_up_status")
	@ApiModelProperty(value = "跟进情况")
	private String followUp;
	/**酷家乐全景链接*/
	@Excel(name = "酷家乐全景链接", width = 15)
	@ApiModelProperty(value = "酷家乐全景链接")
	private String extHref;
	/**地址状态：1为新增的小区-addCell，2为选择小区-chooseCell，3为新增小区内屋室-addHouse，4为选择小区内屋室-chooseHouse*/
	@Excel(name = "地址状态：1为新增的小区-addCell，2为选择小区-chooseCell，3为新增小区内屋室-addHouse，4为选择小区内屋室-chooseHouse", width = 15)
	@ApiModelProperty(value = "地址状态：1为新增的小区-addCell，2为选择小区-chooseCell，3为新增小区内屋室-addHouse，4为选择小区内屋室-chooseHouse")
	private String addressStatus;
	/**所选产品Id*/
	@Excel(name = "所选产品Id", width = 15, dictTable = "tk_product", dicText = "product_name", dicCode = "id")
    @Dict(dictTable = "tk_product", dicText = "product_name", dicCode = "id")
	@ApiModelProperty(value = "所选产品Id")
	private String productId;
	/**签单总价*/
	@Excel(name = "签单总价", width = 15)
	@ApiModelProperty(value = "签单总价")
	private java.math.BigDecimal totalPrice;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private String createBy;
	/**修改人*/
	@ApiModelProperty(value = "修改人")
	private String updateBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新时间")
	private Date updateTime;
	/**业主地址Id，存小区Id*/
	@Excel(name = "业主地址Id，存小区Id", width = 15)
	@ApiModelProperty(value = "业主地址Id，存小区Id")
	private String tkaddrId;
	/**用户Id*/
	@Excel(name = "用户Id", width = 15)
	@ApiModelProperty(value = "用户Id")
	private String tkuserId;
	/**导购ID*/
	@Excel(name = "导购ID", width = 15)
	@ApiModelProperty(value = "导购ID")
	private String guideId;
	/**租户id*/
	@Excel(name = "租户id", width = 15)
	@ApiModelProperty(value = "租户id")
	private String tenantId;

	@ExcelCollection(name="导购跟跟踪信息记录")
	@ApiModelProperty(value = "导购跟跟踪信息记录")
	private List<TkFollowupCustom> tkFollowupCustomList;

}
