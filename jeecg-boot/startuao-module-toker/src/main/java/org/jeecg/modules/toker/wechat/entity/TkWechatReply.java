package org.jeecg.modules.toker.wechat.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 微信关键字回复表
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
@Data
@TableName("tk_wechat_reply")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_wechat_reply对象", description="微信关键字回复表")
public class TkWechatReply implements Serializable {
    private static final long serialVersionUID = 1L;

	/**微信关键字回复id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "微信关键字回复id")
    private java.lang.String id;
	/**关键字*/
	@Excel(name = "关键字", width = 15)
    @ApiModelProperty(value = "关键字")
    private java.lang.String keywords;
	/**回复类型*/
	@Excel(name = "回复类型", width = 15)
    @ApiModelProperty(value = "回复类型")
    private java.lang.String type;
	/**回复数据*/
	@Excel(name = "回复数据", width = 15)
    @ApiModelProperty(value = "回复数据")
    private java.lang.String data;
	/**回复状态*/
	@Excel(name = "回复状态", width = 15)
    @ApiModelProperty(value = "回复状态")
    private java.lang.String status;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
}
