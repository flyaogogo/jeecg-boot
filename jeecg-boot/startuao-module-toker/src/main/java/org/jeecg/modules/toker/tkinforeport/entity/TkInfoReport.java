package org.jeecg.modules.toker.tkinforeport.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 拓客用户报备信息
 * @Author: Luo.0022
 * @Date:   2021-01-02
 * @Version: V1.0
 */
@Data
@TableName("tk_info_report")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_info_report对象", description="拓客用户报备信息")
public class TkInfoReport implements Serializable {
    private static final long serialVersionUID = 1L;

	/**用户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户id")
    private java.lang.String id;
	/**业主姓名*/
	@Excel(name = "业主姓名", width = 15)
    @ApiModelProperty(value = "业主姓名")
    private java.lang.String ownerName;
	/**业主电话*/
	@Excel(name = "业主电话", width = 15)
    @ApiModelProperty(value = "业主电话")
    private java.lang.String ownerPhone;
	/**装修地址，此为客户新增地址储存ID*/
	@Excel(name = "装修地址", width = 15, dictTable = "tk_village_address", dicText = "house_address", dicCode = "id")
	@Dict(dictTable = "tk_village_address", dicText = "house_address", dicCode = "id")
    @ApiModelProperty(value = "装修地址")
    private java.lang.String address;
	/**装修进度*/
	@Excel(name = "装修进度", width = 15, dicCode = "tk_fitup_progress")
	@Dict(dicCode = "tk_fitup_progress")
    @ApiModelProperty(value = "装修进度")
    private java.lang.String progress;
	/**其他信息*/
	@Excel(name = "其他信息", width = 15)
    @ApiModelProperty(value = "其他信息")
    private java.lang.String remark;
	/**审批状态*/
	@Excel(name = "审批状态", width = 15, dicCode = "tk_approval_status")
	@Dict(dicCode = "tk_approval_status")
    @ApiModelProperty(value = "审批状态")
    private java.lang.Integer approvalStatus;
	/**审批说明*/
	@Excel(name = "审批说明", width = 15)
    @ApiModelProperty(value = "审批说明")
    private java.lang.String approvalNote;
	/**跟进情况*/
    @Excel(name = "跟进情况", width = 15, dicCode = "tk_follow_up_status")
    @Dict(dicCode = "tk_follow_up_status")
    @ApiModelProperty(value = "跟进情况")
    private java.lang.String followUp;
	/**业主地址Id*/
	@Excel(name = "业主地址Id，存小区Id", width = 15)
    @ApiModelProperty(value = "业主地址Id，存小区Id")
    private java.lang.String tkaddrId;
	/**酷家乐全景链接*/
	@Excel(name = "酷家乐全景链接", width = 15)
    @ApiModelProperty(value = "酷家乐全景链接")
    private java.lang.String extHref;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**修改人*/
    @ApiModelProperty(value = "修改人")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**所选产品Id*/
    @Excel(name = "所选产品Id", width = 15, dictTable = "tk_product", dicText = "product_name", dicCode = "id")
    @Dict(dictTable = "tk_product", dicText = "product_name", dicCode = "id")
    @ApiModelProperty(value = "所选产品Id")
    private java.lang.String productId;

    /**签单总价*/
    @Excel(name = "签单总价", width = 15)
    @ApiModelProperty(value = "签单总价")
    private java.math.BigDecimal totalPrice;

	/**用户Id*/
	@Excel(name = "用户Id", width = 15)
    @ApiModelProperty(value = "用户Id")
    private java.lang.String tkuserId;

    /**导购ID*/
    @Excel(name = "导购ID", width = 15)
    @ApiModelProperty(value = "导购ID")
    private java.lang.String guideId;

    /**地址状态：
     * 1为新增的小区-addCell，
     * 2为选择小区-chooseCell，
     * 3为新增小区内屋室-addHouse，
     * 4为选择小区内屋室-chooseHouse
     * */
    @Excel(name = "地址状态", width = 15)
    @ApiModelProperty(value = "地址状态")
    private java.lang.String addressStatus;

    /**
     * 为方便展示报备列表中的数据，缓存相关地址信息，详细信息通过ID查询
     */
    @ApiModelProperty(value = "缓存地址")
    private java.lang.String addressTemp;

	/**租户id*/
	@Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;
}
