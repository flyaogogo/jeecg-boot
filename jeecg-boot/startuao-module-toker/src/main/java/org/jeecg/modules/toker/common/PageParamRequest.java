package org.jeecg.modules.toker.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.constant.TokerCommonConstant;

/**
 * 分页公共请求对象
 */
@Data
public class PageParamRequest {

    @ApiModelProperty(value = "页码", example= TokerCommonConstant.DEFAULT_PAGE + "")
    private int page = TokerCommonConstant.DEFAULT_PAGE;

    @ApiModelProperty(value = "每页数量", example = TokerCommonConstant.DEFAULT_LIMIT + "")
    private int limit = TokerCommonConstant.DEFAULT_LIMIT;

}
