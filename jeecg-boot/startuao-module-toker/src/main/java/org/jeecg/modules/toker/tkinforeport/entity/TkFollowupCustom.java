package org.jeecg.modules.toker.tkinforeport.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 导购跟跟踪信息记录
 * @Author: Luo.0022
 * @Date:   2021-04-12
 * @Version: V1.0
 */
@Data
@TableName("tk_followup_custom")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_followup_custom对象", description="导购跟跟踪信息记录")
public class TkFollowupCustom implements Serializable {
    private static final long serialVersionUID = 1L;

	/**地址id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "地址id")
    private String id;
	/**报备ID*/
	@Excel(name = "报备ID", width = 15)
    @ApiModelProperty(value = "报备ID")
    private String reportId;
	/**报备用户ID*/
	@Excel(name = "报备用户ID", width = 15)
    @ApiModelProperty(value = "报备用户ID")
    private String userId;
	/**导购ID*/
	@Excel(name = "导购ID", width = 15)
    @ApiModelProperty(value = "导购ID")
    private String guideId;
	/**装修地址ID*/
	@Excel(name = "装修地址ID", width = 15)
    @ApiModelProperty(value = "装修地址ID")
    private String addressId;
	/**跟进情况*/
    @Excel(name = "跟进情况", width = 15, dicCode = "tk_follow_up_status")
    @Dict(dicCode = "tk_follow_up_status")
    @ApiModelProperty(value = "跟进情况")
    private String followUp;
	/**跟进说明*/
	@Excel(name = "跟进说明", width = 15)
    @ApiModelProperty(value = "跟进说明")
    private String mark;
	/**所选产品Id*/
    @Excel(name = "所选产品Id", width = 15, dictTable = "tk_product", dicText = "product_name", dicCode = "id")
    @Dict(dictTable = "tk_product", dicText = "product_name", dicCode = "id")
    @ApiModelProperty(value = "所选产品Id")
    private String productId;
	/**签单总价*/
	@Excel(name = "签单总价", width = 15)
    @ApiModelProperty(value = "签单总价")
    private BigDecimal totalPrice;
	/**租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)*/
	@Excel(name = "租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)", width = 15)
    @ApiModelProperty(value = "租户Id(0为总后台管理员创建,不为0的时候是租户后台创建)")
    private String tenantId;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
