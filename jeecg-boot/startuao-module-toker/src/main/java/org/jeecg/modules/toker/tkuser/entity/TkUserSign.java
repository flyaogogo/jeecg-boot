package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 签到记录表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
@Data
@TableName("tk_user_sign")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_user_sign对象", description="签到记录表")
public class TkUserSign implements Serializable {
    private static final long serialVersionUID = 1L;

	/**签到记录ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "签到记录ID")
    private String id;
	/**用户uid*/
	@Excel(name = "用户uid", width = 15)
    @ApiModelProperty(value = "用户uid")
    private String uid;
	/**签到说明*/
	@Excel(name = "签到说明", width = 15)
    @ApiModelProperty(value = "签到说明")
    private String title;
	/**获得*/
	@Excel(name = "获得", width = 15)
    @ApiModelProperty(value = "获得")
    private Integer number;
	/**剩余*/
	@Excel(name = "剩余", width = 15)
    @ApiModelProperty(value = "剩余")
    private Integer balance;
	/**类型*/
	@Excel(name = "类型", width = 15, dicCode = "tk_sign_type")
	@Dict(dicCode = "tk_sign_type")
    @ApiModelProperty(value = "类型")
    private Integer type;
	/**签到日期*/
	@Excel(name = "签到日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "签到日期")
    private Date createDay;
	/**添加时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "添加时间")
    private Date createTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;


}
