package org.jeecg.modules.toker.sysconfig.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 系统参数配置
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
@Data
@TableName("tk_system_config")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_system_config对象", description="系统参数配置")
public class TkSystemConfig implements Serializable {
    private static final long serialVersionUID = 1L;

	/**配置id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "配置id")
    private java.lang.String id;
	/**字段名称*/
	@Excel(name = "字段名称", width = 15)
    @ApiModelProperty(value = "字段名称")
    private java.lang.String name;
	/**字段提示文字*/
	@Excel(name = "字段提示文字", width = 15)
    @ApiModelProperty(value = "字段提示文字")
    private java.lang.String title;
	/**表单id*/
	@Excel(name = "表单id", width = 15)
    @ApiModelProperty(value = "表单id")
    private java.lang.Integer formId;
	/**值*/
	@Excel(name = "值", width = 15)
    @ApiModelProperty(value = "值")
    private java.lang.String value;
	/**是否隐藏*/
	@Excel(name = "是否隐藏", width = 15)
    @ApiModelProperty(value = "是否隐藏")
    private java.lang.String status;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;
}
