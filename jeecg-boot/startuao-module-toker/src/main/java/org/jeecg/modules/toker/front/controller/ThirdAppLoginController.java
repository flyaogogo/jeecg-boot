package org.jeecg.modules.toker.front.controller;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.toker.front.request.LoginRequest;
import org.jeecg.modules.toker.front.response.LoginResponse;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 第三方登陆（微信）
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
@Api(tags="用户 -- 登录注册")
@RestController
@RequestMapping("/api/front")
@Slf4j
public class ThirdAppLoginController {

    @Autowired
    private ITkUserService tkUserService;

    @ApiOperation("登录接口")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result<LoginResponse> login(@RequestBody @Validated LoginRequest loginRequest) throws Exception{
        Result<LoginResponse> result = new Result<LoginResponse>();

        LoginResponse loginResponse = tkUserService.login(loginRequest) ;
        return result.OK(loginResponse) ;
    }

    /**
     * 退出登录
     */
    @ApiOperation(value = "退出")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public Result<String> loginOut(HttpServletRequest request){
        tkUserService.loginOut(request);
        return Result.OK();
    }


}
