package org.jeecg.modules.toker.tkinforeport.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 拓客用户报备信息
 * @Author: Luo.0022
 * @Date:   2021-01-02
 * @Version: V1.0
 */
public interface TkInfoReportMapper extends BaseMapper<TkInfoReport> {

}
