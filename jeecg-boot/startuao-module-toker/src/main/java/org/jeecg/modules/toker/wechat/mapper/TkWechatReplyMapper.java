package org.jeecg.modules.toker.wechat.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.wechat.entity.TkWechatReply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信关键字回复表
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
public interface TkWechatReplyMapper extends BaseMapper<TkWechatReply> {

}
