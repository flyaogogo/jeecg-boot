package org.jeecg.modules.toker.front.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.PasswordRequest;
import org.jeecg.modules.toker.front.request.UserBindingRequest;
import org.jeecg.modules.toker.front.request.UserEditRequest;
import org.jeecg.modules.toker.front.response.UserBalanceResponse;
import org.jeecg.modules.toker.front.response.UserCenterResponse;
import org.jeecg.modules.toker.front.service.UserCenterService;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.entity.TkUserBill;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.tkuser.vo.SysUserComboModel;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户 -- 用户中心
 *  Author: Luo.0022
 */
@Slf4j
@RestController("FrontUserController")
@RequestMapping("api/front")
@Api(tags = "用户 -- 用户中心")
public class UserController {

    @Autowired
    private ITkUserService userService;

    @Autowired
    private UserCenterService userCenterService ;

    /**
     * 修改密码
     */
    @ApiOperation(value = "手机号修改密码")
    @RequestMapping(value = "/register/reset", method = RequestMethod.POST)
    public Result<Boolean> password(@RequestBody @Validated PasswordRequest request){
        return Result.OK(userService.password(request));
    }

    /**
     * 修改个人资料
     */
    @ApiOperation(value = "修改个人资料")
    @RequestMapping(value = "/user/edit", method = RequestMethod.POST)
    public Result<Boolean> personInfo(@RequestBody @Validated UserEditRequest request){
        TkUser user = userService.getInfo();
        //user.setAvatar(systemAttachmentService.clearPrefix(request.getAvatar()));
        user.setNickname(request.getNickname());
        user.setRealName(request.getRealName()) ;
        user.setUserType(request.getUserType()) ;

        String birdthDayStr = request.getBirthday() ;
        if( birdthDayStr!=null) {
            user.setBirthday(birdthDayStr);
        }
        user.setSex(request.getSex()) ;
        //user.setCardId(request.getCardId()) ;
        user.setContact(request.getContact()) ;
        user.setPhone(request.getPhone()) ;
        return Result.OK(userService.updateById(user));
    }

    /**
     * 获取用户个人资料
     */
    @ApiOperation(value = "获取个人资料")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Result<UserCenterResponse> getUserCenter(){
        return Result.OK(userService.getUserCenter());
    }

    /**
     * 获取用户个人资料
     */
    @ApiOperation(value = "当前登录用户信息")
    @RequestMapping(value = "/userinfo", method = RequestMethod.GET)
    public Result<TkUser> getInfo(){
        return Result.OK(userService.getUserPromoter());
    }

    /**
     * 绑定手机号
     */
    @ApiOperation(value = "绑定手机号")
    @RequestMapping(value = "/binding", method = RequestMethod.POST)
    public Result<Boolean> bind(@RequestBody @Validated UserBindingRequest request){
        return Result.OK(userService.bind(request));
    }

    /**
     * 积分记录
     */
    @ApiOperation(value = "积分记录")
    @RequestMapping(value = "/integral/list", method = RequestMethod.GET)
    public Result<List<TkUserBill>>  getIntegralList(@Validated PageParamRequest pageParamRequest){
        IPage<TkUserBill> userBillPage =  userCenterService.getUserBillList(TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL, pageParamRequest) ;
        return Result.OK(userBillPage.getRecords());
    }

    /**
     * 用户资金统计
     */
    @ApiOperation(value = "用户资金统计")
    @RequestMapping(value = "/user/balance", method = RequestMethod.GET)
    public Result<UserBalanceResponse>  getUserBalance(){
        return Result.OK(userCenterService.getUserBalance());
    }

    /**
     * 查找所有后台用户信息
     */
    @ApiOperation(value = "查找所有后台用户信息")
    @RequestMapping(value = "/all/sysuser", method = RequestMethod.GET)
    public Result<List<SysUserComboModel>>  getAllSysUsers(){
        return Result.OK(userService.queryAllUserBackSysCombo());
    }

}
