package org.jeecg.modules.toker.tkinforeport.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 导购跟跟踪信息记录
 * @Author: Luo.0022
 * @Date:   2021-04-12
 * @Version: V1.0
 */
public interface TkFollowupCustomMapper extends BaseMapper<TkFollowupCustom> {
    public boolean deleteByMainId(@Param("mainId") String mainId);

    public List<TkFollowupCustom> selectByMainId(@Param("mainId") String mainId);
}
