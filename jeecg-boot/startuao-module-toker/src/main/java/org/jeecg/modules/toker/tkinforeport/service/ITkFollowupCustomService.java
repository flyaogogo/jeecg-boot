package org.jeecg.modules.toker.tkinforeport.service;

import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 导购跟跟踪信息记录
 * @Author: Luo.0022
 * @Date:   2021-04-12
 * @Version: V1.0
 */
public interface ITkFollowupCustomService extends IService<TkFollowupCustom> {

    List<TkFollowupCustom> selectByMainId(String mainId);

    // 添加用户跟进信息
    Boolean addFollowupInfo(TkFollowupCustom followupCustom) ;

    // 修改用户跟进信息
    Boolean editFollowupInfo(TkFollowupCustom followupCustom) ;

    /**
     * 修改导购跟进客户信息，
     * 当从签单状态，更为其他状态，可以从前端做限制，签单后，只有从另一种入口取消
     * @param followupCustom
     * @return
     */
    Boolean cancelOrderFollowupInfo(TkFollowupCustom followupCustom) ;
}
