package org.jeecg.modules.toker.tkinforeport.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.modules.toker.constants.BrokerageRecordConstants;
import org.jeecg.modules.toker.front.request.UserOperateFundsRequest;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import org.jeecg.modules.toker.tkestate.service.ITkDecoratedAddressService;
import org.jeecg.modules.toker.tkestate.service.ITkHousingEstateService;
import org.jeecg.modules.toker.tkinforeport.entity.TkFollowupCustom;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;
import org.jeecg.modules.toker.tkinforeport.mapper.TkFollowupCustomMapper;
import org.jeecg.modules.toker.tkinforeport.mapper.TkInfoReportMapper;
import org.jeecg.modules.toker.tkinforeport.service.ITkFollowupCustomService;
import org.jeecg.modules.toker.tkinforeport.service.ITkInfoReportService;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.service.ITkUserBrokerageRecordService;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.tkvaddress.entity.TkVillageAddress;
import org.jeecg.modules.toker.tkvaddress.service.ITkVillageAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 拓客用户报备信息
 * @Author: Luo.0022
 * @Date:   2021-01-02
 * @Version: V1.0
 */
@Service
public class TkInfoReportServiceImpl extends ServiceImpl<TkInfoReportMapper, TkInfoReport> implements ITkInfoReportService {

    @Autowired
    private TkInfoReportMapper infoReportMapper ;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ITkUserBrokerageRecordService userBrokerageRecordService ;

    @Autowired
    private ITkDecoratedAddressService decoratedAddressService ;

    @Autowired
    private ITkHousingEstateService housingEstateService ;

    @Autowired
    private ITkVillageAddressService villageAddressService ;

    @Autowired
    private ITkSystemConfigService systemConfigService ;

    @Autowired
    private ITkUserService userService ;

    @Autowired
    private ITkFollowupCustomService followupCustomService ;

    @Autowired
    private TkFollowupCustomMapper tkFollowupCustomMapper;

    /**
     * 通过用户ID，列出所有的报备信息
     * @param userId
     * @return
     */
    @Override
    public List<TkInfoReport> getMyReportListByUserId(String userId) {

        LambdaQueryWrapper<TkInfoReport> wrapper = new LambdaQueryWrapper<>() ;
        wrapper.eq(TkInfoReport::getTkuserId,userId);
        wrapper.orderByDesc(TkInfoReport::getCreateTime) ;

        List<TkInfoReport> reportList = infoReportMapper.selectList(wrapper) ;
        return reportList ;
    }

    /**
     * 审批用户报备信息
     * @param tkInfoReport
     */
    @Override
    public Boolean approvalReportInfoByUid(TkInfoReport tkInfoReport) {
        Boolean execute = transactionTemplate.execute(e -> {

            TkInfoReport approvalInfo = new TkInfoReport() ;
            approvalInfo.setId(tkInfoReport.getId()) ;
            approvalInfo.setTkuserId(tkInfoReport.getTkuserId()) ;
            approvalInfo.setAddress(tkInfoReport.getAddress()) ;
            approvalInfo.setTkaddrId(tkInfoReport.getTkaddrId()) ;
            approvalInfo.setApprovalStatus(tkInfoReport.getApprovalStatus()) ;
            approvalInfo.setApprovalNote(tkInfoReport.getApprovalNote()) ;
            //approvalInfo.setTenantId(tkInfoReport.getTenantId()) ;// tenantID

            //1 当前期是审批通过的，则不做处理,只做审批变更更新
            //infoReportMapper.updateById(approvalInfo) ;

            // 若审批通过，则做如下操作：返佣金，更新地址等操作
            if(tkInfoReport.getApprovalStatus() == 3) {
                //2 获取设置的佣金，默认为5个元宝
                String brokerageMoney = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_STORE_BROKERAGE_REPORT_SUCCESS_MONEY);
                if (brokerageMoney == null || brokerageMoney == "") {
                    brokerageMoney = "5";
                }
                BigDecimal brokeragePrice = new BigDecimal(brokerageMoney).setScale(2, BigDecimal.ROUND_HALF_UP);
                // 若审批通过，则需要返报备成功 佣金 ，并记录到佣金表中。
                userBrokerageRecordService.calculateCommissionAndSave(tkInfoReport.getTkuserId(), tkInfoReport.getId(),
                        brokeragePrice,
                        BrokerageRecordConstants.BROKERAGE_RECORD_COMMISSION_STANDARD);

                // 把佣金更新到 用户佣金字段
                this.setBrokeragePrice(tkInfoReport.getId(),tkInfoReport.getTkuserId(),
                        brokeragePrice, 1, "report");

                //3 判断是否在正式库中，若不，则把地址加入到正试库中
                String addrStatus = tkInfoReport.getAddressStatus();

                TkVillageAddress villageAddress = villageAddressService.getById(tkInfoReport.getAddress());
                TkDecoratedAddress decoratedAddress ;
                //4 若是新增小区(addCell)，则需要入库  小区  及  住户信息，两个表
                // TODO 此时没有完全考虑，当后台添加了，此时则有两条记录，
                // TODO 最好后台进行一个是否重复判断
                if ("addCell".equals(addrStatus)) {

                    TkHousingEstate housingEstate = addHousingEstate(villageAddress);
                    housingEstateService.save(housingEstate);
                    String cellId = housingEstate.getId();

                    villageAddress.setRealEstate(cellId);
                    decoratedAddress = addDecoratedAddress(villageAddress);
                    decoratedAddressService.save(decoratedAddress);

                    // 更新报备信息表中的地址ID
                    approvalInfo.setAddress(decoratedAddress.getId()) ; // 存中不存在，则存放的是临时表中的地址ID，审批通过后存放的是正式库中的ID
                    approvalInfo.setTkaddrId(housingEstate.getId()) ;// 存放的是小区地址ID
                    // 若是新增小区内屋室-addHouse ，则需要入库  住户信息，一个表
                } else if ("addHouse".equals(addrStatus)) {
                    villageAddress.setRealEstate(tkInfoReport.getTkaddrId());
                    decoratedAddress = addDecoratedAddress(villageAddress);
                    decoratedAddressService.save(decoratedAddress);

                    // 更新报备信息表中的地址ID
                    approvalInfo.setAddress(decoratedAddress.getId()) ; // 存中不存在，则存放的是临时表中的地址ID，审批通过后存放的是正式库中的ID
                }

                // 5、审批通过时，同时更新导购跟进表
                TkUser user = userService.getById(approvalInfo.getTkuserId()) ;
                approvalInfo.setGuideId(user.getContact()) ;//存储 导购 联系人

                TkFollowupCustom followupCustom = new TkFollowupCustom() ;
                followupCustom.setReportId(approvalInfo.getId()) ;
                followupCustom.setUserId(approvalInfo.getTkuserId()) ;
                followupCustom.setGuideId(user.getContact());
                followupCustom.setAddressId(approvalInfo.getAddress()) ;
                followupCustom.setFollowUp("4") ; // 4:报备客户
                followupCustom.setMark("通过管理员审批，客户报备成功 ！") ;
                followupCustomService.save(followupCustom) ;

                //同时更新报备信息表中，导购初始跟进状态
                approvalInfo.setFollowUp("4") ; // 4:报备客户

            }
            //6、当前期是审批通过的，则不做处理
            infoReportMapper.updateById(approvalInfo) ;
            return Boolean.TRUE;
        });

        return execute ;
    }

    /**
     * 封装小区信息
     * @param villageAddress
     * @return
     */
    private TkHousingEstate addHousingEstate(TkVillageAddress villageAddress){
        TkHousingEstate housingEstate = new TkHousingEstate() ;
        housingEstate.setRealEstate(villageAddress.getRealEstate()) ;
        housingEstate.setCellAddress(villageAddress.getCellAddress()) ;
        housingEstate.setStatus(1) ;// 默认为  1：入住
        housingEstate.setRegion(villageAddress.getRegion()) ;
        housingEstate.setIsShow("Y") ;
        housingEstate.setIsHot("Y") ;
        housingEstate.setIsBest("N") ;
        housingEstate.setIsNew("N") ;
        housingEstate.setMark("审批成功入库") ;

        return housingEstate ;
    }

    /**
     * 封装客户装修地址信息
     * @param villAddr
     * @return
     */
    private TkDecoratedAddress addDecoratedAddress(TkVillageAddress villAddr) {
        TkDecoratedAddress decoratedAddress = new TkDecoratedAddress() ;
        decoratedAddress.setEstateId(villAddr.getRealEstate()) ;
        decoratedAddress.setBuildingNumber(villAddr.getBuildingNumber() + "") ;
        decoratedAddress.setUnit(villAddr.getUnit()+ "") ;
        decoratedAddress.setFloor(villAddr.getFloor()+ "") ;
        decoratedAddress.setHouseNumber(villAddr.getHouseNumber() + "") ;
        decoratedAddress.setCellAddress(villAddr.getCellAddress()) ;
        decoratedAddress.setHouseAddress(villAddr.getHouseAddress()) ;
        decoratedAddress.setStatus(4) ; //状态为报备客户（4）
        decoratedAddress.setMark("审批成功入库") ;

        return decoratedAddress ;
    }

    /**
     * 报备客户获得佣金
     * @param reportInfoId 报备信息ID
     * @param userId 报备用户ID
     * @param type int 类型 1 增加，2 减少
     * @param titleType String 1，报备 - report ；2 ，进店 - enterStore
     */
    private void setBrokeragePrice(String reportInfoId, String userId, BigDecimal brokeragePrice, int type , String titleType){
        //资金明细加一条数据
        UserOperateFundsRequest userOperateFundsRequest = new UserOperateFundsRequest();
        userOperateFundsRequest.setLinkId(reportInfoId);
        if( "report".equals(titleType)){ // 报备成功
            userOperateFundsRequest.setTitle(TokerCommonConstant.USER_REPORT_STATUS_SUCCESS);
        }else{// 客户进店
            userOperateFundsRequest.setTitle(TokerCommonConstant.USER_REPORT_ENTER_STORE_SUCCESS);
        }

        userOperateFundsRequest.setFoundsCategory(TokerCommonConstant.USER_BILL_CATEGORY_BROKERAGE_PRICE);
        userOperateFundsRequest.setFoundsType(TokerCommonConstant.USER_BILL_TYPE_BROKERAGE);
        userOperateFundsRequest.setType(type);

        //获得报备佣金
        userOperateFundsRequest.setUid(userId);
        userOperateFundsRequest.setValue(brokeragePrice);
        userService.updateFounds(userOperateFundsRequest, true);

    }

    /**
     *  封装   报备客户获得佣金
     * @param reportInfoId 报备信息ID
     * @param userId 报备用户ID
     * @param type int 类型 1 增加，2 减少
     * @param titleType String 1，报备 - report ；2 ，进店 - enterStore
     */
    /*public Boolean doUserOperateFunds(String reportInfoId, String userId, BigDecimal brokeragePrice, int type , String titleType){

        return null ;
    }*/
    /**
     * 导购跟进信息列表
     * @param tkInfoReport
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @Override
    public IPage<TkInfoReport> guideFollowupCustomList(TkInfoReport tkInfoReport,Integer pageNo,
                                                       Integer pageSize, HttpServletRequest req){

        Page<TkInfoReport> guideInfoPage = new Page<>(pageNo,pageSize) ;

        // 获取后台登陆系统 的  当前用户信息
        String loginUserStr = req.getUserPrincipal().getName() ;
        //System.out.println(loginUserStr);
        int start = loginUserStr.split(",")[0].indexOf("id=");
        String currentUserId = loginUserStr.split(",")[0].substring(start+3) ;
        //System.out.println(currentUserId);

        LambdaQueryWrapper<TkInfoReport> wrapper = new LambdaQueryWrapper<>() ;
        wrapper.eq(TkInfoReport::getGuideId,currentUserId);
        wrapper.orderByDesc(TkInfoReport::getFollowUp) ;

        IPage<TkInfoReport> guideInfoPageList = infoReportMapper.selectPage(guideInfoPage,wrapper) ;

        return guideInfoPageList ;
    }

    @Override
    @Transactional
    public void saveMain(TkInfoReport tkInfoReport, List<TkFollowupCustom> tkFollowupCustomList) {
        infoReportMapper.insert(tkInfoReport);
        if(tkFollowupCustomList!=null && tkFollowupCustomList.size()>0) {
            for(TkFollowupCustom entity:tkFollowupCustomList) {
                //外键设置
                entity.setReportId(tkInfoReport.getId());
                tkFollowupCustomMapper.insert(entity);
            }
        }
    }

    @Override
    @Transactional
    public void updateMain(TkInfoReport tkInfoReport,List<TkFollowupCustom> tkFollowupCustomList) {
        infoReportMapper.updateById(tkInfoReport);

        //1.先删除子表数据
        tkFollowupCustomMapper.deleteByMainId(tkInfoReport.getId());

        //2.子表数据重新插入
        if(tkFollowupCustomList!=null && tkFollowupCustomList.size()>0) {
            for(TkFollowupCustom entity:tkFollowupCustomList) {
                //外键设置
                entity.setReportId(tkInfoReport.getId());
                tkFollowupCustomMapper.insert(entity);
            }
        }
    }

    @Override
    @Transactional
    public void delMain(String id) {
        tkFollowupCustomMapper.deleteByMainId(id);
        infoReportMapper.deleteById(id);
    }

    @Override
    @Transactional
    public void delBatchMain(Collection<? extends Serializable> idList) {
        for(Serializable id:idList) {
            tkFollowupCustomMapper.deleteByMainId(id.toString());
            infoReportMapper.deleteById(id);
        }
    }
}
