package org.jeecg.modules.toker.front.service;

import org.jeecg.modules.toker.front.request.ReportAddressRequest;
import org.jeecg.modules.toker.front.response.ReportAddressResponse;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import org.jeecg.modules.toker.tkinforeport.entity.TkInfoReport;

import java.util.List;

public interface ReportAddressService {

    /**
     * 报备信息添加
     * @param reportAddressRequest
     * @return
     */
    boolean addReportAndAddrs(ReportAddressRequest reportAddressRequest) ;

    /**
     * 查找所有热门小区
     * @param keywords
     * @return
     */
    List<TkHousingEstate> getAllHotCells(String keywords) ;

    /**
     * 通过楼盘ID获取对应小区热装住户地址
     * @param estateId
     * @return
     */
    List<TkDecoratedAddress> getUserAddressByCellId(String estateId) ;

    /**
     * 通过用户ID，列出所有的报备信息
     * @param userId
     * @return
     */
    List<TkInfoReport> getMyReportsByUserId(String userId) ;
}
