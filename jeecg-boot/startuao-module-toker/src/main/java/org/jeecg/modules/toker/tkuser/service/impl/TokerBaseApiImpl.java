package org.jeecg.modules.toker.tkuser.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.TokerCommonAPI;
import org.jeecg.common.system.vo.WxUser;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.service.UserTokenService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: 底层共通业务API，提供其他独立模块调用
 * @Author: Luo.0022
 * @Date:2021/01/14
 */
@Slf4j
@Service
public class TokerBaseApiImpl implements TokerCommonAPI {
    @Autowired
    private TkUserServiceImpl userService ;

    @Autowired
    private UserTokenService userTokenService ;
    @Override
    public WxUser getUserByName(String username) {
        TkUser user = userService.getUserByAccount(username) ;

        WxUser wxUser = new WxUser();
        BeanUtils.copyProperties(user, wxUser);
        return wxUser ;
    }

    @Override
    public boolean deleteUserToken(String userId){
        userTokenService.deleteUserTokenByUid(userId) ;
        return true ;
    }

    @Override
    public boolean updateUserTokenByUid(String token ,String userId){

        userTokenService.updateUserTokenByUid(token,userId) ;
        return true ;
    }
}
