package org.jeecg.modules.toker.tkvaddress.service;

import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkvaddress.entity.TkVillageAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 业主装修地址
 * @Author: Luo.0022
 * @Date:   2020-12-31
 * @Version: V1.0
 */
public interface ITkVillageAddressService extends IService<TkVillageAddress> {

    /**
     * 通过uuid获取实体信息
     * @param uuid
     * @return
     */
    TkVillageAddress getTkVillageAddressByUUID(String uuid) ;
}
