package org.jeecg.modules.toker.tkestate.service.impl;

import org.jeecg.modules.toker.tkestate.entity.TkHousingEstate;
import org.jeecg.modules.toker.tkestate.mapper.TkHousingEstateMapper;
import org.jeecg.modules.toker.tkestate.service.ITkHousingEstateService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 商品房楼盘
 * @Author: Luo.0022
 * @Date:   2021-02-10
 * @Version: V1.0
 */
@Service
public class TkHousingEstateServiceImpl extends ServiceImpl<TkHousingEstateMapper, TkHousingEstate> implements ITkHousingEstateService {

}
