package org.jeecg.modules.toker.tkuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.UserLevelSearchRequest;
import org.jeecg.modules.toker.tkuser.entity.TkMemberUserLevel;
import org.jeecg.modules.toker.tkuser.mapper.TkMemberUserLevelMapper;
import org.jeecg.modules.toker.tkuser.service.ITkMemberUserLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 普通会员等级
 * @Author: Luo.0022
 * @Date:   2021-01-29
 * @Version: V1.0
 */
@Service
public class TkMemberUserLevelServiceImpl extends ServiceImpl<TkMemberUserLevelMapper, TkMemberUserLevel> implements ITkMemberUserLevelService {

    @Autowired
    private TkMemberUserLevelMapper memberUserLevelMapper ;

    /**
     * 分页显示设置用户等级表
     * @param request 搜索条件
     * @return List<TkMemberUserLevel>
     */
    @Override
    public List<TkMemberUserLevel> getList(UserLevelSearchRequest request) {
        //PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        LambdaQueryWrapper<TkMemberUserLevel> levelLambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isBlank(request.getName())){
            levelLambdaQueryWrapper.like(TkMemberUserLevel::getName, request.getName());
        }

        if(request.getIsShow() != null){
            levelLambdaQueryWrapper.eq(TkMemberUserLevel::getIsShow, request.getIsShow());
        }

        levelLambdaQueryWrapper.eq(TkMemberUserLevel::getIsDel, false);
        return memberUserLevelMapper.selectList(levelLambdaQueryWrapper);
    }
}
