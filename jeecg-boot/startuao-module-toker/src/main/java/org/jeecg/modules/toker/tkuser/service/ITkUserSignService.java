package org.jeecg.modules.toker.tkuser.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.UserSignInfoRequest;
import org.jeecg.modules.toker.front.response.UserSignInfoResponse;
import org.jeecg.modules.toker.tkuser.entity.TkUserSign;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.toker.tkuser.vo.UserSignConfigVo;
import org.jeecg.modules.toker.tkuser.vo.UserSignMonthVo;
import org.jeecg.modules.toker.tkuser.vo.UserSignVo;

import java.util.HashMap;
import java.util.List;


/**
 * @Description: 签到记录表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
public interface ITkUserSignService extends IService<TkUserSign> {

    /**
     * 获取用户签到记录信息
     * @param pageParamRequest
     * @return
     */
    Page<UserSignVo> getUserSignList(PageParamRequest pageParamRequest) ;

    /**
     * 列表年月
     * @param pageParamRequest 分页类参数
     * @return List<UserSignVo>
     */
    Page<UserSignMonthVo> getListGroupMonth(PageParamRequest pageParamRequest) ;

    /**
     * 签到
     */
    UserSignConfigVo sign() ;

    /**
     * 签到积分详情
     * @return map
     */
    HashMap<String, Object> get() ;

    /**
     * 查询签到记录表信息
     * @return UserSignInfoResponse
     */
    UserSignInfoResponse getUserInfo(UserSignInfoRequest request) ;

    /**
     * 签到配置
     * @return List<UserSignConfigVo>
     */
    Page<UserSignConfigVo> config() ;
}
