package org.jeecg.modules.toker.tkuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.UserAddressRequest;
import org.jeecg.modules.toker.sysconfig.entity.SystemCity;
import org.jeecg.modules.toker.sysconfig.service.SystemCityService;
import org.jeecg.modules.toker.tkuser.entity.TkUserAddress;
import org.jeecg.modules.toker.tkuser.entity.TkUserSign;
import org.jeecg.modules.toker.tkuser.mapper.TkUserAddressMapper;
import org.jeecg.modules.toker.tkuser.service.ITkUserAddressService;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.Optional;
import java.util.jar.JarException;

/**
 * @Description: 用户地址表
 * @Author: Luo.0022
 * @Date:   2021-03-02
 * @Version: V1.0
 */
@Service
public class TkUserAddressServiceImpl extends ServiceImpl<TkUserAddressMapper, TkUserAddress> implements ITkUserAddressService {

    @Autowired
    private ITkUserService userService ;

    @Autowired
    private TkUserAddressMapper userAddressMapper ;

    @Autowired
    private SystemCityService systemCityService;

    /**
     * 列表
     * @return List<UserAddress>
     */
    @Override
    public List<TkUserAddress> getList(PageParamRequest pageParamRequest) {
        Page<TkUserAddress> page = new Page<>(pageParamRequest.getPage(), pageParamRequest.getLimit());

        LambdaQueryWrapper<TkUserAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        return userAddressMapper.selectPage(page, lambdaQueryWrapper).getRecords() ;

    }

    /**
     * 根据基本题条件查询
     * @param address 查询条件
     * @return 结果地址
     */
    @Override
    public TkUserAddress getUserAddress(TkUserAddress address) {
        LambdaQueryWrapper lq = new LambdaQueryWrapper();
        lq.setEntity(address);
        return userAddressMapper.selectOne(lq);
    }

    /**
     * 创建地址
     * @param request UserAddressRequest 参数
     * @return List<UserAddress>
     */
    @Override
    public TkUserAddress create(UserAddressRequest request) {
        TkUserAddress userAddress = new TkUserAddress();
        BeanUtils.copyProperties(request, userAddress);
        userAddress.setCity(request.getAddress().getCity());
        userAddress.setCityId(request.getAddress().getCityId());
        userAddress.setDistrict(request.getAddress().getDistrict());
        userAddress.setProvince(request.getAddress().getProvince());

        if(request.getAddress().getCityId() > 0 && StringUtils.isNotBlank(request.getAddress().getCity())){
            checkCity(userAddress.getCityId());
        }
        userAddress.setUid(userService.getUserIdException());
        if(userAddress.getIsDefault()){
            //把当前用户其他默认地址取消
            cancelDefault(userAddress.getUid());
        }
        userAddress.setIsDel(false);
        saveOrUpdate(userAddress);
        return userAddress;
    }

    /**
     * 设置默认
     * @param id String id
     * @return UserAddress
     */
    @Override
    public boolean def(String id) {
        //把当前用户其他默认地址取消
        cancelDefault(userService.getUserIdException());
        TkUserAddress userAddress = new TkUserAddress();
        userAddress.setId(id);
        userAddress.setUid(userService.getUserIdException());
        userAddress.setIsDefault(true);
        return updateById(userAddress);
    }

    /**
     * 删除
     * @param id String id
     * @return UserAddress
     */
    @Override
    public boolean delete(String id) {
        //把当前用户其他默认地址取消
        LambdaQueryWrapper<TkUserAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserAddress::getId, id);
        lambdaQueryWrapper.eq(TkUserAddress::getUid, userService.getUserIdException());
        userAddressMapper.delete(lambdaQueryWrapper);
        return true;
    }

    /**
     * 获取默认地址
     * @author Mr.Zhang
     * @since 2020-04-28
     * @return UserAddress
     */
    @Override
    public TkUserAddress getDefault() {
        //把当前用户其他默认地址取消
        LambdaQueryWrapper<TkUserAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserAddress::getIsDefault, true);
        lambdaQueryWrapper.eq(TkUserAddress::getUid, userService.getUserId());
        return userAddressMapper.selectOne(lambdaQueryWrapper);
    }

    @Override
    public TkUserAddress getById(Integer addressId) {
        LambdaQueryWrapper<TkUserAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserAddress::getId, addressId);
        return userAddressMapper.selectOne(lambdaQueryWrapper);
    }


    /**
     * 根据地址参数获取用户收货地址
     * @param userAddress
     * @param pageParamRequest
     * @return
     */
    @Override
    public List<TkUserAddress> getListByUserAddress(TkUserAddress userAddress, PageParamRequest pageParamRequest) {
        //PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        Page<TkUserAddress> page = new Page<>(pageParamRequest.getPage(), pageParamRequest.getLimit());

        LambdaQueryWrapper<TkUserAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.setEntity(userAddress);
        IPage<TkUserAddress> userSignList = userAddressMapper.selectPage(page,lambdaQueryWrapper) ;

        return userSignList.getRecords() ;
    }

    /**
     * 检测城市id是否合法
     * @param cityId Integer 城市id
     */
    private void checkCity(Integer cityId){
        //检测城市Id是否存在
        SystemCity systemCity = systemCityService.getCityByCityId(cityId);
        if(systemCity == null){
            throw new JeecgBootException( "请选择正确的城市");
        }
    }

    /**
     * 取消默认地址
     * @param userId Integer 城市id
     */
    private void cancelDefault(String userId){
        //检测城市Id是否存在
        TkUserAddress userAddress = new TkUserAddress();
        userAddress.setIsDefault(false);
        LambdaQueryWrapper<TkUserAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserAddress::getUid, userId);
        update(userAddress, lambdaQueryWrapper);
    }
}
