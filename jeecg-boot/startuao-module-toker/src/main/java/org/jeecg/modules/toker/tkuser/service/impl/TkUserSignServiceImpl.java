package org.jeecg.modules.toker.tkuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.UserOperateFundsRequest;
import org.jeecg.modules.toker.front.request.UserSignInfoRequest;
import org.jeecg.modules.toker.front.response.UserSignInfoResponse;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.entity.TkUserSign;
import org.jeecg.modules.toker.tkuser.mapper.TkUserMapper;
import org.jeecg.modules.toker.tkuser.mapper.TkUserSignMapper;
import org.jeecg.modules.toker.tkuser.service.ITkUserBillService;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.tkuser.service.ITkUserSignService;
import org.jeecg.modules.toker.tkuser.vo.UserSignConfigVo;
import org.jeecg.modules.toker.tkuser.vo.UserSignMonthVo;
import org.jeecg.modules.toker.tkuser.vo.UserSignVo;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description: 签到记录表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
@Service
public class TkUserSignServiceImpl extends ServiceImpl<TkUserSignMapper, TkUserSign> implements ITkUserSignService {

    @Autowired
    private TkUserSignMapper userSignMapper ;

    @Autowired
    private ITkUserService userService ;

    @Autowired
    private ITkUserBillService userBillService ;

    //@Autowired
    //private WechatSendMessageForMinService wechatSendMessageForMinService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ITkSystemConfigService systemConfigService ;

    /**
     * 获取用户签到记录信息
     * @param pageParamRequest
     * @return
     */
    @Override
    public Page<UserSignVo> getUserSignList(PageParamRequest pageParamRequest) {
        Page<UserSignVo> signUsersPage = new Page<>();

        Page<TkUserSign> page = new Page<>(pageParamRequest.getPage(), pageParamRequest.getLimit());
        LambdaQueryWrapper<TkUserSign> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserSign::getType, 1);
        lambdaQueryWrapper.eq(TkUserSign::getUid, userService.getUserIdException());
        lambdaQueryWrapper.orderByDesc(TkUserSign::getId);
        IPage<TkUserSign> userSignList = userSignMapper.selectPage(page,lambdaQueryWrapper) ;
//        List<TkUserSign> userSignList = userSignMapper.selectList(lambdaQueryWrapper);

        ArrayList<UserSignVo> userSignVoList = new ArrayList<>();
        if(userSignList.getTotal() < 1){
            return signUsersPage;
        }

        for (TkUserSign userSign : userSignList.getRecords()) {
            userSignVoList.add(new UserSignVo(userSign.getTitle(), userSign.getNumber(), userSign.getCreateDay()));
        }

        return signUsersPage.setRecords(userSignVoList);
    }

    /**
     * 列表年月
     * @param pageParamRequest 分页类参数
     * @return List<UserSignVo>
     */
    @Override
    public Page<UserSignMonthVo> getListGroupMonth(PageParamRequest pageParamRequest) {
        //PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        Page<UserSignMonthVo> signMonthVoPage = new Page<>();

        Page<TkUserSign> page = new Page<>(pageParamRequest.getPage(), pageParamRequest.getLimit());
        LambdaQueryWrapper<TkUserSign> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserSign::getType, 1);
        lambdaQueryWrapper.eq(TkUserSign::getUid, userService.getUserIdException());
        lambdaQueryWrapper.orderByDesc(TkUserSign::getCreateDay);
        //List<TkUserSign> userSignList = dao.selectList(lambdaQueryWrapper);
        IPage<TkUserSign> userSignList = userSignMapper.selectPage(page, lambdaQueryWrapper) ;

        ArrayList<UserSignMonthVo> signMonthVoArrayList = new ArrayList<>();
        //Page<UserSignMonthVo> signMonthVoArrayList = new Page<>();

        if(userSignList.getTotal() < 1){
            return signMonthVoPage;
        }

        for (TkUserSign userSign : userSignList.getRecords()) {
            String date = DateUtil.dateToStr(userSign.getCreateDay(), TokerCommonConstant.DATE_FORMAT_MONTH);
            UserSignVo userSignVo = new UserSignVo(userSign.getTitle(), userSign.getNumber(), userSign.getCreateDay());

            boolean findResult = false;
            if(signMonthVoArrayList.size() > 0){
                //有数据之后则 判断是否已存在，存在则更新
                for (UserSignMonthVo userSignMonthVo: signMonthVoArrayList) {
                    if(userSignMonthVo.getMonth().equals(date)){
                        userSignMonthVo.getList().add(userSignVo);
                        findResult = true;
                        break;
                    }
                }
            }

            //不存在则创建
            if(!findResult){
                //如果没有找到则需要单独添加
                ArrayList<UserSignVo> userSignVoArrayList = new ArrayList<>();
                userSignVoArrayList.add(userSignVo);
                signMonthVoArrayList.add(new UserSignMonthVo(date, userSignVoArrayList));
            }
        }

        return signMonthVoPage.setRecords(signMonthVoArrayList) ;
    }

    /**
     * 签到
     */
    @Override
    public UserSignConfigVo sign() {
        TkUser user = userService.getInfoException();
        UserSignConfigVo configVo = getSignInfo(user.getId());
        if(configVo == null){
            throw new JeecgBootException("请先配置签到天数！");
        }

        //保存签到数据
        TkUserSign userSign = new TkUserSign();
        userSign.setUid(user.getId());
        userSign.setTitle(TokerCommonConstant.SIGN_TYPE_INTEGRAL_TITLE);
        userSign.setNumber(configVo.getIntegral());
        userSign.setType(TokerCommonConstant.SIGN_TYPE_INTEGRAL);
        userSign.setBalance(user.getIntegral().intValue() + configVo.getIntegral());
        userSign.setCreateDay(DateUtil.strToDate(DateUtil.nowDate(TokerCommonConstant.DATE_FORMAT_DATE), TokerCommonConstant.DATE_FORMAT_DATE));

        //更新用户积分信息
        UserOperateFundsRequest integralFundsRequest = new UserOperateFundsRequest();
        integralFundsRequest.setUid(user.getId());
        integralFundsRequest.setFoundsCategory(TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL);
        integralFundsRequest.setFoundsType(TokerCommonConstant.USER_BILL_TYPE_SIGN);
        integralFundsRequest.setTitle(TokerCommonConstant.SIGN_TYPE_INTEGRAL_TITLE);
        integralFundsRequest.setType(1);
        integralFundsRequest.setValue(new BigDecimal(configVo.getIntegral()));

        //更新用户经验信息
        UserOperateFundsRequest experienceFundsRequest = new UserOperateFundsRequest();
        experienceFundsRequest.setUid(user.getId());
        experienceFundsRequest.setFoundsCategory(TokerCommonConstant.USER_BILL_CATEGORY_EXPERIENCE);
        experienceFundsRequest.setFoundsType(TokerCommonConstant.USER_BILL_TYPE_SIGN);
        experienceFundsRequest.setTitle(TokerCommonConstant.SIGN_TYPE_EXPERIENCE_TITLE);
        experienceFundsRequest.setType(1);
        experienceFundsRequest.setValue(new BigDecimal(configVo.getExperience()));

        //更新用户签到天数
        user.setSignNum(configVo.getDay());
        //更新用户积分
        user.setIntegral(user.getIntegral().add(integralFundsRequest.getValue()));
        // 更新用户经验
        user.setExperience(user.getExperience() + experienceFundsRequest.getValue().intValue());

        Boolean execute = transactionTemplate.execute(e -> {
            //保存签到数据
            save(userSign);
            //更新用户积分信息
            userService.updateFounds(integralFundsRequest, true);
            //更新用户经验信息
            userService.updateFounds(experienceFundsRequest, true);
            //更新用户 签到天数、积分、经验
            userService.updateById(user);
            return Boolean.TRUE;
        });

        if (!execute) {
            throw new JeecgBootException("修改用户签到信息失败!");
        }
        /*
        // 小程序消息积分变动通知
        WechatSendMessageForIntegral integralPram = new WechatSendMessageForIntegral(
                "您的积分变动如下","签到获得积分","签到","0",configVo.getIntegral()+"",
                (user.getIntegral().add(BigDecimal.valueOf(configVo.getIntegral())))+"",
                DateUtil.nowDateTimeStr(),"暂无","暂无","签到赠送积分"
        );
        wechatSendMessageForMinService.sendIntegralMessage(integralPram,user.getId());
*/
        return configVo;
    }

    /**
     * 签到配置
     * @return List<UserSignConfigVo>
     */
    @Override
    public Page<UserSignConfigVo> config() {
        Page<UserSignConfigVo> signMonthVoPage = new Page<>();
        List<UserSignConfigVo> userSignConfigVoList = new ArrayList<>() ;
        //获取配置数据
        for(int i = 1; i <= TokerCommonConstant.DEFAULT_SIGN_CYCLE; i++) {
            userSignConfigVoList.add(config(i)) ;
        }
        return signMonthVoPage.setRecords(userSignConfigVoList) ;
    }

    /**
     * 签到积分详情
     * @return map
     */
    @Override
    public HashMap<String, Object> get() {
        HashMap<String, Object> map = new HashMap<>();
        //当前积分
        TkUser info = userService.getInfo();
        map.put("integral", info.getIntegral());
        //总计签到天数
        map.put("count", signCount(info.getId()));
        //连续签到数据

        //今日是否已经签到
        map.put("today", false);
        return map;
    }

    /**
     * 查询签到记录表信息
     * @return UserSignInfoResponse
     */
    @Override
    public UserSignInfoResponse getUserInfo(UserSignInfoRequest request) {
        UserSignInfoResponse userSignInfoResponse = new UserSignInfoResponse();

        //用户信息
        TkUser user = userService.getInfo();
        BeanUtils.copyProperties(user, userSignInfoResponse);

        //当前用户已经签到完一个周期，那么重置
        if(user.getSignNum().equals(TokerCommonConstant.DEFAULT_SIGN_CYCLE)){
            userSignInfoResponse.setSignNum(0);
            userService.repeatSignNum(user.getId());
        }

        //签到
        if(request.getAll() || request.getSign()){
            boolean isYesterdaySign = checkYesterdaySign(user.getId()) ;
            boolean isDaySign = checkDaySign(user.getId()) ;
            userSignInfoResponse.setSumSignDay(getCount(user.getId()));
            userSignInfoResponse.setIsDaySign(isDaySign);
            userSignInfoResponse.setIsYesterdaySign(isYesterdaySign);
            // 若当天未签到  昨天也未签到，设置签到次数为0
            if ( !(isYesterdaySign || isDaySign) ){
                userSignInfoResponse.setSignNum(0);
            }
        }

        //积分
        if(request.getAll() || request.getSign()){
            userSignInfoResponse.setSumIntegral(userBillService.getSumInteger(1, user.getId(), TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL, null, null));
            userSignInfoResponse.setDeductionIntegral(userBillService.getSumInteger(0, user.getId(), TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL, null, null));
//            userSignInfoResponse.setYesterdayIntegral(userBillService.getSumInteger(1, user.getUid(), Constants.USER_BILL_CATEGORY_INTEGRAL, Constants.SEARCH_DATE_YESTERDAY, null));
            //实际上是今日获得积分
            userSignInfoResponse.setYesterdayIntegral(userBillService.getSumInteger(1, user.getId(), TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL, TokerCommonConstant.SEARCH_DATE_DAY, null));
        }

        return userSignInfoResponse;
    }

    /**
     * 检测今天是否签到
     * @param userId String 用户id
     * @return UserSignInfoResponse
     */
    private Boolean checkDaySign(String userId) {
        List<TkUserSign> userSignList = getInfoByDay(userId, DateUtil.nowDate(TokerCommonConstant.DATE_FORMAT_DATE));
        return userSignList.size() != 0;
    }

    /**
     * 检测昨天天是否签到
     * @param userId String 用户id
     * @return UserSignInfoResponse
     */
    private Boolean checkYesterdaySign(String userId) {
        String day = DateUtil.nowDate(TokerCommonConstant.DATE_FORMAT_DATE);
        String yesterday = DateUtil.addDay(day, -1, TokerCommonConstant.DATE_FORMAT_DATE);
        List<TkUserSign> userSignList = getInfoByDay(userId, yesterday);
        return userSignList.size() != 0;
    }

    /**
     * 根据日期查询数据
     * @param userId String 用户id
     * @param date Date 日期
     * @return UserSignInfoResponse
     */
    private List<TkUserSign> getInfoByDay(String userId, String date) {
        LambdaQueryWrapper<TkUserSign> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserSign::getUid, userId).eq(TkUserSign::getType, 1).eq(TkUserSign::getCreateDay, date);
        return userSignMapper.selectList(lambdaQueryWrapper);

    }


    /**
     * 累计签到次数
     * @param userId String 用户id
     * @return UserSignInfoResponse
     */
    private Integer getCount(String userId) {
        LambdaQueryWrapper<TkUserSign> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserSign::getUid, userId).eq(TkUserSign::getType, 1);
        return userSignMapper.selectCount(lambdaQueryWrapper);
    }

    /**
     * 获取签到积分
     * @author Mr.Zhang
     * @param userId Integer 用户id
     * @since 2020-04-30
     * @return Integer
     */
    private UserSignConfigVo getSignInfo(String userId) {
        //先看用户上次签到是什么日期， 如果有断开那么就重置
        checkRepeat(userId);

        //获取用户连续签到天数
        TkUser user = userService.getInfo();

        //如果已经签到一个周期，那么再次签到的时候直接从第一天重新开始
        if(user.getSignNum().equals(TokerCommonConstant.DEFAULT_SIGN_CYCLE)){
            user.setSignNum(0);
            userService.repeatSignNum(userId);
        }
        Integer signTimes = 1 ;//当天签到次数
        if(user.getSignNum() + 1 <= TokerCommonConstant.DEFAULT_SIGN_CYCLE){
            signTimes = user.getSignNum() + 1 ;
        }
        //获取签到当天数据
        UserSignConfigVo config = config(signTimes);
        return config;
    }

    /**
     * 检测是否需要重置累计签到天数
     * @param userId Integer 用户id
     * @param userId Integer 用户id
     */
    private void checkRepeat(String userId) {
       // PageHelper.startPage(TokerCommonConstant.DEFAULT_PAGE, TokerCommonConstant.DEFAULT_PAGE);
        Page<TkUserSign> page = new Page<>(TokerCommonConstant.DEFAULT_PAGE, TokerCommonConstant.DEFAULT_PAGE);
        LambdaQueryWrapper<TkUserSign> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.select(TkUserSign::getCreateDay).eq(TkUserSign::getUid, userId).orderByDesc(TkUserSign::getCreateDay);
        IPage<TkUserSign> userSignList = userSignMapper.selectPage(page, lambdaQueryWrapper) ;
        if(userSignList.getTotal() < 1){
            //没有签到过
            userService.repeatSignNum(userId);
            return;
        }

        //签到时间 +1 天
        String lastDate = DateUtil.dateToStr(userSignList.getRecords().get(0).getCreateDay(), TokerCommonConstant.DATE_FORMAT_DATE);
        String nowDate = DateUtil.nowDate(TokerCommonConstant.DATE_FORMAT_DATE);
        String nextDate = DateUtil.addDay(userSignList.getRecords().get(0).getCreateDay(), 1, TokerCommonConstant.DATE_FORMAT_DATE);

        int compareDate = DateUtil.compareDate(nextDate, nowDate, TokerCommonConstant.DATE_FORMAT_DATE);

        //对比今天数据
        if(DateUtil.compareDate(lastDate, nowDate, TokerCommonConstant.DATE_FORMAT_DATE) == 0){
            throw new JeecgBootException("今日已签到。不可重复签到");
        }

        if(compareDate != 0){
            //不相等，所以不是连续签到
            userService.repeatSignNum(userId);
        }
    }

    /**
     * 累计签到天数
     * @param userId String 用户id
     * @return Integer
     */
    private Integer signCount(String userId) {
        LambdaQueryWrapper<TkUserSign> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUserSign::getUid, userId);
        return userSignMapper.selectCount(lambdaQueryWrapper);
    }

    /**
     * dayNum 第几天
     * 每次签到10积分，10经验值，
     *
     * 若连续签到，以天数相乘，但最多连接签到7天，7天后归0
     *
     * @return
     */
    private UserSignConfigVo config(Integer dayNum){
        String signIntegralStr = systemConfigService.getValueByKey(TokerCommonConstant.DEFAULT_SET_SIGN_INTEGRAL);
        String signExperienceStr = systemConfigService.getValueByKey(TokerCommonConstant.DEFAULT_SET_SIGN_EXPERIENCE);
        Integer signIntegral , signExperience ;
        if (signIntegralStr != null ) {
            signIntegral = Integer.parseInt(signIntegralStr) ;
        } else {
            signIntegral = TokerCommonConstant.DEFAULT_SIGN_INTEGRAL ;
        }
        if (signExperienceStr != null ) {
            signExperience = Integer.parseInt(signExperienceStr) ;
        } else {
            signExperience = TokerCommonConstant.DEFAULT_SIGN_EXPERIENCE ;
        }

        UserSignConfigVo configVo = new UserSignConfigVo() ;
        String dayTitle = null ;
        if (dayNum==1){
            dayTitle = "第一天" ;
        } else if (dayNum==2){
            dayTitle = "第二天" ;
        } else if (dayNum==3){
            dayTitle = "第三天" ;
        } else if (dayNum==4){
            dayTitle = "第四天" ;
        } else if (dayNum==5){
            dayTitle = "第五天" ;
        } else if (dayNum==6){
            dayTitle = "第六天" ;
        } else if (dayNum==7){
            dayTitle = "第七天" ;
        }
        configVo.setTitle(dayTitle) ;
        configVo.setDay(dayNum) ;
        configVo.setIntegral(signIntegral * dayNum) ;
        configVo.setExperience(signExperience * dayNum ) ;

        return configVo ;
    }
}
