package org.jeecg.modules.toker.tkuser.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.toker.authorization.manager.TokenManager;
import org.jeecg.modules.toker.authorization.model.TokenModel;
import org.jeecg.modules.toker.constants.SmsConstants;
import org.jeecg.modules.toker.front.request.*;
import org.jeecg.modules.toker.front.response.LoginResponse;
import org.jeecg.modules.toker.front.response.UserCenterResponse;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.jeecg.modules.toker.tkuser.entity.TkMemberUserLevel;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.entity.TkUserBill;
import org.jeecg.modules.toker.tkuser.mapper.TkUserMapper;
import org.jeecg.modules.toker.tkuser.service.ITkMemberUserLevelService;
import org.jeecg.modules.toker.tkuser.service.ITkUserBillService;
import org.jeecg.modules.toker.tkuser.service.ITkUserLevelService;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.tkuser.vo.SysUserComboModel;
import org.jeecg.modules.toker.tkuser.vo.UserOperateFundsVo;
import org.jeecg.modules.toker.utils.CrmebUtil;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 拓客用户信息
 * @Author: Luo.0022
 * @Date:   2020-12-30
 * @Version: V1.0
 */
@Service
public class TkUserServiceImpl extends ServiceImpl<TkUserMapper, TkUser> implements ITkUserService {

    @Autowired
    private TkUserMapper userMapper ;

    @Autowired
    private ITkSystemConfigService systemConfigService ;

    @Autowired
    private TokenManager tokenManager;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private ITkUserBillService userBillService;

    @Autowired
    private ITkMemberUserLevelService memberUserLevelService ;

    @Autowired
    private ITkUserLevelService userLevelService ;


    /**
     * 账号密码登录
     * @author Mr.Zhang
     * @since 2020-04-28
     */
    @Override
    public LoginResponse login(LoginRequest request) throws Exception {
        TkUser user = getUserByAccount(request.getPhone());
        if(user == null){
            throw new JeecgBootException("此账号未注册");
        }

        if(user.getStatus()==0){
            throw new JeecgBootException("此账号被禁用") ;
        }

        String password = CrmebUtil.encryptPassword(request.getPassword(), request.getPhone());
        if(!user.getPwd().equals(password)){
            throw new JeecgBootException("密码错误");
        }

        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setToken(makeToken(user));
        //loginResponse.setToken(token(user));
        user.setPwd(null);

        //绑定推广关系
        //bindSpread(user, request.getSpreadPid());

        loginResponse.setUser(user);

        return loginResponse;
    }

    /**
     *  生成Token  ， 并 把值保存在Redis中
     * @param user
     * @return
     */
    private String makeToken(TkUser user) {
        String passwordId = user.getId() ;
        String username = user.getAccount() ;
        // 生成token
        String token = JwtUtil.sign(username, passwordId);
        // 设置token缓存有效时间
        redisUtil.set(TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX + token, token);
        redisUtil.expire(TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX, JwtUtil.EXPIRE_TIME*2 / 1000);

        return token ;
    }

    /**
     * 退出
     * @param request HttpServletRequest
     * @author Luo.0022
     * @since 2021-02-19
     */
    @Override
    public void loginOut(HttpServletRequest request) {
        String token = getTokenFormRequest(request) ;
        tokenManager.deleteToken(token, TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX);
    }

    /**
     * 获取请求Token
     * @param request
     * @return
     * @author Luo.0022
     * @since 2021-02-19
     */
    private String getTokenFormRequest(HttpServletRequest request){
        String pathToken =request.getParameter(TokerCommonConstant.HEADER_AUTHORIZATION_KEY);
        if(null != pathToken){
            return pathToken;
        }
        return request.getHeader(TokerCommonConstant.HEADER_AUTHORIZATION_KEY);
    }
    /**
     * 通过账号获取用户信息
     * @author Luo.0022
     * @since 2021-01-14
     */
    public TkUser getUserByAccount(String account) {

        return userMapper.getUserByAccount(account) ;
    }

    /**
     * 绑定手机号
     * @author Luo.0022
     * @since 2021-01-15
     * @return boolean
     */
    @Override
    public boolean bind(UserBindingRequest request) {
        //检测验证码
        checkValidateCode(request.getPhone(), request.getValidateCode());

        //删除验证码
        redisUtil.del(getValidateCodeRedisKey(request.getPhone()));

        //检测当前手机号是否已经是账号
        TkUser user = getUserByAccount(request.getPhone());
        if(null != user){
            throw new JeecgBootException("此手机号码已被注册");
        }

        //查询手机号信息
        TkUser bindUser = getInfoException();
        bindUser.setAccount(request.getPhone());
        bindUser.setPhone(request.getPhone());

        return updateById(bindUser);
    }
    /**
     * 检测手机验证码
     * @param phone String 手机号
     * @return String
     */
    @Override
    public String getValidateCodeRedisKey(String phone) {
        return SmsConstants.SMS_VALIDATE_PHONE + phone;
    }

    /**
     * 获取个人资料
     * @author Mr.Zhang
     * @since 2020-04-28
     * @return User
     */
    @Override
    public TkUser getUserPromoter() {
        TkUser user = getInfo();
        if(null == user){
            return null;
        }

        if(user.getStatus()==0){
            user.setIsPromoter(0);
            return user;
        }

        //获取分销
        //检测商城是否开启分销功能
        String isOpen = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_STORE_BROKERAGE_IS_OPEN);
        if(StringUtils.isBlank(isOpen) || isOpen.equals("0")){
            user.setIsPromoter(0);
        }else{
            String type = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_STORE_BROKERAGE_MODEL);
            if(StringUtils.isBlank(type) || type.equals("2")){
                //人人分销
                user.setIsPromoter(1);
            }
        }
        return user;
    }
    /**
     * 获取个人资料
     * @return User
     */
    @Override
    public TkUser getInfoException() {
        TkUser user = getInfo();
        if(user == null){
            throw new JeecgBootException("用户信息不存在！");
        }

        if(user.getStatus()==0){
            throw new JeecgBootException("用户已经被禁用！");
        }
        return user;
    }

    @Override
    public TkUser getInfoEmpty() {
        TkUser user = getInfo();
        if(user == null){
            user = new TkUser();
        }

        return user;
    }

    /**
     * 获取当前用户id
     * @return String
     */
    @Override
    public String getUserIdException() {
        //return Integer.parseInt(tokenManager.getLocalInfoException("id"));
        return tokenManager.getLocalInfoException("id");
    }

    /**
     * 获取当前用户id
     * @return Integer
     */
    @Override
    public Integer getUserId() {
        Object id = tokenManager.getLocalInfo("id");
        if(null == id){
            return 0;
        }
        return Integer.parseInt(id.toString());
    }
    /**
     * 获取当前用户id
     * @return String
     */
    @Override
    public String getUserIdStr() {
        Object id = tokenManager.getLocalInfo("id");
        if(null == id){
            return "0";
        }
        return id.toString();
    }
    /**
     * 获取个人资料
     * @return User
     */
    @Override
    public TkUser getInfo() {
        //if(getUserId() == 0){
        if( "0" == getUserIdStr()){
            return null;
        }
        return getById(getUserIdStr());
    }

    /**
     * 修改密码
     * @param request PasswordRequest 密码
     * @return boolean
     */
    @Override
    public boolean password(PasswordRequest request) {
        //检测验证码
        checkValidateCode(request.getPhone(), request.getValidateCode());

        LambdaQueryWrapper<TkUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkUser::getAccount, request.getPhone());
        TkUser user = userMapper.selectOne(lambdaQueryWrapper);

        //密码
        user.setPwd(CrmebUtil.encryptPassword(request.getPassword(), user.getAccount()));
        return update(user, lambdaQueryWrapper);
    }

    /**
     * 用户中心
     * @return UserCenterResponse
     */
    @Override
    public UserCenterResponse getUserCenter() {
        UserCenterResponse userCenterResponse = new UserCenterResponse();

        TkUser currentUser = getInfo();
        BeanUtils.copyProperties(currentUser, userCenterResponse);
        userCenterResponse.setIntegral(currentUser.getIntegral().intValue());
//        UserCenterOrderStatusNumResponse userCenterOrderStatusNumResponse = new UserCenterOrderStatusNumResponse();
//        userCenterOrderStatusNumResponse.setNoBuy(0);
//        userCenterOrderStatusNumResponse.setNoPink(0);
//        userCenterOrderStatusNumResponse.setNoPostage(0);
//        userCenterOrderStatusNumResponse.setNoRefund(0);
//        userCenterOrderStatusNumResponse.setNoReply(0);
//        userCenterOrderStatusNumResponse.setNoTake(0);
//        userCenterResponse.setOrderStatusNum(userCenterOrderStatusNumResponse);
//        PageParamRequest pageParamRequest = new PageParamRequest();
//        pageParamRequest.setPage(1); pageParamRequest.setLimit(999);
        // 优惠券
//        List<StoreCouponUserResponse> storeCoupons = storeCouponUserService.getListFront(getUserIdException(), pageParamRequest);
//        userCenterResponse.setCouponCount(null != storeCoupons ? storeCoupons.size():0);
        userCenterResponse.setLevel(currentUser.getLevel());

        // 判断是否开启会员功能
//        Integer memberFuncStatus = Integer.valueOf(systemConfigService.getValueByKey("member_func_status"));
//        if(memberFuncStatus == 0){
//            userCenterResponse.setVip(false);
//        }else{
//            userCenterResponse.setVip(userCenterResponse.getLevel() > 0);
//            UserLevel userLevel = userLevelService.getUserLevelByUserId(currentUser.getUid());
//            if(null != userLevel){
//                SystemUserLevel systemUserLevel = systemUserLevelService.getByLevelId(userLevel.getLevelId());
//                userCenterResponse.setVipIcon(systemUserLevel.getIcon());
//                userCenterResponse.setVipName(systemUserLevel.getName());
//            }
//        }
        String rechargeSwitch = systemConfigService.getValueByKey("recharge_switch");
        if (StrUtil.isNotBlank(rechargeSwitch)) {
            userCenterResponse.setRechargeSwitch(Boolean.valueOf(rechargeSwitch));
        }

        return userCenterResponse;
    }

    /**
     * 登录用户生成token
     */
    @Override
    public String token(TkUser user) throws Exception {
        TokenModel token = tokenManager.createToken(user.getAccount(), user.getId(), TokerCommonConstant.USER_TOKEN_REDIS_KEY_PREFIX);
        return token.getToken();
    }

    /**
     * 通过微信信息注册用户
     * @param thirdUserRequest RegisterThirdUser 三方用户登录注册信息
     * @return User
     */
    @Override
    public TkUser registerByThird(RegisterThirdUserRequest thirdUserRequest, String type) {
        TkUser user = new TkUser();
        user.setAccount(DigestUtils.md5Hex(CrmebUtil.getUuid() + DateUtil.getNowTime()));
        //user.setAccount(thirdUserRequest.getAccount()) ; //Luo.0022  以OpenId 作了用户名
        // user.setUserType(type);
        user.setLoginType(type) ; // 登陆类型
        user.setNickname(thirdUserRequest.getNickName());

        if(thirdUserRequest.getTenantId()!=null){
            user.setTenantId(thirdUserRequest.getTenantId()) ;
        }

        String _avatar = null;
        switch (type) {
            case TokerCommonConstant.USER_LOGIN_TYPE_PUBLIC:
                _avatar = thirdUserRequest.getHeadimgurl();
                break;
            case TokerCommonConstant.USER_LOGIN_TYPE_PROGRAM:
            case TokerCommonConstant.USER_LOGIN_TYPE_H5:
                _avatar = thirdUserRequest.getAvatar();
                break;
        }
        user.setAvatar(_avatar);
        user.setSpreadUid(thirdUserRequest.getSpreadPid());
        user.setSpreadTime(DateUtil.nowDateTime());
        user.setSex(Integer.parseInt(thirdUserRequest.getSex()));
        user.setAddres(thirdUserRequest.getCountry() + "," + thirdUserRequest.getProvince() + "," + thirdUserRequest.getCity());
        user.setStatus(1) ; // 注册默认设置状态 为正常 1 。

        save(user);
        return user;
    }


    /**
     * 检测手机验证码
     * @author Luo.0022
     * @since 2021-01-15
     */
    private void checkValidateCode(String phone, String value) {
        Object validateCode = redisUtil.get(getValidateCodeRedisKey(phone));
        if(validateCode == null){
            throw new JeecgBootException("验证码已过期");
        }

        if(!validateCode.toString().equals(value)){
            throw new JeecgBootException("验证码错误");
        }
    }

    /**
     * 重置连续签到天数
     * @param userId Integer 用户id
     */
    @Override
    public void repeatSignNum(String userId) {
        TkUser user = new TkUser();
        user.setId(userId);
        user.setSignNum(0);
        updateById(user);
    }


    /**
     * 等级升级
     * @param userId String 用户id
     */
    public void upLevel(String userId) {
        //确定当前经验所达到的等级
        UserLevelSearchRequest userLevelSearchRequest = new UserLevelSearchRequest();
        userLevelSearchRequest.setIsDel(false);
        userLevelSearchRequest.setIsShow(true);

        LambdaQueryWrapper<TkMemberUserLevel> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkMemberUserLevel::getIsDel, false);
        lambdaQueryWrapper.eq(TkMemberUserLevel::getIsShow, false);


        List<TkMemberUserLevel> list = memberUserLevelService.getList(userLevelSearchRequest);

        TkUser user = getById(userId);
        TkMemberUserLevel userLevelConfig = new TkMemberUserLevel();
        for (TkMemberUserLevel systemUserLevel : list) {
            if(user.getExperience() > systemUserLevel.getExperience()){
                userLevelConfig = systemUserLevel;
                continue;
            }
            break;
        }

        if(userLevelConfig.getId() == null){
            return;
        }

        //更新用户等级
        userLevelService.level(userId, userLevelConfig.getId());
    }

    /**
     * 更新操作资金
     */
    @Override
    public boolean updateFounds(UserOperateFundsRequest request, boolean isSaveBill) {
        UserOperateFundsVo userOperateFundsVo = new UserOperateFundsVo(request.getUid(), request.getFoundsCategory(), getFounds(request));
        //增加日志， 创建计算数值之前的数据
        if(isSaveBill){
            createUserBill(request, getUserBalance(request));
        }

        Boolean result = userMapper.updateFounds(userOperateFundsVo);    //更新数值

        if(request.getFoundsCategory().equals(TokerCommonConstant.USER_BILL_CATEGORY_EXPERIENCE)){
            //经验升级
            upLevel(request.getUid());
        }

        return result;
    }

    /**
     * 资金操作日志增加
     * @author Mr.Zhang
     * @since 2020-04-10
     */
    private void createUserBill(UserOperateFundsRequest request, BigDecimal balance) {
        TkUserBill userBill = new TkUserBill();
        userBill.setTitle(request.getTitle());
        userBill.setUid(request.getUid());
        userBill.setCategory(request.getFoundsCategory());
        userBill.setType(request.getFoundsType());
        userBill.setNumber(request.getValue());
        userBill.setLinkId(request.getLinkId());  //链接id
        userBill.setMark(getTitle(request).replace("{$value}", request.getValue().toString()));
        userBill.setPm(request.getType());
        userBill.setBalance(balance.add(request.getValue()));
        userBill.setStatus(1) ; // 默认有效为 1
        userBillService.save(userBill);
    }

    /**
     * 日志title
     * @author Mr.Zhang
     * @since 2020-04-10
     * @return String
     */
    private String getTitle(UserOperateFundsRequest request) {
        String operate = (request.getType() == 1) ? "增加" : "减少";
        String founds = "";
        switch (request.getFoundsCategory()){
            case TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL:
                founds = "积分";
                break;
            case TokerCommonConstant.USER_BILL_CATEGORY_MONEY:
                founds = "余额";
                break;
            case TokerCommonConstant.USER_BILL_CATEGORY_EXPERIENCE:
                founds = "经验";
                break;
            case TokerCommonConstant.USER_BILL_CATEGORY_BROKERAGE_PRICE:
                founds = "佣金";
                break;
        }

        return TokerCommonConstant.USER_BILL_OPERATE_LOG_TITLE.replace("{$title}", request.getTitle()).replace("{$operate}", operate).replace("{$founds}", founds);
    }

    /**
     * 获取用户资金
     */
    private BigDecimal getUserBalance(UserOperateFundsRequest request) {
        //获取用户信息
        TkUser user = getById(request.getUid());

        BigDecimal value = null;
        if(request.getFoundsCategory().equals(TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL)){
            value = user.getIntegral();
        }

        if(request.getFoundsCategory().equals(TokerCommonConstant.USER_BILL_CATEGORY_MONEY)){
            value = user.getNowMoney();
        }

        if(request.getFoundsCategory().equals(TokerCommonConstant.USER_BILL_CATEGORY_EXPERIENCE)){
            value = new BigDecimal(user.getExperience());
        }

        if(request.getFoundsCategory().equals(TokerCommonConstant.USER_BILL_CATEGORY_BROKERAGE_PRICE)){
            value = user.getBrokeragePrice();
        }


        return value;
    }

    /**
     * 操作资金
     */
    private String getFounds(UserOperateFundsRequest request) {
        if(request.getType() == 1){
            return " +" + request.getValue();
        }
        if(request.getType() == 0){
            checkFounds(request);
            return " -" + request.getValue();
        }

        return " +0.00";
    }

    /**
     * 操作资金
     */
    private void checkFounds(UserOperateFundsRequest request) {
        BigDecimal value = getUserBalance(request);
        if(value == null){
            throw new JeecgBootException("请选择正确的操作类型");
        }

        int result = value.subtract(request.getValue()).compareTo(BigDecimal.ZERO);
        if(result < 0){
            throw new JeecgBootException("此用户当前资金为 " + value + "， 需要减少的资金不能大于 " + value);
        }
    }

    /**
     *
     * 通过 比率 进行换算成积分
     *
     * 客户通过报备之后增加经验和积分
     * @param uid String 用户id
     * @param price BigDecimal 实际支付金额
     * @return void
     */
    @Override
    public void consumeAfterUpdateUserFounds(String uid, BigDecimal price, String type) {
        //赠送积分比例
        String integralStr = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_INTEGRAL_RATE);
        BigDecimal integral = new BigDecimal(integralStr);

        //更新用户积分信息
        UserOperateFundsRequest founds = new UserOperateFundsRequest();
        founds.setFoundsType(type);
        founds.setTitle(TokerCommonConstant.ORDER_LOG_MESSAGE_PAY_SUCCESS);

        founds.setUid(uid);
        founds.setFoundsCategory(TokerCommonConstant.USER_BILL_CATEGORY_INTEGRAL);
        founds.setType(1);

        //参考 CrmebUtil getRate说明
        founds.setValue(integral.multiply(price).setScale(0, BigDecimal.ROUND_DOWN));
        updateFounds(founds, true);

        //更新用户经验信息
        founds.setUid(uid);
        founds.setFoundsCategory(TokerCommonConstant.USER_BILL_CATEGORY_EXPERIENCE);
        founds.setType(1);
        founds.setValue(price.setScale(0, BigDecimal.ROUND_DOWN));
        updateFounds(founds, true);
    }


    /**
     * 添加/扣减佣金
     * @param uid 用户id
     * @param price 金额
     * @param brokeragePrice 历史金额
     * @param type 类型：add—添加，sub—扣减
     * @return Boolean
     */
    @Override
    public Boolean operationBrokerage(String uid, BigDecimal price, BigDecimal brokeragePrice, String type) {
        UpdateWrapper<TkUser> updateWrapper = new UpdateWrapper<>();
        if (type.equals("add")) {
            updateWrapper.setSql(StrUtil.format("brokerage_price = brokerage_price + {}", price));
        } else {
            updateWrapper.setSql(StrUtil.format("brokerage_price = brokerage_price - {}", price));
            updateWrapper.last(StrUtil.format(" and (brokerage_price - {} >= 0)", price));
        }
        updateWrapper.eq("id", uid);
        updateWrapper.eq("brokerage_price", brokeragePrice);
        return update(updateWrapper);
    }

    /**
     * 添加/扣减余额
     * @param uid 用户id
     * @param price 金额
     * @param nowMoney 历史金额
     * @param type 类型：add—添加，sub—扣减
     */
    @Override
    public Boolean operationNowMoney(String uid, BigDecimal price, BigDecimal nowMoney, String type) {
        UpdateWrapper<TkUser> updateWrapper = new UpdateWrapper<>();
        if (type.equals("add")) {
            updateWrapper.setSql(StrUtil.format("now_money = now_money + {}", price));
        } else {
            updateWrapper.setSql(StrUtil.format("now_money = now_money - {}", price));
            updateWrapper.last(StrUtil.format(" and (now_money - {} >= 0)", price));
        }
        updateWrapper.eq("id", uid);
        updateWrapper.eq("now_money", nowMoney);
        return update(updateWrapper);
    }


    /**
     * 添加/扣减积分
     * @param uid 用户id
     * @param integral 积分
     * @param nowIntegral 历史积分
     * @param type 类型：add—添加，sub—扣减
     * @return Boolean
     */
    @Override
    public Boolean operationIntegral(String uid, Integer integral, Integer nowIntegral, String type) {
        UpdateWrapper<TkUser> updateWrapper = new UpdateWrapper<>();
        if (type.equals("add")) {
            updateWrapper.setSql(StrUtil.format("integral = integral + {}", integral));
        } else {
            updateWrapper.setSql(StrUtil.format("integral = integral - {}", integral));
            updateWrapper.last(StrUtil.format(" and (integral - {} >= 0)", integral));
        }
        updateWrapper.eq("id", uid);
        updateWrapper.eq("integral", nowIntegral);
        return update(updateWrapper);
    }

    /**
     * add Luo.0022 at 2021-04-20
     * 查询后台系统所有用户 SysUserComboModel
     * @return
     */
    @Override
    public List<SysUserComboModel> queryAllUserBackSysCombo() {
        
        return userMapper.queryAllUserBackSysCombo();
    }

}
