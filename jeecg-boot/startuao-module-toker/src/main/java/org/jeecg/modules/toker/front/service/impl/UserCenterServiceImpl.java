package org.jeecg.modules.toker.front.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.constants.BrokerageRecordConstants;
import org.jeecg.modules.toker.front.request.FundsMonitorSearchRequest;
import org.jeecg.modules.toker.front.request.RegisterThirdUserRequest;
import org.jeecg.modules.toker.front.response.LoginResponse;
import org.jeecg.modules.toker.front.response.UserBalanceResponse;
import org.jeecg.modules.toker.front.response.UserRechargeBillRecordResponse;
import org.jeecg.modules.toker.front.service.UserCenterService;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.entity.TkUserBill;
import org.jeecg.modules.toker.tkuser.entity.TkUserBrokerageRecord;
import org.jeecg.modules.toker.tkuser.entity.UserToken;
import org.jeecg.modules.toker.tkuser.mapper.TkUserBrokerageRecordMapper;
import org.jeecg.modules.toker.tkuser.mapper.TkUserMapper;
import org.jeecg.modules.toker.tkuser.service.*;
import org.jeecg.modules.toker.utils.DateUtil;
import org.jeecg.modules.toker.wechat.response.WeChatAuthorizeLoginGetOpenIdResponse;
import org.jeecg.modules.toker.wechat.response.WeChatAuthorizeLoginUserInfoResponse;
import org.jeecg.modules.toker.wechat.response.WeChatProgramAuthorizeLoginGetOpenIdResponse;
import org.jeecg.modules.toker.wechat.service.WeChatService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * 用户中心 服务实现类
 */
@Service
public class UserCenterServiceImpl extends ServiceImpl<TkUserMapper, TkUser> implements UserCenterService {

    @Autowired
    private ITkUserService userService ;
    @Autowired
    private ITkSystemConfigService systemConfigService ;

    @Autowired
    private UserTokenService userTokenService;

    @Autowired
    private WeChatService weChatService;

    @Autowired
    private ITkUserBillService userBillService ;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ITkUserBrokerageRecordService userBrokerageRecordService;

    @Autowired
    private ITkUserExtractService userExtractService ;

    /**
     * 获取微信授权logo
     * @return String;
     */
    @Override
    public String getLogo() {
        String url = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_SITE_LOGO);
        return url;
    }

    /**
     * 微信登录
     * @return UserSpreadOrderResponse;
     */
    @Override
    @Transactional(rollbackFor = {RuntimeException.class, Error.class, JeecgBootException.class})
    public LoginResponse weChatAuthorizeLogin(String code, Integer spreadUid) {
        try{
            //System.out.println("code = " + code);
            WeChatAuthorizeLoginGetOpenIdResponse response = weChatService.authorizeLogin(code);
            //System.out.println("response = " + response);
            TkUser user = publicLogin(response.getOpenId(), response.getAccessToken());
            //System.out.println("user = " + user);
            //通过用户id获取登录token信息
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setToken(userService.token(user));
            user.setPwd(null);
            //绑定推广关系
            //userService.bindSpread(user, spreadUid);
            loginResponse.setUser(user);

            return loginResponse;
        }catch (Exception e){
            throw new JeecgBootException(e.getMessage());
        }
    }

    /**
     * 小程序登录
     * @param code String 前端临时授权code
     * @param request RegisterThirdUserRequest 用户信息
     * @return UserSpreadOrderResponse;
     */
    @Override
    public LoginResponse weChatAuthorizeProgramLogin(String code, RegisterThirdUserRequest request) {
        try{
            WeChatProgramAuthorizeLoginGetOpenIdResponse response = weChatService.programAuthorizeLogin(code);
            //System.out.println("小程序登陆成功 = " + JSON.toJSONString(response));
            System.out.println("小程序登陆成功 UserCenterServiceImpl weChatAuthorizeProgramLogin openId = " + response.getOpenId());
            TkUser user = programLogin(response.getOpenId(), response.getUnionId(), request);
            //通过用户id获取登录token信息
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setSessionKey(response.getSessionKey()) ;
            loginResponse.setToken(userService.token(user));
            user.setPwd(null);
            //绑定推广关系
            //userService.bindSpread(user, request.getSpreadPid());
            loginResponse.setUser(user);
            // 设置Token过期时间
            long time = TokerCommonConstant.TOKEN_EXPRESS_MINUTES * 60;
            loginResponse.setExpiresTime(DateUtil.addSecond(DateUtil.nowDateTime(), (int)time));
            return loginResponse;
        }catch (Exception e){
            throw new JeecgBootException(e.getMessage());
        }
    }




    /**
     * 通过openId登录
     * @param openId  String 微信公众号通过code取回的openId对象
     * @param token String code换取的token
     * @return List<LoginResponse>
     */
    private TkUser publicLogin(String openId, String token) {
        try {
            //检测是否存在
            //System.out.println("openId = " + openId);
            //System.out.println("token = " + token);
            UserToken userToken = userTokenService.checkToken(openId,  TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_PUBLIC);
            //System.out.println("userToken = " + userToken);
            if(null != userToken){
                return userService.getById(userToken.getUid());
            }

            //没有注册， 获取微信用户信息， 小程序需要前端传入用户信息参数
            WeChatAuthorizeLoginUserInfoResponse userInfo = weChatService.getUserInfo(openId, token);
            //System.out.println("userInfo = " + userInfo);
            RegisterThirdUserRequest registerThirdUserRequest = new RegisterThirdUserRequest();
            BeanUtils.copyProperties(userInfo, registerThirdUserRequest);
            //System.out.println("registerThirdUserRequest = " + registerThirdUserRequest);
            String unionId = userInfo.getUnionId();

            //看unionid是否已经绑定
            if(StringUtils.isNotBlank(unionId)){
                userToken = userTokenService.checkToken(userInfo.getUnionId(), TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_UNION_ID);
                if(null != userToken){
                    return userService.getById(userToken.getUid());
                }
            }

            //TODO 是否需要强制注册用户，1 强制注册，2 需要返回数据给前端，让其输入手机号和验证码
            TkUser user = userService.registerByThird(registerThirdUserRequest, TokerCommonConstant.USER_LOGIN_TYPE_PUBLIC);

            userTokenService.bind(openId, TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_PUBLIC, user.getId());
            if(StringUtils.isNotBlank(unionId)) {
                //有就绑定
                userTokenService.bind(unionId, TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_UNION_ID, user.getId());
            }
            return user;
        }catch (Exception e){
            throw new JeecgBootException(e.getMessage());
        }
    }
    /**
     * 小程序登录
     * @param openId  String 微信公众号通过code取回的openId对象
     * @param unionId String unionId
     * @return List<LoginResponse>
     */
    private TkUser programLogin(String openId, String unionId, RegisterThirdUserRequest request) {
        try {
            //检测是否存在
            UserToken userToken = userTokenService.checkToken(openId, TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_PROGRAM);
            if(null != userToken){
                return userService.getById(userToken.getUid());
            }

            if(StringUtils.isNotBlank(unionId)) {
                userToken = userTokenService.checkToken(unionId, TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_PROGRAM);
                if (null != userToken) {
                    return userService.getById(userToken.getUid());
                }
            }

            //TODO 是否需要强制注册用户，1 强制注册，2 需要返回数据给前端，让其输入手机号和验证码
            //request.setAccount(openId) ; //add Luo.0022 以唯一标识作为小程序用户名
            TkUser user = userService.registerByThird(request, TokerCommonConstant.USER_LOGIN_TYPE_PROGRAM);

            userTokenService.bind(openId, TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_PROGRAM, user.getId());
            if(StringUtils.isNotBlank(unionId)) {
                //有就绑定
                userTokenService.bind(unionId, TokerCommonConstant.THIRD_LOGIN_TOKEN_TYPE_UNION_ID, user.getId());
            }
            return user;
        }catch (Exception e){
            throw new JeecgBootException(e.getMessage());
        }
    }

    /**
     * 修改或增加用户手机号
     * @param userId
     * @param phone
     * @return
     */
    @Override
    public boolean updateUserPhoneInfo(String userId , String phone){
        TkUser user = new TkUser() ;
        user.setId(userId) ;
        user.setPhone(phone) ;
        return userService.updateById(user) ;
//        return true ;
    }

    /**
     * 积分记录
     * @return List<UserBill>
     */
    @Override
    public IPage<TkUserBill> getUserBillList(String category, PageParamRequest pageParamRequest) {
        FundsMonitorSearchRequest request = new FundsMonitorSearchRequest();
        request.setUid(userService.getUserIdException());
        request.setCategory(category);
        return userBillService.getList(request, pageParamRequest);
    }


    /**
     * 佣金转入余额
     * @return Boolean
     */
    @Override
    public Boolean transferIn(BigDecimal price) {

        //当前可提现佣金
        TkUser user = userService.getInfo();
        if (ObjectUtil.isNull(user)) {
            throw new JeecgBootException("用户数据异常");
        }
        BigDecimal subtract = user.getBrokeragePrice();

        if (price.compareTo(BigDecimal.ZERO) <= 0) {
            throw new JeecgBootException("转入金额不能为0");
        }
        if(subtract.compareTo(price) < 0){
            throw new JeecgBootException("您当前可充值余额为 " + subtract + "元");
        }
        // userBill现金增加记录
        TkUserBill userBill = new TkUserBill();
        userBill.setUid(user.getId());
        userBill.setLinkId("0");
        userBill.setPm(1);
        userBill.setTitle("佣金转余额");
        userBill.setCategory(TokerCommonConstant.USER_BILL_CATEGORY_MONEY);
        userBill.setType(TokerCommonConstant.USER_BILL_TYPE_TRANSFER_IN);
        userBill.setNumber(price);
        userBill.setBalance(user.getNowMoney().add(price));
        userBill.setMark(StrUtil.format("佣金转余额,增加{}", price));
        userBill.setStatus(1);
        userBill.setCreateTime(DateUtil.nowDateTime());

        userBill.setTenantId(user.getTenantId()) ; // add tenantId

        // userBrokerage转出记录
        TkUserBrokerageRecord brokerageRecord = new TkUserBrokerageRecord();
        brokerageRecord.setUid(user.getId());
        brokerageRecord.setLinkId("0");
        brokerageRecord.setLinkType(BrokerageRecordConstants.BROKERAGE_RECORD_LINK_TYPE_YUE);
        brokerageRecord.setType(BrokerageRecordConstants.BROKERAGE_RECORD_TYPE_SUB);
        brokerageRecord.setTitle(BrokerageRecordConstants.BROKERAGE_RECORD_TITLE_BROKERAGE_YUE);
        brokerageRecord.setPrice(price);
        brokerageRecord.setBalance(user.getNowMoney().add(price));
        brokerageRecord.setMark(StrUtil.format("佣金转余额，减少{}", price));
        brokerageRecord.setStatus(BrokerageRecordConstants.BROKERAGE_RECORD_STATUS_COMPLETE);
        brokerageRecord.setCreateTime(DateUtil.nowDateTime());

        brokerageRecord.setTenantId(user.getTenantId()) ; // add tenantId

        Boolean execute = transactionTemplate.execute(e -> {
            // 扣佣金
            userService.operationBrokerage(user.getId(), price, user.getBrokeragePrice(), "sub");
            // 加余额
            userService.operationNowMoney(user.getId(), price, user.getNowMoney(), "add");
            userBillService.save(userBill);
            userBrokerageRecordService.save(brokerageRecord);
            return Boolean.TRUE;
        });
        return execute;

    }

    /**
     * 提现总金额
     */
    @Override
    public BigDecimal getExtractTotalMoney(){
        // return userExtractService.getExtractTotalMoney(userService.getUserIdException());
        return null ;
    }

    /**
     * 用户账单记录（现金）
     * @param type 记录类型：all-全部，expenditure-支出，income-收入
     * @return CommonPage
     */
    @Override
    public Page<UserRechargeBillRecordResponse> nowMoneyBillRecord(String type, PageParamRequest pageRequest) {

        TkUser user = userService.getInfo();
        if (ObjectUtil.isNull(user)) {
            throw new JeecgBootException("用户数据异常");
        }

        Page<TkUserBill> billPage = new Page<>(pageRequest.getPage(), pageRequest.getLimit());
        List<TkUserBill> list = userBillService.nowMoneyBillRecord(user.getId(), type, pageRequest);
        Page<TkUserBill> billPageInfo = copyPageInfo(billPage, list) ;

        // 获取年-月
        Map<String, List<TkUserBill>> map = CollUtil.newHashMap();
        list.forEach(i -> {
            String month = StrUtil.subPre(DateUtil.dateToStr(i.getCreateTime(), TokerCommonConstant.DATE_FORMAT), 7);
            if (map.containsKey(month)) {
                map.get(month).add(i);
            } else {
                List<TkUserBill> billList = CollUtil.newArrayList();
                billList.add(i);
                map.put(month, billList);
            }
        });
        List<UserRechargeBillRecordResponse> responseList = CollUtil.newArrayList();
        map.forEach((key, value) -> {
            UserRechargeBillRecordResponse response = new UserRechargeBillRecordResponse();
            response.setDate(key);
            response.setList(value);
            responseList.add(response);
        });

        Page<UserRechargeBillRecordResponse> pageInfo = copyPageInfo(billPageInfo, responseList);
        return pageInfo ;

    }

    /**
     * 对象A复制对象B的分页信息 //TODO 多次数据查询导致分页数据异常解决办法
     */
    private  <T> Page<T> copyPageInfo(Page<?> originPageInfo, List<T> list) {
        Page<T> pageInfo = new Page<>();
        pageInfo.setPages(originPageInfo.getPages());
        pageInfo.setSize(originPageInfo.getSize());
        pageInfo.setMaxLimit(originPageInfo.getMaxLimit());
        pageInfo.setTotal(originPageInfo.getTotal());
        pageInfo.setRecords(list) ;

        return pageInfo;
    }

    /**
     * 用户资金统计
     * @return UserBalanceResponse
     */
    @Override
    public UserBalanceResponse getUserBalance() {
        TkUser info = userService.getInfo();
        BigDecimal recharge = userBillService.getSumBigDecimal(1, info.getId(), TokerCommonConstant.USER_BILL_CATEGORY_MONEY, null, null);
        //BigDecimal orderStatusSum = storeOrderService.getSumBigDecimal(info.getId(), null);
        return new UserBalanceResponse(info.getNowMoney(), recharge, info.getBrokeragePrice());
    }


}
