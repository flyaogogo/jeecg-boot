package org.jeecg.modules.toker.wechat.service.impl;

import org.jeecg.modules.toker.wechat.entity.TkWechatReply;
import org.jeecg.modules.toker.wechat.mapper.TkWechatReplyMapper;
import org.jeecg.modules.toker.wechat.service.ITkWechatReplyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信关键字回复表
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
@Service
public class TkWechatReplyServiceImpl extends ServiceImpl<TkWechatReplyMapper, TkWechatReply> implements ITkWechatReplyService {

}
