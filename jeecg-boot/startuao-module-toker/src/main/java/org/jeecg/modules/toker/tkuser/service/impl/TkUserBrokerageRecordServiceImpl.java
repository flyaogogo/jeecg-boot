package org.jeecg.modules.toker.tkuser.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.modules.toker.constants.BrokerageRecordConstants;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.jeecg.modules.toker.tkuser.entity.TkUserBrokerageRecord;
import org.jeecg.modules.toker.tkuser.mapper.TkUserBrokerageRecordMapper;
import org.jeecg.modules.toker.tkuser.service.ITkUserBrokerageRecordService;
import org.jeecg.modules.toker.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

/**
 * @Description: 用户佣金记录表
 * @Author: Luo.0022
 * @Date:   2021-02-27
 * @Version: V1.0
 */
@Service
public class TkUserBrokerageRecordServiceImpl extends ServiceImpl<TkUserBrokerageRecordMapper, TkUserBrokerageRecord> implements ITkUserBrokerageRecordService {

    @Autowired
    private ITkSystemConfigService systemConfigService ;

    @Autowired
    private TkUserBrokerageRecordMapper userBrokerageRecordMapper ;


    /**
     * 计算佣金并生成记录信息
     * 当类型Type：
     *      1、为标准（standard）方式时，则报备、进店都设置的返佣金，比例按1:1进行
     *      2、为其他(other)，则以订单价与比较进行计算进行返佣金
     * @param uid
     * @param reportInfoId  把报备单Id当订单Id使用
     * @param orderPrice
     * @param type
     */
    @Override
    public TkUserBrokerageRecord calculateCommissionAndSave(String uid, String reportInfoId, BigDecimal orderPrice, String type) {

        // 获取佣金冻结期
        String fronzenTime = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_STORE_BROKERAGE_EXTRACT_TIME);

        String rate , markTemplate ;
        if(BrokerageRecordConstants.BROKERAGE_RECORD_COMMISSION_STANDARD.equals(type)){
            rate = "100" ;
            markTemplate = "报备或进店成功获得推广佣金，分佣{}" ;
        } else {
            rate = systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_STORE_BROKERAGE_RATE_ONE);
            markTemplate = "签单成功获得推广佣金，分佣{}" ;
        }

        //佣金比例整数存储， 例如80， 所以计算的时候要 除以 (10*10)
        BigDecimal rateBigDecimal = orderPrice ;
        if(StringUtils.isNotBlank(rate)){
            rateBigDecimal = new BigDecimal(rate).divide(BigDecimal.TEN.multiply(BigDecimal.TEN));
        }

        BigDecimal brokeragePrice = BigDecimal.ZERO;
        if(!rateBigDecimal.equals(BigDecimal.ZERO)){
            // 订单商品有设置对应等级的分佣比例
            // 舍入模式向零舍入。
            brokeragePrice = orderPrice.multiply(rateBigDecimal).setScale(2, BigDecimal.ROUND_DOWN);
        }

        TkUserBrokerageRecord brokerageRecord = new TkUserBrokerageRecord();
        brokerageRecord.setUid(uid);
        brokerageRecord.setLinkId(reportInfoId) ;
        brokerageRecord.setLinkType(BrokerageRecordConstants.BROKERAGE_RECORD_LINK_TYPE_REPORT);
        brokerageRecord.setType(BrokerageRecordConstants.BROKERAGE_RECORD_TYPE_ADD);
        brokerageRecord.setTitle(BrokerageRecordConstants.BROKERAGE_RECORD_TITLE_ORDER);
        brokerageRecord.setPrice(brokeragePrice);
        brokerageRecord.setBalance(BigDecimal.ZERO) ;// 此字段 暂时不知何用处，默认为0，标注
        brokerageRecord.setMark(StrUtil.format(markTemplate, brokeragePrice));
        brokerageRecord.setStatus(BrokerageRecordConstants.BROKERAGE_RECORD_STATUS_CREATE);
        brokerageRecord.setFrozenTime(Integer.valueOf(Optional.ofNullable(fronzenTime).orElse("0")));
        // 计算解冻时间
        Long thawTime = cn.hutool.core.date.DateUtil.current(false);
        if (brokerageRecord.getFrozenTime() > 0) {
            DateTime dateTime = cn.hutool.core.date.DateUtil.offsetDay(new Date(), brokerageRecord.getFrozenTime());
            thawTime = dateTime.getTime();
        }
        brokerageRecord.setThawTime(thawTime) ;// 计算解冻时间

        brokerageRecord.setCreateTime(DateUtil.nowDateTime());

        userBrokerageRecordMapper.insert(brokerageRecord) ;
        return brokerageRecord ;
    }
}
