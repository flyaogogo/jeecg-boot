package org.jeecg.modules.toker.front.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.RegisterThirdUserRequest;
import org.jeecg.modules.toker.front.response.LoginResponse;
import org.jeecg.modules.toker.front.response.UserBalanceResponse;
import org.jeecg.modules.toker.front.response.UserRechargeBillRecordResponse;
import org.jeecg.modules.toker.tkuser.entity.TkUserBill;

import java.math.BigDecimal;

public interface UserCenterService {
    /**
     * 获取微信授权logo
     * @return String;
     */
    String getLogo() ;

    /**
     * 微信登录
     * @return UserSpreadOrderResponse;
     */
    LoginResponse weChatAuthorizeLogin(String code, Integer spreadUid) ;

    /**
     * 小程序登录
     * @param code String 前端临时授权code
     * @param request RegisterThirdUserRequest 用户信息
     * @return UserSpreadOrderResponse;
     */
    LoginResponse weChatAuthorizeProgramLogin(String code, RegisterThirdUserRequest request) ;

    /**
     * 修改或增加用户手机号
     * @param userId
     * @param phone
     * @return
     */
    boolean updateUserPhoneInfo(String userId , String phone) ;

    /**
     * 积分记录
     * @return List<UserBill>
     */
    IPage<TkUserBill> getUserBillList(String category, PageParamRequest pageParamRequest) ;

    /**
     * 佣金转入余额
     * @return Boolean
     */
    Boolean transferIn(BigDecimal price) ;

    /**
     * 提现总金额
     */
    BigDecimal getExtractTotalMoney() ;

    /**
     * 推广佣金明细
     * @param pageParamRequest 分页参数
     */
    //PageInfo<SpreadCommissionDetailResponse> getSpreadCommissionDetail(PageParamRequest pageParamRequest) ;

    /**
     * 用户账单记录（现金）
     * @param type 记录类型：all-全部，expenditure-支出，income-收入
     * @return CommonPage
     */
    Page<UserRechargeBillRecordResponse> nowMoneyBillRecord(String type, PageParamRequest pageRequest) ;

    /**
     * 用户资金统计
     * @return UserBalanceResponse
     */
    UserBalanceResponse getUserBalance() ;

}
