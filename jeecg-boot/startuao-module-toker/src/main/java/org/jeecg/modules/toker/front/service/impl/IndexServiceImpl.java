package org.jeecg.modules.toker.front.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.IndexTkProductSearchRequest;
import org.jeecg.modules.toker.front.response.*;
import org.jeecg.modules.toker.front.service.IndexService;
import org.jeecg.modules.toker.sysconfig.service.ITkSystemConfigService;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import org.jeecg.modules.toker.tkproduct.service.ITkProductService;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.utils.CrmebUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
* IndexServiceImpl 接口实现
*/
@Service
public class IndexServiceImpl implements IndexService {

//    @Autowired
//    private SystemGroupDataService systemGroupDataService;

    @Autowired
    private ITkProductService productService;

    @Autowired
    private ITkSystemConfigService systemConfigService;

    @Autowired
    private ITkUserService userService;

    /**
     * 首页产品的轮播图和产品信息
     * @param type integer 类型
     * @author Mr.Zhang
     * @since 2020-06-02
     * @return HashMap<String, Object>
     */
    @Override
    public IndexProductBannerResponse getProductBanner(int type, PageParamRequest pageParamRequest) {

        IndexProductBannerResponse indexProductBannerResponse = new IndexProductBannerResponse();
        /*
        IndexTkProductSearchRequest request = new IndexTkProductSearchRequest();
        int gid;
        String key;
        switch (type){
            case TokerCommonConstant.INDEX_RECOMMEND_BANNER: //精品推荐
                gid = TokerCommonConstant.GROUP_DATA_ID_INDEX_RECOMMEND_BANNER;
                key = TokerCommonConstant.INDEX_BAST_LIMIT;
                request.setIsBest(true);
                break;
            case TokerCommonConstant.INDEX_HOT_BANNER: //热门榜单
                gid = TokerCommonConstant.GROUP_DATA_ID_INDEX_HOT_BANNER;
                key = TokerCommonConstant.INDEX_HOT_LIMIT;
                request.setIsHot(true);
                break;
            case TokerCommonConstant.INDEX_NEW_BANNER: //首发新品
                gid = TokerCommonConstant.GROUP_DATA_ID_INDEX_NEW_BANNER;
                key = TokerCommonConstant.INDEX_FIRST_LIMIT;
                request.setIsNew(true);
                break;
            case TokerCommonConstant.INDEX_BENEFIT_BANNER: //促销单品
                gid = TokerCommonConstant.GROUP_DATA_ID_INDEX_BENEFIT_BANNER;
                key = TokerCommonConstant.INDEX_SALES_LIMIT;
                request.setIsBenefit(true);
                break;
            default:
                return null;
        }

        if(StringUtils.isNotBlank(key)){
            String num = systemConfigService.getValueByKey(TokerCommonConstant.INDEX_BAST_LIMIT);
            if(pageParamRequest.getLimit() == 0){
                //首页limit传0，则读取默认数据， 否则后台设置的首页配置不起作用
                pageParamRequest.setLimit(Integer.parseInt(num));
            }
        }

        indexProductBannerResponse.setBanner(systemGroupDataService.getListMapByGid(gid));
        indexProductBannerResponse.setList(productService.getIndexProduct(request, pageParamRequest).getList());
        */


        return indexProductBannerResponse;
    }

    /**
     * 首页数据
     * @return HashMap<String, Object>
     */
    @Override
    public IndexInfoResponse getIndexInfo() {
        IndexInfoResponse indexInfoResponse =  new IndexInfoResponse();

//        HashMap<String, List<TkProduct>> activityByProduct = getIndexProduct()
//                indexInfoResponse.setActivityProduct(getIndexProduct())

        /*
        indexInfoResponse.setBanner(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_BANNER)); //首页banner滚动图
        indexInfoResponse.setMenus(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_MENU)); //导航模块
        indexInfoResponse.setRoll(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_NEWS_BANNER)); //首页滚动新闻
        indexInfoResponse.setActivity(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_ACTIVITY_BANNER)); //首页活动区域图片
        indexInfoResponse.setInfo(null); //活动
        indexInfoResponse.setActivity(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_ACTIVITY_BANNER)); //首页活动区域图片

        //首页配置
        HashMap<String, String> info = systemConfigService.info(TokerCommonConstant.CONFIG_FORM_ID_INDEX);
        IndexInfoItemResponse indexInfoItemResponse = CrmebUtil.mapStringToObj(info, IndexInfoItemResponse.class);


        int limit = TokerCommonConstant.INDEX_LIMIT_DEFAULT;

        IndexTkProductSearchRequest request = new IndexTkProductSearchRequest();
        request.setIsBest(true);
        PageParamRequest pageParamRequest = new PageParamRequest();
        pageParamRequest.setLimit(limit);

        if(!StringUtils.isBlank(indexInfoItemResponse.getBastNumber())){
            pageParamRequest.setLimit(Integer.parseInt(indexInfoItemResponse.getBastNumber()));
        }
        indexInfoItemResponse.setBastList(productService.getIndexProduct(request, pageParamRequest).getList()); //精品推荐个数

        request.setIsBest(false);
        request.setIsNew(true);
        if(!StringUtils.isBlank(indexInfoItemResponse.getFirstNumber())){
            pageParamRequest.setLimit(Integer.parseInt(indexInfoItemResponse.getFirstNumber()));
        }
        indexInfoItemResponse.setFirstList(productService.getIndexProduct(request, pageParamRequest).getList()); //首发新品个数

        //首页展示的二级分类  排序默认降序
        indexInfoItemResponse.setFastList(null);


        indexInfoResponse.setInfo(indexInfoItemResponse); //首页配置

        indexInfoItemResponse.setBastBanner(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_BEST_BANNER)); //首页精品推荐图片

        request.setIsNew(false);
        request.setIsBenefit(true);
        if(!StringUtils.isBlank(indexInfoItemResponse.getPromotionNumber())){
            pageParamRequest.setLimit(Integer.parseInt(indexInfoItemResponse.getPromotionNumber()));
        }
        indexInfoResponse.setBenefit(new ArrayList<>()); //首页促销单品

        if(productService.getIndexProduct(request, pageParamRequest) != null){
            indexInfoResponse.setBenefit(productService.getIndexProduct(request, pageParamRequest).getList()); //首页促销单品
        }

        request.setIsBenefit(false);
        request.setIsHot(true);
        pageParamRequest.setLimit(limit);
        indexInfoResponse.setLikeInfo(productService.getIndexProduct(request, pageParamRequest).getList()); //热门榜单

        indexInfoResponse.setLovely(null);//首发新品广告图
        List<HashMap<String, Object>> lovelyList = systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_NEW_BANNER);
        if(lovelyList != null && lovelyList.size() > 0){
            indexInfoResponse.setLovely(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_NEW_BANNER).get(0));//首发新品广告图
        }
        indexInfoResponse.setExplosiveMoney(systemGroupDataService.getListMapByGid(TokerCommonConstant.GROUP_DATA_ID_INDEX_EX_BANNER));//首页超值爆款

        indexInfoResponse.setLogoUrl(systemConfigService.getValueByKey(TokerCommonConstant.CONFIG_KEY_SITE_LOGO));

        TkUser user = userService.getInfo();
        if(null != user){
            indexInfoResponse.setSubscribe(user.getSubscribe());
        }
        */
        return indexInfoResponse;
    }


    /**
     * 热门搜索
     * @author Mr.Zhang
     * @since 2020-06-03
     * @return List<HashMap<String, String>>
     */
    @Override
    public List<HashMap<String, Object>> hotKeywords() {

        //return systemGroupDataService.getListMapByGid(Constants.GROUP_DATA_ID_INDEX_KEYWORDS);
        return null ;
    }

    /**
     * 微信分享配置
     * @author Mr.Zhang
     * @since 2020-05-25
     * @return Object
     */
    @Override
    public HashMap<String, String> getShareConfig() {

        HashMap<String, String> map = new HashMap<>();
        /*
        HashMap<String, String> info = systemConfigService.info(Constants.CONFIG_FORM_ID_PUBLIC);
        if(info == null){
            throw new JeecgBootException("请配置公众号分享信息！");
        }

        map.put("img", info.get("wechat_share_img"));
        map.put("title", info.get("wechat_share_title"));
        map.put("synopsis", info.get("wechat_share_synopsis"));
        */
        return map;

    }

    /**
     * 获取公共配置
     *
     * @return 公共配置
     */
    @Override
    public HashMap<String, String> getCommConfig() {
        HashMap<String,String> result = new HashMap<>();
        //result.put("yzfUrl", systemConfigService.getValueByKey(Constants.CONFIG_KEY_YZF_H5_URL));
        return result;
    }

    /**
     * 首页 加载 选中产品信息列表
     * @param request
     * @param pageParamRequest
     * @return
     */
    @Override
    public List<TkProduct> getIndexProduct(IndexTkProductSearchRequest request, PageParamRequest pageParamRequest){

        if ("1".equals(request.getActivityType())){
            request.setIsBest("Y") ;
        } else if ("2".equals(request.getActivityType())){
            request.setIsHot("Y") ;
        } else if ("3".equals(request.getActivityType())){
            request.setIsNew("Y") ;
        } else if ("4".equals(request.getActivityType())){
            request.setSalesOrder("desc") ;
        } else {
            request.setIsBest("Y") ;
        }

        IPage<TkProduct> storeProducts= productService.getList(request, pageParamRequest);
        //CommonPage<StoreProduct> storeProductCommonPage = CommonPage.restPage(storeProductList);

        if(storeProducts.getSize()< 1){
            return null ;
        }
        List<TkProduct> productList = storeProducts.getRecords() ;

/*
        HashMap<String, List<TkProduct>> activityByProduct = new HashMap<>() ;
        activityByProduct.put(request.getActivityType(),productList) ;
*/

        /*
        List<ProductResponse> productResponseArrayList = new ArrayList<>();
        for (TkProduct storeProduct : productList) {
            ProductResponse productResponse = new ProductResponse();
            // 根据参与活动添加对应商品活动标示
            if(StringUtils.isNotBlank(storeProduct.getActivity())){
                HashMap<Integer, ProductActivityItemResponse> activityByProduct =
                        productUtils.getActivityByProduct(storeProduct.getId(), storeProduct.getActivity());
                List<Integer> activityList = CrmebUtil.stringToArrayInt(storeProduct.getActivity());
                if (CollUtil.isNotEmpty(activityByProduct) && activityList.size() > 0) {
                    if(activityList.get(0) == TokerConstants.PRODUCT_TYPE_SECKILL){
                        productResponse.setActivityH5(activityByProduct.get(Constants.PRODUCT_TYPE_SECKILL));
                    }
                    if(activityList.get(0) == Constants.PRODUCT_TYPE_BARGAIN){
                        productResponse.setActivityH5(activityByProduct.get(Constants.PRODUCT_TYPE_BARGAIN));
                    }
                    if(activityList.get(0) == Constants.PRODUCT_TYPE_PINGTUAN){
                        productResponse.setActivityH5(activityByProduct.get(Constants.PRODUCT_TYPE_PINGTUAN));
                    }
                }
            }

            BeanUtils.copyProperties(storeProduct, productResponse);

            productResponse.setCateId(CrmebUtil.stringToArray(storeProduct.getCateId()));
            productResponseArrayList.add(productResponse);
        }
        CommonPage<ProductResponse> productResponseCommonPage = CommonPage.restPage(productResponseArrayList);
        BeanUtils.copyProperties(storeProductCommonPage, productResponseCommonPage, "list");

        return productResponseCommonPage;
        */
        return productList ;
    }

    /**
     * 搜索查询产品信息列表
     * @param request
     * @param pageParamRequest
     * @return
     */
    @Override
    public List<TkProduct> getSearchProduct(IndexTkProductSearchRequest request, PageParamRequest pageParamRequest){
        IPage<TkProduct> products= productService.getList(request, pageParamRequest);
        if(products.getSize()< 1){
            return null ;
        }
        List<TkProduct> productList = products.getRecords() ;
        return productList ;
    }
}

