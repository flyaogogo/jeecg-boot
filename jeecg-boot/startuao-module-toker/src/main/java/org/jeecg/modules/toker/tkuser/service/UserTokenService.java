package org.jeecg.modules.toker.tkuser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.toker.tkuser.entity.UserToken;

import java.util.List;

/**
 * UserTokenService 接口实现
 */
public interface UserTokenService extends IService<UserToken> {

    UserToken checkToken(String token, int type);

    void bind(String openId, int type, String uid);

    Boolean unBind(int type, String uid);

    UserToken getTokenByUserId(String userId, int type);

    UserToken getTokenByUserIdException(String userId, int type);

    List<UserToken> getList(List<String> userIdList);

    UserToken getUserIdByOpenId(String openid, int type);

    UserToken getByOpenid(String openid);

    UserToken getByUid(String uid);

    boolean deleteUserTokenByUid(String uid);

    boolean updateUserTokenByUid(String token ,String uid) ;
}