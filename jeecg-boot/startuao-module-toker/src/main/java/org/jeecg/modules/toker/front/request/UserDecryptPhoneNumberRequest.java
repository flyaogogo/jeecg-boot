package org.jeecg.modules.toker.front.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.modules.toker.constants.RegularConstants;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 对微信获取手机号进行解码
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserDecryptPhoneNumberRequest对象", description="解码手机号")
public class UserDecryptPhoneNumberRequest implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户ID", required = true)
    @JsonProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "手机号", required = true)
    @Pattern(regexp = RegularConstants.PHONE, message = "手机号码格式错误")
    @JsonProperty(value = "account")
    private String phone;

    @ApiModelProperty(value = "sessionKey", required = true)
    @JsonProperty(value = "sessionKey")
    private String sessionKey;

    @ApiModelProperty(value = "iv", required = true)
    @JsonProperty(value = "iv")
    private String iv;

    @ApiModelProperty(value = "encryptedData", required = true)
    @JsonProperty(value = "encryptedData")
    private String encryptedData;


}
