package org.jeecg.modules.toker.front.response;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ReportAddressResponse对象", description="客户信息报备")
public class ReportAddressResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**用户id*/
    private String id;
    /**业主姓名*/
    private String ownerName;
    /**业主电话*/
    private String ownerPhone;
    /**装修地址*/
    private String address;
    /**装修进度*/
    private String progress;
    /**其他信息*/
    private String remark;


    /**楼盘名称*/
    private String realEstate;
    /**楼栋号*/
    private Integer buildingNumber;
    /**单元*/
    private Integer unit;
    /**楼层*/
    private Integer floor;
    /**门牌号*/
    private Integer houseNumber;
    /**房屋地址*/
    private String houseAddress;
    /**小区地址*/
    private String cellAddress;

}
