package org.jeecg.modules.toker.tkestate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.mapper.TkDecoratedAddressMapper;
import org.jeecg.modules.toker.tkestate.service.ITkDecoratedAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * @Description: 热装小区客户地址表
 * @Author: Luo.0022
 * @Date:   2021-02-15
 * @Version: V1.0
 */
@Service
public class TkDecoratedAddressServiceImpl extends ServiceImpl<TkDecoratedAddressMapper, TkDecoratedAddress> implements ITkDecoratedAddressService {

    @Autowired
    private TkDecoratedAddressMapper addressMapper ;

    /**
     * 通过uuid获取实体信息
     * @param uuid
     * @return
     */
    @Override
    public TkDecoratedAddress getTkDecoratedAddressByUUID(String uuid) {
        LambdaQueryWrapper<TkDecoratedAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkDecoratedAddress::getUuid, uuid);
        TkDecoratedAddress address = addressMapper.selectOne(lambdaQueryWrapper);
        return address ;
    }

    /**
     * 通过热装小区Id获取所有住户信息
     * @param cellId
     * @return
     */
    @Override
    public List<TkDecoratedAddress> getUserHouseByCellId(String cellId) {
        LambdaQueryWrapper<TkDecoratedAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkDecoratedAddress::getEstateId, cellId);
        //lambdaQueryWrapper.groupBy(TkDecoratedAddress::getBuildingNumber) ;
        List<TkDecoratedAddress> list = addressMapper.selectList(lambdaQueryWrapper) ;
        return list;
    }

    /**
     * 通过小区ID及楼号获取热装住户信息
     * @param cellId
     * @param buildNum
     * @return
     */
    @Override
    public List<TkDecoratedAddress> getUserHouseByCellIdAndbuildNum(String cellId, String buildNum) {
        LambdaQueryWrapper<TkDecoratedAddress> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TkDecoratedAddress::getEstateId, cellId);
        lambdaQueryWrapper.eq(TkDecoratedAddress::getBuildingNumber, buildNum);
        List<TkDecoratedAddress> list = addressMapper.selectList(lambdaQueryWrapper) ;
        return  list ;
    }
}
