package org.jeecg.modules.toker.front.request;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 设置用户等级表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tk_system_user_level")
@ApiModel(value="UserLevelSearchRequest对象", description="设置用户等级表")
public class UserLevelSearchRequest implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "会员名称")
    @NotNull
    private String name;

    @ApiModelProperty(value = "是否显示 1=显示,0=隐藏")
    @NotNull
    private Boolean isShow;

    @ApiModelProperty(value = "是否删除.1=删除,0=未删除")
    @NotNull
    private Boolean isDel;
}
