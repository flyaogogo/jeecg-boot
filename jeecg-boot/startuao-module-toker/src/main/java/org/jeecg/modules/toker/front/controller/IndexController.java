package org.jeecg.modules.toker.front.controller;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.IndexTkProductSearchRequest;
import org.jeecg.modules.toker.front.response.IndexInfoResponse;
import org.jeecg.modules.toker.front.response.IndexProductBannerResponse;
import org.jeecg.modules.toker.front.service.IndexService;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * 用户 -- 用户首页中心
 */
@Slf4j
@RestController("IndexController")
@RequestMapping("api/front")
@Api(tags = "首页")
public class IndexController {

    @Autowired
    private IndexService indexService;

    /**
     * 首页产品的轮播图和产品信息
     * @author Mr.Zhang
     * @since 2020-06-02
     */
    @ApiOperation(value = "首页产品的轮播图和产品信息")
    @RequestMapping(value = "/groom/list/{type}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "类型 【1 精品推荐 2 热门榜单 3首发新品 4促销单品】", dataType = "int", required = true)
    public Result<IndexProductBannerResponse> getProductBanner(@PathVariable(value = "type") int type, PageParamRequest pageParamRequest){
        if(type < TokerCommonConstant.INDEX_RECOMMEND_BANNER || type > TokerCommonConstant.INDEX_BENEFIT_BANNER){
            return Result.OK() ; //
        }
        return Result.OK(indexService.getProductBanner(type, pageParamRequest));
    }

    /**
     * 首页数据
     * @author Mr.Zhang
     * @since 2020-06-02
     */
    @ApiOperation(value = "首页数据")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public Result<IndexInfoResponse> getIndexInfo(){
        return Result.OK(indexService.getIndexInfo());
    }

    /**
     * 热门搜索
     * @author Mr.Zhang
     * @since 2020-06-03
     */
    @ApiOperation(value = "热门搜索")
    @RequestMapping(value = "/search/keyword", method = RequestMethod.GET)
    public Result<List<HashMap<String, Object>>> hotKeywords(){
        return Result.OK(indexService.hotKeywords());
    }

    /**
     * 分享配置
     * @author Mr.Zhang
     * @since 2020-05-25
     */
    @ApiOperation(value = "分享配置")
    @RequestMapping(value = "/share", method = RequestMethod.GET)
    public Result<HashMap<String, String>> share(){
        return Result.OK(indexService.getShareConfig());
    }

    /**
     * 公共配置 云智服
     * @return 公共配置
     */
    @ApiOperation(value = "公共配置")
    @RequestMapping(value = "/config", method = RequestMethod.GET)
    public Result<HashMap<String,String>> getConfig(){
        return Result.OK(indexService.getCommConfig());
    }

    /**
     * 首页产品数据展示或搜索
     */
    @ApiOperation(value = "首页产品数据展示或搜索")
    @RequestMapping(value = "/index/products", method = RequestMethod.GET)
    public Result<List<TkProduct>> indexProducts(@Validated IndexTkProductSearchRequest request, @Validated PageParamRequest pageParamRequest){

        return Result.OK(indexService.getIndexProduct(request,pageParamRequest));
    }

    /**
     * 产品数据展示或搜索
     */
    @ApiOperation(value = "产品数据展示或搜索")
    @RequestMapping(value = "/index/search/products", method = RequestMethod.GET)
    public Result<List<TkProduct>> searchProducts(@Validated IndexTkProductSearchRequest request, @Validated PageParamRequest pageParamRequest){

        return Result.OK(indexService.getSearchProduct(request,pageParamRequest));
    }



}



