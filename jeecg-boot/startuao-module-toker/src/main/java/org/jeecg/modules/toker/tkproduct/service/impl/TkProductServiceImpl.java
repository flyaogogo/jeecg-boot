package org.jeecg.modules.toker.tkproduct.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.modules.toker.common.PageParamRequest;
import org.jeecg.modules.toker.front.request.IndexTkProductSearchRequest;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import org.jeecg.modules.toker.tkproduct.mapper.TkProductMapper;
import org.jeecg.modules.toker.tkproduct.service.ITkProductService;
import org.jeecg.modules.toker.tkuser.vo.UserSignVo;
import org.jeecg.modules.toker.utils.CrmebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 产品详细信息
 * @Author: Luo.0022
 * @Date:   2021-01-05
 * @Version: V1.0
 */
@Service
public class TkProductServiceImpl extends ServiceImpl<TkProductMapper, TkProduct> implements ITkProductService {

    @Autowired
    private TkProductMapper tkProductMapper ;

    @Override
    public boolean editStatusById(String id, String isShow) {
        return tkProductMapper.editStatusById(id, isShow) ;
    }

    /**
     * 产品列表
     * @param request 商品查询参数
     * @param pageParamRequest  分页参数
     * @return  商品查询结果
     */
    @Override
    public IPage<TkProduct> getList(IndexTkProductSearchRequest request, PageParamRequest pageParamRequest) {
        //PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        //Page<TkProduct> productPage = new Page<>();

        Page<TkProduct> page = new Page<>(pageParamRequest.getPage(), pageParamRequest.getLimit()) ;

        LambdaQueryWrapper<TkProduct> lambdaQueryWrapper = Wrappers.lambdaQuery();
        if(request.getIsBest() != null){
            lambdaQueryWrapper.eq(TkProduct::getIsBest, request.getIsBest());
        }

        if(request.getIsHot() != null){
            lambdaQueryWrapper.eq(TkProduct::getIsHot, request.getIsHot());
        }

        if(request.getIsNew() != null){
            lambdaQueryWrapper.eq(TkProduct::getIsNew, request.getIsNew());
        }

        /*if(request.getIsBenefit() != null){
            lambdaQueryWrapper.eq(TkProduct::getIsBenefit, request.getIsBenefit());
        }*/

        // 只有上架的产品才能显示给用户看
        lambdaQueryWrapper.eq(TkProduct::getIsShow, "Y")
                .gt(TkProduct::getStock, 0)
                .orderByDesc(TkProduct::getSort);

        /*if(!StringUtils.isBlank(request.getPriceOrder())){
            if(request.getPriceOrder().equals(TokerCommonConstant.SORT_DESC)){
                lambdaQueryWrapper.orderByDesc(TkProduct::getPrice);
            }else{
                lambdaQueryWrapper.orderByAsc(TkProduct::getPrice);
            }
        }*/

        // 按销量排序
        if(!StringUtils.isBlank(request.getSalesOrder())){
            if(request.getSalesOrder().equals(TokerCommonConstant.SORT_DESC)){
                lambdaQueryWrapper.orderByDesc(TkProduct::getSales);
            }else{
                lambdaQueryWrapper.orderByAsc(TkProduct::getSales);
            }
        }
        if(null != request.getCateId() && request.getCateId().size() > 0 ){
            lambdaQueryWrapper.apply(CrmebUtil.getFindInSetSql("cate_id", (ArrayList<Integer>) request.getCateId()));
        }

        if(StringUtils.isNotBlank(request.getKeywords())){
            if(CrmebUtil.isString2Num(request.getKeywords())){
                Integer productId = Integer.valueOf(request.getKeywords());
                lambdaQueryWrapper.like(TkProduct::getId, productId);
            }else{
                lambdaQueryWrapper
                        .like(TkProduct::getProductName, request.getKeywords())
                        .or().like(TkProduct::getProductInfo, request.getKeywords())
                        .or().like(TkProduct::getKeywords, request.getKeywords())
                        .or().like(TkProduct::getProductModel, request.getKeywords());
            }
        }

        lambdaQueryWrapper.orderByDesc(TkProduct::getId);

        IPage<TkProduct> proPage = tkProductMapper.selectPage(page, lambdaQueryWrapper) ;

        //return tkProductMapper.selectList(lambdaQueryWrapper);
        return proPage ;
    }

    /**
     * 通过id表，查找所有的产品信息
     * @param ids
     * @return
     */
    @Override
    public List<TkProduct> getProductsByIds(String ids) {
        //if(StringUtils.isNotBlank(ids)){
        String[] arr = ids.split(",");
        List<String> idsList = Arrays.asList(arr);

        LambdaQueryWrapper<TkProduct> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(TkProduct::getId,idsList) ;
        List<TkProduct> list = tkProductMapper.selectList(lambdaQueryWrapper) ;
        return list ;

    }
}
