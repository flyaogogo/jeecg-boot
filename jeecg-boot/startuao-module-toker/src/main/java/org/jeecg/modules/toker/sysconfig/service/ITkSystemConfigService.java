package org.jeecg.modules.toker.sysconfig.service;

import org.jeecg.modules.toker.sysconfig.entity.TkSystemConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 系统参数配置
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
public interface ITkSystemConfigService extends IService<TkSystemConfig> {
    /**
     * 根据menu name 获取 value
     * @param name menu name
     * @author Luo.0022
     * @since 2021-01-15
     * @return String
     */
    String getValueByKey(String name) ;


    /**
     * 同时获取多个配置
     * @param keys 多个配置key
     * @return List<String>
     *
     * @author Luo.0022
     * @since 2021-01-15
     */
    List<String> getValuesByKes(List<String> keys) ;

    /**
     * 根据 name 获取 value 找不到抛异常
     * @param name menu name
     * @author Luo.0022
     * @since 2021-01-15
     * @return String
     */
    String getValueByKeyException(String name) ;
}
