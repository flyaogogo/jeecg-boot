package org.jeecg.modules.toker.tkuser.service;

import org.jeecg.modules.toker.tkuser.entity.TkUserExtract;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 用户提现表
 * @Author: Luo.0022
 * @Date:   2021-02-27
 * @Version: V1.0
 */
public interface ITkUserExtractService extends IService<TkUserExtract> {

}
