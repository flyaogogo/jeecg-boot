package org.jeecg.modules.toker.tkuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.constant.TokerCommonConstant;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import org.jeecg.modules.toker.tkuser.entity.UserToken;
import org.jeecg.modules.toker.tkuser.mapper.UserTokenMapper;
import org.jeecg.modules.toker.tkuser.service.ITkUserService;
import org.jeecg.modules.toker.tkuser.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * UserTokenServiceImpl 接口实现
 */
@Service
public class UserTokenServiceImpl extends ServiceImpl<UserTokenMapper, UserToken> implements UserTokenService {

    @Resource
    private UserTokenMapper dao;

    @Autowired
    private ITkUserService userService;

    /**
     * 检测token是否存在
     * @param token String openId
     * @param type int 类型
     * @author Mr.Zhang
     * @since 2020-05-25
     * @return UserToken
     */
    @Override
    public UserToken checkToken(String token, int type) {
        LambdaQueryWrapper<UserToken> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserToken::getType, type).eq(UserToken::getToken, token);
        return dao.selectOne(lambdaQueryWrapper);
    }

    /**
     * 绑定token关系
     * @param token String token
     * @param type int 类型
     * @author Mr.Zhang
     * @since 2020-05-25
     */
    @Override
    public void bind(String token, int type, String userId) {
        UserToken userToken = new UserToken();
        userToken.setToken(token);
        userToken.setType(type);
        userToken.setUid(userId);
        save(userToken);
    }

    /**
     * 解绑微信
     * @author Mr.Zhang
     * @since 2020-04-28
     * @return Boolean
     */
    @Override
    public Boolean unBind(int type, String uid) {
        try{
            LambdaQueryWrapper<UserToken> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(UserToken::getUid, uid).eq(UserToken::getType, type);
            dao.delete(lambdaQueryWrapper);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public UserToken getTokenByUserId(String userId, int type) {
        LambdaQueryWrapper<UserToken> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserToken::getUid, userId).eq(UserToken::getType, type);
        return dao.selectOne(lambdaQueryWrapper);
    }

    @Override
    public UserToken getTokenByUserIdException(String userId, int type) {
        UserToken userToken = getTokenByUserId(userId, type);
        if(null == userToken){
            throw new JeecgBootException("没有找到和用户相关的微信数据");
        }

        TkUser user = userService.getById(userId);

        if(null != user.getSubscribe() && user.getSubscribe()==0){
            throw new JeecgBootException("此用户没有关注公众号");
        }

        return userToken;
    }

    @Override
    public List<UserToken> getList(List<String> userIdList) {
        LambdaQueryWrapper<UserToken> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(UserToken::getUid, userIdList).eq(UserToken::getType, TokerCommonConstant.PAY_TYPE_WE_CHAT_FROM_PUBLIC);
        return dao.selectList(lambdaQueryWrapper);
    }

    @Override
    public UserToken getUserIdByOpenId(String openid, int type) {
        LambdaQueryWrapper<UserToken> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserToken::getToken, openid);
        lambdaQueryWrapper.eq(UserToken::getType, type);
        return dao.selectOne(lambdaQueryWrapper);
    }

    @Override
    public UserToken getByOpenid(String openid) {
        LambdaQueryWrapper<UserToken> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserToken::getToken, openid);
        return dao.selectOne(lambdaQueryWrapper);
    }

    @Override
    public UserToken getByUid(String uid) {
        LambdaQueryWrapper<UserToken> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UserToken::getUid, uid);
        return dao.selectOne(lambdaQueryWrapper);
    }

    /**
     * 通过用户，删除过期Token
     * @param uid
     * @return
     */
    @Override
    public boolean deleteUserTokenByUid(String uid){
        dao.deleteUserTokenByUid(uid) ;
        return true ;
    }

    /**
     * 更新用户Token
     * @param token
     * @param uid
     * @return
     */
    @Override
    public boolean updateUserTokenByUid(String token , String uid){
        dao.updateUserTokenByUid(token,uid) ;
        return true ;
    }
}

