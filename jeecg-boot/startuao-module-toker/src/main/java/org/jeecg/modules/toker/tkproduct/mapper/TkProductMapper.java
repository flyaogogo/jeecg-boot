package org.jeecg.modules.toker.tkproduct.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 产品详细信息
 * @Author: Luo.0022
 * @Date:   2021-01-05
 * @Version: V1.0
 */
public interface TkProductMapper extends BaseMapper<TkProduct> {

    @Update("update tk_product set is_show=#{isShow} where id=#{id}")
    public boolean editStatusById(String id, String isShow) ;
}
