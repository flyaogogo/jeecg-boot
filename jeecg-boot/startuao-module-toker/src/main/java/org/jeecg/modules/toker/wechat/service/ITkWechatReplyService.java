package org.jeecg.modules.toker.wechat.service;

import org.jeecg.modules.toker.wechat.entity.TkWechatReply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信关键字回复表
 * @Author: Luo.0022
 * @Date:   2021-01-14
 * @Version: V1.0
 */
public interface ITkWechatReplyService extends IService<TkWechatReply> {

}
