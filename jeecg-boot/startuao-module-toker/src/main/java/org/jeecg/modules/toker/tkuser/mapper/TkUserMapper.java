package org.jeecg.modules.toker.tkuser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jeecg.modules.toker.tkuser.entity.TkUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.toker.tkuser.vo.SysUserComboModel;
import org.jeecg.modules.toker.tkuser.vo.UserOperateFundsVo;

/**
 * @Description: 拓客用户信息
 * @Author: Luo.0022
 * @Date:   2020-12-30
 * @Version: V1.0
 */
public interface TkUserMapper extends BaseMapper<TkUser> {

    /**
     * 通过账号获取用户信息
     * @author Luo.0022
     * @since 2021-01-14
     */
    // @Select("select id,account,realName,pwd,birthday,phone,status,avatar, from tk_user where account=#{account}")
    @Select("select * from tk_user where account=#{account}")
    TkUser getUserByAccount(String account) ;

    /**
     * 更新操作资金
     * @param userOperateFundsVo
     * @return
     */
    @Update("update tk_user set ${foundsType} = ${foundsType}+${value} where id = #{uid}")
    Boolean updateFounds(UserOperateFundsVo userOperateFundsVo);

    /**
     * 查询后台系统所有用户 SysUserComboModel
     * @return
     */
    @Select("select id,username,realname,email from sys_user where status=1 and del_flag=0 ")
    List<SysUserComboModel> queryAllUserBackSysCombo() ;
}
