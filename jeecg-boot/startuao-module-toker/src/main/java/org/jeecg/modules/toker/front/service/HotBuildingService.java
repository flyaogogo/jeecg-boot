package org.jeecg.modules.toker.front.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkproduct.entity.TkProduct;

import java.util.List;
import java.util.Map;

public interface HotBuildingService {

    /**
     * 通过热装小区Id获取所有住户信息，并组装数据
     * @param cellId
     * @return
     */
    Map<String, List<TkDecoratedAddress>> getUserHouseByCellId(String cellId) ;

    /**
     * 通过id表，查找所有的产品信息
     * @param ids
     * @return
     */
    List<TkProduct> getProductsByIds(String ids) ;
}
