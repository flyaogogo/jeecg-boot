package org.jeecg.modules.toker.tkvaddress.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.toker.tkvaddress.entity.TkVillageAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 业主装修地址
 * @Author: Luo.0022
 * @Date:   2020-12-31
 * @Version: V1.0
 */
public interface TkVillageAddressMapper extends BaseMapper<TkVillageAddress> {

}
