package org.jeecg.modules.toker.tkuser.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.toker.tkuser.entity.TkUserBill;
import org.jeecg.modules.toker.tkuser.service.ITkUserBillService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 用户账单表
 * @Author: Luo.0022
 * @Date:   2021-01-27
 * @Version: V1.0
 */
@Api(tags="用户账单表")
@RestController
@RequestMapping("/tkuser/tkUserBill")
@Slf4j
public class TkUserBillController extends JeecgController<TkUserBill, ITkUserBillService> {
	@Autowired
	private ITkUserBillService tkUserBillService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tkUserBill
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "用户账单表-分页列表查询")
	@ApiOperation(value="用户账单表-分页列表查询", notes="用户账单表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkUserBill tkUserBill,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkUserBill> queryWrapper = QueryGenerator.initQueryWrapper(tkUserBill, req.getParameterMap());
		Page<TkUserBill> page = new Page<TkUserBill>(pageNo, pageSize);
		IPage<TkUserBill> pageList = tkUserBillService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tkUserBill
	 * @return
	 */
	@AutoLog(value = "用户账单表-添加")
	@ApiOperation(value="用户账单表-添加", notes="用户账单表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkUserBill tkUserBill) {
		tkUserBillService.save(tkUserBill);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkUserBill
	 * @return
	 */
	@AutoLog(value = "用户账单表-编辑")
	@ApiOperation(value="用户账单表-编辑", notes="用户账单表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkUserBill tkUserBill) {
		tkUserBillService.updateById(tkUserBill);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "用户账单表-通过id删除")
	@ApiOperation(value="用户账单表-通过id删除", notes="用户账单表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkUserBillService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "用户账单表-批量删除")
	@ApiOperation(value="用户账单表-批量删除", notes="用户账单表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkUserBillService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "用户账单表-通过id查询")
	@ApiOperation(value="用户账单表-通过id查询", notes="用户账单表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkUserBill tkUserBill = tkUserBillService.getById(id);
		if(tkUserBill==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkUserBill);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tkUserBill
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkUserBill tkUserBill) {
        return super.exportXls(request, tkUserBill, TkUserBill.class, "用户账单表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkUserBill.class);
    }

}
