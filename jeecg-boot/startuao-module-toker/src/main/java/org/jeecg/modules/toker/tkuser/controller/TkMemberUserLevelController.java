package org.jeecg.modules.toker.tkuser.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.toker.tkuser.entity.TkMemberUserLevel;
import org.jeecg.modules.toker.tkuser.service.ITkMemberUserLevelService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 普通会员等级
 * @Author: Luo.0022
 * @Date:   2021-01-29
 * @Version: V1.0
 */
@Api(tags="普通会员等级")
@RestController
@RequestMapping("/tkuser/tkMemberUserLevel")
@Slf4j
public class TkMemberUserLevelController extends JeecgController<TkMemberUserLevel, ITkMemberUserLevelService> {
	@Autowired
	private ITkMemberUserLevelService tkMemberUserLevelService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tkMemberUserLevel
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "普通会员等级-分页列表查询")
	@ApiOperation(value="普通会员等级-分页列表查询", notes="普通会员等级-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkMemberUserLevel tkMemberUserLevel,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkMemberUserLevel> queryWrapper = QueryGenerator.initQueryWrapper(tkMemberUserLevel, req.getParameterMap());
		Page<TkMemberUserLevel> page = new Page<TkMemberUserLevel>(pageNo, pageSize);
		IPage<TkMemberUserLevel> pageList = tkMemberUserLevelService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tkMemberUserLevel
	 * @return
	 */
	@AutoLog(value = "普通会员等级-添加")
	@ApiOperation(value="普通会员等级-添加", notes="普通会员等级-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkMemberUserLevel tkMemberUserLevel) {
		tkMemberUserLevelService.save(tkMemberUserLevel);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkMemberUserLevel
	 * @return
	 */
	@AutoLog(value = "普通会员等级-编辑")
	@ApiOperation(value="普通会员等级-编辑", notes="普通会员等级-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkMemberUserLevel tkMemberUserLevel) {
		tkMemberUserLevelService.updateById(tkMemberUserLevel);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "普通会员等级-通过id删除")
	@ApiOperation(value="普通会员等级-通过id删除", notes="普通会员等级-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkMemberUserLevelService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "普通会员等级-批量删除")
	@ApiOperation(value="普通会员等级-批量删除", notes="普通会员等级-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkMemberUserLevelService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "普通会员等级-通过id查询")
	@ApiOperation(value="普通会员等级-通过id查询", notes="普通会员等级-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkMemberUserLevel tkMemberUserLevel = tkMemberUserLevelService.getById(id);
		if(tkMemberUserLevel==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkMemberUserLevel);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tkMemberUserLevel
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkMemberUserLevel tkMemberUserLevel) {
        return super.exportXls(request, tkMemberUserLevel, TkMemberUserLevel.class, "普通会员等级");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkMemberUserLevel.class);
    }

}
