package org.jeecg.modules.toker.tkestate.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.toker.tkestate.entity.TkDecoratedAddress;
import org.jeecg.modules.toker.tkestate.service.ITkDecoratedAddressService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 热装小区客户地址表
 * @Author: Luo.0022
 * @Date:   2021-02-15
 * @Version: V1.0
 */
@Api(tags="热装小区客户地址表")
@RestController
@RequestMapping("/tkestate/tkDecoratedAddress")
@Slf4j
public class TkDecoratedAddressController extends JeecgController<TkDecoratedAddress, ITkDecoratedAddressService> {
	@Autowired
	private ITkDecoratedAddressService tkDecoratedAddressService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tkDecoratedAddress
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "热装小区客户地址表-分页列表查询")
	@ApiOperation(value="热装小区客户地址表-分页列表查询", notes="热装小区客户地址表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TkDecoratedAddress tkDecoratedAddress,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TkDecoratedAddress> queryWrapper = QueryGenerator.initQueryWrapper(tkDecoratedAddress, req.getParameterMap());
		Page<TkDecoratedAddress> page = new Page<TkDecoratedAddress>(pageNo, pageSize);
		IPage<TkDecoratedAddress> pageList = tkDecoratedAddressService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tkDecoratedAddress
	 * @return
	 */
	@AutoLog(value = "热装小区客户地址表-添加")
	@ApiOperation(value="热装小区客户地址表-添加", notes="热装小区客户地址表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TkDecoratedAddress tkDecoratedAddress) {
		// 拼装用户住址信息
		joinAddressInfo(tkDecoratedAddress) ;
		tkDecoratedAddressService.save(tkDecoratedAddress);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tkDecoratedAddress
	 * @return
	 */
	@AutoLog(value = "热装小区客户地址表-编辑")
	@ApiOperation(value="热装小区客户地址表-编辑", notes="热装小区客户地址表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TkDecoratedAddress tkDecoratedAddress) {
		// 拼装用户住址信息
		joinAddressInfo(tkDecoratedAddress) ;
		tkDecoratedAddressService.updateById(tkDecoratedAddress);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "热装小区客户地址表-通过id删除")
	@ApiOperation(value="热装小区客户地址表-通过id删除", notes="热装小区客户地址表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		tkDecoratedAddressService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "热装小区客户地址表-批量删除")
	@ApiOperation(value="热装小区客户地址表-批量删除", notes="热装小区客户地址表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tkDecoratedAddressService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "热装小区客户地址表-通过id查询")
	@ApiOperation(value="热装小区客户地址表-通过id查询", notes="热装小区客户地址表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TkDecoratedAddress tkDecoratedAddress = tkDecoratedAddressService.getById(id);
		if(tkDecoratedAddress==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tkDecoratedAddress);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tkDecoratedAddress
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TkDecoratedAddress tkDecoratedAddress) {
        return super.exportXls(request, tkDecoratedAddress, TkDecoratedAddress.class, "热装小区客户地址表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TkDecoratedAddress.class);
    }

	 /**
	  * 拼装 业住 地址信息
	  * @param da
	  */
    private void joinAddressInfo(TkDecoratedAddress da) {
    	StringBuilder strB = new StringBuilder() ;
    	strB.append(da.getBuildingNumber()).append("幢") ;
    	strB.append("-") ;
    	strB.append(da.getUnit()).append("单元") ;
		strB.append("-") ;
		strB.append(da.getFloor()).append("层") ;
		strB.append("-") ;
		strB.append(da.getHouseNumber()).append("室") ;

		da.setHouseAddress(strB.toString()) ;

	}
}
