package org.jeecg.modules.toker.tkuser.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 用户佣金记录表
 * @Author: Luo.0022
 * @Date:   2021-02-27
 * @Version: V1.0
 */
@Data
@TableName("tk_user_brokerage_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tk_user_brokerage_record对象", description="用户佣金记录表")
public class TkUserBrokerageRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**记录id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "记录id")
    private String id;
	/**用户uid*/
	@Excel(name = "用户uid", width = 15)
    @ApiModelProperty(value = "用户uid")
    private String uid;
	/**关联id(orderNo,提现id)*/
	@Excel(name = "关联id(orderNo,提现id)", width = 15)
    @ApiModelProperty(value = "关联id(orderNo,提现id)")
    private String linkId;
	/**关联类型*/
	@Excel(name = "关联类型", width = 15, dicCode = "tk_brokerage_record_link_type")
	@Dict(dicCode = "tk_brokerage_record_link_type")
    @ApiModelProperty(value = "关联类型")
    private String linkType;
	/**类型*/
	@Excel(name = "类型", width = 15, dicCode = "tk_brokerage_record_type")
	@Dict(dicCode = "tk_brokerage_record_type")
    @ApiModelProperty(value = "类型")
    private Integer type;
	/**标题*/
	@Excel(name = "标题", width = 15)
    @ApiModelProperty(value = "标题")
    private String title;
	/**金额*/
	@Excel(name = "金额", width = 15)
    @ApiModelProperty(value = "金额")
    private BigDecimal price;
	/**剩余*/
	@Excel(name = "剩余", width = 15)
    @ApiModelProperty(value = "剩余")
    private BigDecimal balance;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String mark;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "tk_brokerage_record_status")
	@Dict(dicCode = "tk_brokerage_record_status")
    @ApiModelProperty(value = "状态")
    private Integer status;
	/**冻结期时间（天）*/
	@Excel(name = "冻结期时间（天）", width = 15)
    @ApiModelProperty(value = "冻结期时间（天）")
    private Integer frozenTime;
	/**解冻时间*/
	@Excel(name = "解冻时间", width = 15)
    @ApiModelProperty(value = "解冻时间")
    private Long thawTime;
	/**添加时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "添加时间")
    private Date createTime;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**租户id*/
    @Excel(name = "租户id", width = 15)
    @ApiModelProperty(value = "租户id")
    private java.lang.String tenantId;
}
