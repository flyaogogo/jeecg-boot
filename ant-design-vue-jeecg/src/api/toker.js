import { getAction, deleteAction, putAction, postAction, httpAction } from '@/api/manage'
// import Vue from 'vue'
// import {UI_CACHE_DB_DICT_DATA } from "@/store/mutation-types"

// 产品管理
//const addRole = (params)=>postAction("/sys/role/add",params);
const editProductIsShow = (params)=>putAction("/tkproduct/tkProduct/ishow",params);
//const checkRoleCode = (params)=>getAction("/sys/role/checkRoleCode",params);
// 根据用户ID获取报备用户相关信息
const getReportUserInfoByUid = (params)=>getAction("/tkuser/tkUser/queryById",params);



export {
  editProductIsShow,
  getReportUserInfoByUid

}